"""
Simple script to generate a data set for eps_eff.

TODO-0(@amerlo): add vmec id to file
TODO-1(@amerlo): compute eps_eff directly from wout and not via Equilibrium
TODO-1(@amerlo): add command line options
"""

import netCDF4
import numpy as np
import matplotlib.pyplot as plt

from w7x_datagen.datasets import VmecEquilibria

from vmecnn.physics.equilibrium import Equilibrium
from vmecnn.plotting.utils import (
    set_paper_style,
    savefig,
    SMALL_SQUARED_FIGSIZE,
)

root = "data/dev"
num = -1
num_surfs = 5

dataset = VmecEquilibria(root=root, train_test_split=1.0)

data = []
for i, item in enumerate(dataset):

    #  Extract features
    wout = netCDF4.Dataset(item["wout_path"])
    extcur = wout["extcur"][:].tolist()
    phiedge = wout["phi"][:][-1].item()
    p0 = wout["presf"][:][0].item()
    ctor = wout["ctor"][:].item()
    betatotal = wout["betatotal"][:].item()

    value = []
    value.extend(extcur)
    value.extend([phiedge, p0, ctor, betatotal])

    #  Build features
    equi = Equilibrium(
        rmnc=item["rmnc"],
        lmns=item["lmns"],
        zmns=item["zmns"],
        iota=item["iota"],
        phiedge=item["phiedge"],
        pressure=item["pressure"],
        toroidal_current=item["toroidal_current"] * item["curtor"],
        normalized_flux=np.linspace(0, 1, len(item["iota"]), dtype=np.float64),
        wout_path=item["wout_path"],
    )

    compute_surfs = np.linspace(1, equi.ns - 2, num_surfs, dtype=int)
    equi.compute_surfs = compute_surfs

    eps_eff = (equi.eps_eff ** (2 / 3)).tolist()
    eps_eff_proxy = equi.eps_eff_proxy.tolist()
    s_b = equi.bx.s_b.tolist()

    value.extend(s_b)
    value.extend(eps_eff)
    value.extend(eps_eff_proxy)
    data.append(value)

    #  Release memory and file handlers
    wout.close()
    del wout
    del equi

    if num != -1 and i == num - 1:
        break

#  Save data to file
header = []
header.extend([f"I{i}" for i in range(1, 6)])
header.extend(["IA", "IB"])
header.extend(["phiedge", "p0", "ctor", "betatotal"])
header.extend([f"s_{i}" for i in range(num_surfs)])
header.extend([f"eps_eff_{i}" for i in range(num_surfs)])
header.extend([f"eps_eff_proxy_{i}" for i in range(num_surfs)])
header = ",".join(header)

data = np.asarray(data)
np.savetxt("eps_eff.txt", data, header=header, fmt="%1.4e")

#  Make plot for eps_eff_proxy
set_paper_style()
fig, ax = plt.subplots(1, 1, figsize=SMALL_SQUARED_FIGSIZE, tight_layout=True)
y = data[:, 11 + num_surfs : 11 + 2 * num_surfs].reshape(-1)
y_hat = data[:, 11 + 2 * num_surfs : 11 + 3 * num_surfs].reshape(-1)
ax.scatter(y_hat, y)
fit = np.polynomial.polynomial.Polynomial.fit(y_hat, y, 1)
ax.plot(y_hat, fit(y_hat), "r--")
ax.set_xlabel(r"$\hat{\epsilon}_{\text{eff}}$")
ax.set_ylabel(r"$\epsilon_{\text{eff}}$")
savefig(fig, "eps_eff.pdf", save_tex=True)
