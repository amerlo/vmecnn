"""
Profiling script for the datamodule.

Add the @profile decorator to function you want to profile and run as:

$ kernprof -v -l tests/time_datamodule.py
"""
import argparse

from time import time
from vmecnn.utils import seed_everything
from vmecnn.datamodules.vmec_equilibria import VmecEquilibriaDataModule

#  Set random seed for reproducibility
seed = 42
seed_everything(seed)


def get_datamodule(features, labels, normalization):
    return VmecEquilibriaDataModule(
        root="data/dev",
        features=features,
        labels=labels,
        metadata=[],
        mpol=1,
        ntor=1,
        fraction=0.2,
        normalization={k: normalization for k in features + labels},
        seed=seed,
        to_double=True,
    )


def read_data(epochs: int):
    t0 = time()
    for _ in range(epochs):
        for _, _, _ in datamodule.train_data:
            pass
    tottime = time() - t0
    time_per_item = tottime / epochs / len(datamodule.train_data)
    print(f"Epochs: {epochs}")
    print(f"Dataset len: {len(datamodule.train_data)}")
    print(f"Total time: {tottime:.6g}s")
    print(f"Time per __getitem__: {time_per_item:.6f}s")


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--all", dest="all", action="store_true")
    args = parser.parse_args()

    features = [
        "npc",
        "pc",
        "phiedge",
        "p0",
        "curtor",
        "pressure",
        "toroidal_current",
        "normalized_flux",
    ]
    labels = ["iota"]
    normalization = None
    if args.all:
        labels = ["xmn", "iota"]
        normalization = ["mean", "std"]

    datamodule = get_datamodule(features, labels, normalization)
    print("Cold read ...")
    datamodule.setup()
    print(f"Transforms: {datamodule.train_data.dataset.dataset.transforms}")
    read_data(epochs=5)
