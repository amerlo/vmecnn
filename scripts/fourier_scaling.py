"""
Visualization script to investigate the scaling of quantities with respect to the Fourier budget.

TODO-0(@amerlo): volume averaged f should have vp as weighting factor
"""

from copy import deepcopy

import matplotlib.pyplot as plt
import numpy as np
import netCDF4
import torch
import torch.nn.functional as F


from w7x_datagen.datasets import VmecEquilibria

from vmecnn.utils import seed_everything
from vmecnn.physics.equilibrium import Equilibrium
from vmecnn.physics.utils import gradient, to_full_mesh

#  Set random seed for reproducibility
seed_everything(42)


def dataset():
    root = "tests/resources/data"
    return VmecEquilibria(root=root, train_test_split=1.0)


def get_run(item):
    return netCDF4.Dataset(item["wout_path"])


def runs(data):
    return [get_run(item) for item in data]


def equis(data):
    return [
        Equilibrium(
            rmnc=item["rmnc"],
            lmns=item["lmns"],
            zmns=item["zmns"],
            iota=item["iota"],
            phiedge=item["phiedge"],
            pressure=item["pressure"],
            normalized_flux=np.linspace(0, 1, len(item["iota"]), dtype=np.float64),
        )
        for item in data
    ]


def _extrapolate(tensor):
    """Extrapolate profile in place on axis and on LCFS."""
    tensor[0] = 2.0 * tensor[1] - tensor[2]
    tensor[-1] = 2.0 * tensor[-2] - tensor[-3]
    return tensor


def compute_from_run(run, attr: str):
    """
    See tests/test_equilibrium.py.
    """
    mu0 = 4 * np.pi * 1e-7
    twopi = 2 * np.pi

    signgs = torch.from_numpy(run["signgs"][:].data)
    phiedge = torch.from_numpy(run["phi"][-1].data)
    iotaf = torch.from_numpy(run["iotaf"][:].data)
    pres = torch.from_numpy(run["pres"][:].data) * mu0

    phipf = phiedge / twopi / signgs

    presgrad = gradient(pres, order=1, factor=2.0)

    vp = to_full_mesh(torch.from_numpy(run["vp"][:].data), mode="implicit")

    bsubu = torch.from_numpy(run["bsubumnc"][:, 0].data)
    bsubv = torch.from_numpy(run["bsubvmnc"][:, 0].data)

    jcuru = -gradient(bsubv, order=1, factor=2.0) * signgs
    jcurv = gradient(bsubu, order=1, factor=2.0) * signgs

    f = phipf * iotaf * jcurv - phipf * jcuru + presgrad * vp
    f = _extrapolate(f)

    vp = torch.from_numpy(run["vp"][:].data)

    vp_real = vp * 4 * np.pi**2 / phiedge * signgs
    well = gradient(vp_real, order=1) / phiedge
    well = -well * signgs

    b = torch.from_numpy(run["bmnc"][:, 0].data)

    dwell = torch.from_numpy(run["DWell"][:].data)

    if attr == "f":
        return f

    if attr == "jcuru":
        return jcuru

    if attr == "jcurv":
        return jcurv

    if attr == "vp":
        return vp

    if attr == "well":
        return well

    if attr == "b":
        return b

    if attr == "dwell":
        return dwell

    raise RuntimeError(f"Attribute {attr} is not supported")


def grid_scaling(profile: str, grid: tuple, log: bool):
    data = dataset()
    for run, equi in zip(runs(data), equis(data)):
        fig, ax = plt.subplots()
        grids = list(zip(grid, grid))
        #  Add Nyquist sampling based on mpol, ntor
        grids.insert(0, (equi.mpol * 2 + 1, equi.ntor * 2 + 1))
        #  Add Nyquist sampling based on nyq_mpol, nyq_ntor
        grids.insert(1, (equi.mpol_nyq * 2 + 1, equi.ntor_nyq * 2 + 1))
        #  Add VMEC resolution
        grids.insert(2, (32, 36))
        for i, (ntheta, nzeta) in enumerate(grids):
            #  Set grid resolution
            equi.ntheta = ntheta
            equi.nzeta = nzeta
            #  Build data
            flux = torch.linspace(0, 1, equi.ns, dtype=torch.float64)
            attr = getattr(equi, profile)
            if log:
                attr = torch.abs(attr)
            if i == len(grids) - 1:
                fmt = "x-"
            else:
                fmt = "-"
            ax.plot(flux, attr, fmt, label=f"({ntheta}, {nzeta})")
        if log:
            ax.set_yscale("log")
        title = f"equi.{profile}, beta = {run['betatotal'][:]:.2e} - Grid Scaling (ntheta, nzeta)"
        ax.set_title(title)
        ax.legend()
    plt.show()


def profile_fourier_scaling(profile: str, modes: tuple, log: bool):
    data = dataset()
    for run, equi in zip(runs(data), equis(data)):
        fig, ax = plt.subplots()
        #  Keep original equi
        equi_ = deepcopy(equi)
        #  Color gradation
        colors = plt.cm.get_cmap("Reds")(np.linspace(0, 1, len(modes)))
        for i, (mpol, ntor) in enumerate(modes):
            #  Crop equilibrium
            equi_.rmnc = equi.rmnc[:, 0 : mpol + 1, 12 - ntor : 12 + ntor + 1]
            equi_.lmns = equi.lmns[:, 0 : mpol + 1, 12 - ntor : 12 + ntor + 1]
            equi_.zmns = equi.zmns[:, 0 : mpol + 1, 12 - ntor : 12 + ntor + 1]
            grids = ((mpol * 2 + 1, ntor * 2 + 1), (32, 36))
            for j, (ntheta, nzeta) in enumerate(grids):
                #  Set fix grid resolution
                equi_.ntheta = ntheta
                equi_.nzeta = nzeta
                #  Build data
                flux = torch.linspace(0, 1, equi_.ns, dtype=torch.float64)
                attr = getattr(equi_, profile)
                #  Use flux surface averaged
                if len(attr.shape) > 1:
                    attr = attr.mean(dim=(1, 2))
                #  Extrapolate to avoid outliers in plot
                attr = _extrapolate(attr)
                if log:
                    attr = torch.abs(attr)
                fmt = ""
                if j == 0:
                    fmt += "--"
                else:
                    fmt += "-"
                ax.plot(
                    flux,
                    attr,
                    fmt,
                    color=colors[i],
                    label=f"({mpol}, {ntor}), ({ntheta}, {nzeta})",
                )
        #  Add VMEC profile
        try:
            vmec_attr = compute_from_run(run, attr=profile)
            vmec_attr = _extrapolate(vmec_attr)
            if profile == "f":
                vmec_attr = torch.abs(vmec_attr)
            ax.plot(flux, vmec_attr, "k", label="true")
        except RuntimeError as e:
            print(e)
        if log:
            ax.set_yscale("log")
        title = f"equi.{profile}, beta = {run['betatotal'][:]:.2e} - Fourier Scaling"
        ax.set_title(title)
        ax.legend()
    plt.show()


def attr_fourier_scaling(attr: str, modes: tuple, log: bool, num_samples: int):
    data = dataset()
    fig, ax = plt.subplots()
    values = []
    num = 0
    #  From where to compute mean_f
    start = 2
    for i, equi in enumerate(equis(data)):
        #  Compute VMEC attr
        run = get_run(data[i])
        vmec = compute_from_run(run, attr=attr)
        if attr == "f":
            vmec = torch.abs(vmec[start:]).mean()
        #  Equi values
        equi_values = []
        #  Keep original equi
        equi_ = deepcopy(equi)
        for mpol, ntor in modes:
            #  Crop equilibrium
            equi_.rmnc = equi.rmnc[:, 0 : mpol + 1, 12 - ntor : 12 + ntor + 1]
            equi_.lmns = equi.lmns[:, 0 : mpol + 1, 12 - ntor : 12 + ntor + 1]
            equi_.zmns = equi.zmns[:, 0 : mpol + 1, 12 - ntor : 12 + ntor + 1]
            scalar = getattr(equi_, attr)
            #  Compute difference between vmec and equi
            #  Leave equi in case of f
            if attr == "f":
                scalar = torch.abs(scalar[start:]).mean()
            else:
                if len(scalar.shape) > 1:
                    scalar = scalar.mean(dim=(1, 2))
                scalar = torch.abs(scalar - vmec).mean()
            equi_values.append(scalar.item())
        #  In case of f, add VMEC
        if attr == "f":
            equi_values.append(vmec.item())
        #  Append values
        values.append(equi_values)
        if num == num_samples - 1:
            break
        num += 1
    #  Compute stats
    values = np.array(values)
    median = np.median(values, axis=0)
    q5 = np.quantile(values, q=0.05, axis=0)
    q95 = np.quantile(values, q=0.95, axis=0)
    x = np.arange(len(median))
    ax.plot(x, median)
    ax.fill_between(x, q5, q95, alpha=0.2)
    ax.set_xticks(x)
    if attr == "f":
        xticklabels = [str(m) for m in modes] + ["true"]
    else:
        xticklabels = [str(m) for m in modes]
    ax.set_xticklabels(xticklabels)
    if log:
        ax.set_yscale("log")
    title = f"{attr} Fourier Scaling"
    ax.set_title(title)
    plt.show()


def ideal_mhd_loss_scaling(modes: tuple, grids: tuple):
    data = dataset()
    start = 2
    for run, equi in zip(runs(data), equis(data)):
        fig, ax = plt.subplots()
        #  Keep original equi
        equi_ = deepcopy(equi)
        #  Prepare array for data
        values = np.empty((len(modes), len(grids)))
        for i, (mpol, ntor) in enumerate(modes):
            #  Crop equilibrium
            equi_.rmnc = equi.rmnc[:, 0 : mpol + 1, 12 - ntor : 12 + ntor + 1]
            equi_.lmns = equi.lmns[:, 0 : mpol + 1, 12 - ntor : 12 + ntor + 1]
            equi_.zmns = equi.zmns[:, 0 : mpol + 1, 12 - ntor : 12 + ntor + 1]
            for j, (ntheta, nzeta) in enumerate(grids):
                #  Set fix grid resolution
                equi_.ntheta = ntheta
                equi_.nzeta = nzeta
                loss = equi_.f[start:].abs().mean()
                values[i, j] = loss
                ax.text(j - 0.25, i, f"{loss:.2e}", {"color": "white"})
        ax.imshow(values)
        #  Add VMEC f
        f = compute_from_run(run, attr="f")
        loss = f[start:].abs().mean()
        ax.set_yticks(range(len(modes)))
        ax.set_yticklabels([str(m) for m in modes])
        ax.set_ylabel("Fourier modes")
        ax.set_xticks(range(len(grids)))
        ax.set_xticklabels([str(g) for g in grids])
        ax.set_xlabel("Grid resolution")
        title = f"loss = {loss:.2e}, beta = {run['betatotal'][:]:.2e} - Fourier Scaling"
        ax.set_title(title)
    plt.show()


def convergence(
    profile: str, modes: tuple, noise_levels: tuple, absolute: bool, log: bool
):
    data = dataset()
    #  First index from where to display the profile
    start = 0
    if profile == "f":
        start = 2
    mpol, ntor = modes
    attrs_to_perturbe = (
        ("rmnc",),
        ("lmns",),
        ("zmns",),
        ("iota",),
        ("rmnc", "lmns", "zmns", "iota"),
    )
    for equi, run in zip(equis(data), runs(data)):
        fig, axs = plt.subplots(2, 3)
        axs = axs.reshape(-1)
        #  Prepare data structure
        exps = {k: ([], []) for k in attrs_to_perturbe}
        flux = np.linspace(0, 1, 99)
        #  Crop equilibrium
        equi__ = deepcopy(equi)
        equi__.rmnc = equi.rmnc[:, 0 : mpol + 1, 12 - ntor : 12 + ntor + 1]
        equi__.lmns = equi.lmns[:, 0 : mpol + 1, 12 - ntor : 12 + ntor + 1]
        equi__.zmns = equi.zmns[:, 0 : mpol + 1, 12 - ntor : 12 + ntor + 1]
        for j, attrs in enumerate(attrs_to_perturbe):
            #  Plot the full resolution, unperturbed profile
            values = getattr(equi, profile)
            if log:
                values = torch.abs(values)
            axs[j].plot(flux, values.numpy(), label="true")
            for i, noise in enumerate(noise_levels):
                #  Keep original equi
                equi_ = deepcopy(equi__)
                #  Apply noise to equi and compute loss
                mse = 0
                for attr in attrs:
                    value = getattr(equi__, attr).clone()
                    #  Apply a uniform noise between [-noise,+noise]
                    if len(value.shape) > 1:
                        if absolute:
                            value += (2 * torch.rand(value.shape[1:]) - 1) * noise
                        else:
                            value *= 1 + (2 * torch.rand(value.shape[1:]) - 1) * noise
                    else:
                        if absolute:
                            value += (2 * torch.rand(1) - 1) * noise
                        else:
                            value *= 1 + (2 * torch.rand(1) - 1) * noise
                    setattr(equi_, attr, value)
                    if attr == "rmnc":
                        mse += F.mse_loss(equi__.r, equi_.r).item()
                    elif attr == "lmns":
                        mse += F.mse_loss(equi__.l, equi_.l).item()
                    elif attr == "zmns":
                        mse += F.mse_loss(equi__.z, equi_.z).item()
                    elif attr == "iota":
                        mse += F.mse_loss(equi__.iota, equi_.iota).item()
                mse /= len(attr)
                exps[attrs][0].append(mse)
                #  Compute scalar value
                if profile == "f":
                    scalar = equi_.f[start:].abs().mean().item()
                else:
                    scalar = (
                        torch.abs(
                            getattr(equi_, profile)[start:]
                            - getattr(equi__, profile)[start:]
                        )
                        .mean()
                        .item()
                    )
                exps[attrs][1].append(scalar)
                #  Plot profile
                values = getattr(equi_, profile)
                if log:
                    values = torch.abs(values)
                axs[j].plot(flux, values.numpy(), label=f"{mse:.2e}")
                axs[j].set_xlabel("s")
                axs[j].set_ylabel("f")
                if log:
                    axs[j].set_yscale("log")
                axs[j].legend()
                axs[j].set_title(attrs)
            axs[-1].plot(exps[attrs][0], exps[attrs][1], "x-", label=attrs)
        axs[-1].set_yscale("log")
        axs[-1].set_xscale("log")
        axs[-1].set_xlabel("RealMSELoss")
        if profile == "f":
            axs[-1].set_ylabel("IdealMHDLoss")
        else:
            axs[-1].set_ylabel("MAELoss")
        axs[-1].legend()
        title = (
            f"noise = {noise_levels}, "
            + f"modes = {modes}, "
            + f"beta = {run['betatotal'][:]:.2e} - {profile} convergence"
        )
        fig.suptitle(title)
    plt.show()


if __name__ == "__main__":
    convergence(
        profile="f",
        modes=(6, 4),
        noise_levels=(0, 1e-6, 1e-5, 1e-4, 1e-3, 1e-2, 1e-1),
        absolute=True,
        log=True,
    )
    ideal_mhd_loss_scaling(
        modes=(
            (1, 1),
            (2, 1),
            (2, 2),
            (3, 2),
            (4, 3),
            (4, 4),
            (6, 4),
            (8, 6),
            (10, 8),
            (10, 10),
            (11, 12),
        ),
        grids=((8, 9), (12, 14), (16, 18), (24, 27), (32, 26)),
    )
    grid_scaling(profile="f", grid=(8, 16, 32, 64), log=True)
    profile_fourier_scaling(
        profile="f",
        modes=(
            (2, 1),
            (3, 2),
            (6, 4),
            (8, 6),
            (10, 8),
            (11, 8),
            (11, 12),
        ),
        log=True,
    )
    attr_fourier_scaling(
        attr="f",
        modes=(
            (1, 1),
            (2, 1),
            (2, 2),
            (3, 2),
            (4, 3),
            (4, 4),
            (6, 4),
            (6, 6),
            (8, 6),
            (8, 8),
            (10, 8),
            (10, 10),
            (11, 12),
        ),
        log=True,
        num_samples=32,
    )
