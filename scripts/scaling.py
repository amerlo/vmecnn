"""
Script for models scaling.
"""

import argparse
from itertools import product


def fc_nparams(
    width: int, factor: float, depth: int, in_features: int, out_features: int
) -> int:

    if depth == 0:
        return in_features * out_features + out_features

    # nparams = in_features * width + width
    # if depth == 1:
    #     return nparams + width * out_features + out_features

    nparams = 0
    for i in range(depth):
        nparams += width * factor**i * width * factor ** (i + 1) + width * factor ** (
            i + 1
        )

    nparams += width * factor ** (i + 1) * out_features + out_features
    return nparams


def fc(parser):

    parser.add_argument(
        "-w", "--width", nargs="+", help="layer width", required=True, type=int
    )
    parser.add_argument(
        "-f",
        "--factor",
        nargs="+",
        help="layer growth factor",
        required=True,
        type=float,
    )
    parser.add_argument(
        "-d",
        "--depth",
        nargs="+",
        help="number of hidden layer",
        required=True,
        type=int,
    )
    parser.add_argument(
        "-i", "--in_features", help="in features", required=True, type=int
    )
    parser.add_argument(
        "-o", "--out_features", help="out features", required=True, type=int
    )
    parser.add_argument("-b", "--budget", help="target budget", default=None, type=int)
    parser.add_argument(
        "-r", "--rtol", help="budget relative tolerance", default=0.05, type=float
    )

    args = parser.parse_args()

    width = args.width
    factor = args.factor
    depth = args.depth

    budget = args.budget
    rtol = args.rtol

    #  If target budget is given, treat args as range
    if budget is not None:
        assert len(width) == len(factor) == len(depth) == 2
        width = list(range(width[0], width[1] + 1))
        depth = list(range(depth[0], depth[1] + 1))
        factor = list(range(int(factor[0] * 10), int(factor[1] * 10) + 1))
        factor = [f / 10 for f in factor]

    #  Create hps cobmination and remove meaningless ones
    hps = list(product(width, factor, depth))
    hps = [(w, f, d) for w, f, d in hps if d != 1 or (d == 1 and f == 1.0)]

    nparams = {}
    for w, f, d in hps:
        nparam = fc_nparams(w, f, d, args.in_features, args.out_features)
        #  Add configuration only if close to target budget
        if budget is not None:
            delta = abs(nparam - budget) / budget
            if delta > rtol:
                continue
        nparams[(w, f, d)] = nparam

    nparams_sorted = sorted(nparams.items(), key=lambda x: x[1])
    for (w, f, d), num in nparams_sorted:
        print(f"w={w}, f={f}, d={d}, nparams={num:0.0f}")


def mhdnet_nparams(
    mpol: int,
    ntor: int,
    trunk_width: int,
    trunk_depth: int,
    trunk_basis: int,
    bias_order: int,
    conv_width: int,
    branch_width: int,
    branch_depth: int,
) -> int:

    nparams = 0

    scalar_features = 9
    out_quantities = 3 * ((mpol + 1) * (2 * ntor + 1) - ntor) - 2 + 1
    trunk_features = out_quantities * trunk_basis
    out_features = out_quantities * (1 + bias_order + trunk_basis)

    #  trunk net
    #  We assume a not shared trunk net
    nparams += 2 * trunk_width
    nparams += trunk_depth * (trunk_width**2 + trunk_width)
    nparams += trunk_features * (trunk_width + 1)

    #  TODO: add profil net

    #  branch_net
    #  4 is the assumed number of profile output length
    branch_input_features = scalar_features + conv_width * 4
    nparams += branch_input_features * branch_width
    nparams += branch_depth * (branch_width**2 + branch_width)
    nparams += (branch_width + 1) * out_features

    return nparams


def mhdnet(parser):

    #  TODO: add bias_order and conv_width as args
    parser.add_argument("--mpol", help="poloidal modes", required=True, type=int)
    parser.add_argument("--ntor", help="toroidal modes", required=True, type=int)
    parser.add_argument(
        "-tw", "--trunk_width", nargs="+", help="trunk width", required=True, type=int
    )
    parser.add_argument(
        "-td", "--trunk_depth", nargs="+", help="trunk depth", required=True, type=int
    )
    parser.add_argument(
        "-tb", "--trunk_basis", nargs="+", help="trunk basis", required=True, type=int
    )

    parser.add_argument("-bo", "--bias_order", help="bias order", default=4, type=int)

    parser.add_argument(
        "-bw", "--branch_width", nargs="+", help="branch width", required=True, type=int
    )
    parser.add_argument(
        "-bd", "--branch_depth", nargs="+", help="branch depth", required=True, type=int
    )

    parser.add_argument("-b", "--budget", help="target budget", default=None, type=int)
    parser.add_argument(
        "-r", "--rtol", help="budget relative tolerance", default=0.05, type=float
    )

    args = parser.parse_args()

    tw = args.trunk_width
    td = args.trunk_depth
    tb = args.trunk_basis

    bo = args.bias_order

    bw = args.branch_width
    bd = args.branch_depth

    #  If target budget is given, treat args as range
    if args.budget is not None:
        for a in (tw, td, tb, bw, bd):
            assert len(a) == 2
        tw = list(range(tw[0], tw[1] + 1))
        td = list(range(td[0], td[1] + 1))
        tb = list(range(tb[0], tb[1] + 1))
        bw = list(range(bw[0], bw[1] + 1))
        bd = list(range(bd[0], bd[1] + 1))

    hps = list(product(tw, td, tb, bw, bd))

    nparams = {}
    for tw_, td_, tb_, bw_, bd_ in hps:
        nparam = mhdnet_nparams(
            mpol=args.mpol,
            ntor=args.ntor,
            trunk_width=tw_,
            trunk_depth=td_,
            trunk_basis=tb_,
            bias_order=bo,
            conv_width=16,
            branch_depth=bd_,
            branch_width=bw_,
        )
        #  Add configuration only if close to target budget
        if args.budget is not None:
            delta = abs(nparam - args.budget) / args.budget
            if delta > args.rtol:
                continue
        nparams[(tw_, td_, tb_, bw_, bd_)] = nparam

    nparams_sorted = sorted(nparams.items(), key=lambda x: x[1])
    for (tw_, td_, tb_, bw_, bd_), num in nparams_sorted:
        print(
            f"trunk width={tw_}, trunk depth={td_}, trunk basis={tb_}, "
            + f"branch width={bw_}, branch depth={bd_}, nparams={num / 1e6:0.1f}M"
        )


if __name__ == "__main__":

    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument("-m", "--model", required=True, type=str)
    model_parser = argparse.ArgumentParser(parents=[parser])
    args = parser.parse_known_args()[0]

    if args.model == "fc":
        fc(model_parser)
    elif args.model == "mhdnet":
        mhdnet(model_parser)
    else:
        raise ValueError
