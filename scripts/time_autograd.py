"""
$ kernprof -v -l scripts/time_autograd.py
"""
from time import time

import torch

from vmecnn.utils import seed_everything
from vmecnn.datamodules.utils import get_random_equi
from vmecnn.metrics.metric import (
    grad_1d,
    grad_3d,
    compute_flux_surface_averaged_f,
)


#  Set random seed for reproducibility
seed_everything(42)


def func_1d(x):
    return x**2


def func_3d(x):
    shape = (8, 9)
    b = torch.randn(x.size(0), shape[0] * shape[1])
    y = x.view(-1, 1) ** 2 * b
    return y.view(-1, *shape)


def compute_grad(number=1000, size=99, kind="1d", device="cpu"):
    x = torch.rand(size, dtype=torch.float64).requires_grad_(True)
    if device == "cuda":
        x = x.cuda()
    if kind == "1d":
        y = func_1d(x)
        t0 = time()
        for _ in range(number):
            grad_1d(y, x, retain_graph=True)
    elif kind == "3d":
        y = func_3d(x)
        t0 = time()
        for _ in range(number):
            grad_3d(y, x, retain_graph=True)
    elif kind == "1d1d":
        y = func_1d(x)
        t0 = time()
        for _ in range(number):
            grad = grad_1d(y, x, create_graph=True)
            grad_1d(grad, x)
    elif kind == "3d1d":
        y = func_3d(x)
        t0 = time()
        for _ in range(number):
            grad = grad_3d(y, x, create_graph=True)
            grad_1d(grad.mean(dim=(1, 2)), x)
    else:
        assert "Not suppoted kind " + kind
    tottime = time() - t0
    time_per_dim = tottime / number / x.size(0)
    print(f"Time per dim: {time_per_dim * 1e6:.2f} us")


def compute_equif_loss(number: int = 100, ns: int = 99):

    mpol, ntor = (6, 4)
    ntheta = 16
    nzeta = 18
    dtype = torch.float64
    device = "cpu"

    equi = get_random_equi(ns=ns, mpol=mpol, ntor=ntor, dtype=dtype, device=device)

    rmnc = equi["rmnc"]
    lmns = equi["lmns"]
    zmns = equi["zmns"]
    iota = equi["iota"]
    normalized_flux = equi["normalized_flux"]
    phiedge = equi["phiedge"].expand(ns).clone()
    pressure = equi["pressure"].expand(ns, ns).clone()

    t0 = torch.cuda.Event(enable_timing=True)
    t1 = torch.cuda.Event(enable_timing=True)
    t0.record()

    for _ in range(number):
        res = compute_flux_surface_averaged_f(
            rmnc=rmnc,
            lmns=lmns,
            zmns=zmns,
            iota=iota,
            normalized_flux=normalized_flux,
            phiedge=phiedge,
            pressure=pressure,
            ntheta=ntheta,
            nzeta=nzeta,
            use_nn_gradients=True,
        )

    t1.record()
    torch.cuda.synchronize()
    tottime = t0.elapsed_time(t1)

    for v in res.values():
        if device == "cuda":
            assert v.is_cuda

    time_per_equi = tottime / number
    print(f"Time per equi: {time_per_equi:.2f} ms")


if __name__ == "__main__":
    compute_grad(kind="1d")
    compute_equif_loss()
