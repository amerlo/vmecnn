"""
Plot model layers statistics.
"""
import argparse
from collections import namedtuple

import matplotlib.pyplot as plt
import torch

from vmecnn.utils import seed_everything
from vmecnn.datamodules.utils import get_num_input
from vmecnn.models import FullyConnected

#  Set random seed for reproducibility
seed = 42
seed_everything(seed)

batch_size = 1024
depth = 10
X = {
    "npc": torch.randn(batch_size, 4),
    "pc": torch.randn(batch_size, 2),
    "phiedge": torch.randn(batch_size, 1),
    "normalized_flux": torch.rand(batch_size, 1),
}


#  TODO-1(@amerlo): support MHDNet
def get_model(model: str, mpol: int, ntor: int):
    kwargs = {
        "input_features": get_num_input(X.keys(), ns=0),
        "mpol": mpol,
        "ntor": ntor,
        "labels": ["xmn", "iota"],
        "width": 64,
        "depth": depth,
        "criterion": {"_target_": torch.nn.MSELoss},
        "optimizer": namedtuple("optimizer", ["lr"])(3e-4),
        "activation": {"_target_": "torch.nn.SiLU"},
    }
    model = FullyConnected(**kwargs)
    #  Assume scaled output
    model.init_bias("zeros")
    return model


def plot_stats(model):
    stats = {k: [] for k in ("mean", "std", "act")}
    act = torch.cat([v for v in X.values()], dim=1)
    #  Add input
    stats["mean"].append(act.mean().item())
    stats["std"].append(act.std().item())
    stats["act"].append(act.view(-1).tolist())
    for i in range(depth + 2):
        layer = getattr(model.layers, f"fc{i}").layers
        #  Linear layer
        act = torch.einsum("bi,oi->bo", act, layer[0].weight)
        act += layer[0].bias
        if len(layer) > 1:
            act = layer[1](act)
        stats["mean"].append(act.mean().item())
        stats["std"].append(act.std().item())
        stats["act"].append(act.view(-1).tolist())
    #  Plot stats
    fig, axs = plt.subplots(1, 3)
    x = list(range(len(stats["mean"])))
    axs[0].plot(x, stats["mean"])
    axs[0].set_title("mean")
    axs[1].plot(x, stats["std"])
    axs[1].set_title("std")
    #  Plot activations distributions
    for i, act in enumerate(stats["act"]):
        axs[2].hist(act, density=True, label=str(i))
    axs[2].set_title("activations")
    axs[2].legend()
    plt.show()


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--model", dest="model", type=str, default="fully_connected")
    args = parser.parse_args()
    model = get_model(args.model, mpol=1, ntor=1)
    plot_stats(model)
