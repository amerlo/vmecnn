"""
Profiling script for the inverse Fourier transform implementation.

Add the @profile decorator to the ift function in `physics.py` and run as:

$ kernprof -v -l scripts/time_ift.py
"""

import timeit
from time import time

import matplotlib.pyplot as plt
import numpy as np
import torch

from vmecnn.utils import seed_everything
from vmecnn.physics.equilibrium import ift


#  Set random seed for reproducibility
seed_everything(42)

mpol = 11
ntor = 12
ns = 256

ntheta = 32
nzeta = 36


def op_timing(number=1000):
    bs = 128
    n = 16
    a = torch.rand(bs, n, n, n, n)
    b = torch.rand(bs, n, n)
    #  mul and sum
    b = b.view(bs, 1, 1, n, n)
    t0 = time()
    for _ in range(number):
        torch.sum(a * b, dim=(-2, -1))
    sum_time = (time() - t0) / number / bs
    #  einsum
    b = b.view(bs, n, n)
    t0 = time()
    for _ in range(number):
        torch.einsum("stzmn,smn->stz", a, b)
    einsum_time = (time() - t0) / number / bs
    #  bmm
    a = a.view(bs, n**2, n**2)
    b = b.view(bs, n**2, 1)
    t0 = time()
    for _ in range(number):
        torch.bmm(a, b)
    bmm_time = (time() - t0) / number / bs
    print(f"sum:    {sum_time * 1e6:.2f} us")
    print(f"einsum: {einsum_time * 1e6:.2f} us")
    print(f"bmm:    {bmm_time * 1e6:.2f} us")


def op_grad_timing(number=1000, warmup=10):

    bs = 128
    m = 16
    n = 8

    def grad(y, x):
        g = torch.autograd.grad(
            y,
            x,
            grad_outputs=torch.ones(y.shape),
            create_graph=True,
            retain_graph=True,
        )[0]
        g = torch.autograd.grad(
            g,
            x,
            grad_outputs=torch.ones(g.shape),
            retain_graph=True,
        )[0]
        return g

    a = torch.rand(bs, m, m, n, n)
    x = torch.rand(bs).requires_grad_(True)
    b = x.view(-1, 1, 1) ** 2 * torch.rand(bs, n, n)
    #  einsum
    t = torch.einsum("ijkhl,ihl->ijk", a, b).view(-1)
    #  warming up
    for _ in range(warmup):
        grad(t, x)
    t0 = time()
    for _ in range(number):
        grad(t, x)
    einsum_time = (time() - t0) / number / bs
    #  bmm
    t = torch.bmm(a.view(bs, m**2, n**2), b.view(bs, n**2, 1)).view(-1)
    #  warming up
    for _ in range(warmup):
        grad(t, x)
    t0 = time()
    for _ in range(number):
        grad(t, x)
    bmm_time = (time() - t0) / number / bs
    print(f"einsum: {einsum_time * 1e6:.2f} us")
    print(f"bmm:    {bmm_time * 1e6:.2f} us")


def compute_ift(number=1000, device="cuda"):
    tensor = torch.rand(ns, mpol + 1, 2 * ntor + 1).to(device)
    start = torch.cuda.Event(enable_timing=True)
    end = torch.cuda.Event(enable_timing=True)
    start.record()
    for _ in range(number):
        ift((tensor, None), ntheta=ntheta, nzeta=nzeta)
    end.record()
    torch.cuda.synchronize()
    tottime = start.elapsed_time(end)
    time_flux_surface = tottime / number / tensor.size(0)
    print(f"Time per flux surface: {time_flux_surface * 1e3:.2f} us")


def grid_scaling(number=10):
    nthetas = np.linspace(1, ntheta, 10, dtype=int)
    nzetas = np.linspace(1, nzeta, 10, dtype=int)
    num_points = []
    times = []
    for t, z in zip(nthetas, nzetas):
        tensor = torch.rand(ns, mpol + 1, 2 * ntor + 1)  # noqa
        times.append(
            timeit.timeit(
                f"ift((tensor, None), ntheta={t}, nzeta={z})",
                number=number,
                globals=globals(),
            )
            / number
            / ns
        )
        num_points.append(t * z)
    fig, ax = plt.subplots(1)
    ax.plot(num_points, times)
    ax.set_xlabel("num grid points")
    ax.set_ylabel("time per flux surface")
    ax.set_title(f"batch_size={ns}, time@{num_points[-1]}={times[-1]:.6f}")
    plt.yscale("log")
    plt.show()


def batch_scaling(number=10):
    batch_sizes = np.linspace(1, ns, 10, dtype=int)
    times = []
    for batch_size in batch_sizes:
        tensor = torch.rand(batch_size, mpol + 1, 2 * ntor + 1)  # noqa
        times.append(
            timeit.timeit(
                "ift((tensor, None), ntheta=ntheta, nzeta=nzeta)",
                number=number,
                globals=globals(),
            )
            / number
            / batch_size
        )
    fig, ax = plt.subplots(1)
    ax.plot(batch_sizes, times)
    ax.set_xlabel("batch size")
    ax.set_ylabel("time per flux surface")
    ax.set_title(
        f"ntheta={ntheta}, nzeta={nzeta}, time@{batch_sizes[-1]}={times[-1]:.6f}"
    )
    plt.yscale("log")
    plt.show()


if __name__ == "__main__":
    op_timing()
    op_grad_timing()
    grid_scaling()
    batch_scaling()
    compute_ift()
