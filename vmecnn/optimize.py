"""Scan and optimizer actions."""

import os
from copy import deepcopy
from typing import Iterable, Optional, List

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import torch
from hydra.core.hydra_config import HydraConfig
from omegaconf import DictConfig
from pytorch_lightning import LightningModule

from vmecnn.utils import get_logger, load
from vmecnn.models import MHDNet
from vmecnn.physics.equilibrium import Equilibrium
from vmecnn.datamodules.utils import NS
from vmecnn.plotting.utils import (
    set_paper_style,
    savefig,
    RED,
    BLUE,
    LARGE_SQUARED_FIGSIZE,
)

set_paper_style()


def _load_from_checkpoint(cfg: DictConfig) -> LightningModule:

    assert cfg.model_checkpoint is not None

    log = get_logger(__name__)

    model_checkpoint = cfg.model_checkpoint
    model_checkpoint = os.path.join(cfg.orig_cwd, model_checkpoint)
    log.info("Model checkpoint: %s" % model_checkpoint)

    #  TODO-1(@amerlo): be model class agnostic here
    model = MHDNet.load_from_checkpoint(
        model_checkpoint,
        enable_grad_on_predict=cfg.model.enable_grad_on_predict,
        to_double=cfg.datamodule.to_double,
    )
    model.setup()
    model.eval()

    return model


def _get_standard_config(ns: int, dtype):
    """Get W7-X standard configuration as model input."""
    #  As from: http://svvmec1.ipp-hgw.mpg.de:8080/vmecrest/v1/geiger/w7x/1000_1000_1000_1000_+0000_+0000/01/00jh_l/  # noqa
    #  Normalized flux for profiles generation
    s = torch.linspace(0, 1, NS, dtype=dtype)
    return {
        "npc": torch.ones(ns, 4, dtype=dtype),
        "pc": torch.zeros(ns, 2, dtype=dtype),
        "phiedge": -1.9085 * torch.ones(ns, 1, dtype=dtype),
        "p0": 1e-6 * torch.ones(ns, 1, dtype=dtype),
        "curtor": torch.zeros(ns, 1, dtype=dtype),
        "pressure": ((1 - s**1) ** 2).view(1, -1).expand(ns, NS),
        "toroidal_current": (2 / torch.pi * torch.arctan(s / (1 - s))).expand(ns, NS),
        "normalized_flux": torch.linspace(0, 1, ns, dtype=dtype).view(ns, 1),
    }


def _normalize(data: dict, metadata: dict):
    """Normalize model input data with normalization parameters."""
    return {
        k: (v - metadata["mean"][k][0]) / metadata["std"][k][0]
        if k in metadata["mean"]
        else v
        for k, v in data.items()
    }


def _check_model_consistency(model: LightningModule, metadata: dict) -> None:
    """Check if model provides same outputs for given saved input."""

    sample_input = {
        k: torch.as_tensor(v, dtype=model.dtype).view(1, -1)
        for k, v in metadata["sample_input"].items()
    }

    sample_output = model.forward(sample_input)
    for k, v in sample_output.items():
        _v = torch.as_tensor(metadata["sample_output"][k], dtype=model.dtype)
        assert torch.allclose(_v, v[0], rtol=0, atol=1e-15)


def _solve(
    model: LightningModule,
    metadata: dict,
    config: dict,
    compute_surfs: Optional[List[int]] = None,
) -> Equilibrium:
    normalized_config = _normalize(config, metadata)
    equi = Equilibrium.from_model({**config, **model.forward(normalized_config)})
    #  Set reduced resolution for booz_xform
    if compute_surfs is not None:
        equi.compute_surfs = np.array(compute_surfs, dtype=int)
    return equi


def _check_standard_config(model: LightningModule, metadata: dict) -> None:
    """Check if model reproduces standard configuration properties."""

    ns = 99
    standard_config = _get_standard_config(ns=ns, dtype=model.dtype)
    with torch.no_grad():
        equi = _solve(model, metadata, standard_config)

    #  Check standard configuration is faithfully reproduced
    #  Standard configuration values for this phiedge are taken from link
    #  in `_get_standard_config`
    assert abs(equi.iota[0].item() - 0.856) < 1e-2
    assert abs(equi.iota[-1].item() - 0.968) < 1e-2
    assert abs(equi.volume.item() - 28.598786069587568) < 1e-1
    assert abs(equi.r_axis.item() - 5.948689212362945) < 1e-3
    assert abs(equi.vacuum_well.item() - 0.01032587564605251) < 1e-3


def scan(cfg: DictConfig) -> None:

    #  Load model from checkpoint
    model = _load_from_checkpoint(cfg)

    #  Load metadata
    assert cfg.metadata_file is not None
    metadata = load(os.path.join(cfg.orig_cwd, cfg.metadata_file))

    _check_model_consistency(model, metadata)
    _check_standard_config(model, metadata)

    ns = 128
    s = torch.linspace(0, 1, NS, dtype=model.dtype)
    standard_config = _get_standard_config(ns=ns, dtype=model.dtype)

    def _set(data: dict, k: str, v: float):
        """Set key-value pair to model input data."""
        if k in ("p0", "phiedge", "curtor"):
            data[k][:, 0] = v
        elif k in ("i2", "i3", "i4", "i5"):
            idx = int(k[-1]) - 2
            data["npc"][:, idx] = v
        elif k == "iA":
            data["pc"][:, 0] = v
        elif k == "iB":
            data["pc"][:, 1] = v
        elif k == "pressure_c1":
            data["pressure"] = ((1 - s**v) ** 2).view(1, -1).expand(ns, NS)
        elif k == "pressure_c2":
            data["pressure"] = ((1 - s**1) ** v).view(1, -1).expand(ns, NS)
        elif k == "toroidal_current_c4":
            data["toroidal_current"] = (
                2 / torch.pi * torch.arctan(s / (1 - s) ** v)
            ).expand(ns, NS)
        else:
            raise NotImplementedError

    for fig_title, figure in cfg.figures.items():

        fig, ax = plt.subplots(1, 1, figsize=LARGE_SQUARED_FIGSIZE)

        #  Shorthand
        num = figure.num
        x = figure.x.label
        y = figure.y.label if figure.y is not None else None
        z = eval(figure.z)

        if figure.x.log:
            x_data = torch.logspace(figure.x.start, figure.x.end, num)
        else:
            x_data = torch.linspace(figure.x.start, figure.x.end, num)

        if y is not None:
            if figure.y.log:
                y_data = torch.logspace(figure.y.start, figure.y.end, num)
            else:
                y_data = torch.linspace(figure.y.start, figure.y.end, num)

        #  (M, N) where M=len(y), N=len(x)
        z_data = torch.empty(num if y is not None else 1, num)

        with torch.no_grad():
            for j in range(z_data.shape[0]):
                for i in range(z_data.shape[1]):
                    data = deepcopy(standard_config)
                    _set(data, x, x_data[i])
                    if y is not None:
                        _set(data, y, y_data[j])
                    equi = _solve(model, metadata, data)
                    z_data[j, i] = z(equi)

        if y is None:
            ax.plot(x_data, z_data[0], **figure.fig_kwargs)
        else:
            cset = ax.contourf(x_data, y_data, z_data, **figure.fig_kwargs)
            norm = matplotlib.colors.Normalize(
                vmin=cset.cvalues.min(), vmax=cset.cvalues.max()
            )
            sm = plt.cm.ScalarMappable(norm=norm, cmap=cset.cmap)
            sm.set_array([])
            fig.colorbar(sm, ticks=cset.levels, ax=ax)

        ax.set_xlabel(x)
        if figure.x.log:
            ax.set_xscale("log")

        if y is not None:
            ax.set_ylabel(y)
            if figure.y.log:
                ax.set_yscale("log")

        ax.set_title(fig_title)
        savefig(fig, f"{fig_title}.pdf")


def _optimize_set(
    data, keys: Iterable[str], values: torch.Tensor, metadata: dict, ns: int
):
    """Set and backnormalize optimization array."""
    for i, k in enumerate(keys):
        if k in ("phiedge", "p0", "curtor"):
            mean = metadata["mean"][k][0]
            std = metadata["std"][k][0]
            data[k] = mean + std * values[i].expand(ns, 1)
        elif k in ("i2", "i3", "i4", "i5"):
            mean = metadata["mean"]["npc"][0]
            std = metadata["std"]["npc"][0]
            idx = int(k[-1]) - 2
            data["npc"] = torch.cat(
                [
                    data["npc"][:, :idx],
                    (mean + std * values[i]).expand(ns, 1),
                    data["npc"][:, idx + 1 :],
                ],
                dim=-1,
            )
        elif k in ("iA", "iB"):
            mean = metadata["mean"]["pc"][0]
            std = metadata["std"]["pc"][0]
            idx = 0 if k == "iA" else 1
            data["pc"] = torch.cat(
                [
                    data["pc"][:, :idx],
                    (mean + std * values[i]).expand(ns, 1),
                    data["pc"][:, idx + 1 :],
                ],
                dim=-1,
            )
        else:
            raise NotImplementedError
    #  Clip p0 to positive values
    if data["p0"][0] <= 0:
        data["p0"][:] = 1e-6


def optimize(cfg: DictConfig) -> None:

    from scipy.optimize import minimize

    assert cfg.optimization is not None

    log = get_logger(__name__)

    #  Load model from checkpoint
    model = _load_from_checkpoint(cfg)

    #  Load metadata
    assert cfg.metadata_file is not None
    metadata = load(os.path.join(cfg.orig_cwd, cfg.metadata_file))

    _check_model_consistency(model, metadata)
    _check_standard_config(model, metadata)

    #  Make sure model is not in the graph
    model.requires_grad_(False)

    #  Shorthand
    ns = cfg.optimization.ns
    keys = cfg.optimization.space.keys()

    #  Start from the standard configuration
    standard_config = _get_standard_config(ns=ns, dtype=model.dtype)

    #  Copy standard config in optimization config
    config = deepcopy(standard_config)

    #  Define objective function
    def loss_fn(equi):
        loss = 0
        for term in cfg.optimization.loss.values():
            v = eval(term["fn"])(equi)
            loss += term["weight"] * (v - term["target"]) ** 2
        return loss

    def to_torch(x):
        if isinstance(x, dict):
            x = np.array([x[k] for k in keys], dtype=np.float64)
        return torch.from_numpy(x)

    #  Define function to minimize
    def fn(x):
        x = to_torch(x)
        x.requires_grad_(True)
        _optimize_set(config, keys=keys, values=x, ns=ns, metadata=metadata)
        equi = _solve(
            model, metadata, config, compute_surfs=cfg.optimization.compute_surfs
        )
        loss = loss_fn(equi)
        if (
            cfg.optimization.local
            and cfg.optimization.local_options.jac
            and loss.requires_grad
        ):
            grad = torch.autograd.grad(loss, x)[0]
            assert grad.shape == x.shape
            return loss.item(), grad.detach().numpy()
        return loss.item()

    #  Set initial guess
    x0 = np.empty(len(keys), dtype=np.float64)
    for i, k in enumerate(keys):
        if cfg.optimization.space[k].x0 is not None:
            x0[i] = cfg.optimization.space[k].x0
        else:
            temp = np.random.random_sample()
            x0[i] = (
                cfg.optimization.space[k].bounds[1]
                - cfg.optimization.space[k].bounds[0]
            ) * temp + cfg.optimization.space[k].bounds[0]

    #  Log initial guess
    log.info(f"x0         : {x0.tolist()}")

    # Set up bounds and minimize objective function
    if cfg.optimization.local:
        res = minimize(
            fn,
            x0,
            jac=cfg.optimization.local_options.jac,
            bounds=[tuple(cfg.optimization.space[k].bounds) for k in keys],
            method=cfg.optimization.local_options.method,
            options=cfg.optimization.local_options.options,
        )
        x = to_torch(res.x)
    else:
        from hyperopt import hp, fmin, tpe

        best = fmin(
            fn,
            space={
                k: hp.uniform(k, *v["bounds"])
                for k, v in cfg.optimization.space.items()
            },
            algo=tpe.suggest,
            rstate=np.random.default_rng(cfg.seed),
            **cfg.optimization.global_options,
        )
        x = to_torch(best)

    #  Evaluate optimized equilibrium
    _optimize_set(config, keys=keys, values=x, ns=ns, metadata=metadata)
    equi = _solve(model, metadata, config, compute_surfs=cfg.optimization.compute_surfs)

    #  Log optimization targets
    targets = {k: cfg.optimization.loss[k].target for k in cfg.optimization.loss.keys()}
    log.info(f"targets    : {targets}")

    #  Log optimization results
    if cfg.optimization.local:
        for k in ("fun", "success", "status", "message", "nfev", "nit"):
            log.info(f"{k:11s}: {res[k]}")

    #  Log optimized configuration
    log.info(f"x          : {x.tolist()}")
    log.info(f"npc        : {equi.npc.tolist()}")
    log.info(f"pc         : {equi.pc.tolist()}")
    log.info(f"phiedge    : {equi.phiedge.item():.2f}")
    log.info(f"p0         : {equi.p0.item():.2e}")
    log.info(f"curtor     : {equi.curtor.item():.2e}")

    #  Log objective terms for the optimized configuration
    for k, term in cfg.optimization.loss.items():
        log.info(f"{k:11s}: {eval(term['fn'])(equi).item():.4f}")

    #  Save VMEC input file to validate optimized configuration
    proto_fpath = os.path.join(cfg.orig_cwd, "data/input.standard")
    hydra_cfg = HydraConfig.get()
    fname = "input." + hydra_cfg.runtime.choices.optimization
    indata = equi.to_vmec_indata(proto_fpath)
    with open(fname, "w") as f:
        f.write(indata)


def pareto(cfg: DictConfig) -> None:

    #  Load model from checkpoint
    model = _load_from_checkpoint(cfg)

    assert cfg.metadata_file is not None
    metadata = load(os.path.join(cfg.orig_cwd, cfg.metadata_file))

    _check_model_consistency(model, metadata)
    _check_standard_config(model, metadata)

    #  Make sure model is not in the graph
    model.requires_grad_(False)

    #  Radial resolution
    ns = 128

    #  Start from the standard configuration
    standard_config = _get_standard_config(ns=ns, dtype=model.dtype)

    #  Copy standard config in optimization config
    config = deepcopy(standard_config)

    for fig_fname, figure in cfg.figures.items():

        fig, ax = plt.subplots(1, 1, figsize=LARGE_SQUARED_FIGSIZE)

        #  Shorthand
        keys = figure.space.keys()
        x_fn = eval(figure.x.fn)
        y_fn = eval(figure.y.fn)

        x_data = np.empty(figure.num)
        y_data = np.empty(figure.num)

        for i in range(figure.num):
            #  Sample for uniform [0, 1] distribution
            x = torch.rand(len(keys), dtype=torch.float64)
            #  Map to given bounds
            for j, k in enumerate(keys):
                x[j] = (
                    x[j] * (figure.space[k].bounds[1] - figure.space[k].bounds[0])
                    + figure.space[k].bounds[0]
                )
            _optimize_set(config, keys=keys, values=x, ns=ns, metadata=metadata)
            with torch.no_grad():
                equi = _solve(
                    model,
                    metadata,
                    config,
                    compute_surfs=cfg.optimization.compute_surfs,
                )
            x_data[i] = x_fn(equi)
            y_data[i] = y_fn(equi)

        #  Classify points on the Pareto frontier
        pareto_idx = np.empty(figure.num, dtype=bool)
        for i in range(figure.num):
            x_prime = x_data - x_data[i]
            y_prime = y_data - y_data[i]
            pareto_idx[i] = (
                False
                if (
                    (eval(figure.x.pareto_fn)(x_prime))
                    & (eval(figure.y.pareto_fn)(y_prime))
                ).any()
                else True
            )

        ax.scatter(x_data, y_data, c=BLUE, **figure.fig_kwargs)

        sorted_idx = np.argsort(x_data[pareto_idx])
        ax.plot(x_data[pareto_idx][sorted_idx], y_data[pareto_idx][sorted_idx], c=RED)

        ax.set_xlabel(figure.x.label)
        ax.set_ylabel(figure.y.label)
        if figure.x.log:
            ax.set_xscale("log")
        if figure.y.log:
            ax.set_yscale("log")

        ax.set_title(figure.title)
        savefig(fig, f"{fig_fname}.pdf")
