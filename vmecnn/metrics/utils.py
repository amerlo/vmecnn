"""Metric utilities."""

import numpy as np


def mape(y_pred, y_true):
    """
    Mean absolute percentage error (raw_values by default).

    See: https://github.com/scikit-learn/scikit-learn/blob/844b4be24/sklearn/metrics/_regression.py#L286
    """  # noqa
    eps = np.finfo(np.float64).eps
    mape = np.abs(y_pred - y_true) / np.maximum(np.abs(y_true), eps)
    return np.mean(mape, axis=0)


def nrmse(y_pred, y_true):
    """
    Normalized root mean squared error (raw_values by default).
    """
    eps = np.finfo(np.float64).eps
    mse = ((y_pred - y_true) ** 2).mean(axis=0)
    rmse = np.sqrt(mse)
    std = np.std(y_true, axis=0)
    return rmse / np.maximum(std, eps)


def r2_score(y_pred, y_true):
    """
    R2 score (raw_values by default).
    """
    eps = np.finfo(np.float64).eps
    num = ((y_pred - y_true) ** 2).sum(axis=0)
    denom = ((y_true - y_true.mean(axis=0)) ** 2).sum(axis=0)
    return 1 - num / np.maximum(denom, eps)
