import math
from typing import Any, Callable, Dict, Optional, List, Union
from warnings import warn

import torch
import torch.nn.functional as F
from torch import Tensor, tensor
from torch.nn.modules.loss import _Loss
from torchmetrics import MeanSquaredError as _MeanSquaredError
from torchmetrics import MeanAbsolutePercentageError as _MeanAbsolutePercentageError
from torchmetrics.metric import Metric

from vmecnn.physics.utils import (
    to_full_mesh,
    to_half_mesh,
    derivative,
    gradient,
    grad_1d,
    grad_3d,
    ift,
    ft,
    get_s_half_mesh,
    compute_jacobian,
    MEAN_F_START,
)
from vmecnn.datamodules.utils import NS
from vmecnn.transforms.transforms import BackNormalize
from vmecnn.utils import stack_keys, gather

#########
# Hooks #
#########


def get_loss_named_hook(keys: List[str]):
    def hook(loss, input):
        return stack_keys(input[0], keys), stack_keys(input[1], keys)

    return hook


def get_transforms_hook(*transforms: List[callable]):

    transforms = torch.nn.Sequential(*transforms)

    def hook(loss, input):
        return transforms(input[0]), transforms(input[1])

    return hook


def get_loss_flux_surface_hook(ntheta: int, nzeta: int):
    """Compute flux surface preds and target from model output."""

    def hook(loss, input):

        rmnc_hat = input[0]["xmn"][..., 0]
        zmns_hat = input[0]["xmn"][..., 2]
        r_hat = ift((rmnc_hat, None), ntheta, nzeta)
        z_hat = ift((None, zmns_hat), ntheta, nzeta)

        rmnc = input[1]["xmn"][..., 0]
        zmns = input[1]["xmn"][..., 2]
        r = ift((rmnc, None), ntheta, nzeta)
        z = ift((None, zmns), ntheta, nzeta)

        target = torch.stack([r, z], dim=-1)
        preds = torch.stack([r_hat, z_hat], dim=-1)
        return preds, target

    return hook


def get_loss_lambda_hook(ntheta: int, nzeta: int):
    """Compute lambda preds and target from model output."""

    def hook(loss, input):

        lmns_hat = input[0]["xmn"][..., 1]
        l_hat = ift((None, lmns_hat), ntheta, nzeta)

        lmns = input[1]["xmn"][..., 1]
        l = ift((None, lmns), ntheta, nzeta)  # noqa: E741

        return l_hat, l

    return hook


###########
# Physics #
###########


mu0 = 4 * math.pi * 1e-7
signgs = -1


def compute_flux_surface_averaged_f(
    rmnc,
    lmns,
    zmns,
    iota,
    normalized_flux,
    phiedge,
    pressure,
    ntheta,
    nzeta,
    use_nn_gradients: Optional[bool] = True,
    use_proxy: Optional[bool] = False,
    mpol_nyq: Optional[int] = 16,
    ntor_nyq: Optional[int] = 18,
    full: Optional[bool] = False,
):
    """
    Compute flux surface averaged f and other quantities.

    TODO-0(@amerlo): in case of nn gradients, quantities are not expected to be from
    the same equilibrium, therefore, we cannot perform `to_half_mesh` or `to_full_mesh`
    operations. In addition, (R, Z, iota) are computed on full mesh, while
    lambda is computed on half-mesh, however, here, we combine them all together
    like if they were on the same mesh, which is wrong.
    TODO-1(@amerlo): add documentation.
    """

    #  Compute equilibrium properties
    phip = phiedge / math.pi / 2 / signgs
    ns = rmnc.shape[0]
    hs = 1 / (ns - 1)
    assert pressure.shape[1] == ns

    ######################
    #  Pressure gradient #
    ######################

    if not use_nn_gradients:
        #  If nn gradients are not used, quantities are expected to be the flux surfaces
        #  of a single equilibrium.
        pres = to_half_mesh(pressure[0] * mu0)
        presgrad = gradient(pres, order=1, factor=2.0).view(-1)
    else:
        #  Move pressure to_half_mesh(*)
        #  See vmecnn.physics.equilibrium.to_half_mesh
        pressure = pressure * mu0
        pres = torch.empty_like(pressure)
        pres[:, 0] = 0.0
        pres[:, 1] = 0.5 * (pressure[:, 0] + pressure[:, 1])
        for i in range(2, ns - 1):
            pres[:, i] = 2.0 * pressure[:, i - 1] - pres[:, i - 1]
        pres[:, -1] = (2.0 * pressure[:, -1] + pres[:, -2]) / 3.0
        #  Now compute gradient(*, order=1, factor=2.0)
        #  See vmecnn.physics.equilibrium.gradient
        presgrad = torch.empty_like(pressure)
        presgrad[:, 1:-1] = (pres[:, 2:] - pres[:, 1:-1]) / hs
        presgrad[:, 0] = 2.0 * presgrad[:, 1] - presgrad[:, 2]
        presgrad[:, -1] = 2.0 * presgrad[:, -2] - presgrad[:, -3]
        #  Extract correct values
        presgrad = gather(presgrad, dim=1, value=normalized_flux).view(-1)

    ############
    # Jacobian #
    ############

    rumns = derivative(rmnc, "cos", "u")
    zumnc = derivative(zmns, "sin", "u")

    r = ift((rmnc, None), ntheta, nzeta)

    ru = ift((None, rumns), ntheta, nzeta)
    zu = ift((zumnc, None), ntheta, nzeta)

    if use_nn_gradients:
        #  Use naive jacobian scheme since,
        #  if nn gradients are used,
        #  there is no notion of a mesh.
        #  The scheme use here is equivalent to equi.jacobian_:
        #
        #  sqrt(g) = R * (Ru * Zs - Rs * Zu)
        #
        #  Compute grad first, and then ift to speed up backward pass.
        #  In such case, the backward pass is now parallelized over (mpol, ntor)
        #  instead of (ntheta, nzeta) (the number of Fourier coefficients are
        #  generally fewer than the number of grid points).
        rsmnc = grad_3d(rmnc, normalized_flux, create_graph=True)
        zsmns = grad_3d(zmns, normalized_flux, create_graph=True)
        rs = ift((rsmnc, None), ntheta, nzeta)
        zs = ift((None, zsmns), ntheta, nzeta)
        jacobian = r * (ru * zs - rs * zu)
    else:
        shalf = get_s_half_mesh(ns, dtype=rmnc.dtype, device=rmnc.device)
        jacobian = compute_jacobian(
            rmnc,
            zmns,
            rumns,
            zumnc,
            shalf,
            ntheta,
            nzeta,
        )

    #########
    # bsupx #
    #########

    lvmnc = derivative(lmns, "sin", "v")
    lv = ift((lvmnc, None), ntheta, nzeta)

    lumnc = derivative(lmns, "sin", "u")
    lu = ift((lumnc, None), ntheta, nzeta)

    if not use_nn_gradients:
        iotas = to_half_mesh(iota)
    else:
        iotas = iota

    bsupu = phip.view(-1, 1, 1) * (iotas.view(-1, 1, 1) - lv) / jacobian
    bsupv = phip.view(-1, 1, 1) * (1 + lu) / jacobian

    if not use_nn_gradients:
        bsupu[0] = 0
        bsupv[0] = 0

    #########
    # bsubx #
    #########

    rvmns = derivative(rmnc, "cos", "v")
    rv = ift((None, rvmns), ntheta, nzeta)

    zvmnc = derivative(zmns, "sin", "v")
    zv = ift((zvmnc, None), ntheta, nzeta)

    guu = ru * ru + zu * zu
    guv = ru * rv + zu * zv
    gvv = rv * rv + r * r + zv * zv

    if not use_nn_gradients:
        guu = to_half_mesh(guu, mode="simple")
        guv = to_half_mesh(guv, mode="simple")
        gvv = to_half_mesh(gvv, mode="simple")

    bsubv = bsupu * guv + bsupv * gvv
    bsubu = bsupu * guu + bsupv * guv

    if not use_proxy:
        if not use_nn_gradients:

            rsmnc = gradient(rmnc, order=-1)
            rs = ift((rsmnc, None), ntheta, nzeta)

            zsmns = gradient(zmns, order=-1)
            zs = ift((None, zsmns), ntheta, nzeta)

        if not use_nn_gradients:
            gus = (
                to_half_mesh(ru, mode="simple") * rs
                + to_half_mesh(zu, mode="simple") * zs
            )
            gvs = (
                to_half_mesh(rv, mode="simple") * rs
                + to_half_mesh(zv, mode="simple") * zs
            )
        else:
            gus = ru * rs + zu * zs
            gvs = rv * rs + zv * zs

        bsubs = bsupu * gus + bsupv * gvs

    ##########
    # bsubsx #
    ##########

    if not use_proxy:

        bsubsmns = ft(bsubs, "sin", mpol_nyq, ntor_nyq)

        bsubsumnc = derivative(bsubsmns, "sin", "u")
        bsubsu = ift((bsubsumnc, None), ntheta, nzeta)

        bsubsvmnc = derivative(bsubsmns, "sin", "v")
        bsubsv = ift((bsubsvmnc, None), ntheta, nzeta)

    ##########
    # bsubxx #
    ##########

    if not use_proxy:

        bsubvmnc = ft(bsubv, "cos", mpol_nyq, ntor_nyq)
        bsubvumns = derivative(bsubvmnc, "sin", "u")
        bsubvu = ift((None, bsubvumns), ntheta, nzeta)

        bsubumnc = ft(bsubu, "cos", mpol_nyq, ntor_nyq)
        bsubuvmns = derivative(bsubumnc, "sin", "v")
        bsubuv = ift((None, bsubuvmns), ntheta, nzeta)

    #########
    # jcurx #
    #########

    if use_proxy:
        if use_nn_gradients:
            jcuru = -grad_1d(
                torch.mean(bsubv, dim=(1, 2)), normalized_flux, create_graph=True
            )
            jcurv = grad_1d(
                torch.mean(bsubu, dim=(1, 2)), normalized_flux, create_graph=True
            )
        else:
            #  Achtung: this makes sense only if the batch
            #  represents a single equilibrium,
            #  and if the flux surfaces are ordered.
            jcuru = -gradient(torch.mean(bsubv, dim=(1, 2)), order=1, factor=2.0).view(
                -1
            )
            jcurv = gradient(torch.mean(bsubu, dim=(1, 2)), order=1, factor=2.0).view(
                -1
            )

    #####
    # f #
    #####

    vp = torch.abs(torch.mean(jacobian, dim=(1, 2)))

    if use_proxy:

        vp_full_mesh = vp
        if not use_nn_gradients:
            vp_full_mesh = to_full_mesh(vp)

        f = signgs * (phip * iota * jcurv - phip * jcuru) + presgrad * vp_full_mesh

    ########
    # wmhd #
    ########

    if full:
        bsq = bsupu**2 * guu + 2 * bsupu * bsupv * guv + bsupv**2 * gvv

        wb = jacobian * bsq / 2
        wp = vp * pres

        wb = torch.mean(torch.abs(wb)[1:])
        wp = torch.mean(wp[1:])

        wmhd = (wb - wp) * (2 * math.pi) ** 2

    #####
    # F #
    #####

    if not use_proxy:

        bsubsu = to_full_mesh(bsubsu)
        bsubsv = to_full_mesh(bsubsv)

        bsupu = to_full_mesh(bsupu)
        bsupv = to_full_mesh(bsupv)

        bsubvu = to_full_mesh(bsubvu)
        bsubuv = to_full_mesh(bsubuv)

        gsqrt = to_full_mesh(jacobian).abs()

        if use_nn_gradients:
            bsubvs = grad_3d(bsubv, normalized_flux, create_graph=True)
            bsubus = grad_3d(bsubu, normalized_flux, create_graph=True)
        else:
            bsubvs = gradient(bsubv, order=1, factor=2.0)
            bsubus = gradient(bsubu, order=1, factor=2.0)

        fs = (
            bsupv * (bsubsv - bsubvs)
            + bsupu * (bsubsu - bsubus)
            - presgrad[:, None, None]
        )
        fbeta = (bsubvu - bsubuv) / gsqrt

        grad_rho = (
            (ru**2 + zu**2) * r**2 + (ru * zv - rv * zu) ** 2
        ) / gsqrt**2

        if not use_nn_gradients:
            rs = to_full_mesh(rs)
            zs = to_full_mesh(zs)

        grad_theta = (zs**2 + rs**2) * r**2 + (zv * rs - rv * zs) ** 2
        grad_zeta = (zs * ru - zu * rs) ** 2
        grad_theta_zeta = (zv * rs - rv * zs) * (zs * ru - zu * rs)
        grad_beta = (
            grad_theta * bsupv**2
            + grad_zeta * bsupu**2
            - 2 * grad_theta_zeta * bsupv * bsupu
        )

        f = (torch.sqrt(fs**2 * grad_rho + fbeta**2 * grad_beta) * gsqrt).mean(
            dim=(1, 2)
        )

    if not use_nn_gradients:
        f[0] = 2.0 * f[1] - f[2]
        f[-1] = 2.0 * f[-2] - f[-3]

    ###########
    # Outputs #
    ###########

    #  Output additional quantities for debugging purposes
    res = {
        "f": f,
        "jacobian": jacobian,
    }

    if not use_nn_gradients:
        #  Compute phiedge and itor from the predicted equilibrium
        res["phiedge"] = torch.mean((bsupv * torch.abs(jacobian))[1:]) * 2 * math.pi
        res["itor"] = 2 * math.pi * signgs * bsubu.mean(dim=(1, 2))

    if use_nn_gradients:
        res["rsmnc"] = rsmnc
        res["zsmns"] = zsmns

    if full:
        res["wmhd"] = wmhd

    return res


##########
# Losses #
##########


class _MultiObjectiveLoss(_Loss):

    __constants__ = ["beta", "update_every_n_steps", "step_mode"]

    #  Loss terms
    _keys = None

    def __init__(
        self,
        alpha: Dict[str, float],
        beta: Optional[float] = 0.9,
        step_mode: Optional[str] = "max",
        update_every_n_steps: Optional[int] = 0,
        **kwargs,
    ) -> None:
        super(_MultiObjectiveLoss, self).__init__(**kwargs)

        assert self._keys is not None
        assert set(self._keys) == set(alpha.keys())
        assert sum(alpha.values()) == 1.0

        self.alpha = alpha
        self.beta = beta
        self.update_every_n_steps = update_every_n_steps

        if step_mode not in ("max", "norm", "mean"):
            raise ValueError("Step mode %s not supported" % step_mode)
        self.step_mode = step_mode

        self._loss = {}
        self._moments = torch.zeros(len(alpha.keys()))

        self._step = 0

    def step(self, parameters, step: int) -> None:
        """Update loss alpha coefficients."""
        if step == 0:
            return
        if self.update_every_n_steps == 0:
            return
        if step % self.update_every_n_steps != 0:
            return
        alpha = torch.as_tensor([a for a in self.alpha.values()])
        for i, loss in enumerate(self._loss.values()):
            loss.backward(retain_graph=True)
            grads = []
            num = 0
            for p in parameters():
                if p.grad is None:
                    continue
                if self.step_mode == "max":
                    grads.append(torch.abs(p.grad).max())
                elif self.step_mode == "norm":
                    grads.append(torch.linalg.norm(p.grad))
                    num += 1
                else:
                    grads.append(torch.abs(p.grad).sum())
                    num += p.numel()
            if self.step_mode == "max":
                moment = max(grads)
            else:
                moment = sum(grads) / num
            self._moments[i] = self.beta * self._moments[i] + (1 - self.beta) * moment
            #  Compute new alpha
            alpha[i] = 1 / self._moments[i]

        #  Normalize alphas
        alpha /= alpha.sum()

        #  Update alphas
        for i, k in enumerate(self.alpha.keys()):
            self.alpha[k] = alpha[i]


class RealMSELoss(_MultiObjectiveLoss):
    r"""Creates a criterion that measures the mean squared error (squared L2 norm) between
    the inverse Fourier transform of the input :math:`x` and target :math:`y`.

    The mean operation still operates over all the real space elements.
    The reduction can be avoided if one sets ``reduction = 'sum'``.

    See torch.nn.MSELoss.

    Examples:
        >>> from vmecnn.metrics import RealMSELoss
        >>> loss = RealMSELoss(ntheta=40, nzeta=36)
        >>> input, target = {}, {}
        >>> input["xmn"] = torch.randn(3, 2, 3, 3, requires_grad=True)
        >>> target["xmn"] = torch.randn(3, 2, 3, 3)
        >>> input["iota"] = torch.randn(3, requires_grad=True)
        >>> target["iota"] = torch.randn(3)
        >>> output = loss(input, target)
        >>> output.backward()

    """

    _keys = ("iota", "r", "l", "z")

    def __init__(
        self,
        ntheta: int,
        nzeta: int,
        mean: Optional[Dict[str, List[float]]] = {},
        std: Optional[Dict[str, List[float]]] = {},
        **kwargs,
    ) -> None:
        super(RealMSELoss, self).__init__(**kwargs)
        self.ntheta = ntheta
        self.nzeta = nzeta

        if "iota" in mean.keys() or "xmn" in mean.keys():
            self.register_forward_pre_hook(
                get_transforms_hook(BackNormalize(mean=mean, std=std))
            )

    def forward(self, input: Dict[str, Tensor], target: Dict[str, Tensor]) -> Tensor:

        rmnc_hat = input["xmn"][..., 0]
        lmns_hat = input["xmn"][..., 1]
        zmns_hat = input["xmn"][..., 2]
        r_hat = ift((rmnc_hat, None), self.ntheta, self.nzeta)
        l_hat = ift((None, lmns_hat), self.ntheta, self.nzeta)
        z_hat = ift((None, zmns_hat), self.ntheta, self.nzeta)

        rmnc = target["xmn"][..., 0]
        lmns = target["xmn"][..., 1]
        zmns = target["xmn"][..., 2]
        r = ift((rmnc, None), self.ntheta, self.nzeta)
        l = ift((None, lmns), self.ntheta, self.nzeta)  # noqa: E741
        z = ift((None, zmns), self.ntheta, self.nzeta)

        self._loss["iota"] = F.mse_loss(input["iota"], target["iota"])
        self._loss["r"] = F.mse_loss(r_hat, r)
        self._loss["l"] = F.mse_loss(l_hat, l)
        self._loss["z"] = F.mse_loss(z_hat, z)

        #  Aggregate loss
        loss = 0
        for k in self._keys:
            loss += self.alpha[k] * self._loss[k]

        #  Advance step for the multi-objective scaling
        self._step += 1

        return loss


class IdealMHDLoss(_MultiObjectiveLoss):

    _keys = ("iota", "r", "l", "z", "shear", "rs", "ls", "zs", "f", "boundary")

    def __init__(
        self,
        ntheta: int,
        nzeta: int,
        use_nn_gradients: Optional[bool] = True,
        loss_threshold: Optional[float] = 1,
        use_proxy: Optional[float] = True,
        fixed_boundary: Optional[float] = False,
        lock: Optional[bool] = None,
        mean: Optional[Dict[str, List[float]]] = {},
        std: Optional[Dict[str, List[float]]] = {},
        **kwargs,
    ) -> None:
        super(IdealMHDLoss, self).__init__(**kwargs)

        if lock is not None:
            warn("lock is deprecated", DeprecationWarning, stacklevel=2)

        assert (
            self.update_every_n_steps == 0
        ), f"{self.__class__.__name__} does not support loss scaling yet"

        if use_nn_gradients:
            for k in ("shear", "boundary"):
                assert (
                    self.alpha[k] == 0
                ), f"{k} loss can be computed only if finite difference gradients are used"
            assert (
                not fixed_boundary
            ), "fixed boundary option can be used only if finite differences gradients are used"

        self.ntheta = ntheta
        self.nzeta = nzeta

        self.use_nn_gradients = use_nn_gradients
        self.loss_threshold = loss_threshold
        self.use_proxy = use_proxy
        self.fixed_boundary = fixed_boundary

        #  Add normalization hook if needed
        #  - normalizing the model output
        #  - normalizing quantities to compute the physics loss
        if (
            "iota" in mean.keys()
            or "xmn" in mean.keys()
            or self.alpha["f"] != 0
            or self.alpha["boundary"] != 0
        ):
            self.register_forward_pre_hook(
                get_transforms_hook(BackNormalize(mean=mean, std=std))
            )

    def forward(self, input: Dict[str, Tensor], target: Dict[str, Tensor]) -> Tensor:

        rmnc_hat = input["xmn"][..., 0]
        lmns_hat = input["xmn"][..., 1]
        zmns_hat = input["xmn"][..., 2]
        r_hat = ift((rmnc_hat, None), self.ntheta, self.nzeta)
        l_hat = ift((None, lmns_hat), self.ntheta, self.nzeta)
        z_hat = ift((None, zmns_hat), self.ntheta, self.nzeta)

        if self._step == 0 and self.fixed_boundary:
            #  Save flux surface boundary for fine-tuning
            self._rb = r_hat[-1].detach()
            self._zb = z_hat[-1].detach()

        rmnc = target["xmn"][..., 0]
        lmns = target["xmn"][..., 1]
        zmns = target["xmn"][..., 2]
        r = ift((rmnc, None), self.ntheta, self.nzeta)
        l = ift((None, lmns), self.ntheta, self.nzeta)  # noqa: E741
        z = ift((None, zmns), self.ntheta, self.nzeta)

        if not self.fixed_boundary:
            self._loss["iota"] = F.mse_loss(input["iota"], target["iota"])
            self._loss["r"] = F.mse_loss(r_hat, r)
            self._loss["l"] = F.mse_loss(l_hat, l)
            self._loss["z"] = F.mse_loss(z_hat, z)
        else:
            self._loss["r"] = F.mse_loss(r_hat[-1], self._rb)
            self._loss["z"] = F.mse_loss(z_hat[-1], self._zb)

        #  Aggregate loss
        loss = 0
        for k in ("iota", "r", "l", "z"):
            if self.alpha[k] == 0:
                continue
            loss += self.alpha[k] * self._loss[k]

        #  Compute gradient loss for xmns
        for k, idx in zip(("rs", "ls", "zs"), (0, 1, 2)):
            if self.alpha[k] == 0:
                continue
            grad = target["xsmn"][..., idx]
            if self.use_nn_gradients:
                grad_hat = grad_3d(
                    input["xmn"][..., idx], input["normalized_flux"], create_graph=True
                )
            else:
                grad_hat = gradient(input["xmn"][..., idx], order=-2)
            if k == "rs":
                ift_args = (grad, None)
                ift_hat_args = (grad_hat, None)
            else:
                ift_args = (None, grad)
                ift_hat_args = (None, grad_hat)
            grad = ift(ift_args, self.ntheta, self.nzeta)
            grad_hat = ift(ift_hat_args, self.ntheta, self.nzeta)
            if self.use_nn_gradients:
                #  Do not consider flux surfaces too close to the axis
                idx = input["normalized_flux"].view(-1) >= MEAN_F_START / (NS - 1)
                grad = grad[idx]
                grad_hat = grad_hat[idx]
            self._loss[k] = F.mse_loss(grad_hat, grad)

        #  Add shear
        if self.alpha["shear"] != 0 and not self.use_nn_gradients:
            shear = gradient(target["iota"], order=-2)
            shear_hat = gradient(input["iota"], order=-2)
            self._loss["shear"] = F.mse_loss(shear_hat, shear)

        #  Aggregate gradient loss
        for k in ("rs", "ls", "zs", "shear"):
            if self.alpha[k] == 0:
                continue
            loss += self.alpha[k] * self._loss[k]

        #  Do not include physics loss if losses on R or Z are too high
        #  R and Z are the major quantities affecting the ideal mhd loss
        #  This should stabilize the training.
        if "f" in self._loss:
            del self._loss["f"]
        if self.alpha["f"] != 0 and (
            0.5 * (self._loss["r"] + self._loss["z"]) <= self.loss_threshold
        ):

            normalized_flux = input["normalized_flux"]
            phiedge = input["phiedge"].view(-1)

            pressure = input["pressure"] * input["p0"]

            iota = input["iota"].view(-1)

            rmnc = input["xmn"][..., 0]
            lmns = input["xmn"][..., 1]
            zmns = input["xmn"][..., 2]

            equi = compute_flux_surface_averaged_f(
                rmnc=rmnc,
                lmns=lmns,
                zmns=zmns,
                iota=iota,
                normalized_flux=normalized_flux,
                phiedge=phiedge,
                pressure=pressure,
                ntheta=self.ntheta,
                nzeta=self.nzeta,
                use_nn_gradients=self.use_nn_gradients,
                use_proxy=self.use_proxy,
            )

            if self.use_nn_gradients:
                idx = normalized_flux.view(-1) >= MEAN_F_START / (NS - 1)
                f_loss = (equi["f"][idx] ** 2).mean()
            else:
                f_loss = (equi["f"][MEAN_F_START:] ** 2).mean()

            self._loss["f"] = f_loss
            loss += self.alpha["f"] * self._loss["f"]

            #  Add loss for mu0 * itor
            if not self.use_nn_gradients and self.alpha["boundary"] != 0:
                boundary_loss = F.mse_loss(
                    equi["itor"][MEAN_F_START:],
                    mu0
                    * input["curtor"][0]
                    * input["toroidal_current"][0][MEAN_F_START:],
                )

                self._loss["boundary"] = boundary_loss
                loss += self.alpha["boundary"] * self._loss["boundary"]

        #  Advance step for the multi-objective scaling
        self._step += 1

        return loss


###########
# Metrics #
###########


class NormalizedReducedMeanSquaredError(Metric):
    r"""
    Computes the normalized reduced mean squared error between
    each element in the input :math:`x` and target :math:`y`.

    Args:
        num_outputs:
            Number of outputs in multioutput setting (default is 1).
        ignore_nan:
            Ignore nan values across multiple output scores.
        multioutput:
            Defines aggregation in the case of multiple output scores. Can be one
            of the following strings (default is ``'uniform_average'``.):
            * ``'raw_values'`` returns full set of scores
            * ``'uniform_average'`` scores are uniformly averaged

    Examples:
        >>> from vmecnn.metrics import NormalizedReducedMeanSquaredError
        >>> target = torch.tensor([3, -0.5, 2, 7])
        >>> preds = torch.tensor([2.5, 0.0, 2, 8])
        >>> nrmse = NormalizedReducedMeanSquaredError()
        >>> nrmse(preds, target)
        tensor(0.2267)

        >>> input = torch.randn(3, 5, requires_grad=True)
        >>> target = torch.randn(3, 5)
        >>> nrmse = NormalizedReducedMeanSquaredError(num_outputs=5)
        >>> output = nrmse(input, target)
        >>> output.backward()

        >>> input = torch.randn(3, 5, requires_grad=True)
        >>> target = torch.randn(3, 5)
        >>> nrmse = NormalizedReducedMeanSquaredError(num_outputs=5, multioutput='raw_values')
        >>> output = nrmse(input, target)
        >>> output.size(0)
        5

    TODO-2(@amerlo): add option to use IQR as scale factor.
    """

    is_differentiable = True
    higher_is_better = False
    full_state_update = False
    residual: Tensor
    sub_obs: Tensor
    sum_squared_obs: Tensor
    total: Tensor

    def __init__(
        self,
        num_outputs: int = 1,
        ignore_nan: bool = False,
        keys: List[str] = None,
        multioutput: str = "uniform_average",
        compute_on_step: bool = True,
        dist_sync_on_step: bool = False,
        process_group: Optional[Any] = None,
        dist_sync_fn: Callable = None,
    ) -> None:
        super().__init__(
            compute_on_step=compute_on_step,
            dist_sync_on_step=dist_sync_on_step,
            process_group=process_group,
            dist_sync_fn=dist_sync_fn,
        )
        self.num_outputs = num_outputs
        self.ignore_nan = ignore_nan
        self.keys = keys

        allowed_multioutput = ("raw_values", "uniform_average")
        if multioutput not in allowed_multioutput:
            raise ValueError(
                "Invalid input to argument `multioutput`. "
                "Choose one of the following: %s" % allowed_multioutput
            )
        self.multioutput = multioutput

        self._update_hooks = []
        if keys is not None:
            self._update_hooks.append(get_loss_named_hook(keys))

        self.add_state(
            "residual", default=torch.zeros(self.num_outputs), dist_reduce_fx="sum"
        )
        self.add_state(
            "sum_squared_obs",
            default=torch.zeros(self.num_outputs),
            dist_reduce_fx="sum",
        )
        self.add_state(
            "sum_obs", default=torch.zeros(self.num_outputs), dist_reduce_fx="sum"
        )
        self.add_state("total", default=tensor(0), dist_reduce_fx="sum")

    def update(
        self,
        preds: Union[Dict[str, Tensor], Tensor],
        target: Union[Dict[str, Tensor], Tensor],
    ) -> None:
        """Update state with predictions and targets.
        Args:
            preds: Predictions from model
            target: Ground truth values
        """
        for hook in self._update_hooks:
            preds, target = hook(self, (preds, target))
        self.sum_obs += torch.sum(target, dim=0)
        self.sum_squared_obs += torch.sum(target * target, dim=0)
        residual = target - preds
        self.residual += torch.sum(residual * residual, dim=0)
        self.total += target.size(0)

    def compute(self) -> Tensor:
        """Computes nrmse over the metric states."""
        if self.total < 2:
            raise ValueError("Needs at least two samples to calculate nrmse metric.")

        mean_obs = self.sum_obs / self.total
        tss = self.sum_squared_obs - self.sum_obs * mean_obs
        raw_loss = torch.sqrt(self.residual / tss)

        if self.multioutput == "raw_values":
            nrmse = raw_loss
        elif self.multioutput == "uniform_average":
            if self.ignore_nan:
                #  see: https://github.com/pytorch/pytorch/pull/62671
                nrmse = torch.mean(raw_loss[~raw_loss.isnan()])
            else:
                nrmse = torch.mean(raw_loss)
        else:
            raise ValueError("Invalid input to argument `multioutput`.")

        return nrmse

    def __repr__(self) -> str:
        return (
            self.__class__.__name__
            + f"(num_outputs={self.num_outputs}, ignore_nan={self.ignore_nan}, "
            + f"keys={self.keys}, multioutput={self.multioutput})"
        )


class MeanAbsolutePercentageError(_MeanAbsolutePercentageError):
    """See torchmetrics.MeanAbsolutePercentageError."""

    def __init__(
        self,
        *args,
        keys: Optional[List[str]] = None,
        **kwargs,
    ) -> None:
        super().__init__(*args, **kwargs)

        self._update_hooks = []
        if keys is not None:
            self._update_hooks.append(get_loss_named_hook(keys))

    def update(
        self,
        preds: Union[Dict[str, Tensor], Tensor],
        target: Union[Dict[str, Tensor], Tensor],
    ) -> None:
        """Update state with predictions and targets.
        Args:
            preds: Predictions from model
            target: Ground truth values
        """
        for hook in self._update_hooks:
            preds, target = hook(self, (preds, target))
        super().update(preds, target)


class MeanSquaredError(_MeanSquaredError):
    """See torchmetrics.MeanSquaredError."""

    def __init__(
        self,
        *args,
        keys: Optional[List[str]] = None,
        **kwargs,
    ) -> None:
        super().__init__(*args, **kwargs)

        self._update_hooks = []
        if keys is not None:
            self._update_hooks.append(get_loss_named_hook(keys))

    def update(
        self,
        preds: Union[Dict[str, Tensor], Tensor],
        target: Union[Dict[str, Tensor], Tensor],
    ) -> None:
        """Update state with predictions and targets.
        Args:
            preds: Predictions from model
            target: Ground truth values
        """
        for hook in self._update_hooks:
            preds, target = hook(self, (preds, target))
        super().update(preds, target)


class MeanL2RelativeError(_MeanAbsolutePercentageError):
    r"""
    Relative L2 error.

    TODO-1(@amerlo): add documentation.

    Examples:
        >>> from vmecnn.metrics import MeanL2RelativeError
        >>> target = torch.tensor([1, 10, 1e6]).view(-1, 1)
        >>> preds = torch.tensor([0.9, 15, 1.2e6]).view(-1, 1)
        >>> metric = MeanL2RelativeError()
        >>> metric(preds, target)
        tensor(0.2667)
    """

    def __init__(
        self,
        *args,
        keys: Optional[List[str]] = None,
        **kwargs,
    ) -> None:
        super().__init__(*args, **kwargs)

        self._update_hooks = []
        if keys is not None:
            self._update_hooks.append(get_loss_named_hook(keys))

    def update(
        self,
        preds: Union[Dict[str, Tensor], Tensor],
        target: Union[Dict[str, Tensor], Tensor],
    ) -> None:
        """Update state with predictions and targets.
        Args:
            preds: Predictions from model
            target: Ground truth values
        """
        for hook in self._update_hooks:
            preds, target = hook(self, (preds, target))

        if preds.shape != target.shape:
            raise RuntimeError(
                "Predictions and targets are expected to have the same shape"
            )

        epsilon = 1.17e-6
        abs_diff = torch.sum((preds - target) ** 2, dim=1)
        abs_per_error = abs_diff / torch.clamp(
            torch.sum(target**2, dim=1), min=epsilon
        )

        self.sum_abs_per_error += torch.sum(torch.sqrt(abs_per_error))
        self.total += target.size(0)


class MeanL2Error(_MeanSquaredError):
    """
    Compute the mean L2 error,
    where the L2 error is computed across the last dimension.
    """

    def __init__(
        self,
        *args,
        keys: Optional[List[str]] = None,
        **kwargs,
    ) -> None:
        super().__init__(*args, squared=True, **kwargs)

        self._update_hooks = []
        if keys is not None:
            self._update_hooks.append(get_loss_named_hook(keys))

    def update(
        self,
        preds: Union[Dict[str, Tensor], Tensor],
        target: Union[Dict[str, Tensor], Tensor],
    ) -> None:
        """Update state with predictions and targets.
        Args:
            preds: Predictions from model
            target: Ground truth values
        """
        for hook in self._update_hooks:
            preds, target = hook(self, (preds, target))

        if preds.shape != target.shape:
            raise RuntimeError(
                "Predictions and targets are expected to have the same shape"
            )

        l2_error = torch.sqrt(((preds - target) ** 2).mean(dim=-1))

        self.sum_squared_error += torch.sum(l2_error)
        self.total += l2_error.numel()


class FluxSurfaceRMSE(MeanSquaredError):
    r"""
    Computes the rmse between all R and Z values on the the flux surfaces.

    TODO-1(@amerlo): add documentation.
    """

    def __init__(
        self,
        ntheta: int,
        nzeta: int,
        **kwargs,
    ) -> None:
        super().__init__(squared=False, **kwargs)
        self.ntheta = ntheta
        self.nzeta = nzeta

        self._update_hooks.insert(0, get_loss_flux_surface_hook(ntheta, nzeta))

    def __repr__(self) -> str:
        return self.__class__.__name__ + f"(ntheta={self.ntheta}, nzeta={self.nzeta})"


class FluxSurfaceRelativeError(MeanL2RelativeError):
    """
    Relative L2 flux surface error.

    TODO-1(@amerlo): add documentation.
    """

    def __init__(
        self,
        ntheta: int,
        nzeta: int,
        **kwargs,
    ) -> None:
        super().__init__()
        self.ntheta = ntheta
        self.nzeta = nzeta

        self._update_hooks.insert(0, get_loss_flux_surface_hook(ntheta, nzeta))

    def __repr__(self) -> str:
        return self.__class__.__name__ + f"(ntheta={self.ntheta}, nzeta={self.nzeta})"


class FluxSurfaceError(MeanL2Error):
    r"""
    Computes the L2 norm between the flux surfaces represented by
    the input :math:`x` and target :math:`y`.

    This is equivalent to Ex in DESC.
    See eq. (33a) in Dudt 2020.
    """

    def __init__(
        self,
        ntheta: int,
        nzeta: int,
        **kwargs,
    ) -> None:
        super().__init__(**kwargs)
        self.ntheta = ntheta
        self.nzeta = nzeta

        self._update_hooks.insert(0, get_loss_flux_surface_hook(ntheta, nzeta))

    def __repr__(self) -> str:
        return self.__class__.__name__ + f"(ntheta={self.ntheta}, nzeta={self.nzeta})"


class IotaRelativeError(_MeanAbsolutePercentageError):
    """Relative iota error."""

    def update(self, preds: Dict[str, Tensor], target: Dict[str, Tensor]) -> None:
        """Update state with predictions and targets.
        Args:
            preds: Predictions from model
            target: Ground truth values
        """
        super().update(preds["iota"], target["iota"])


class LambdaError(MeanSquaredError):
    """Lambda RMSE error."""

    def __init__(
        self,
        ntheta: int,
        nzeta: int,
        **kwargs,
    ) -> None:
        super().__init__(squared=False, **kwargs)
        self.ntheta = ntheta
        self.nzeta = nzeta

        self._update_hooks.insert(0, get_loss_lambda_hook(ntheta, nzeta))

    def __repr__(self) -> str:
        return self.__class__.__name__ + f"(ntheta={self.ntheta}, nzeta={self.nzeta})"
