"""
DataModule for the VmecEquilibria dataset.

TODO-1(@amerlo): add documentation.
"""

import hashlib
import os
import random
from typing import Any, Callable, Dict, Optional, Tuple, List, Union, Iterator

import matplotlib.pyplot as plt
import numpy as np
import torch
from pytorch_lightning import LightningDataModule
from torch.utils.data import Dataset, DataLoader, Subset, random_split, Sampler

from w7x_datagen.datasets import CacheDataset, VmecEquilibria

from vmecnn.datamodules.utils import NS, MPOL, NTOR, get_xmn_mask, get_feature_as_string
from vmecnn.transforms.transforms import (
    BuildItem,
    Compose,
    ZeroKeys,
    Normalize,
    BackNormalize,
    Crop,
    Pad,
    ToTuple,
    ZeroAxis,
)
from vmecnn.utils import stack_keys, load


def train_test_split(
    data,
    test_size: float,
    shuffle: bool,
    train_size: Optional[float] = -1,
    random_state: Optional[int] = None,
) -> Tuple[List[int]]:
    """
    Split indexable data (e.g., data set indices) into random train and test sub sets.

    This function assumes that data are indices from a `VmecEquilibria`
    dataset. The split is performed across runs and not indices.

    Args:
        data (iterable): iterable data from which generate splitted indices
        test_size (float): share of test indices
        shuffle (bool): whether to shuffle both the train and test indices
        train_split (float, optional): share of train indices to keep. If -1,
            keep all train indices (default: -1)
        random_state (int, optional): seed for the split (default: None)
    """

    #  Split dataset across runs
    num_runs = int(len(data) / NS)
    num_test_runs = int(num_runs * test_size)
    train_runs, test_runs = random_split(
        range(num_runs),
        (num_runs - num_test_runs, num_test_runs),
        generator=torch.Generator().manual_seed(random_state),
    )

    if train_size != -1:
        num_train_runs = int(len(train_runs) * train_size)
        indices = list(range(num_train_runs))
        train_runs = Subset(train_runs, indices)

    #  Build train and test indices
    train_indices = []
    for idx in train_runs:
        train_indices.extend([idx * NS + i for i in range(NS)])
    test_indices = []
    for idx in test_runs:
        test_indices.extend([idx * NS + i for i in range(NS)])

    #  Shuffle indices to mix runs in dataset
    if shuffle:
        random.shuffle(train_indices)
        random.shuffle(test_indices)

    assert set(train_indices).isdisjoint(test_indices)
    return train_indices, test_indices


def get_dataset(
    root: str,
    fraction: Optional[float] = 1.0,
    transforms: Optional[Callable] = None,
    cached: Optional[bool] = False,
):
    dataset = VmecEquilibria(
        root=root,
        train=True,
        transforms=transforms,
        train_test_split=fraction,
        expand_along_flux=True,
    )
    if cached:
        return CacheDataset(dataset=dataset)
    return dataset


class EquilibriaSampler(Sampler):
    """Batch sampler which provides a single equilibrium per batch."""

    def __init__(self, data_source, shuffle: bool) -> None:
        assert len(data_source) % NS == 0
        self.data_source = data_source
        self.shuffle = shuffle

    def __iter__(self) -> Iterator[int]:

        if self.shuffle:
            seed = int(torch.empty((), dtype=torch.int64).random_().item())
            generator = torch.Generator()
            generator.manual_seed(seed)
            batches = torch.randperm(
                len(self.data_source) // NS, generator=generator
            ).tolist()
        else:
            batches = list(range(len(self.data_source) // NS))

        indices = [list(range(i * NS, (i + 1) * NS)) for i in batches]
        yield from indices

    def __len__(self) -> int:
        return len(self.data_source) // NS


# pylint: disable=too-many-instance-attributes,too-many-arguments
class VmecEquilibriaDataModule(LightningDataModule):
    """
    VmecEquilibria DataModule.

    TODO-1(@amerlo): documentation.
    """

    def __init__(
        self,
        root: Union[str, List[str]],
        features: List[str],
        labels: List[str],
        metadata: Optional[List[str]] = [],
        ns: Optional[float] = 99,
        mpol: Optional[int] = 11,
        ntor: Optional[int] = 12,
        crop: Optional[bool] = True,
        fraction: Optional[float] = 1.0,
        tune_fraction: Optional[float] = 1.0,
        train_split: Optional[float] = -1,
        val_split: Optional[Union[float, List[int]]] = 0.1,
        test_split: Optional[Union[float, List[int]]] = 0.2,
        batch_size: Optional[int] = 32,
        shuffle_batch: Optional[bool] = True,
        normalization: Optional[Dict[str, Tuple[str]]] = None,
        predict_strategy: Optional[str] = "test",
        predict_root: Optional[Union[str, List[str]]] = None,
        zeros_input: Optional[bool] = False,
        to_double: Optional[bool] = False,
        seed: Optional[int] = 42,
        pin_memory: Optional[bool] = False,
        shuffle: Optional[bool] = True,
        params_file: Optional[str] = None,
        strict: Optional[bool] = True,
        *args: Any,
        **kwargs: Any,
    ) -> None:

        super().__init__(*args, **kwargs)

        self.root = root
        self.features = features
        self.labels = labels
        self.metadata = metadata
        self.ns = ns
        self.mpol = mpol
        self.ntor = ntor
        self.crop = crop

        self.fraction = fraction
        self.tune_fraction = tune_fraction
        self.train_split = train_split
        self.val_split = val_split
        self.test_split = test_split

        self.predict_strategy = predict_strategy
        self.predict_root = predict_root

        self.batch_size = batch_size
        self.shuffle_batch = shuffle_batch
        self.normalization = normalization
        self.zeros_input = zeros_input
        self.to_double = to_double

        self.seed = seed
        self.pin_memory = pin_memory
        self.shuffle = shuffle

        self.train_data = None
        self.val_data = None
        self.test_data = None
        self.predict_data = None

        self.params_file = params_file
        self.strict = strict
        self._params = None

        dataset = get_dataset(root=self.root, fraction=fraction)
        indices = list(range(len(dataset)))

        if isinstance(val_split, list) and isinstance(test_split, list):
            #  Indices are given directly in the __init__()
            self._train_indices = list(set(indices) - set(val_split) - set(test_split))
            self._val_indices = val_split
            self._test_indices = test_split
        else:
            #  Split dataset according to val and test split
            train_val_indices, self._test_indices = train_test_split(
                indices,
                test_split,
                shuffle=False,
                random_state=seed,
            )
            train_indices, val_indices = train_test_split(
                train_val_indices,
                val_split,
                train_size=train_split,
                shuffle=True if self.shuffle_batch is True else False,
                random_state=seed,
            )
            self._train_indices = [train_val_indices[i] for i in train_indices]
            self._val_indices = [train_val_indices[i] for i in val_indices]

        assert set(self._train_indices).isdisjoint(self._val_indices)
        assert set(self._train_indices).isdisjoint(self._test_indices)
        assert set(self._test_indices).isdisjoint(self._val_indices)

    def __repr__(self) -> str:
        return (
            self.__class__.__name__
            + f"(root={self.root}, features={self.features}, labels={self.labels}, "
            + f"metadata={self.metadata}, "
            + f"ns={self.ns}, mpol={self.mpol}, ntor={self.ntor}, crop={self.crop}, "
            + f"fraction={self.fraction}, tune_fraction={self.tune_fraction}, "
            + f"val_split={self.val_split}, test_split={self.test_split}, "
            + f"predict_strategy={self.predict_strategy}, predict_root={self.predict_root}, "
            + f"batch_size={self.batch_size},  shuffle_batch={self.shuffle_batch}, "
            + f"normalization={self.normalization}, "
            + f"seed={self.seed}, to_double={self.to_double}, "
            + f"zeros_input={self.zeros_input}, pin_memory={self.pin_memory}, "
            + f"shuffle={self.shuffle}, params_file={self.params_file}, "
            + f"strict={self.strict})"
        )

    def setup(self, stage: Optional[str] = None) -> None:

        #  Build train, val and test data set
        transforms = Compose(self.get_transforms(crop=self.crop))
        dataset = get_dataset(
            root=self.root,
            fraction=self.fraction,
            transforms=transforms,
            cached=True,
        )
        self.train_data = Subset(dataset, self._train_indices)
        self.val_data = Subset(dataset, self._val_indices)
        self.test_data = Subset(dataset, self._test_indices)

        #  Build predict data set
        if self.predict_strategy not in ("test", "train", "root"):
            raise ValueError(
                "%s predict strategy is not supported" % self.predict_strategy
            )

        transforms = Compose(self.get_transforms(normalize_labels=False, crop=False))
        dataset = get_dataset(
            root=self.root, fraction=self.fraction, transforms=transforms
        )
        if self.predict_strategy == "test":
            self.predict_data = Subset(dataset, self._test_indices)
        elif self.predict_strategy == "train":
            self.predict_data = Subset(dataset, self._train_indices)
        elif self.predict_strategy == "root":
            assert self.predict_root is not None
            self.predict_data = get_dataset(
                root=self.predict_root, transforms=transforms
            )

    def get_transforms(
        self,
        to_tensor: Optional[bool] = True,
        crop: Optional[bool] = True,
        normalize_features: Optional[bool] = True,
        normalize_labels: Optional[bool] = True,
        back_normalize: Optional[bool] = False,
        pad: Optional[bool] = False,
        to_tuple: Optional[bool] = True,
    ) -> List[Callable]:
        """
        Build dataset transforms.

        Args:
            to_tensor: Whether to build tensors out of the dataset item.
            back_normalize: Whether to use BackNormalize transforms instead of Normalize ones.
            crop: Whether to crop the output FCs.
            pad: Whether to pad the output FCs.
            normalize_features: Whether to normalize the input features.
            normalize_labels: Whether to normalize the output labels.
            to_tuple: Whether to pack the item into a (x, y) tuple
        """

        transforms = []

        if to_tensor:
            transforms.append(
                BuildItem(
                    self.features,
                    self.labels,
                    self.ns,
                    self.to_double,
                    metadata=self.metadata,
                )
            )

        if crop and "xmn" in self.labels and (self.mpol != MPOL or self.ntor != NTOR):
            transforms.append(Crop(self.mpol, self.ntor))

        if self.normalization is not None and (normalize_features or normalize_labels):
            if normalize_features and normalize_labels:
                keys = self.features + self.labels
            elif normalize_features:
                keys = self.features
            elif normalize_labels:
                keys = self.labels
            normalization = {k: v for k, v in self.normalization.items() if k in keys}
            mean, std = self.get_params(normalization)
            if back_normalize:
                transforms.append(BackNormalize(mean=mean, std=std))
            else:
                transforms.append(Normalize(mean=mean, std=std))

            if "xmn" in normalization and normalization["xmn"] is not None:
                transforms.append(ZeroAxis())

        if pad and "xmn" in self.labels and (self.mpol != MPOL or self.ntor != NTOR):
            transforms.append(Pad(self.mpol, self.ntor))

        if self.zeros_input:
            transforms.append(ZeroKeys(keys=self.features))

        if to_tuple:
            transforms.append(ToTuple(self.features, self.labels, self.metadata))

        return transforms

    def train_dataloader(self):
        if isinstance(self.shuffle_batch, str):
            assert self.shuffle_batch == "equilibria"
            return self._get_dataloader(
                self.train_data,
                batch_sampler=EquilibriaSampler(self.train_data, shuffle=self.shuffle),
                shuffle=None,
                drop_last=None,
            )
        return self._get_dataloader(
            self.train_data, batch_size=self.batch_size, shuffle=self.shuffle
        )

    def val_dataloader(self):
        return self._get_dataloader(
            self.val_data, batch_size=self.batch_size, shuffle=False
        )

    def test_dataloader(self):
        return self._get_dataloader(
            self.test_data, batch_size=self.batch_size, shuffle=False
        )

    def predict_dataloader(self):
        return self._get_dataloader(self.predict_data, batch_size=NS, shuffle=False)

    def check(self):
        """Check disjoint train, val and test splits."""
        sets = {}
        for stage, data in zip(
            ("train", "val", "test"), (self.train_data, self.val_data, self.test_data)
        ):
            sets[stage] = set(
                [
                    tuple(stack_keys(data[i][0], self.features)[0].tolist())
                    for i in range(len(data))
                ]
            )
        assert sets["train"].isdisjoint(sets["val"])
        assert sets["train"].isdisjoint(sets["test"])
        assert sets["test"].isdisjoint(sets["val"])

    def get_params(
        self, stats: Dict[str, List[str]], keep_zero_fcs: Optional[bool] = True
    ) -> Tuple[Dict[str, List[float]]]:
        """
        Scaling parameters for input and output features to be applied
        in transforms.
        """

        params = [{}, {}]

        for k in stats.keys():
            if stats[k] is None:
                continue
            for i, stat in enumerate(stats[k]):

                if stat not in self._params[k]:
                    continue

                param = self._params[k][stat]

                if k == "xmn":
                    mask = get_xmn_mask(
                        self.mpol,
                        self.ntor,
                        keep_zero_fcs=keep_zero_fcs,
                    )
                    param = np.array(param)[mask]
                    if keep_zero_fcs:
                        param = param.reshape(self.mpol + 1, 2 * self.ntor + 1, -1)
                    param = param.tolist()

                params[i][k] = param

        return tuple(params)

    def plot(self, keys: Union[str, List[str]], stage: Optional[str] = "fit", **kwargs):
        """
        Plot the distribution of the features for the specific stage.

        TODO-1(@amerlo): review the signature of this method.
        """

        if isinstance(keys, str):
            keys = [keys]

        if stage == "fit":
            data = self.train_dataloader()
        elif stage == "validate":
            data = self.val_dataloader()
        elif stage == "test":
            data = self.test_dataloader()
        else:
            raise ValueError("Stage %s is not defined" % stage)

        #  Disable ToTuple transforms
        _to_tuple = None
        transforms = data.dataset.dataset.dataset.transforms.transforms
        if transforms[-1].__class__.__name__ == "ToTuple":
            _to_tuple = transforms.pop()

        item = next(iter(data))
        nplots = torch.cat(
            [v.flatten(start_dim=1) for k, v in item.items() if k in keys], dim=1
        ).size(1)

        labels = []
        for k in keys:
            label = get_feature_as_string(k, ns=self.ns, mpol=self.mpol, ntor=self.ntor)
            if isinstance(label, str):
                labels.append(label)
            else:
                labels.extend(label)
        assert len(labels) == nplots

        ncols = int(np.ceil(np.sqrt(nplots)))
        nrows = int(np.ceil(nplots / ncols))
        fig, axs = plt.subplots(
            nrows=nrows, ncols=ncols, figsize=(27 / 2.54, 18 / 2.54)
        )

        values = [[] for i in range(nplots)]
        for item in data:
            batch = torch.cat(
                [v.flatten(start_dim=1) for k, v in item.items() if k in keys], dim=1
            )
            for i in range(nplots):
                values[i].extend(list(batch[:, i].numpy()))

        for i in range(nplots):
            ax = axs.flatten()[i]
            ax.hist(np.array(values[i]), **kwargs)
            ax.set_title(labels[i])

        #  Put back ToTuple
        if _to_tuple is not None:
            transforms.append(_to_tuple)

        return fig

    def _get_dataloader(
        self,
        dataset: Dataset,
        **kwargs,
    ):
        return DataLoader(
            dataset,
            pin_memory=self.pin_memory,
            num_workers=0,
            persistent_workers=False,
            **kwargs,
        )

    def tune(self) -> None:

        params = {
            k: {}
            for k in (
                "npc",
                "pc",
                "phiedge",
                "p0",
                "curtor",
                "gradp",
                "pressure",
                "itor",
                "peaking_factor",
                "toroidal_current",
                "toroidal_current_density",
                "normalized_flux",
                "xmn",
                "xsmn",
                "iota",
            )
        }

        #  Define physics base params
        params["npc"]["physics_mean"] = [0.9]
        params["pc"]["physics_mean"] = [0.0]
        params["phiedge"]["physics_mean"] = [-2.0]
        params["p0"]["physics_mean"] = [1e5]
        params["gradp"]["physics_mean"] = [-1e5]
        params["curtor"]["physics_mean"] = [0.0]
        params["itor"]["physics_mean"] = [0.0]
        params["pressure"]["physics_mean"] = [0.0]
        params["peaking_factor"]["physics_mean"] = [0.0]
        params["toroidal_current"]["physics_mean"] = [0.0]
        params["toroidal_current_density"]["physics_mean"] = [0.0]
        params["normalized_flux"]["physics_mean"] = [0.0]

        params["npc"]["physics_scale"] = [0.3]
        params["pc"]["physics_scale"] = [1.0]
        params["phiedge"]["physics_scale"] = [0.6]
        params["p0"]["physics_scale"] = [1e5]
        params["gradp"]["physics_scale"] = [1e5]
        params["curtor"]["physics_scale"] = [5e4]
        params["itor"]["physics_scale"] = [5e4]
        params["pressure"]["physics_scale"] = [1.0]
        params["peaking_factor"]["physics_scale"] = [1.0]
        params["toroidal_current"]["physics_scale"] = [1.0]
        params["toroidal_current_density"]["physics_scale"] = [1.0]
        params["normalized_flux"]["physics_scale"] = [1.0]

        #  Get raw data as tensors
        transforms = self.get_transforms(
            normalize_features=False,
            pad=False,
            normalize_labels=False,
            back_normalize=False,
            crop=False,
            to_tuple=False,
        )
        transforms = Compose(transforms)
        dataset = get_dataset(
            root=self.root, fraction=self.fraction, transforms=transforms
        )

        #  Use only `tune_fraction` of the dataset
        train_indices = self._train_indices
        if self.fraction > self.tune_fraction:
            num = int(self.tune_fraction / self.fraction * len(train_indices))
            train_indices = random.sample(train_indices, num)

        dataset = Subset(dataset, train_indices)

        #  Create hash signature based on root folder and train_indices
        roots = [os.path.basename(r) for r in dataset.dataset.root]
        params["hash"] = hashlib.md5(
            str(roots + train_indices).encode("utf-8")
        ).hexdigest()

        #  Load params file if provided, and check hash
        if self.params_file is not None:
            data = load(self.params_file)
            if self.strict:
                assert params["hash"] == data["hash"]
            self._params = data
            return

        values = {k: [] for k in self.features + self.labels}
        for item in dataset:
            for k in values.keys():
                values[k].append(item[k])
        for k in values:
            values[k] = torch.stack(values[k])

        def _std(tensor):
            std = torch.std(tensor, dim=0)
            std[std == 0.0] = 1.0
            return std.tolist()

        def _scale(tensor):
            scale = (torch.max(tensor, dim=0)[0] - torch.min(tensor, dim=0)[0]) / 2
            scale[scale == 0.0] = 1.0
            return scale.tolist()

        def _iqr(tensor):
            iqr = (
                torch.quantile(tensor, q=0.95, dim=0)
                - torch.quantile(tensor, q=0.05, dim=0)
            ) / 2
            iqr[iqr == 0.0] = 1.0
            return iqr.tolist()

        def _q(q):
            def stat(tensor):
                return torch.quantile(tensor, q=q, dim=0).tolist()

            return stat

        def _max(tensor):
            scale = torch.max(torch.abs(tensor), dim=0)[0]
            scale[scale == 0.0] = 1.0
            return scale.tolist()

        def _median_max(tensor):
            scale = torch.max(torch.abs(tensor), dim=0)[0]
            scale[scale == 0.0] = 1.0
            tensor = tensor / scale
            return torch.median(tensor, dim=0)[0].tolist()

        #  Define statists
        stats = {
            "median": lambda x: torch.median(x, dim=0)[0].tolist(),
            "mean": lambda x: torch.mean(x, dim=0).tolist(),
            "median_max": _median_max,
            "std": _std,
            "scale": _scale,
            "max": _max,
            "iqr": _iqr,
            "q95": _q(0.95),
            "q75": _q(0.75),
            "q25": _q(0.25),
            "q5": _q(0.05),
            "zero": lambda x: torch.zeros_like(x[0]).tolist(),
            "one": lambda x: torch.ones_like(x[0]).tolist(),
        }

        for k in self.features:
            for stat, fn in stats.items():
                params[k][stat] = fn(values[k].view(-1, 1))

        for k in self.labels:
            for stat, fn in stats.items():
                params[k][stat] = fn(values[k])

        self._params = params
