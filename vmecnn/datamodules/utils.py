"""
Dataset utility functions.
"""

from typing import List, Optional, Union

import torch
import numpy as np

#  Data Fourier resolution
NS = 99
MPOL = 11
NTOR = 12
COMP = 3


def fluxspace(num: int, ns: int = NS):
    """Return `num` flux indices."""
    return np.linspace(0, ns - 1, num, dtype=int)


def get_xmn_shape(
    mpol: int,
    ntor: int,
) -> int:
    """Compute FCs shape."""
    return (mpol + 1, 2 * ntor + 1, COMP)


def get_num_output(
    mpol: int,
    ntor: int,
    labels: List[str],
    keep_zero_fcs: bool = True,
) -> int:
    """Compute number output features."""
    num = 0
    if "xmn" in labels:
        num += np.prod(get_xmn_shape(mpol, ntor))
        if not keep_zero_fcs:
            #  m < 0 FCs and L_00, Z_00
            num -= ntor * COMP + 2
    if "iota" in labels:
        num += 1
    return num


def get_xmn_mask(
    mpol: int,
    ntor: int,
    orig_mpol: int = MPOL,
    orig_ntor: int = NTOR,
    dtype: type = bool,
    keep_zero_fcs: bool = True,
    keep_zero_axis_fcs: bool = True,
):
    """Build FCs and iota output mask."""

    #  Create Fourier coefficient output mask
    mask = np.zeros(get_xmn_shape(orig_mpol, orig_ntor), dtype=dtype)

    #  Select flux surface coordinates
    mask[0 : mpol + 1, orig_ntor - ntor : orig_ntor + ntor + 1, :] = 1

    #  Remove null Fourier coefficients
    if not keep_zero_fcs:
        # R
        mask[0, :orig_ntor, 0] = 0
        # Lambda
        mask[0, : orig_ntor + 1, 1] = 0
        # Z
        mask[0, : orig_ntor + 1, 2] = 0

    #  Remove axis null Fourier coefficients
    if not keep_zero_axis_fcs:
        # R
        mask[1:, :, 0] = 0
        # Lambda
        mask[:, :, 1] = 0
        # Z
        mask[1:, :, 2] = 0

    return mask


def get_num_input(features: List[str], ns: int):
    """Get the number of dimensions for the input features."""
    num = 0
    if "npc" in features:
        num += 4
    if "pc" in features:
        num += 2
    if "phiedge" in features:
        num += 1
    if "p0" in features:
        num += 1
    if "curtor" in features:
        num += 1
    if "pressure" in features:
        num += ns
    if "peaking_factor" in features:
        num += 1
    if "toroidal_current" in features:
        num += ns
    if "toroidal_current_density" in features:
        num += ns
    if "normalized_flux" in features:
        num += 1
    return num


def get_feature_as_string(
    feature: str,
    ns: Optional[int] = None,
    mpol: Optional[int] = None,
    ntor: Optional[int] = None,
) -> Union[str, List[str]]:
    """Build features as strings."""
    if feature == "npc":
        return ["i2", "i3", "i4", "i5"]
    if feature == "pc":
        return ["iA", "iB"]
    if feature in (
        "phiedge",
        "p0",
        "gradp",
        "curtor",
        "itor",
        "iota",
        "peaking_factor",
    ):
        return feature
    if feature in ("pressure", "toroidal_current", "toroidal_current_density"):
        return [f"{feature}_{i}" for i in range(ns)]
    if feature == "normalized_flux":
        return "s"
    if feature == "xmn":
        x_labels = ("r", "l", "z")
        labels = np.empty(get_xmn_shape(mpol, ntor), dtype=object)
        mpols = range(mpol + 1)
        ntors = range(-ntor, ntor + 1)
        for i, m in enumerate(mpols):
            for j, n in enumerate(ntors):
                for k, comp in enumerate(x_labels):
                    labels[i][j][k] = "$" + comp + "_{" + str(m) + "," + str(n) + "}$"
        return labels.reshape(-1).tolist()


def get_random_equi(ns: int, mpol: int, ntor: int, dtype=torch.float64, device="cpu"):
    """Generate a random differentiate equilibrium."""

    normalized_flux = torch.linspace(
        0, 1, ns, device=device, dtype=dtype
    ).requires_grad_(True)
    phiedge = -torch.rand(1, dtype=dtype, device=device) * 2
    pressure = 1 - normalized_flux**2
    pressure = pressure.detach()
    iota = 0.87 + normalized_flux**2 / 10
    shape = (ns, mpol + 1, 2 * ntor + 1)
    mask = get_xmn_mask(mpol, ntor, mpol, ntor, keep_zero_fcs=False)
    mask = torch.from_numpy(mask)
    mask = mask.to(device)
    mpols = torch.arange(mpol + 1, device=device).view(1, -1, 1, 1)
    normalized_flux = normalized_flux.view(-1, 1, 1, 1)
    x0 = torch.randn(1, *shape[1:], 3, device=device, dtype=dtype) / (mpols + 1)
    x1 = torch.randn(1, *shape[1:], 3, device=device, dtype=dtype) * 5e-1
    x2 = torch.randn(1, *shape[1:], 3, device=device, dtype=dtype) * 1e-1
    #  mask coefficients
    x0 *= mask.view(1, *shape[1:], 3)
    #  rmnc_00 and rmnc_01 goes like ~-s
    x1[:, 0, mpol + 1 : mpol + 2, 0] = -torch.abs(x1[:, 0, mpol + 1 : mpol + 2, 0])
    #  zmns_01 and zmns_02 goes like ~+s
    x1[:, 0, mpol + 1 : mpol + 2, 2] = torch.abs(x1[:, 0, mpol + 1 : mpol + 2, 2])
    #  xmn ~ sqrt(s) ** m * x0 * (1 + x1 * s + x2 * s ** 2)
    #  Add eps to sqrt(s) to avoid 1/0 in gradient
    eps = 1e-16
    xmn = (
        torch.sqrt(normalized_flux + eps) ** mpols
        * x0
        * (1 + x1 * normalized_flux + x2 * normalized_flux**2)
    )
    rmnc = xmn[..., 0]
    lmns = xmn[..., 1]
    zmns = xmn[..., 2]

    return {
        "rmnc": rmnc,
        "lmns": lmns,
        "zmns": zmns,
        "iota": iota,
        "normalized_flux": normalized_flux,
        "phiedge": phiedge,
        "pressure": pressure,
        #  Add x0, x1, x2 for debugging purposes
        "mpols": mpols,
        "x0": x0,
        "x1": x1,
        "x2": x2,
        "eps": eps,
    }
