from collections import OrderedDict
from typing import Optional, Dict, Union, List

import torch
from omegaconf import DictConfig
from torch import nn
from torch import Tensor

from vmecnn.datamodules.utils import COMP
from vmecnn.models.model import Model, FullyConnectedBlock


class FullyConnected(Model):
    """A fully connected model."""

    def __init__(
        self,
        *args,
        width: Optional[int] = 32,
        depth: Optional[int] = 1,
        factor: Optional[float] = 1.0,
        activation: Optional[DictConfig] = {},
        batchnorm: Optional[bool] = False,
        layernorm: Optional[bool] = False,
        **kwargs,
    ) -> None:

        super().__init__(*args, **kwargs)
        self.save_hyperparameters(
            "width", "depth", "factor", "activation", "batchnorm", "layernorm"
        )

        #  depth is the number of hidden layers
        num_layers = depth + 2

        #  in case of overfitting a single equilibrium, s is the only input features
        if self.overfit:
            input_features = 1
        else:
            input_features = self.input_features

        layers = []
        for i in range(num_layers):
            first = i == 0
            last = i == num_layers - 1
            units = input_features if first else int(width * factor**i)
            out = self.output_features if last else int(width * factor ** (i + 1))
            block = FullyConnectedBlock(
                units,
                out,
                bias=last if layernorm or batchnorm else True,
                layernorm=layernorm and not last,
                batchnorm=batchnorm and not last,
                gain_factor=1e-1 if last else 1.0,
                activation=False if last else activation,
            )
            layers.append((f"fc{i}", block))
        self.layers = nn.Sequential(OrderedDict(layers))

    def forward(self, x: Dict[str, Tensor]) -> Dict[str, Tensor]:
        s = x["normalized_flux"]
        #  Pack features into single tensor and apply linear layers
        y = self.layers(
            s if self.overfit else torch.cat([v for v in x.values()], dim=1)
        )
        #  Build output labels
        dtype = y.dtype
        device = y.device
        xmn = [
            #  m < 0 Fourier coefficients
            torch.zeros(y.size(0), self.ntor * COMP, dtype=dtype, device=device),
            #  rmnc_00
            y[:, :1],
            #  lmns_00 and zmns_00
            torch.zeros(y.size(0), 2, dtype=dtype, device=device),
            #  all the others Fourier coefficients
            y[:, 1:-1],
        ]
        xmn = torch.cat(xmn, dim=1)
        xmn = xmn.view(-1, *self._xmn_shape)
        #  Force m!=0 to be zero on axis
        xmn[:, 1:, :, :] *= (s != 0).view(-1, 1, 1, 1)
        #  Set lambda to zero on axis
        xmn[:, :, :, 1] *= (s != 0).view(-1, 1, 1)
        iota = y[:, -1:]
        return {"xmn": xmn, "iota": iota}

    def init_bias(self, bias: Union[str, List[float]]) -> None:
        """Init model last layer bias."""
        tensor = self.layers[-1].layers[-1].bias
        if bias == "zeros":
            #  Replace zeros initialization with small values
            torch.nn.init.normal_(tensor, std=1e-2)
        elif bias == "ones":
            torch.nn.init.ones_(tensor)
        else:
            with torch.no_grad():
                tensor.data = torch.as_tensor(bias, dtype=self.dtype).data
