import math
from typing import Dict, Optional, List, Union

import torch
import pytorch_lightning as pl
import hydra
from omegaconf import DictConfig, OmegaConf
from torch import Tensor
from torch import nn
from torchmetrics import MetricCollection

from vmecnn.datamodules.utils import get_xmn_shape, get_num_output
from vmecnn.metrics.metric import get_loss_named_hook
from vmecnn.utils import instantiate_list


def get_activation_hook(name, act):
    def hook(model, input, output):
        act[name] = output.detach()

    return hook


class FullyConnectedBlock(nn.Module):
    """A fully connected block."""

    def __init__(
        self,
        in_features: int,
        out_features: int,
        bias: bool,
        batchnorm: bool,
        layernorm: bool,
        gain_factor: Optional[float] = 1.0,
        activation: Optional[Union[bool, DictConfig]] = False,
    ) -> None:

        assert not (batchnorm and layernorm)
        assert not (bias and (batchnorm or layernorm))

        super().__init__()
        linear = nn.Linear(
            in_features=in_features, out_features=out_features, bias=bias
        )

        # Initialize linear layer as suggested in Glorot2010
        # See https://pytorch.org/docs/stable/nn.init.html
        if activation is False:
            act = "linear"
        else:
            act = activation["_target_"].split(".")[-1].lower()
        if act == "silu":
            act = "leaky_relu"
        gain = torch.nn.init.calculate_gain(act) * gain_factor
        torch.nn.init.xavier_normal_(linear.weight, gain=gain)

        layers = [linear]

        if layernorm:
            layers.append(nn.LayerNorm(out_features))

        if batchnorm:
            layers.append(nn.BatchNorm1d(out_features))

        if activation:
            layers.append(hydra.utils.instantiate(activation))

        self.layers = nn.Sequential(*layers)

    def forward(self, x: Tensor) -> Tensor:
        return self.layers(x)


class ConvBlock(nn.Module):
    """A 1D convolutional block."""

    def __init__(
        self,
        in_channels: int,
        out_channels: int,
        conv_kernel_size: int,
        conv_stride: int,
        conv_padding: int,
        bias: bool,
        batchnorm: bool,
        layernorm: bool,
        pool_kernel_size: int,
        pool_stride: int,
        pool_padding: int,
        activation: Optional[Union[bool, DictConfig]] = False,
    ) -> None:

        assert not (batchnorm and layernorm)
        assert not (bias and (batchnorm or layernorm))

        super().__init__()
        conv = nn.Conv1d(
            in_channels=in_channels,
            out_channels=out_channels,
            kernel_size=conv_kernel_size,
            stride=conv_stride,
            padding=conv_padding,
            bias=bias,
        )

        # Initialize linear layer as suggested in Glorot2010
        # See https://pytorch.org/docs/stable/nn.init.html
        if activation is False:
            act = "linear"
        else:
            act = activation["_target_"].split(".")[-1].lower()
        if act == "silu":
            act = "leaky_relu"
        gain = torch.nn.init.calculate_gain(act)
        torch.nn.init.xavier_normal_(conv.weight, gain=gain)

        layers = [conv]

        if layernorm:
            layers.append(nn.LayerNorm(out_channels))

        if batchnorm:
            layers.append(nn.BatchNorm1d(out_channels))

        if activation:
            layers.append(hydra.utils.instantiate(activation))

        if pool_kernel_size > 0:
            pool = nn.AvgPool1d(
                kernel_size=pool_kernel_size, stride=pool_stride, padding=pool_padding
            )
            layers.append(pool)

        self.layers = nn.Sequential(*layers)

    def forward(self, x: Tensor) -> Tensor:
        return self.layers(x)


class DeConvBlock(nn.Module):
    """A 3D transpose all convolution block."""

    def __init__(
        self,
        in_channels: int,
        out_channels: int,
        conv_kernel_size: int,
        conv_stride: int,
        conv_padding: int,
        bias: bool,
        batchnorm: bool,
        layernorm: bool,
        activation: Optional[Union[bool, DictConfig]] = False,
    ) -> None:

        assert not (batchnorm and layernorm)
        assert not (bias and (batchnorm or layernorm))

        super().__init__()
        conv = nn.ConvTranspose3d(
            in_channels=in_channels,
            out_channels=out_channels,
            kernel_size=conv_kernel_size,
            stride=conv_stride,
            padding=conv_padding,
            bias=bias,
        )

        # Initialize linear layer as suggested in Glorot2010
        # See https://pytorch.org/docs/stable/nn.init.html
        if activation is False:
            act = "linear"
        else:
            act = activation["_target_"].split(".")[-1].lower()
        if act == "silu":
            act = "leaky_relu"
        gain = torch.nn.init.calculate_gain(act)
        torch.nn.init.xavier_normal_(conv.weight, gain=gain)

        layers = [conv]

        if layernorm:
            layers.append(nn.LayerNorm(out_channels))

        if batchnorm:
            layers.append(nn.BatchNorm3d(out_channels))

        if activation:
            layers.append(hydra.utils.instantiate(activation))

        self.layers = nn.Sequential(*layers)

    def forward(self, x: Tensor) -> Tensor:
        return self.layers(x)


class CropBlock(nn.Module):
    def __init__(self, mpol: int, ntor: int, lrad: int, channels: int):
        super().__init__()
        self.mpol = mpol
        self.ntor = ntor
        self.lrad = lrad
        self.channels = channels

    def forward(self, x: Tensor) -> Tensor:
        # B, C, M, N, L -> B, M, N, C, L
        x = torch.transpose(x, 1, 3)
        x = torch.transpose(x, 1, 2)
        x = x[:, : self.mpol + 1, : 2 * self.ntor + 1, :, : self.lrad]
        x = x.reshape(
            -1, (self.mpol + 1) * (2 * self.ntor + 1), self.channels, self.lrad
        )
        x = torch.cat(
            [
                x[:, self.ntor :, 0],  # R
                x[:, self.ntor + 1 :, 1],  # l
                x[:, self.ntor + 1 :, 2],  # Z
                x[:, :1, 3],  # iota
            ],
            dim=1,
        )
        return x


class FlattenBlock(nn.Module):
    def __init__(self):
        super().__init__()

    def forward(self, x: Tensor) -> Tensor:
        return x.flatten(start_dim=1)


class BasicFourierEncoding(nn.Module):
    """
    Layer for mapping coordinates using the basic encoding.

    Differently from the original proposed `BasicEncoding`, this embedding
    adds also the not encoded input tensor.

    See: https://github.com/jmclong/random-fourier-features-pytorch
    """

    def forward(self, v: Tensor) -> Tensor:
        vp = 2 * math.pi * v
        return torch.cat((torch.cos(vp), torch.sin(vp), v), dim=-1)


class GaussianFourierEncoding(nn.Module):
    """
    Layer for mapping coordinates using random Fourier features.

    See: https://github.com/jmclong/random-fourier-features-pytorch
    """

    def __init__(
        self,
        in_features: int,
        out_features: int,
        sigma: float,
    ):
        super().__init__()
        b = torch.rand(in_features, out_features) * sigma
        self.b = nn.parameter.Parameter(b, requires_grad=False)

    def forward(self, v: Tensor) -> Tensor:
        vp = 2 * math.pi * v * self.b
        return torch.cat((torch.cos(vp), torch.sin(vp), v), dim=-1)


class Model(pl.LightningModule):
    """
    An generic model to regress VMEC FCs and the iota value.

    TODO-1(@amerlo): add documentation.
    """

    def __init__(
        self,
        input_features: int,
        mpol: int,
        ntor: int,
        ns: int,
        labels: List[str],
        log_training: Optional[bool] = False,
        to_double: Optional[bool] = False,
        overfit: Optional[bool] = False,
        enable_grad_on_predict: Optional[bool] = False,
        criterion: Optional[DictConfig] = {},
        optimizer: Optional[DictConfig] = {},
        scheduler: Optional[DictConfig] = {},
        metrics: Optional[DictConfig] = {},
    ):

        super().__init__()
        self.save_hyperparameters(
            "input_features",
            "mpol",
            "ntor",
            "ns",
            "labels",
            "log_training",
            "to_double",
            "overfit",
            "enable_grad_on_predict",
            "criterion",
            "optimizer",
            "scheduler",
            "metrics",
        )

        #  Build model signature
        self.input_features = input_features
        self.mpol = mpol
        self.ntor = ntor
        self.ns = ns
        self.labels = labels

        #  Whether to log gradient and weights during training
        self.log_training = log_training

        #  Whether to use double tensor in layers
        self.to_double = to_double

        #  Build architecture to overfit a single equilibrium
        self.overfit = overfit

        #  Enable grad computation in predict step
        self.enable_grad_on_predict = enable_grad_on_predict

        #  Number of outputs returned by the model
        self.num_outputs = get_num_output(mpol, ntor, labels, keep_zero_fcs=True)

        #  Instantiate the loss
        self.criterion = hydra.utils.instantiate(criterion)

        #  Register loss forward pre hook
        if self.criterion.__class__.__name__ not in ("RealMSELoss", "IdealMHDLoss"):
            hook = get_loss_named_hook(self.labels)
            self.criterion.register_forward_pre_hook(hook)

        #  Define "lr" as attribute to leverage pl learning rate tuner
        self.optimizer = optimizer
        self.lr = optimizer.lr
        self.scheduler = scheduler

        #  Metrics
        metrics = instantiate_list(
            metrics,
            num_outputs=self.num_outputs,
            keys=self.labels,
        )
        metrics = MetricCollection(metrics)
        self.train_metrics = metrics.clone(prefix="train/")
        self.val_metrics = metrics.clone(prefix="val/")
        self.test_metrics = metrics.clone(prefix="test/")

        #  Shape for the xmn label
        self._xmn_shape = get_xmn_shape(mpol, ntor)

        #  Dict to keep track of activations
        self._activations = {}

    def setup(self, stage: Optional[str] = None) -> None:
        """Initialize model."""

        #  Push model parameters to double
        if self.to_double:
            self.double()

        #  Register activation hook
        if self.log_training:
            for name, module in self.named_modules():
                module.register_forward_hook(
                    get_activation_hook(name, self._activations)
                )

    def step(self, batch: any, batch_idx: int):
        x, y, _ = batch
        #  Force grad on the normalized_flux (i.e., s) to compute physics loss
        x["normalized_flux"].requires_grad_(True)
        y_hat = self(x)
        #  Add inputs and metadata into output to be able to compute physics loss
        loss = self.criterion({**x, **y_hat}, {**x, **y})
        return loss, y_hat, y

    def training_step(self, batch: any, batch_idx: int):
        loss, y_hat, y = self.step(batch, batch_idx)
        self.log("train/loss", loss, on_step=False, on_epoch=True)
        if self.log_training:
            self.train_metrics(y_hat, y)
            self.log_dict(self.train_metrics, on_step=False, on_epoch=True)
        return loss

    def validation_step(self, batch: any, batch_idx: int):
        loss, y_hat, y = self.step(batch, batch_idx)
        self.log("val/loss", loss)
        self.val_metrics(y_hat, y)
        self.log_dict(self.val_metrics)
        return loss

    def test_step(self, batch: any, batch_idx: int):
        loss, y_hat, y = self.step(batch, batch_idx)
        self.log("test/loss", loss)
        self.test_metrics(y_hat, y)
        self.log_dict(self.test_metrics)
        return loss

    def predict_step(self, batch: any, batch_idx: int, dataloader_idx: int = 0) -> any:
        x, y, metadata = batch
        if self.enable_grad_on_predict:
            #  Keep gradients information is costly and cannot be enabled
            #  for the entire dataset.
            torch.set_grad_enabled(True)
            x["normalized_flux"].requires_grad_(True)
        y_hat = self(x)
        return {**x, **y, **metadata}, {**x, **y_hat, **metadata}

    def configure_optimizers(self):
        optimizer = hydra.utils.instantiate(
            self.optimizer, self.parameters(), lr=self.lr
        )
        if self.scheduler != {}:
            monitor = (
                None if "monitor" not in self.scheduler else self.scheduler.monitor
            )
            keys = set(self.scheduler.keys()) - set(["monitor"])
            scheduler = OmegaConf.masked_copy(self.scheduler, keys)
            lr_scheduler = {
                "scheduler": hydra.utils.instantiate(scheduler, optimizer),
                "monitor": monitor,
            }
            return {"optimizer": optimizer, "lr_scheduler": lr_scheduler}
        else:
            return optimizer

    def training_step_end(self, *args, **kwargs) -> None:
        """Called in the training loop after ``training_step()``."""

        if hasattr(self.criterion, "_loss"):
            for k in self.criterion._loss.keys():
                self.log(
                    f"train/loss.{k}",
                    self.criterion._loss[k],
                    on_step=False,
                    on_epoch=True,
                )
                self.log(
                    f"train/alpha.{k}",
                    self.criterion.alpha[k],
                    on_step=False,
                    on_epoch=True,
                )

        #  Log debugging quantities
        if not self.should_log_training:
            return

        #  TensorBoard should be the first logger in the collection
        tensorboard = self.loggers[0].experiment

        #  Log layer activations
        for name, act in self._activations.items():
            tensorboard.add_histogram(
                f"train/{name}", act, global_step=self.global_step
            )

    def on_before_zero_grad(self, optimizer) -> None:
        """Called after ``training_step()`` and before ``optimizer.zero_grad()``."""

        #  TODO-1(@amerlo): disable computing and logging gradients on GPU
        #  These are per step values, and not averaged over an epoch
        if self.should_log_training and self.global_step != 0:
            for layer, param in self.named_parameters():
                if param.grad is None:
                    continue
                #  Compute estimate of weight updates
                lr = self.trainer.optimizers[0].param_groups[-1]["lr"]
                ratio = lr * torch.linalg.norm(param.grad / param)
                self.log(f"train/{layer}.update_ratio", ratio)

        if hasattr(self.criterion, "step"):
            self.criterion.step(self.parameters, self.global_step)

    def training_epoch_end(self, outputs) -> None:
        """Called at the end of the training epoch with the outputs of all training steps."""
        norm = 0
        for layer, param in self.named_parameters():
            norm += torch.linalg.norm(param)
        self.log("train/weight_norm", norm)

    def validation_step_end(self, *args, **kwargs) -> None:
        if hasattr(self.criterion, "_loss"):
            for k in self.criterion._loss.keys():
                self.log(
                    f"val/loss.{k}",
                    self.criterion._loss[k],
                    on_step=False,
                    on_epoch=True,
                )

    @property
    def output_features(self) -> int:
        """Number of output features to compute."""
        return get_num_output(self.mpol, self.ntor, self.labels, keep_zero_fcs=False)

    @property
    def num_trainable_parameters(self) -> int:
        """Number of trainable parameters."""
        return sum(p.numel() for p in self.parameters() if p.requires_grad)

    @property
    def dtype(self) -> type:
        return torch.float64 if self.to_double else torch.float32

    def zeros_all_trainable_parameters(self) -> None:
        """Zeros all trainable parameters."""
        for param in self.parameters():
            torch.nn.init.constant_(param, 0)

    def init_bias(self, bias: Union[str, List[float]]) -> None:
        """Init model last layer bias."""
        raise NotImplementedError

    def to_onnx(self, dummy_input: Dict[str, Tensor]) -> None:
        """Export model to ONNX."""
        #  See: https://github.com/onnx/onnx/issues/2636
        file_name = f"{self.__class__.__name__}.onnx"
        torch.onnx.export(self, dummy_input, file_name, verbose=False, opset_version=11)

    @property
    def should_log_training(self) -> bool:
        return (
            self.log_training and self.global_step % self.trainer.log_every_n_steps == 0
        )

    def _get_last_layer_bias(self):
        raise NotImplementedError
