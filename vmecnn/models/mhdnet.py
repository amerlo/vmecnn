import math
from collections import OrderedDict
from typing import Optional, Dict, Union, List

import torch
from omegaconf import DictConfig, ListConfig
from torch import nn
from torch import Tensor

from vmecnn.datamodules.utils import COMP
from vmecnn.models.model import (
    Model,
    FullyConnectedBlock,
    ConvBlock,
    DeConvBlock,
    CropBlock,
    FlattenBlock,
    BasicFourierEncoding,
    GaussianFourierEncoding,
)
from vmecnn.utils import compute_conv_out_length, eps, gather


def net(
    input_features: int,
    width: int,
    depth: int,
    factor: float,
    bias: bool,
    layernorm: bool,
    batchnorm: bool,
    activation: DictConfig,
    gain_factor: float,
    use_linear_for_last_layer: Optional[bool] = True,
    fourier_encoding: Optional[str] = None,
    fourier_features: Optional[int] = None,
    fourier_sigma: Optional[float] = None,
    output_features: Optional[int] = None,
):

    net = []

    #  Add Fourier encoding
    if fourier_encoding == "basic":
        fourier = BasicFourierEncoding()
        input_features = 2 * input_features + 1
    elif fourier_encoding == "gaussian":
        fourier = GaussianFourierEncoding(
            in_features=input_features,
            out_features=fourier_features,
            sigma=fourier_sigma,
        )
        input_features = 2 * fourier_features * input_features + 1
    elif fourier_encoding is None:
        fourier = None

    if fourier is not None:
        net.append(("fourier", fourier))

    #  depth is the number of hidden layers
    num_layers = depth + 2

    #  Build network
    for i in range(num_layers):
        first = i == 0
        last = i == num_layers - 1
        units = input_features if first else int(width * factor**i)
        out = (
            output_features
            if output_features is not None and last
            else int(width * factor ** (i + 1))
        )
        block = FullyConnectedBlock(
            units,
            out,
            bias=last and bias if layernorm or batchnorm else bias,
            layernorm=layernorm and not last,
            batchnorm=batchnorm and not last,
            gain_factor=gain_factor if last else 1.0,
            activation=False if last and use_linear_for_last_layer else activation,
        )
        net.append((f"fc{i}", block))

    return nn.Sequential(OrderedDict(net))


def convnet(
    in_channels: int,
    width: int,
    depth: int,
    factor: float,
    conv_kernel_size: Union[int, List[int]],
    conv_stride: Union[int, List[int]],
    conv_padding: Union[int, List[int]],
    pool_kernel_size: Union[int, List[int]],
    pool_stride: Union[int, List[int]],
    pool_padding: Union[int, List[int]],
    bias: bool,
    layernorm: bool,
    batchnorm: bool,
    activation: DictConfig,
    out_channels: Optional[int] = None,
):

    num_layers = depth

    def validate_arg(arg):
        if isinstance(arg, (list, ListConfig)):
            assert len(arg) == num_layers
        else:
            arg = [arg] * num_layers
        return arg

    conv_kernel_size = validate_arg(conv_kernel_size)
    conv_stride = validate_arg(conv_stride)
    conv_padding = validate_arg(conv_padding)
    pool_kernel_size = validate_arg(pool_kernel_size)
    pool_stride = validate_arg(pool_stride)
    pool_padding = validate_arg(pool_padding)

    #  Build network
    convnet = []
    for i in range(num_layers):
        first = i == 0
        last = i == num_layers - 1
        units = in_channels if first else int(width * factor ** (i - 1))
        out = (
            out_channels
            if last and out_channels is not None
            else int(width * factor**i)
        )
        conv = ConvBlock(
            in_channels=units,
            out_channels=out,
            conv_kernel_size=conv_kernel_size[i],
            conv_stride=conv_stride[i],
            conv_padding=conv_padding[i],
            bias=last and bias if layernorm or batchnorm else bias,
            layernorm=layernorm and not last,
            batchnorm=batchnorm and not last,
            pool_kernel_size=pool_kernel_size[i],
            pool_stride=pool_stride[i],
            pool_padding=pool_padding[i],
            activation=activation,
        )
        convnet.append((f"cn{i}", conv))

    return nn.Sequential(OrderedDict(convnet))


def deconvnet(
    in_channels: int,
    out_channels: int,
    width: int,
    depth: int,
    factor: float,
    conv_kernel_size: Union[int, List[int]],
    conv_stride: Union[int, List[int]],
    conv_padding: Union[int, List[int]],
    bias: bool,
    layernorm: bool,
    batchnorm: bool,
    activation: DictConfig,
    mpol: Optional[int] = None,
    ntor: Optional[int] = None,
    lrad: Optional[int] = None,
    linear_projection: Optional[bool] = False,
    output_features: Optional[int] = None,
    gain_factor: Optional[float] = None,
):

    num_layers = depth

    def validate_arg(arg):
        if isinstance(arg, (list, ListConfig)):
            assert len(arg) == num_layers
        else:
            arg = [arg] * num_layers
        return arg

    conv_kernel_size = validate_arg(conv_kernel_size)
    conv_stride = validate_arg(conv_stride)
    conv_padding = validate_arg(conv_padding)

    #  Build network
    deconvnet = []

    #  Add convolutional layers
    for i in range(num_layers):
        first = i == 0
        last = i == num_layers - 1
        units = in_channels if first else int(width * factor ** (i - 1))
        out = out_channels if last else int(width * factor**i)
        block = DeConvBlock(
            in_channels=units,
            out_channels=out,
            conv_kernel_size=conv_kernel_size[i],
            conv_stride=conv_stride[i],
            conv_padding=conv_padding[i],
            bias=False if layernorm or batchnorm or last else bias,
            layernorm=layernorm,
            batchnorm=batchnorm,
            activation=False if last else activation,
        )
        deconvnet.append((f"cn{i}", block))

    if linear_projection:

        #  Flatten
        block = FlattenBlock()
        deconvnet.append(("flatten", block))

        #  Get output dim from transpose convolution
        out_dim = 1
        for i in range(num_layers):
            out_dim = (
                (out_dim - 1) * conv_stride[i]
                - 2 * conv_padding[i]
                + conv_kernel_size[i]
            )

        #  Linear projection
        block = FullyConnectedBlock(
            out_channels * out_dim**COMP,
            output_features,
            bias=bias,
            layernorm=False,
            batchnorm=False,
            gain_factor=gain_factor,
            activation=False,
        )
        deconvnet.append((f"fc{0}", block))
    else:
        block = CropBlock(mpol=mpol, ntor=ntor, lrad=lrad, channels=COMP + 1)
        deconvnet.append(("crop", block))

    return nn.Sequential(OrderedDict(deconvnet))


class MHDNet(Model):
    """A MHDNet model."""

    def __init__(
        self,
        *args,
        fourier_encoding: Optional[str] = None,
        fourier_features: Optional[int] = 16,
        fourier_sigma: Optional[float] = 1.0,
        trunk_width: Optional[int] = 32,
        trunk_depth: Optional[int] = 1,
        trunk_factor: Optional[float] = 1.0,
        trunk_bias: Optional[bool] = False,
        trunk_basis: Optional[int] = None,
        trunk_batchnorm: Optional[bool] = False,
        trunk_layernorm: Optional[bool] = False,
        trunk_activation: Optional[DictConfig] = {},
        shared_trunk: Optional[bool] = False,
        bias_type: Optional[str] = None,
        bias_order: Optional[int] = 2,
        conv_width: Optional[int] = 8,
        conv_depth: Optional[int] = 1,
        conv_factor: Optional[float] = 1.0,
        conv_bias: Optional[bool] = True,
        conv_batchnorm: Optional[bool] = False,
        conv_layernorm: Optional[bool] = False,
        conv_activation: Optional[DictConfig] = {},
        conv_kernel_size: Optional[int] = 7,
        conv_stride: Optional[int] = 2,
        conv_padding: Optional[int] = 0,
        pool_kernel_size: Optional[int] = 3,
        pool_stride: Optional[int] = 2,
        pool_padding: Optional[int] = 0,
        branch_width: Optional[int] = 32,
        branch_depth: Optional[int] = 1,
        branch_factor: Optional[float] = 1.0,
        branch_bias: Optional[bool] = True,
        branch_batchnorm: Optional[bool] = False,
        branch_layernorm: Optional[bool] = False,
        branch_activation: Optional[DictConfig] = {},
        multiply_profile_features: Optional[bool] = False,
        branch_kernel_size: Optional[Union[List[int], int]] = -1,
        branch_stride: Optional[Union[List[int], int]] = 1,
        branch_padding: Optional[Union[List[int], int]] = 0,
        branch_linear_projection: Optional[bool] = False,
        **kwargs,
    ) -> None:

        super().__init__(*args, **kwargs)
        self.save_hyperparameters(
            "fourier_encoding",
            "fourier_features",
            "fourier_sigma",
            "conv_width",
            "conv_depth",
            "conv_factor",
            "conv_bias",
            "conv_batchnorm",
            "conv_layernorm",
            "conv_activation",
            "conv_kernel_size",
            "conv_stride",
            "conv_padding",
            "pool_kernel_size",
            "pool_stride",
            "pool_padding",
            "branch_width",
            "branch_depth",
            "branch_factor",
            "branch_bias",
            "branch_batchnorm",
            "branch_layernorm",
            "branch_activation",
            "branch_kernel_size",
            "branch_stride",
            "branch_padding",
            "branch_linear_projection",
            "trunk_width",
            "trunk_depth",
            "trunk_factor",
            "trunk_bias",
            "trunk_batchnorm",
            "trunk_layernorm",
            "trunk_activation",
            "trunk_basis",
            "shared_trunk",
            "bias_type",
            "bias_order",
            "multiply_profile_features",
        )

        if shared_trunk:
            self._trunk_output_dim = 1
        else:
            self._trunk_output_dim = self.output_features

        if trunk_depth == -1:
            self._trunk_output_features = 0
        else:
            self._trunk_output_features = (
                trunk_basis if trunk_basis is not None else trunk_width
            )

        if trunk_depth == -1:
            assert bias_type in ("series", "legendre")
            self.trunk_net = None
        else:
            self.trunk_net = net(
                input_features=1,
                fourier_encoding=fourier_encoding,
                fourier_features=fourier_features,
                fourier_sigma=fourier_sigma,
                width=trunk_width,
                depth=trunk_depth,
                factor=trunk_factor,
                bias=trunk_bias,
                batchnorm=trunk_batchnorm,
                layernorm=trunk_layernorm,
                gain_factor=1.0,
                activation=trunk_activation,
                use_linear_for_last_layer=False,
                output_features=self._trunk_output_dim * self._trunk_output_features,
            )

        #  trunk_net is (-1, output_dim, output_features)
        trunk_shape = (self._trunk_output_dim, self._trunk_output_features)

        #  Order of the polynomial bias (i.e., skip connections)
        self.bias_order = bias_order

        #  Learned-based trunk net
        def _nn_trunk(s: Tensor) -> Tensor:
            #  Scale s to be in [-1, 1] to improve training
            trunk = self.trunk_net(2 * s - 1)
            trunk = torch.cat(
                [trunk.view(-1, *trunk_shape)]
                + [
                    s.view(-1, 1, 1).expand(-1, trunk_shape[0], -1) ** i
                    for i in range(1, bias_order + 1)
                ],
                dim=-1,
            )
            return trunk

        #  Polynomial-based trunk net
        def _series_trunk(s: Tensor) -> Tensor:
            return torch.cat(
                [
                    s.view(-1, 1, 1).expand(-1, trunk_shape[0], -1) ** i
                    for i in range(1, bias_order + 1)
                ],
                dim=-1,
            )

        #  Legendre-based trunk net
        def _legendre_trunk(s: Tensor) -> Tensor:
            #  Use shifted Legendre polynomials to be orthogonal on [0, 1]
            s = 2 * s - 1
            s = s.view(-1, 1).expand(-1, trunk_shape[0])
            polys = [1, s]
            for n in range(1, bias_order):
                #  Normalize Legendre polynomials
                poly = (
                    (
                        math.sqrt(2 * n + 1) * s * polys[n]
                        - n / math.sqrt(2 * n - 1) * polys[n - 1]
                    )
                    / (n + 1)
                    * math.sqrt(2 * n + 3)
                )
                polys.append(poly)
            return torch.stack(polys[1:], dim=-1)

        if self.trunk_net is None:
            if bias_type == "series":
                self._trunk_net = _series_trunk
            elif bias_type == "legendre":
                self._trunk_net = _legendre_trunk
        else:
            self._trunk_net = _nn_trunk

        #  Number of features for profile net
        self._profile_in_channels = 2
        conv_input_features = self.ns * self._profile_in_channels

        #  Number of scalar input features
        scalar_input_features = self.input_features - 1 - conv_input_features

        #  trunk features plus bias
        self._branch_features_per_output = self._trunk_output_features + bias_order + 1

        #  Use profile net
        self._use_profile_net = conv_kernel_size != -1

        #  Scalar feature keys
        #  TODO-1(@amerlo): avoid hardcoding of the features
        self.scalar_features = ("npc", "pc", "phiedge", "gradp", "itor")
        if self._use_profile_net:
            self.scalar_features = ("npc", "pc", "phiedge", "p0", "curtor")

        if self.overfit:
            self.profile_net = None
            self.branch_net = torch.nn.Parameter(
                torch.randn(self.output_features, self._branch_features_per_output),
                requires_grad=True,
            )
        else:

            if self._use_profile_net:
                profile_out_channels = None
                if multiply_profile_features:
                    profile_out_channels = scalar_input_features
                self.profile_net = convnet(
                    in_channels=self._profile_in_channels,
                    out_channels=profile_out_channels,
                    width=conv_width,
                    depth=conv_depth,
                    factor=conv_factor,
                    conv_kernel_size=conv_kernel_size,
                    conv_stride=conv_stride,
                    conv_padding=conv_padding,
                    pool_kernel_size=pool_kernel_size,
                    pool_stride=pool_stride,
                    pool_padding=pool_padding,
                    bias=conv_bias,
                    batchnorm=conv_batchnorm,
                    layernorm=conv_layernorm,
                    activation=conv_activation,
                )

                self._profile_out_length = compute_conv_out_length(
                    in_length=self.ns,
                    depth=conv_depth,
                    conv_kernel_size=conv_kernel_size,
                    conv_stride=conv_stride,
                    conv_padding=conv_padding,
                    pool_kernel_size=pool_kernel_size,
                    pool_stride=pool_stride,
                    pool_padding=pool_padding,
                )

            branch_output_features = (
                self.output_features * self._branch_features_per_output
            )

            if not self._use_profile_net:
                branch_input_features = scalar_input_features
            else:
                if multiply_profile_features:
                    branch_input_features = (
                        scalar_input_features * self._profile_out_length
                    )
                else:
                    branch_input_features = (
                        scalar_input_features
                        + self._profile_out_length
                        * self.profile_net[-1].layers[0].weight.shape[0]
                    )

            self._use_deconv_branch = branch_kernel_size != -1

            if not self._use_deconv_branch:
                self.branch_net = net(
                    input_features=branch_input_features,
                    width=branch_width,
                    depth=branch_depth,
                    factor=branch_factor,
                    bias=branch_bias,
                    batchnorm=branch_batchnorm,
                    layernorm=branch_layernorm,
                    activation=branch_activation,
                    use_linear_for_last_layer=True,
                    gain_factor=1e-1,
                    output_features=branch_output_features,
                )
            else:
                self.branch_net = deconvnet(
                    in_channels=branch_input_features,
                    out_channels=COMP + 1,
                    width=branch_width,
                    depth=branch_depth,
                    factor=branch_factor,
                    conv_kernel_size=branch_kernel_size,
                    conv_stride=branch_stride,
                    conv_padding=branch_padding,
                    bias=branch_bias,
                    layernorm=branch_layernorm,
                    batchnorm=branch_batchnorm,
                    activation=branch_activation,
                    mpol=self.mpol,
                    ntor=self.ntor,
                    lrad=self._branch_features_per_output,
                    linear_projection=branch_linear_projection,
                    output_features=branch_output_features,
                    gain_factor=1e-1,
                )
                self.bias = torch.nn.Parameter(
                    torch.randn(
                        1, self.output_features, self._branch_features_per_output
                    ),
                    requires_grad=True,
                )

        #  Initialization s**n terms (e.g., 1st, 2nd order biases)
        if self.overfit:
            bias = self.branch_net
        elif self._use_deconv_branch:
            bias = self.bias
        else:
            bias = self.branch_net[-1].layers[0].bias
        torch.nn.init.normal_(bias, std=1e-2)
        with torch.no_grad():
            bias = bias.view(self.output_features, -1)
            #  force a ~ -s dependency for x_0n
            bias[: self.ntor * COMP + 1, :-1] *= -torch.sign(
                bias[: self.ntor * COMP + 1, :-1]
            )
            #  force a ~ s dependency for lambda
            bias[2:-1:COMP, :-1] *= -torch.sign(bias[2:-1:COMP, :-1])

        #  Freeze last layer to avoid degradation due to L2 regularization
        if not self.overfit:
            if self._use_deconv_branch:
                self.bias.requires_grad_(False)
            else:
                self.branch_net[-1].layers[0].bias.requires_grad_(False)

        #  Build function to apply branch net
        def _full_branch_net(x: Dict[str, Tensor]) -> Tensor:

            # Scalar features
            u = torch.cat(
                [v for k, v in x.items() if k in self.scalar_features],
                dim=-1,
            )

            if self._use_profile_net:
                # Apply profile head
                p = torch.cat([x["pressure"], x["toroidal_current"]], dim=-1).view(
                    -1, self._profile_in_channels, self.ns
                )
                p = self.profile_net(p)

                if multiply_profile_features:
                    u = u.view(-1, scalar_input_features, 1)
                    u = torch.mul(u, p).flatten(start_dim=1)
                else:
                    u = torch.cat([u, p.flatten(start_dim=1)], dim=-1)

            if self._use_deconv_branch:
                u = u.view(-1, u.shape[1], 1, 1, 1)

            #  Apply branch net
            b = self.branch_net(u)

            if self._use_deconv_branch:
                b += self.bias

            return b

        def _overfit_branch_net(x: Dict[str, Tensor]) -> Tensor:
            return self.branch_net

        if self.overfit:
            self._branch_net = _overfit_branch_net
        else:
            self._branch_net = _full_branch_net

        if self.to_double:
            self.double()

    def forward(self, x: Dict[str, Tensor]) -> Dict[str, Tensor]:

        s = x["normalized_flux"]

        #  Apply trunk net and skip connection
        trunk = self._trunk_net(s)

        #  Apply branch net
        branch = self._branch_net(x).view(
            -1, self.output_features, self._branch_features_per_output
        )

        y = torch.mul(branch[..., :-1], trunk).sum(dim=2)

        #  Get 0th order bias
        y0 = branch[..., -1]

        #  xmn
        dtype = y.dtype
        device = y.device
        end = 1 + self.ntor * COMP
        xmn = [
            #  m < 0 Fourier coefficients
            torch.zeros(y.size(0), self.ntor * COMP, dtype=dtype, device=device),
            #  rmnc_00
            y0[:, :1] + y[:, :1],
            #  lmns_00 and zmns_00
            torch.zeros(y.size(0), 2, dtype=dtype, device=device),
            #  m=0
            y0[:, 1:end] + y[:, 1:end],
        ]
        #  m > 0
        #  Use small eps to avoid nan when taking gradients
        rho = torch.sqrt(s + eps)
        for m in range(1, self.mpol + 1):
            start = 1 + (self.ntor + (m - 1) * (2 * self.ntor + 1)) * COMP
            end = 1 + (self.ntor + m * (2 * self.ntor + 1)) * COMP
            #  Use rho**m factor for {R, lambda, Z}
            xmn.append(rho**m * (y0[:, start:end] + y[:, start:end]))
        xmn = torch.cat(xmn, dim=1)
        xmn = xmn.view(-1, *self._xmn_shape)
        #  Set lambda to zero on axis
        xmn[:, :, :, 1] *= (s != 0).view(-1, 1, 1)

        #  iota
        #  See Strand2001 (eq. 8)
        #  iota = mu0 * I / (S11 * phi_edge) - S12 / S11
        #  branch is (bs, out_features, branch coefficients)
        #  branch[..., -1] is y0
        #  branch[..., -2] is the bias_order coefficients
        #  for iota, we replace th s**bias term with toroidal_current
        if self._use_profile_net:
            toroidal_current = (
                gather(x["toroidal_current"], dim=1, value=s) * x["curtor"]
            )
        else:
            toroidal_current = x["itor"]
        y = torch.mul(branch[:, -1:, :-2], trunk[:, -1:, :-1]).sum(dim=2)
        iota = y0[:, -1:] + branch[:, -1:, -2] * toroidal_current + y
        #  TODO-1(@amerlo): this is the non biased approach
        # iota = y0[:, -1:] * (1 + y[:, -1:])

        return {"xmn": xmn, "iota": iota}

    def init_bias(self, bias: Union[str, List[float]]) -> None:
        """Init model last layer bias."""
        if self.overfit:
            tensor = self.branch_net
        elif self._use_deconv_branch:
            tensor = self.bias
        else:
            tensor = self.branch_net[-1].layers[0].bias
        if bias == "zeros":
            #  Replace zeros initialization with small values
            torch.nn.init.normal_(tensor, std=1e-2)
        elif bias == "ones":
            torch.nn.init.ones_(tensor)
        else:
            #  Replace zeros initialization with small values
            bias = torch.as_tensor(bias, dtype=self.dtype)
            bias[bias == 0] = torch.normal(
                mean=0.0, std=1e-2, size=bias.shape, dtype=self.dtype
            )[bias == 0]
            with torch.no_grad():
                tensor.view(self.output_features, -1).data[:, -1] = bias.data

    def tune(self) -> None:
        """Freeze layers not needed to fine-tune model."""
        for param in self.parameters():
            param.requires_grad_(False)
        for block in self.trunk_net:
            block.layers[0].weight.requires_grad = True
            block.layers[0].bias.requires_grad = True
        self.branch_net[-1].layers[0].bias.requires_grad = True


class UnstackedMHDNet(Model):
    """
    A model with a MHDNet per quantity.

    TODO-2(@amerlo): add all HPs here, update net construction to reflect this
    TODO-2(@amerlo): consider to have network building in utils function somewhere
    """

    #  Expected keys for model HPs
    _keys = ("R", "lambda", "Z", "iota")

    def __init__(
        self,
        *args,
        fourier_encoding: Optional[str] = None,
        fourier_features: Optional[int] = 16,
        fourier_sigma: Optional[float] = 1.0,
        trunk_width: Optional[int] = 32,
        trunk_depth: Optional[int] = 1,
        trunk_factor: Optional[float] = 1.0,
        trunk_bias: Optional[bool] = False,
        trunk_basis: Optional[int] = None,
        trunk_batchnorm: Optional[bool] = False,
        trunk_layernorm: Optional[bool] = False,
        trunk_activation: Optional[DictConfig] = {},
        shared_trunk: Optional[bool] = False,
        bias_order: Optional[int] = 2,
        conv_width: Optional[int] = 8,
        conv_depth: Optional[int] = 1,
        conv_factor: Optional[float] = 1.0,
        conv_bias: Optional[bool] = True,
        conv_batchnorm: Optional[bool] = False,
        conv_layernorm: Optional[bool] = False,
        conv_activation: Optional[DictConfig] = {},
        conv_kernel_size: Optional[int] = 7,
        conv_stride: Optional[int] = 2,
        conv_padding: Optional[int] = 0,
        pool_kernel_size: Optional[int] = 3,
        pool_stride: Optional[int] = 2,
        pool_padding: Optional[int] = 0,
        branch_width: Optional[int] = 32,
        branch_depth: Optional[int] = 1,
        branch_factor: Optional[float] = 1.0,
        branch_bias: Optional[bool] = True,
        branch_batchnorm: Optional[bool] = False,
        branch_layernorm: Optional[bool] = False,
        branch_activation: Optional[DictConfig] = {},
        multiply_profile_features: Optional[bool] = False,
        **kwargs,
    ) -> None:

        super().__init__(*args, **kwargs)
        self.save_hyperparameters(
            "fourier_encoding",
            "fourier_features",
            "fourier_sigma",
            "conv_width",
            "conv_depth",
            "conv_factor",
            "conv_bias",
            "conv_batchnorm",
            "conv_layernorm",
            "conv_activation",
            "conv_kernel_size",
            "conv_stride",
            "conv_padding",
            "pool_kernel_size",
            "pool_stride",
            "pool_padding",
            "branch_width",
            "branch_depth",
            "branch_factor",
            "branch_bias",
            "branch_batchnorm",
            "branch_layernorm",
            "branch_activation",
            "trunk_width",
            "trunk_depth",
            "trunk_factor",
            "trunk_bias",
            "trunk_batchnorm",
            "trunk_layernorm",
            "trunk_activation",
            "trunk_basis",
            "shared_trunk",
            "bias_order",
            "multiply_profile_features",
        )

        def _validate_hp(hp):
            if isinstance(hp, (DictConfig, dict)):
                for k in self._keys:
                    assert k in hp
                return hp
            return {k: hp for k in self._keys}

        #  Validate HPs
        (
            branch_width,
            trunk_width,
            trunk_depth,
            trunk_basis,
            shared_trunk,
            bias_order,
        ) = map(
            _validate_hp,
            (
                branch_width,
                trunk_width,
                trunk_depth,
                trunk_basis,
                shared_trunk,
                bias_order,
            ),
        )

        #  Compute number of outputs per quantity (i.e., X)
        self._output_features = {}
        num_xmn = (self.mpol + 1) * (2 * self.ntor + 1) - self.ntor
        self._output_features["R"] = num_xmn
        self._output_features["lambda"] = num_xmn - 1
        self._output_features["Z"] = num_xmn - 1
        self._output_features["iota"] = 1
        assert self.output_features == sum(self._output_features.values())

        #  Order of the polynomial bias (i.e., skip connections)
        self.bias_order = bias_order

        #  Number of output dimensions for each trunk net
        self._trunk_output_dim = {
            k: 1 if shared_trunk[k] else self._output_features[k] for k in self._keys
        }

        #  Number of output features for each trunk net
        self._trunk_output_features = {
            k: 0
            if trunk_depth[k] == -1
            else (trunk_basis[k] if trunk_basis[k] is not None else trunk_width[k])
            for k in self._keys
        }

        #  Number of branch coefficients per dimensions
        #  trunk_basis + bias_order + 0th bias term
        self._branch_features_per_output = {
            k: self._trunk_output_features[k] + self.bias_order[k] + 1
            for k in self._keys
        }

        #  Model internal functions
        self._trunk_net_fn = {}
        self._branch_net_fn = {}

        #  Set trunk nets
        for k in self._keys:

            if trunk_depth[k] == -1:
                setattr(self, f"trunk_net_{k}", None)
            else:
                trunk_output_units = (
                    self._trunk_output_dim[k] * self._trunk_output_features[k]
                )
                setattr(
                    self,
                    f"trunk_net_{k}",
                    net(
                        input_features=1,
                        fourier_encoding=fourier_encoding,
                        fourier_features=fourier_features,
                        fourier_sigma=fourier_sigma,
                        width=trunk_width[k],
                        depth=trunk_depth[k],
                        factor=trunk_factor,
                        bias=trunk_bias,
                        batchnorm=trunk_batchnorm,
                        layernorm=trunk_layernorm,
                        gain_factor=1.0,
                        activation=trunk_activation,
                        use_linear_for_last_layer=False,
                        output_features=trunk_output_units,
                    ),
                )

        #  Define runk net functions
        def _get_trunk_with_net(k: str):

            trunk_net = getattr(self, f"trunk_net_{k}")

            #  trunk_net is (-1, output_dim, output_features)
            trunk_shape = (
                self._trunk_output_dim[k],
                self._trunk_output_features[k],
            )

            #  Build function to apply trunk net
            def _trunk_with_net(s: Tensor) -> Tensor:
                #  Scale s to be in [-1, 1] to improve training
                trunk = trunk_net(2 * s - 1)
                trunk = torch.cat(
                    [trunk.view(-1, *trunk_shape)]
                    + [
                        s.view(-1, 1, 1).expand(-1, trunk_shape[0], -1) ** i
                        for i in range(1, self.bias_order[k] + 1)
                    ],
                    dim=-1,
                )
                return trunk

            return _trunk_with_net

        def _get_trunk_without_net(k: str):

            #  trunk_net is (-1, output_dim, output_features)
            trunk_shape = (
                self._trunk_output_dim[k],
                self._trunk_output_features[k],
            )

            def _trunk_without_net(s: Tensor) -> Tensor:
                return torch.cat(
                    [
                        s.view(-1, 1, 1).expand(-1, trunk_shape[0], -1) ** i
                        for i in range(1, self.bias_order[k] + 1)
                    ],
                    dim=-1,
                )

            return _trunk_without_net

        #  Set trunk net functions
        for k in self._keys:
            if trunk_depth[k] == -1:
                self._trunk_net_fn[k] = _get_trunk_without_net(k)
            else:
                self._trunk_net_fn[k] = _get_trunk_with_net(k)

        #  Number of features for profile net
        self._profile_in_channels = 2
        conv_input_features = self.ns * self._profile_in_channels

        #  Number of scalar input features
        scalar_input_features = self.input_features - 1 - conv_input_features

        #  Define profile and branch nets
        for k in self._keys:

            if self.overfit:
                setattr(self, f"profile_net_{k}", None)
                setattr(
                    self,
                    f"branch_net_{k}",
                    torch.nn.Parameter(
                        torch.randn(
                            self._output_features[k],
                            self._branch_features_per_output[k],
                        ),
                        requires_grad=True,
                    ),
                )
            else:
                profile_out_channels = None
                if multiply_profile_features:
                    profile_out_channels = scalar_input_features
                setattr(
                    self,
                    f"profile_net_{k}",
                    convnet(
                        in_channels=self._profile_in_channels,
                        out_channels=profile_out_channels,
                        width=conv_width,
                        depth=conv_depth,
                        factor=conv_factor,
                        conv_kernel_size=conv_kernel_size,
                        conv_stride=conv_stride,
                        conv_padding=conv_padding,
                        pool_kernel_size=pool_kernel_size,
                        pool_stride=pool_stride,
                        pool_padding=pool_padding,
                        bias=conv_bias,
                        batchnorm=conv_batchnorm,
                        layernorm=conv_layernorm,
                        activation=conv_activation,
                    ),
                )

                profile_out_length = compute_conv_out_length(
                    in_length=self.ns,
                    depth=conv_depth,
                    conv_kernel_size=conv_kernel_size,
                    conv_stride=conv_stride,
                    conv_padding=conv_padding,
                    pool_kernel_size=pool_kernel_size,
                    pool_stride=pool_stride,
                    pool_padding=pool_padding,
                )

                branch_output_features = (
                    self._output_features[k] * self._branch_features_per_output[k]
                )

                if multiply_profile_features:
                    branch_input_features = scalar_input_features * profile_out_length
                else:
                    branch_input_features = (
                        scalar_input_features
                        + profile_out_length
                        * getattr(self, f"profile_net_{k}")[-1]
                        .layers[0]
                        .weight.shape[0]
                    )

                setattr(
                    self,
                    f"branch_net_{k}",
                    net(
                        input_features=branch_input_features,
                        width=branch_width[k],
                        depth=branch_depth,
                        factor=branch_factor,
                        bias=branch_bias,
                        batchnorm=branch_batchnorm,
                        layernorm=branch_layernorm,
                        activation=branch_activation,
                        use_linear_for_last_layer=True,
                        gain_factor=1e-1,
                        output_features=branch_output_features,
                    ),
                )

            #  Initialization s**n terms (e.g., 1st, 2nd order biases)
            if self.overfit:
                bias = getattr(self, f"branch_net_{k}")
            else:
                bias = getattr(self, f"branch_net_{k}")[-1].layers[0].bias
            torch.nn.init.normal_(bias, std=1e-2)
            with torch.no_grad():
                bias = bias.view(self._output_features[k], -1)
                #  force a ~ -s dependency for x_0n (i.e., mpol=0 modes)
                if k == "R":
                    end = self.ntor + 1
                elif k == "iota":
                    end = -1
                else:
                    end = self.ntor
                bias[:end, -bias_order[k] - 1 : -1] *= -torch.sign(
                    bias[:end, -bias_order[k] - 1 : -1]
                )

        #  Define branch net functions
        def _get_full_branch_net(k: str):

            profile_net = getattr(self, f"profile_net_{k}")
            branch_net = getattr(self, f"branch_net_{k}")

            def _full_branch_net(x: Dict[str, Tensor]) -> Tensor:

                # Apply profile head
                p = torch.cat([x["pressure"], x["toroidal_current"]], dim=-1).view(
                    -1, self._profile_in_channels, self.ns
                )
                p = profile_net(p)

                #  Multiply by scalar features
                u = torch.cat(
                    [
                        v
                        for k, v in x.items()
                        if k not in ("normalized_flux", "pressure", "toroidal_current")
                    ],
                    dim=-1,
                )
                if multiply_profile_features:
                    u = u.view(-1, scalar_input_features, 1)
                    u = torch.mul(u, p).flatten(start_dim=1)
                else:
                    u = torch.cat([u, p.flatten(start_dim=1)], dim=-1)

                #  Apply branch net
                return branch_net(u)

            return _full_branch_net

        def _get_overfit_branch_net(k: str):

            branch_net = getattr(self, f"branch_net_{k}")

            def _overfit_branch_net(x: Dict[str, Tensor]) -> Tensor:
                return branch_net

            return _overfit_branch_net

        #  Set branch net functions
        for k in self._keys:
            if self.overfit:
                self._branch_net_fn[k] = _get_overfit_branch_net(k)
            else:
                self._branch_net_fn[k] = _get_full_branch_net(k)

        if self.to_double:
            self.double()

    def forward(self, x: Dict[str, Tensor]) -> Dict[str, Tensor]:

        s = x["normalized_flux"]

        xmns = []

        for k in ("R", "lambda", "Z"):

            trunk_net_fn = self._trunk_net_fn[k]
            branch_net_fn = self._branch_net_fn[k]

            #  Apply trunk net and skip connection
            trunk = trunk_net_fn(s)

            #  Apply branch net
            branch = branch_net_fn(x).view(
                -1,
                self._output_features[k],
                self._branch_features_per_output[k],
            )

            y = torch.mul(branch[..., :-1], trunk).sum(dim=2)

            #  Get 0th order bias
            y0 = branch[..., -1]

            #  xmn
            dtype = y.dtype
            device = y.device
            xmn = [
                #  m < 0 Fourier coefficients
                torch.zeros(y.size(0), self.ntor, dtype=dtype, device=device),
            ]
            if k == "R":
                #  rmnc_00
                xmn.append(y0[:, :1] * (1 + y[:, :1]))
            else:
                #  lmns_00 and zmns_00
                xmn.append(torch.zeros(y.size(0), 1, dtype=dtype, device=device))
            if k == "R":
                bias = 1
            else:
                bias = 0
            start = bias
            end = bias + self.ntor
            #  m=0
            xmn.append(y0[:, start:end] * (1 + y[:, start:end]))

            #  m > 0
            #  Use small eps to avoid nan when taking gradients
            rho = torch.sqrt(s + eps)
            for m in range(1, self.mpol + 1):
                start = bias + (self.ntor + (m - 1) * (2 * self.ntor + 1))
                end = bias + (self.ntor + m * (2 * self.ntor + 1))
                xmn.append(rho**m * y0[:, start:end] * (1 + y[:, start:end]))
            xmn = torch.cat(xmn, dim=1)

            xmn = xmn.view(-1, *self._xmn_shape[:-1])
            xmns.append(xmn)

        #  Merge all together xmn by stacking them along last dimension
        xmn = torch.stack(xmns, dim=-1)

        #  Set lambda to zero on axis
        xmn[:, :, :, 1] *= (s != 0).view(-1, 1, 1)

        #  iota
        #  See Strand2001 (eq. 8)
        #  iota = mu0 * I / (S11 * phi_edge) - S12 / S11
        #  branch is (bs, out_features, branch coefficients)
        #  branch[..., -1] is y0
        #  branch[..., -2] is the bias_order coefficients
        #  for iota, we replace th s**bias term with toroidal_current

        k = "iota"

        trunk_net_fn = self._trunk_net_fn[k]
        branch_net_fn = self._branch_net_fn[k]

        trunk = trunk_net_fn(s)
        branch = branch_net_fn(x).view(
            -1,
            self._output_features[k],
            self._branch_features_per_output[k],
        )
        y = torch.mul(branch[..., :-1], trunk).sum(dim=2)
        y0 = branch[..., -1]

        toroidal_current = (
            gather(x["toroidal_current"], dim=1, value=s) * x["curtor"]
        ).view(-1)
        y = torch.mul(branch[:, 0, :-2], trunk[:, 0, :-1]).sum(dim=1)
        iota = y0[:, 0] * (1 + branch[:, 0, -2] * toroidal_current + y)
        iota = iota.view(-1, 1)

        return {"xmn": xmn, "iota": iota}

    def init_bias(self, bias: Union[str, List[float]]) -> None:
        """Init model last layer bias."""
        if bias == "zeros":
            #  Replace zeros initialization with small values
            std = 1e-2
            if self.overfit:
                for k in self._keys:
                    tensor = getattr(self, f"branch_net_{k}")
                    torch.nn.init.normal_(tensor, std=std)
            else:
                for k in self._keys:
                    tensor = getattr(self, f"branch_net_{k}")[-1].layers[0].bias
                    torch.nn.init.normal_(tensor, std=std)
        elif bias == "ones":
            if self.overfit:
                for k in self._keys:
                    tensor = getattr(self, f"branch_net_{k}")
                    torch.nn.init.ones_(tensor)
            else:
                for k in self._keys:
                    tensor = getattr(self, f"branch_net_{k}")[-1].layers[0].bias
                    torch.nn.init.ones_(tensor)
        else:
            #  Replace zeros initialization with small values
            bias = torch.as_tensor(bias, dtype=self.dtype)
            bias[bias == 0] = torch.normal(
                mean=0.0, std=1e-2, size=bias.shape, dtype=self.dtype
            )[bias == 0]
            with torch.no_grad():
                if self.overfit:
                    self.branch_net_R.data[0, -1] = bias[0]
                    self.branch_net_R.data[1:, -1] = bias[1:-1].view(-1, COMP)[:, 0]
                    self.branch_net_lambda.data[:, -1] = bias[1:-1].view(-1, COMP)[:, 1]
                    self.branch_net_Z.data[:, -1] = bias[1:-1].view(-1, COMP)[:, 2]
                    self.branch_net_iota.data[:, -1] = bias[-1]
                else:
                    #  rmnc_00
                    self.branch_net_R[-1].layers[0].bias.view(
                        self._output_features["R"], -1
                    ).data[0, -1] = bias[0]
                    #  xmn
                    self.branch_net_R[-1].layers[0].bias.view(
                        self._output_features["R"], -1
                    ).data[1:, -1] = bias[1:-1].view(-1, COMP)[:, 0]
                    self.branch_net_lambda[-1].layers[0].bias.view(
                        self._output_features["lambda"], -1
                    ).data[:, -1] = bias[1:-1].view(-1, COMP)[:, 1]
                    self.branch_net_Z[-1].layers[0].bias.view(
                        self._output_features["Z"], -1
                    ).data[:, -1] = bias[1:-1].view(-1, COMP)[:, 2]
                    #  iota
                    self.branch_net_iota[-1].layers[0].bias.view(
                        self._output_features["iota"], -1
                    ).data[:, -1] = bias[-1]
