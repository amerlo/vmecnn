"""Entry point to generate figures."""

import ast
import glob
import os
from copy import deepcopy
from typing import Optional, List, Union, Tuple

import hydra
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import torch
from omegaconf import DictConfig, OmegaConf, ListConfig

from w7x_datagen.datasets import VmecEquilibria

from vmecnn.datamodules.vmec_equilibria import VmecEquilibriaDataModule
from vmecnn.physics.equilibrium import Equilibrium
from vmecnn.metrics.metric import (
    _MeanSquaredError,
    MeanSquaredError,
    FluxSurfaceError,
    MeanAbsolutePercentageError,
)
from vmecnn.utils import get_logger, flatten, asarray
from vmecnn.plotting.utils import (
    savefig,
    SMALL_SQUARED_FIGSIZE,
    MEDIUM_THREETWO_FIGSIZE,
    MEDIUM_SQUARED_FIGSIZE,
    LARGE_THREETWO_FIGSIZE,
    LARGE_SQUARED_FIGSIZE,
)


def to_list(obj):
    if isinstance(obj, str):
        return [obj]
    return list(obj)


def load_data(cfg: DictConfig) -> VmecEquilibria:
    """Load VmecEquilibria dataset from config."""
    root = os.path.join(cfg.orig_cwd, cfg.datamodule.root)
    return VmecEquilibria(root=root, train_test_split=cfg.datamodule.fraction)


def load_datamodule(cfg: DictConfig) -> VmecEquilibriaDataModule:
    """Load VmecEquilibriaDataModule from config."""

    log = get_logger(__name__)

    root = os.path.join(cfg.orig_cwd, cfg.datamodule.root)

    datamodule = hydra.utils.instantiate(
        cfg.datamodule,
        root=root,
        mpol=cfg.mpol,
        ntor=cfg.ntor,
        crop=True,
        zeros_input=cfg.zeros_input,
    )
    log.info("DataModule: %s" % datamodule)

    datamodule.tune()
    datamodule.setup()
    return datamodule


def load_experiments(experiments: List[str]):
    """Read and merge data from experiments."""

    log = get_logger(__name__)

    #  Expand experiment path
    _experiments = []
    for e in experiments:
        if "*" in e:
            _experiments.extend(glob.glob(e))
        else:
            _experiments.append(e)
    experiments = _experiments

    df = []
    for e in experiments:

        logs_dir = os.path.join(e, "logs")
        if not os.path.isdir(logs_dir):
            continue

        #  Get logger foler name
        logger_dir = None
        for _, dirs, _ in os.walk(logs_dir):
            assert len(dirs) == 1
            logger_dir = dirs[0]
            break

        #  Read metrics
        metrics_file = os.path.join(e, "logs", logger_dir, "version_1", "metrics.csv")
        if not os.path.isfile(metrics_file):
            continue
        exp_df = pd.read_csv(metrics_file)

        #  TODO-0(@amerlo): fix logging and remove this line
        #  Remove empty rows due to logging frequency
        exp_df = exp_df[~exp_df["train/loss"].isnull()]

        #  Try to parse list
        def _str_to_list(x):
            if not isinstance(x, str):
                return x
            if "[" not in x and "]" not in x:
                return x
            return ast.literal_eval(x)

        exp_df = exp_df.applymap(_str_to_list)

        #  Add hparams
        hparams_file = os.path.join(e, "logs", logger_dir, "version_1", "hparams.yaml")
        hparams = flatten(OmegaConf.load(hparams_file))

        for h, v in hparams.items():
            exp_df[h] = [v] * len(exp_df)

        #  Add experiment
        exp_df["experiment"] = e

        df.append(exp_df)

        log.info("Experiment %s has been loaded." % e)

    return pd.concat(df)


def plot_inputs(cfg: DictConfig):
    """Plot input features as seen by the model."""
    log = get_logger(__name__)
    datamodule = load_datamodule(cfg)
    log.info("Transforms: %s" % datamodule.train_data.dataset.dataset.transforms)
    fig = datamodule.plot(keys=datamodule.features, stage="fit")
    fig.subplots_adjust(hspace=0.5)
    return fig


def plot_outputs(cfg: DictConfig):
    """Plot output features as seen by the model."""
    datamodule = load_datamodule(cfg)
    fig = datamodule.plot(keys=datamodule.labels, stage="fit")
    fig.subplots_adjust(hspace=0.5)
    return fig


def plot_raw_quantiles(cfg: DictConfig, fraction: List[float]):
    """Plot quantiles of raw input features."""
    quantiles = ("q5", "q25", "median", "q75", "q95")
    values = {k: {q: [] for q in quantiles} for k in cfg.datamodule.features}

    for f in fraction:
        cfg.datamodule.fraction = f
        cfg.datamodule.tune_fraction = 1.0
        datamodule = load_datamodule(cfg)
        for k in cfg.datamodule.features:
            for q in quantiles:
                param = datamodule.get_params({k: [q]})
                param = param[0][k]
                values[k][q].append(param)

    ncols = int(np.ceil(np.sqrt(len(values.keys()))))
    nrows = int(np.ceil(len(values.keys()) / ncols))
    fig, axs = plt.subplots(
        nrows=nrows,
        ncols=ncols,
        squeeze=0,
        tight_layout=True,
        figsize=LARGE_THREETWO_FIGSIZE,
    )

    x = np.asarray(fraction)
    for k, ax in zip(values.keys(), axs.reshape(-1)):
        for q in quantiles:
            y = np.asarray(values[k][q]).reshape(-1)
            ax.plot(x, y, "--", label=q)
        ax.set_title(k)

    #  Plot legend only for last plot
    ax.legend()

    return fig


def plot_raw_data(cfg: DictConfig, num_samples: Optional[int] = None):
    """Plot raw data distributions."""

    data = load_data(cfg)

    #  TODO-1(@amerlo): move figures definition in yaml files
    figures = [
        #  Input features histograms
        ("phiedge [Wb]", "phiedge"),
        ("beta", "beta"),
        ("volume [m3]", "volume"),
        ("iota0", lambda item: item["iota"][0]),
        ("iotab", lambda item: item["iota"][-1]),
        ("I1 [kA]", lambda item: item["extcur"][0]),
        ("curtor [10kA]", lambda item: item["curtor"] / 1e4),
        ("p0 [MPa]", lambda item: item["pressure"][0] / 1e6),
        ("iA", lambda item: item["extcur"][5] / item["extcur"][0]),
        ("iB", lambda item: item["extcur"][6] / item["extcur"][0]),
        ("i2", lambda item: item["extcur"][1] / item["extcur"][0]),
        ("i3", lambda item: item["extcur"][2] / item["extcur"][0]),
        ("i4", lambda item: item["extcur"][3] / item["extcur"][0]),
        ("i5", lambda item: item["extcur"][4] / item["extcur"][0]),
        ("pressure", lambda item: item["pressure"] / item["pressure"][0]),
        (
            "toroidal current",
            lambda item: item["toroidal_current"] / item["toroidal_current"][-1],
        ),
        #  Input features scatter plots
        (
            "i2 vs i3",
            lambda item: (
                item["extcur"][1] / item["extcur"][0],
                item["extcur"][2] / item["extcur"][0],
            ),
        ),
        (
            "i2 vs i4",
            lambda item: (
                item["extcur"][1] / item["extcur"][0],
                item["extcur"][3] / item["extcur"][0],
            ),
        ),
        (
            "i2 vs i5",
            lambda item: (
                item["extcur"][1] / item["extcur"][0],
                item["extcur"][4] / item["extcur"][0],
            ),
        ),
        (
            "iA vs iB",
            lambda item: (
                item["extcur"][5] / item["extcur"][0],
                item["extcur"][6] / item["extcur"][0],
            ),
        ),
        (
            "p0 [MPa] vs curtor [10kA]",
            lambda item: (item["pressure"][0] / 1e6, item["curtor"] / 1e4),
        ),
        (
            "phiedge vs i2",
            lambda item: (item["phiedge"], item["extcur"][1] / item["extcur"][0]),
        ),
        (
            "phiedge vs i3",
            lambda item: (item["phiedge"], item["extcur"][2] / item["extcur"][0]),
        ),
        (
            "phiedge vs i4",
            lambda item: (item["phiedge"], item["extcur"][3] / item["extcur"][0]),
        ),
        (
            "phiedge vs i5",
            lambda item: (item["phiedge"], item["extcur"][4] / item["extcur"][0]),
        ),
        (
            "phiedge vs iA",
            lambda item: (item["phiedge"], item["extcur"][5] / item["extcur"][0]),
        ),
        (
            "phiedge vs iB",
            lambda item: (item["phiedge"], item["extcur"][6] / item["extcur"][0]),
        ),
        #  Output features histograms
        ("iota", lambda item: item["iota"]),
        #  Output features scatter plots
        (
            "iota0 vs p0 [MPa]",
            lambda item: (item["pressure"][0] / 1e6, item["iota"][0]),
        ),
        (
            "iota0 vs curtor [10kA]",
            lambda item: (item["curtor"] / 1e4, item["iota"][0]),
        ),
        (
            "iotab vs curtor [10kA]",
            lambda item: (item["curtor"] / 1e4, item["iota"][-1]),
        ),
        (
            "iotab vs phiedge [Wb]",
            lambda item: (item["phiedge"], item["iota"][-1]),
        ),
        (
            "iota0 vs peaking factor",
            lambda item: (
                item["pressure"][0] / item["pressure"].mean(),
                item["iota"][0],
            ),
        ),
    ]

    ncols = int(np.ceil(np.sqrt(len(figures))))
    nrows = int(np.ceil(len(figures) / ncols))
    fig, axs = plt.subplots(
        nrows=nrows,
        ncols=ncols,
        squeeze=0,
        tight_layout=True,
        figsize=LARGE_THREETWO_FIGSIZE,
    )

    for (title, feature), ax in zip(figures, axs.reshape(-1)):
        data.plot(feature, ax=ax, kind="scatter", num_samples=num_samples)
        ax.set_title(title)

    return fig


def histplot(
    cfg: DictConfig,
    q: str,
    label: Optional[str] = None,
    log_scale: Optional[bool] = False,
    plot_kw: Optional[dict] = None,
):
    """Build a histogram plot for equilibrium property."""

    data = load_data(cfg)

    x = []
    for i, d in enumerate(data):
        equi = Equilibrium.from_dict(d)
        x.append(getattr(equi, q))

    x = asarray(x).reshape(-1)

    fig, ax = plt.subplots(1, 1, figsize=SMALL_SQUARED_FIGSIZE, tight_layout=True)
    ax.hist(x, **plot_kw)
    ax.set_xlabel(label)
    ax.set_ylabel("Counts")

    if log_scale:
        ax.set_xscale("log")

    return ax.get_figure()


def scatterplot(
    cfg: DictConfig,
    experiments: Union[str, List[str]],
    x: Union[str, List[str]],
    y: Union[str, List[str]],
    **kwargs,
):
    """Generic scatter plot for given experiments."""

    if isinstance(experiments, str):
        experiments = [experiments]

    #  Use root path for each experiment
    experiments = [os.path.join(cfg.orig_cwd, e) for e in experiments]
    df = load_experiments(experiments)

    #  Add column if strings are passed
    x_label = x
    if isinstance(x, (list, tuple, ListConfig)):
        x_label = str(tuple(x))
        df[x_label] = df[x].sum(axis=1)

    y_label = y
    if isinstance(y, (list, tuple, ListConfig)):
        y_label = str(tuple(y))
        df[y_label] = df[y].sum(axis=1)

    fig, ax = plt.subplots(1, 1, figsize=MEDIUM_THREETWO_FIGSIZE)
    sns.scatterplot(x=x_label, y=y_label, data=df, ax=ax, **kwargs)
    ax.set_xscale("log")
    ax.set_yscale("log")

    return ax.get_figure()


def lineplot(
    cfg: DictConfig,
    experiments: Union[str, List[str]],
    x: Union[str, List[str]],
    y: str,
    **kwargs,
):
    """
    Plot (x, y) line for given experiments.
    """

    if isinstance(experiments, str):
        experiments = [experiments]

    #  Use root path for each experiment
    experiments = [os.path.join(cfg.orig_cwd, e) for e in experiments]
    df = load_experiments(experiments)

    x_cols, y_cols = map(to_list, (x, y))

    #  Merge x label if needed
    if isinstance(x, (list, ListConfig)):
        x = str(tuple(x)).replace("'", "")
        df[x] = df[x_cols].apply(lambda x: str(tuple(x)), axis=1)

    #  Explode along y to plot ci intervals
    df = df.explode(y, ignore_index=True)

    ax = sns.lineplot(x=x, y=y, data=df, **kwargs)
    ax.set_yscale("log")

    return ax.get_figure()


class RandomCoordinates:
    def __init__(self, origin: List[torch.Tensor]):
        def _normalize_coord(coord, origin):
            return coord / torch.linalg.norm(coord) * torch.abs(origin)

        self.origin = origin
        self._v0 = [
            _normalize_coord(torch.randn(x.shape, dtype=x.dtype), x) for x in origin
        ]
        self._v1 = [
            _normalize_coord(torch.randn(x.shape, dtype=x.dtype), x) for x in origin
        ]

    def __call__(self, a: float, b: float):
        return [
            x + a * v0 + b * v1 for x, v0, v1 in zip(self.origin, self._v0, self._v1)
        ]


def plot_loss_manifold(
    cfg: DictConfig,
    q: str,
    dim: int = 1,
    num: int = 11,
    factor: float = 1,
    log: bool = False,
    num_slices: int = 1,
    kind: str = None,
    wout: str = None,
    plot_kw: dict = None,
):
    """
    Visualize a generic loss manifold across equilibrium random Gaussian direction.

    See: https://arxiv.org/pdf/1712.09913.pdf
    """

    assert dim == 1 or dim == 2
    assert kind in (None, "pareto", "3d")

    if kind == "pareto":
        assert dim == 1

    if num_slices != 1:
        assert dim == 1

    data = load_data(cfg)

    #  If not given, get random equilibrium
    if wout is not None:
        index = data.runs.index(wout)
    else:
        index = np.random.random_integers(0, len(data) - 1)
    equi = Equilibrium.from_dict(data[index])

    iota_metric = MeanSquaredError(squared=False, keys=["iota"])
    flux_surface_metric = FluxSurfaceError(ntheta=32, nzeta=36)

    true = equi.as_dict()

    a_grid = np.linspace(-1, 1, num) ** 3 * factor
    if dim == 1:
        b_grid = np.zeros(1)
    else:
        b_grid = np.linspace(-1, 1, num) ** 3 * factor

    loss_grid = np.empty([num_slices, len(a_grid), len(b_grid)])
    iota_err_grid = np.empty([num_slices, len(a_grid), len(b_grid)])
    flux_surface_err_grid = np.empty([num_slices, len(a_grid), len(b_grid)])

    for n in range(num_slices):
        coord = RandomCoordinates([equi.iota, equi.rmnc, equi.lmns, equi.zmns])
        for i, a in enumerate(a_grid):
            for j, b in enumerate(b_grid):

                loc = coord(a, b)

                #  Set location into equilibrium
                equi_ = deepcopy(equi)
                equi_.iota = loc[0]
                equi_.rmnc = loc[1]
                equi_.lmns = loc[2]
                equi_.zmns = loc[3]

                #  Compute value
                value = getattr(equi_, q)
                if q not in ("mean_f", "mean_F"):
                    value = torch.sqrt(((value - getattr(equi, q)) ** 2).mean())
                loss_grid[n, i, j] = value.item()

                #  Compute iota and flux surface rmse
                pred = equi_.as_dict()
                iota_err_grid[n, i, j] = iota_metric(pred, true).item()
                flux_surface_err_grid[n, i, j] = flux_surface_metric(pred, true).item()

    if log:
        loss_grid = np.log10(loss_grid)

    #  Log estimate of the condition number
    #  P ~ deltaf(X) / deltaX
    x = np.log10(flux_surface_err_grid)
    index = x != -np.inf
    x = x[index]
    y = loss_grid if log else np.log10(loss_grid)
    y = y[index]
    linear = np.polynomial.Polynomial.fit(x.reshape(-1), y.reshape(-1), deg=1)
    log = get_logger(__name__)
    log.info(f"P ~ 10 ** {linear.coef[1]:.2f}")

    fig, ax = plt.subplots(
        1,
        1,
        figsize=MEDIUM_SQUARED_FIGSIZE if dim == 1 else LARGE_SQUARED_FIGSIZE,
        tight_layout=True,
        subplot_kw={"projection": "3d" if kind == "3d" else None},
    )

    #  1D plot
    if dim == 1:
        if kind == "pareto":
            ax.scatter(flux_surface_err_grid[..., 0], loss_grid[..., 0])
            #  Add unperturbed loss
            origin = int((num - 1) / 2)
            ax.plot(
                [
                    flux_surface_err_grid[..., 0].min(),
                    flux_surface_err_grid[..., 0].max(),
                ],
                [loss_grid[0, origin, 0].min(), loss_grid[0, origin, 0].min()],
                "--r",
                label="VMEC",
                **plot_kw,
            )
            ax.set_xscale("log")
            ax.set_xlabel("Flux Surface RMSE [m]")
            ax.set_ylabel(r"$\langle f \rangle$")
            ax.legend()
        else:
            for n in range(num_slices):
                ax.plot(
                    a_grid, loss_grid[n, :, 0], "k", label="<f>" if n == 0 else None
                )
                ax.plot(
                    a_grid,
                    iota_err_grid[n, :, 0],
                    "r",
                    label="iota rmse" if n == 0 else None,
                    **plot_kw,
                )
                ax.plot(
                    a_grid,
                    flux_surface_err_grid[n, :, 0],
                    "b",
                    label="flux surface rmse" if n == 0 else None,
                    **plot_kw,
                )
            ax.set_xlabel("v0")
            ax.legend()
        ax.set_yscale("log")
        ax.set_title(r"$\langle f \rangle$" + " scaling")
    else:
        if kind == "3d":
            ax.contourf3D(a_grid, b_grid, loss_grid[0], **plot_kw)
        else:
            cs = ax.contour(a_grid, b_grid, loss_grid[0], **plot_kw)
            ax.axis("equal")
            ax.clabel(cs, inline=True, fontsize=8, fmt="%1.2e")
        ax.set_xlabel("v0")
        ax.set_ylabel("v1")
        ax.set_title(r"$\langle f \rangle$" + " manifold")

    return fig


def plot_fourier_scaling(
    cfg: DictConfig,
    q: str,
    modes: List[Tuple[int]] = [(3, 2), (6, 4), (8, 6), (10, 8), (11, 8)],
    num_samples: int = 1,
    kind: str = None,
    loss: str = "rmse",
):
    """Visualize how a quantity `q` scales with the Fourier budget."""

    data = load_data(cfg)

    assert loss in ("rmse", "mape")
    if loss == "rmse":
        loss_metric = _MeanSquaredError(squared=False)
        loss_label = "RMSE"
    else:
        loss_metric = MeanAbsolutePercentageError()
        loss_label = "MAPE"

    flux_surface_metric = FluxSurfaceError(ntheta=32, nzeta=36)

    loss = np.empty([num_samples, len(modes)])
    flux_surface_err_grid = np.empty([num_samples, len(modes)])

    for i in range(num_samples):

        #  Get random equilibrium
        equi = Equilibrium.from_dict(data[np.random.random_integers(0, len(data) - 1)])

        #  Copy equilibrium to work on
        equi_ = deepcopy(equi)

        #  Compute true attribute
        true = equi.as_dict()
        true_attr = getattr(equi, q)

        for j, (mpol, ntor) in enumerate(modes):

            #  Reset cached equilibrium computations
            equi_.neo = None
            equi_._has_bx_run = False

            #  Crop equilibrium
            equi_.rmnc = equi.rmnc[:, 0 : mpol + 1, 12 - ntor : 12 + ntor + 1]
            equi_.lmns = equi.lmns[:, 0 : mpol + 1, 12 - ntor : 12 + ntor + 1]
            equi_.zmns = equi.zmns[:, 0 : mpol + 1, 12 - ntor : 12 + ntor + 1]

            #  Compute loss
            pred_attr = getattr(equi_, q)
            loss[i, j] = loss_metric(pred_attr, true_attr).item()

            #  Compute flux surface rmse
            pred = equi_.as_dict()
            flux_surface_err_grid[i, j] = flux_surface_metric(pred, true).item()

    #  Log estimate of convergence
    x = np.log10(flux_surface_err_grid)
    index = x != -np.inf
    x = x[index]
    y = np.log10(loss)
    y = y[index]
    linear = np.polynomial.Polynomial.fit(x.reshape(-1), y.reshape(-1), deg=1)
    log = get_logger(__name__)
    log.info(f"10 ** {q} ~ 10 ** ({linear.coef[1]:.2f} * ...)")

    fig, ax = plt.subplots(1, 1, figsize=LARGE_THREETWO_FIGSIZE)
    y_label = f"{q} {loss_label}"

    if kind == "pareto":
        colors = [list(range(len(modes)))] * num_samples
        ax.scatter(flux_surface_err_grid.flatten(), loss.flatten(), c=colors)
        ax.set_xscale("log")
        ax.set_yscale("log")
        ax.set_xlabel("Flux Surface RMSE [m]")
        ax.set_ylabel(y_label)
    else:
        #  Compute stats
        median = np.median(loss, axis=0)
        q5 = np.quantile(loss, q=0.05, axis=0)
        q95 = np.quantile(loss, q=0.95, axis=0)
        x = np.arange(len(median))
        ax.plot(x, median)
        ax.fill_between(x, q5, q95, alpha=0.2)
        ax.set_xticks(x)
        xticklabels = [str(m) for m in modes]
        ax.set_xticklabels(xticklabels)
        ax.set_yscale("log")
        ax.set_xlabel("(mpol, ntor)")
        ax.set_ylabel(y_label)

    title = f"{q} Fourier Scaling"
    ax.set_title(title)

    return fig


def visualize(cfg: DictConfig) -> None:

    log = get_logger(__name__)

    for fig_name, figure in cfg.figures.items():
        log.info("Plotting figure %s ..." % fig_name)
        fig = hydra.utils.instantiate(figure, cfg)
        log.info("Figure %s has been created" % fig_name)

        if cfg.plot:
            plt.show()

        figures_dir = os.path.join(os.getcwd(), cfg.figures_output_dir)
        os.makedirs(figures_dir, exist_ok=True)
        fig_path = os.path.join(figures_dir, f"{fig_name}.pdf")
        savefig(fig, fig_path, save_tex=True)
        log.info("Figure %s has been saved to %s" % (fig_name, fig_path))
