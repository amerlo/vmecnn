"""Train script."""

import csv
import os
from typing import List

import hydra
import matplotlib.pyplot as plt
import numpy as np
import torch
import pytorch_lightning as pl
from hydra.core.hydra_config import HydraConfig
from omegaconf import OmegaConf, DictConfig, ListConfig
from pytorch_lightning import LightningDataModule, LightningModule, Trainer, Callback
from pytorch_lightning.loggers import LightningLoggerBase

from vmecnn.datamodules.utils import (
    NS,
    MPOL,
    NTOR,
    get_num_input,
    get_num_output,
)
from vmecnn.metrics import (
    FluxSurfaceRelativeError,
    FluxSurfaceError,
    IotaRelativeError,
    MeanSquaredError,
    MeanAbsolutePercentageError,
    NormalizedReducedMeanSquaredError,
)
from vmecnn.plotting.utils import (
    set_paper_style,
    savefig,
    GREEN,
    PINK,
    RED,
    BLUE,
    SMALL_SQUARED_FIGSIZE,
    LARGE_SQUARED_FIGSIZE,
    LARGE_THREETWO_FIGSIZE,
    LARGE_SIXTEENNINE_FIGSIZE,
)
from vmecnn.utils import (
    get_logger,
    dump,
    instantiate_list,
    get_git_revision_hash,
    to_device,
    compute_metric_on_iterable,
    swap,
    save_axis_data_to_file,
)
from vmecnn.physics import Equilibria


def finalize(loggers=None) -> None:
    """Finalize loggers and release memory."""
    for logger in loggers:
        logger.finalize(status="FINISHED")
    torch.cuda.empty_cache()


def search(cfg: DictConfig) -> None:

    from hyperopt import hp, fmin, tpe, space_eval, Trials, STATUS_OK, STATUS_FAIL

    log = get_logger(__name__)

    #  Check that validation loss is returned here
    assert cfg.trainer.limit_predict_batches == 0

    #  Define objective function
    def objective(args):

        _cfg = cfg.copy()

        for k, v in args.items():
            OmegaConf.update(_cfg, k, v)

        try:
            return {"loss": train(_cfg), "status": STATUS_OK}
        except Exception as e:
            log.info(e)
            return {"status": STATUS_FAIL}

    def from_ax_to_hyperopt(param: str, ax: dict) -> hp:
        if ax["type"] == "choice":
            return hp.choice(param, ax["values"])
        if ax["type"] == "range":
            if ax["log_scale"]:
                return hp.loguniform(param, *np.log(ax["bounds"]))
            else:
                return hp.uniform(param, *ax["bounds"])
        raise NotImplementedError

    hydra_config = HydraConfig.get()

    #  Define search space
    space = {}
    for k, v in hydra_config.sweeper.ax_config.params.items():
        space[k] = from_ax_to_hyperopt(k, v)

    #  Search
    trials = Trials()
    max_evals = hydra_config.sweeper.ax_config.max_trials
    best = fmin(
        objective,
        space,
        algo=tpe.suggest,
        max_evals=max_evals,
        trials=trials,
        show_progressbar=False,
    )

    log.info("Best params:")
    for k, v in space_eval(space, best).items():
        log.info(log.info(f"{k:50s}: {v:.2e}"))

    #  Log search trials
    writer = csv.writer(open(os.path.join(os.getcwd(), "search.csv"), "w"))

    keys = ["tid", "loss"] + list(trials.trials[0]["misc"]["vals"].keys())
    writer.writerow(keys)

    for trial in trials.trials:
        row = [trial["tid"], trial["result"]["loss"]]
        row += [v for k, v in trial["misc"]["vals"].items()]
        writer.writerow(row)


def train(cfg: DictConfig) -> None:

    log = get_logger(__name__)

    git_commit = get_git_revision_hash()
    log.info("Commit: %s" % git_commit)

    ##############
    # Datamodule #
    ##############

    if isinstance(cfg.datamodule.root, (list, ListConfig)):
        root = [os.path.join(cfg.orig_cwd, path) for path in cfg.datamodule.root]
    else:
        root = os.path.join(cfg.orig_cwd, cfg.datamodule.root)

    predict_root = cfg.datamodule.predict_root
    if cfg.datamodule.predict_root is not None:
        predict_root = [
            os.path.join(cfg.orig_cwd, path) for path in cfg.datamodule.predict_root
        ]

    params_file = None
    if cfg.datamodule.params_file is not None:
        params_file = os.path.join(cfg.orig_cwd, cfg.datamodule.params_file)

    pin_memory = True if cfg.trainer.accelerator == "gpu" else False

    #  Instatiate datamodule
    datamodule: LightningDataModule = hydra.utils.instantiate(
        cfg.datamodule,
        root=root,
        mpol=cfg.mpol,
        ntor=cfg.ntor,
        crop=not cfg.target_full_resolution,
        predict_root=predict_root,
        pin_memory=pin_memory,
        zeros_input=cfg.zeros_input,
        params_file=params_file,
        seed=cfg.seed,
    )
    log.info("DataModule: %s" % datamodule)

    ###################
    # Tune Datamodule #
    ###################

    log.info("Tune datamodule prior to training ...")
    datamodule.tune()
    datamodule.setup()

    if cfg.check_datamodule:
        datamodule.check()

    log.info("Num of train samples: %d" % len(datamodule.train_data))
    log.info("Num of validation samples: %d" % len(datamodule.val_data))
    log.info("Num of test samples: %d" % len(datamodule.test_data))
    log.info("Train transforms: %s" % datamodule.train_data.dataset.dataset.transforms)

    dump(datamodule._params, os.path.join(os.getcwd(), "params.json"))

    ###########
    # Trainer #
    ###########

    callbacks: List[Callback] = instantiate_list(cfg.callback)
    for c in callbacks:
        log.info("Callback: %s" % c.__class__.__name__)

    #  Instantiate logger
    loggers: List[LightningLoggerBase] = instantiate_list(cfg.logger)
    for logger in loggers:
        log.info("Logger: %s" % logger.__class__.__name__)

    deterministic = True if cfg.seed is not None else False
    trainer: Trainer = hydra.utils.instantiate(
        cfg.trainer, logger=loggers, callbacks=callbacks, deterministic=deterministic
    )
    log.info("Trainer: %s" % trainer.__class__.__name__)

    #########
    # Model #
    #########

    #  Check batch size and shuffle in case of ideal mhd loss without nn gradients
    if (
        cfg.criterion._target_ == "vmecnn.metrics.IdealMHDLoss"
        and not cfg.criterion.use_nn_gradients
        and (cfg.datamodule.batch_size != NS or cfg.datamodule.shuffle_batch is True)
    ):
        raise ValueError("Not supported combination")

    #  Validate loss args
    normalize = cfg.datamodule.normalization is not None and any(
        cfg.datamodule.normalization.values()
    )
    if normalize and (
        cfg.criterion._target_ == "vmecnn.metrics.RealMSELoss"
        or cfg.criterion._target_ == "vmecnn.metrics.IdealMHDLoss"
    ):
        #  Add scaling parameters
        mean, std = datamodule.get_params(cfg.datamodule.normalization)
        cfg.criterion["mean"] = mean
        cfg.criterion["std"] = std

        #  Normalize alphas
        alpha_norm = sum(cfg.criterion.alpha.values())
        if alpha_norm != 1.0:
            for k in cfg.criterion.alpha.keys():
                cfg.criterion.alpha[k] /= alpha_norm

    #  Number of input features
    input_features = get_num_input(cfg.datamodule.features, cfg.datamodule.ns)

    #  Get set of model output labels
    output_labels = list(cfg.datamodule.labels)
    if "xsmn" in output_labels:
        output_labels.remove("xsmn")

    model: LightningModule = hydra.utils.instantiate(
        cfg.model,
        input_features=input_features,
        mpol=cfg.mpol,
        ntor=cfg.ntor,
        ns=cfg.datamodule.ns,
        labels=output_labels,
        to_double=cfg.datamodule.to_double,
        criterion=cfg.criterion,
        optimizer=cfg.optimizer,
        scheduler=cfg.scheduler,
        metrics=cfg.metric,
        _recursive_=False,
    )
    model.setup()
    log.info("Model: %s" % model)

    ##############
    # Init Model #
    ##############

    if cfg.zeros_weight:
        model.zeros_all_trainable_parameters()

    #  Initialize last bias layer to accelerate learning
    if cfg.bias_init is None:
        pass
    elif cfg.bias_init in ("zeros", "ones"):
        model.init_bias(cfg.bias_init)
    elif isinstance(cfg.bias_init, str):
        normalization = {k: [cfg.bias_init] for k in output_labels}
        mean, _ = datamodule.get_params(normalization, keep_zero_fcs=False)
        bias = []
        for k in output_labels:
            bias.extend(mean[k])
        model.init_bias(bias)
    else:
        raise ValueError("Not supported %s bias value" % cfg.bias_init)

    if cfg.dry_run:
        return

    ################
    # Tune Trainer #
    ################

    if cfg.trainer.auto_lr_find:
        log.info("Tune model prior to training ...")
        res = trainer.tune(model, datamodule=datamodule)

        #  Set new lr into hyperparameters
        cfg.optimizer.lr = res["lr_find"].suggestion()

        if cfg.plot:
            res["lr_find"].plot(suggest=True)
            plt.show()

        del res

    ###################
    # Hyperparameters #
    ###################

    hparams = {}

    hparams["datamodule"] = cfg.datamodule
    hparams["model"] = cfg.model
    hparams["optimizer"] = cfg.optimizer
    hparams["scheduler"] = cfg.scheduler
    hparams["criterion"] = cfg.criterion
    hparams["trainer"] = cfg.trainer

    if "callback" in cfg:
        hparams["callback"] = cfg.callback

    #  Add additional training hps
    for h in ("seed", "mpol", "ntor"):
        hparams[h] = cfg[h]

    #  Add datamodule metrics
    hparams["datamodule/num_train"] = len(datamodule.train_data)
    hparams["datamodule/num_val"] = len(datamodule.val_data)
    hparams["datamodule/num_test"] = len(datamodule.test_data)

    #  Add model metrics
    hparams["model/nparams"] = model.num_trainable_parameters

    #  Add hp metrics
    hp_metrics = {}
    hp_metrics["model/nparams"] = model.num_trainable_parameters
    hp_metrics["val/loss"] = 0

    # disable logging any more hyperparameters for all loggers
    # see: https://github.com/ashleve/lightning-hydra-template/blob/main/src/utils/utils.py
    def empty(*args, **kwargs):
        pass

    #  Log hparams
    #  TensorBoard requires metrics to be defined with hyperparameters
    logger_hyperparams_fns = {}
    for logger in loggers:
        if isinstance(logger, pl.loggers.tensorboard.TensorBoardLogger):
            logger.log_hyperparams(hparams, hp_metrics)
        else:
            logger.log_hyperparams(hparams)
            logger.log_metrics(hp_metrics)
        logger_name = logger.__class__.__name__
        logger_hyperparams_fns[logger_name] = logger.log_hyperparams
        logger.log_hyperparams = empty

    #########
    # Train #
    #########

    if cfg.resume_training:
        assert cfg.model_checkpoint is not None

    model_checkpoint = cfg.model_checkpoint
    if model_checkpoint is not None:
        model_checkpoint = os.path.join(cfg.orig_cwd, model_checkpoint)

    #  Load model checkpoint and override hyperparameters
    if model_checkpoint is not None and not cfg.resume_training:
        log.info("Model checkpoint: %s" % model_checkpoint)
        model = model.load_from_checkpoint(
            model_checkpoint,
            log_training=cfg.model.log_training,
            enable_grad_on_predict=cfg.model.enable_grad_on_predict,
            criterion=cfg.criterion,
            optimizer=cfg.optimizer,
            scheduler=cfg.scheduler,
            metrics=cfg.metric,
            to_double=cfg.datamodule.to_double,
        )
        model.setup()

    #  To resume training, use PyTorch Lightning API
    ckpt_path = None
    if model_checkpoint is not None and cfg.resume_training:
        ckpt_path = model_checkpoint

    if cfg.fine_tuning:
        model.tune()

    log.info("Check loss before training ...")
    trainer.validate(model, datamodule=datamodule, ckpt_path=ckpt_path)

    log.info("Start training ...")
    trainer.fit(model, datamodule=datamodule, ckpt_path=ckpt_path)

    #  Log training metrics
    for m, v in trainer.logged_metrics.items():
        log.info(f"{m:50s}: {v:.2e}")

    ############
    # Validate #
    ############

    #  Load the best model according to callback loss
    model_checkpoint = None
    for callback in callbacks:
        if isinstance(callback, pl.callbacks.ModelCheckpoint):
            if callback.best_model_path != "":
                model_checkpoint = callback.best_model_path
            break

    if model_checkpoint is not None:
        log.info("Model checkpoint: %s" % model_checkpoint)
        model = model.load_from_checkpoint(model_checkpoint)
        model.setup()

    model.eval()

    log.info("Validate the model ...")
    val_metrics = trainer.validate(model, datamodule=datamodule)

    ########
    # Test #
    ########

    log.info("Test the model with in sample data ...")
    trainer.test(model, datamodule=datamodule)

    if cfg.trainer.limit_predict_batches == 0:
        finalize(loggers)
        return None if val_metrics == [] else val_metrics[0]["val/loss"]

    ###########
    # Predict #
    ###########

    log.info("Predict on %s data" % cfg.datamodule.predict_strategy)
    results = trainer.predict(model, datamodule=datamodule)

    trues, preds = swap(results)
    trues, preds = map(to_device, (trues, preds), ("cpu", "cpu"))

    #  Build back transforms to be used for model predictions
    transforms = datamodule.get_transforms(
        to_tensor=False,
        back_normalize=True,
        crop=False,
        pad=True,
        to_tuple=False,
    )
    transforms = torch.nn.Sequential(*transforms)
    log.info("Predict back transforms: %s" % transforms)

    preds = [transforms(p) for p in preds]

    #  Build back transforms to be used for true values
    transforms = datamodule.get_transforms(
        to_tensor=False,
        normalize_labels=False,
        back_normalize=True,
        crop=False,
        pad=False,
        to_tuple=False,
    )
    transforms = torch.nn.Sequential(*transforms)

    trues = [transforms(t) for t in trues]

    #  Build crop transform to obtain only the values computed by the model
    crop = datamodule.get_transforms(
        to_tensor=False,
        normalize_features=False,
        pad=False,
        normalize_labels=False,
        back_normalize=False,
        crop=True,
        to_tuple=False,
    )
    crop = torch.nn.Sequential(*crop)

    trues_cropped = [crop(t) for t in trues]
    preds_cropped = [crop(p) for p in preds]

    #  Compute metrics for each xmn
    if "xmn" in cfg.datamodule.labels and not cfg.target_full_resolution:
        metrics = instantiate_list(cfg.metric, num_outputs=1)
        values = {m.__class__.__name__: [] for m in metrics}
        n_xmn = np.prod(trues_cropped[0]["xmn"].shape[1:])
        for i in range(n_xmn):
            xmn = [t["xmn"].flatten(start_dim=1)[:, i] for t in trues_cropped]
            xmn_hat = [p["xmn"].flatten(start_dim=1)[:, i] for p in preds_cropped]
            for m in metrics:
                value = compute_metric_on_iterable(m, xmn_hat, xmn)
                values[m.__class__.__name__].append(value.item())
        dump(values, os.path.join(os.getcwd(), "fcs.json"))

    predict_metrics = {}
    predict_metrics["predict/num_samples"] = sum(
        [t["normalized_flux"].size(0) for t in trues]
    )

    #  Build metrics for cropped data
    metrics = instantiate_list(
        cfg.metric, num_outputs=model.num_outputs, keys=output_labels
    )
    if "iota" in cfg.datamodule.labels:
        metrics.append(IotaRelativeError())
    if "xmn" in cfg.datamodule.labels:
        metrics.append(FluxSurfaceRelativeError(ntheta=cfg.ntheta, nzeta=cfg.nzeta))
        metrics.append(FluxSurfaceError(ntheta=cfg.ntheta, nzeta=cfg.nzeta))

    for m in metrics:
        metric_label = f"predict/cropped/{m.__class__.__name__}"
        predict_metrics[metric_label] = compute_metric_on_iterable(
            m, preds_cropped, trues_cropped
        ).item()

    #  Build metrics for original resolution data
    metrics = instantiate_list(
        cfg.metric,
        num_outputs=get_num_output(MPOL, NTOR, output_labels, keep_zero_fcs=True),
        keys=output_labels,
    )
    if "iota" in cfg.datamodule.labels:
        metrics.append(IotaRelativeError())
    if "xmn" in cfg.datamodule.labels:
        metrics.append(FluxSurfaceRelativeError(ntheta=cfg.ntheta, nzeta=cfg.nzeta))
        metrics.append(FluxSurfaceError(ntheta=cfg.ntheta, nzeta=cfg.nzeta))

    for m in metrics:
        metric_label = f"predict/orig/{m.__class__.__name__}"
        predict_metrics[metric_label] = compute_metric_on_iterable(
            m, preds, trues
        ).item()

    #  Log metrics to logger
    for logger in loggers:
        if isinstance(logger, pl.loggers.tensorboard.TensorBoardLogger):
            logger_name = logger.__class__.__name__
            logger.log_hyperparams = logger_hyperparams_fns[logger_name]
            logger.log_hyperparams(hparams, predict_metrics)
        else:
            logger.log_metrics(predict_metrics)

    #  Log metrics to log file
    for m, v in predict_metrics.items():
        log.info(f"{m:50s}: {v:.2e}")

    ##########
    # Export #
    ##########

    log.info("Export model to onnx file")
    sample_input = {
        k: torch.rand(128, v.size(1), dtype=model.dtype, device=model.device)
        for k, v in trues[0].items()
        if k in cfg.datamodule.features
    }
    model.to_onnx(sample_input)

    #  Add metadata.json file
    #  TODO-0(@amerlo): include instructions on how to reproduce the trained model,
    #                   given the hash commit, the command line call should be enough,
    #                   or include the full cfg dict, or a reference to the outputs
    #                   folder generated for that model, but this has to be
    #                   permanent or tracked in some sort of way
    mean, std = None, None
    if cfg.datamodule.normalization is not None:
        normalization = {
            k: v
            for k, v in cfg.datamodule.normalization.items()
            if k in cfg.datamodule.features + output_labels
        }
        mean, std = datamodule.get_params(normalization)
    sample_output = model.forward(sample_input)
    metadata = {
        "ns": datamodule.ns,
        "mpol": datamodule.mpol,
        "ntor": datamodule.ntor,
        "mean": mean,
        "std": std,
        "sample_input": {k: v[0].tolist() for k, v in sample_input.items()},
        "sample_output": {k: v[0].tolist() for k, v in sample_output.items()},
        "commit": git_commit,
    }
    dump(metadata, "metadata.json")

    ###########
    # Analyze #
    ###########

    full = Equilibria.from_model(trues, preds)
    cropped = Equilibria.from_model(trues_cropped, preds_cropped)

    fs_metric = FluxSurfaceRelativeError(ntheta=cfg.ntheta, nzeta=cfg.nzeta)
    iota_metric = IotaRelativeError()

    def sort_fn(pred, true) -> float:
        return 0.5 * (fs_metric(pred, true) + iota_metric(pred, true)).item()

    full.sort(sort_fn)
    cropped.sort(sort_fn)
    true_equi, pred_equi = full.median
    true_cropped_equi, pred_cropped_equi = cropped.median

    #  Compute additional physics metrics
    physics_metrics = {}

    #  Surface averaged radial force balance
    for q in ("mean_f", "etaf"):
        physics_metrics[f"predict/orig/true_{q}"] = np.array(
            [getattr(t, q).item() for t, _ in full]
        ).mean()
        physics_metrics[f"predict/cropped/true_{q}"] = np.array(
            [getattr(t, q).item() for t, _ in cropped]
        ).mean()
        physics_metrics[f"predict/cropped/pred_{q}"] = np.array(
            [getattr(p, q).item() for _, p in cropped]
        ).mean()

    #  RMSE quantities
    rmse = MeanSquaredError(squared=False)
    for q in ("b", "r_axis", "l"):
        physics_metrics[f"predict/full/rmse_{q}"] = compute_metric_on_iterable(
            rmse, full, attr=q
        )

    #  MAE quantities
    mape = MeanAbsolutePercentageError()
    for q in (
        "vacuum_well",
        "finite_well",
        "well",
        "well_",
        "dwell",
        "beta",
        "shear",
        "mean_shear",
        "r_axis",
    ):
        physics_metrics[f"predict/full/mape_{q}"] = compute_metric_on_iterable(
            mape, full, attr=q
        )

    #  NRMSE quantities
    nrmse = NormalizedReducedMeanSquaredError(ignore_nan=True)
    for q in (
        "vacuum_well",
        "finite_well",
        "well",
        "well_",
        "dwell",
        "beta",
        "shear",
        "mean_shear",
        "r_axis",
    ):
        physics_metrics[f"predict/full/nrmse_{q}"] = compute_metric_on_iterable(
            nrmse, full, attr=q
        )

    #  Quantities for the median regressed equilibrium
    for q in ("mean_f", "mean_F", "etaf", "etaF", "beta"):
        physics_metrics[f"predict/median/true_{q}"] = getattr(true_equi, q).numpy()
        physics_metrics[f"predict/median/pred_{q}"] = getattr(pred_equi, q).numpy()

    #  Log physics metrics to logger (only CSVLogger)
    for logger in loggers:
        if isinstance(logger, pl.loggers.CSVLogger):
            logger.log_metrics(physics_metrics)

    #  Log physics metrics to shell
    for m, v in physics_metrics.items():
        log.info(f"{m:50s}: {v:.2e}")

    if not cfg.plot:
        finalize(loggers)
        return

    #############
    # Visualize #
    #############

    def profileplot(label: str, view: bool = True):
        """
        Plot true and predicted equilibrium profiles and their gradients.

        TODO-1(@amerlo): to be used only for debugging, remove me.

        Examples:
            >>> profileplot("iota", view=True)
            >>> profileplot("f", view=True)
        """
        from vmecnn.physics.utils import gradient, grad_1d
        from vmecnn.metrics.metric import compute_flux_surface_averaged_f

        fig, axs = plt.subplots(1, 3, figsize=(27 / 2.54, 18 / 2.54))

        true_value = getattr(true_equi, label)
        pred_value = getattr(pred_equi, label)

        if label == "f":
            #  Use true cropped value for a fair comparison
            true_value = getattr(true_cropped_equi, label)
            true_value, pred_value = map(abs, (true_value, pred_value))

        s = pred_equi.normalized_flux

        true_equi_d = gradient(getattr(true_equi, label), order=-1, factor=2.0)
        pred_equi_d = gradient(getattr(pred_equi, label), order=-1, factor=2.0)

        true_equi_dd = gradient(true_equi_d, order=-1, factor=2.0)
        pred_equi_dd = gradient(pred_equi_d, order=-1, factor=2.0)

        #  Add analytically computed f
        value_ = None
        if label == "f":
            value_ = (
                compute_flux_surface_averaged_f(
                    rmnc=pred_equi.rmnc,
                    lmns=pred_equi.lmns,
                    zmns=pred_equi.zmns,
                    iota=pred_equi.iota,
                    normalized_flux=pred_equi.normalized_flux,
                    phiedge=pred_equi.phiedge.expand(len(s)),
                    pressure=pred_equi.pressure.expand(len(s), len(s)),
                    ntheta=32,
                    nzeta=36,
                    use_nn_gradients=True,
                    use_proxy=True,
                )["f"]
                .abs()
                .detach()
            )

        #  Add analytical gradients if available
        pred_equi_d_, pred_equi_dd_ = None, None
        if pred_value.requires_grad and (
            label.startswith("iota")
            or label.startswith("rmnc")
            or label.startswith("lmns")
            or label.startswith("zmns")
        ):
            pred_equi_d_ = grad_1d(pred_value, s, create_graph=True)
            pred_equi_dd_ = grad_1d(pred_equi_d_, s, retain_graph=True)

            pred_equi_d_ = pred_equi_d_.detach()
            pred_equi_dd_ = pred_equi_dd_.detach()

            #  Interpolate on axis
            pred_equi_d_[0] = 2 * pred_equi_d_[1] - pred_equi_d_[2]
            pred_equi_dd_[0] = 2 * pred_equi_dd_[1] - pred_equi_dd_[2]

        #  Detach tensors from graph
        s = s.detach()
        pred_value = pred_value.detach()
        pred_equi_d = pred_equi_d.detach()
        pred_equi_dd = pred_equi_dd.detach()

        axs[0].plot(s, true_value, label="true")
        axs[0].plot(s, pred_value, label="pred")

        axs[1].plot(s, true_equi_d, label="true d")
        axs[1].plot(s, pred_equi_d, label="pred d")

        axs[2].plot(s, true_equi_dd, label="true dd")
        axs[2].plot(s, pred_equi_dd, label="pred dd")

        if value_ is not None:
            axs[0].plot(s, value_, label="pred nn")

        if pred_equi_d_ is not None:
            axs[1].plot(s, pred_equi_d_, label="pred d  nn")
            axs[2].plot(s, pred_equi_dd_, label="pred dd nn")

        for ax, title in zip(
            axs, ("$" + label + "$", "$" + label + "$ d", "$" + label + "$ dd")
        ):
            ax.legend()
            ax.set_title(title)

            if label == "f":
                ax.set_yscale("log")

        if view:
            plt.show()
        else:
            return axs

    def profileplots(label: str, view: bool = True):
        """
        TODO-1(@amerlo): to be used for debugging only, remove me!
        """

        from matplotlib import cm

        fig, ax = plt.subplots(1, 1)

        idx = np.linspace(0, 1, len(cropped))
        c = cm.get_cmap("Paired")(idx)

        for i, (t, p) in enumerate(cropped):
            t.profileplot(label, "-", c=c[i], ax=ax, alpha=0.2)
            p.profileplot(label, "--", c=c[i], ax=ax)
            ax.set_title("$" + label + r" \ [m]$")

        if view:
            plt.show()

    def activationplot(layer: str, view: bool = True):
        """Plot the activation for the given layer across predict dataset."""

        #  Force model to log activation
        if not model.log_training:
            model.log_training = True
            model.setup()

        act = []
        for (x, _, _) in datamodule.predict_dataloader():
            model.forward(x)
            act.append(model._activations[layer].detach())
        act = torch.cat(act, dim=0)

        #  Visualize all activations above ~60% of the median activation
        precision = 0.6 * torch.median(act.abs()).item()

        fig, ax = plt.subplots(1, 1)
        ax.spy(act, precision=precision, origin="lower", aspect="auto")
        ax.set_title(layer)
        ax.set_xlabel("unit")
        ax.set_ylabel("sample")

        if view:
            plt.show()

    def trunkplot(index: int, view: bool = True):
        """
        Plot the learned trunk basis functions.

        TODO-1(@amerlo): have an str based indexing.
        """

        assert model.trunk_net is not None

        #  Force model to log activation
        if not model.log_training:
            model.log_training = True
            model.setup()

        trunk_depth = len(model.trunk_net) - 1
        #  TODO-1(@amerlo): fix me!
        last_layer_depth = 1
        layer = f"trunk_net.fc{trunk_depth}.layers.{last_layer_depth}"

        for (x, _, _) in datamodule.predict_dataloader():
            model.forward(x)
            act = model._activations[layer].detach()
            act = act.view(-1, model._trunk_output_dim, model._trunk_output_features)
            act = act[:, index]
            break

        #  Plot learned basis functions
        fig, ax = plt.subplots(1, 1)
        s = torch.linspace(0, 1, act.shape[0])
        for i in range(act.shape[1]):
            ax.plot(s, act[:, i], label=str(i))
        ax.set_title(label)
        ax.set_xlabel("s")
        ax.legend()

        #  Compute orthogonality matrix
        m = (act.T @ act).abs()

        fig, ax = plt.subplots(1, 1)
        im = ax.imshow(m)
        fig.colorbar(im, ax=ax)

        if view:
            plt.show()

    ###########
    # Figures #
    ###########

    set_paper_style()

    #  Figure labels
    true_l = "VMEC"
    pred_l = "NN"

    def get_figure_labels(q: str = None):
        if q is None:
            return (pred_l, true_l)
        return (q + " " + pred_l, q + " " + true_l)

    #  Figure colors
    true_c = PINK
    pred_c = GREEN

    #  Figure strings
    ibar = r"$\mathrel{\hbox{-}\mkern-6.55mu \iota}$"
    shear = r"$\mathrel{\hbox{-}\mkern-6.55mu \iota}^{\prime}$"
    shear_star = r"$\mathrel{\hbox{-}\mkern-6.55mu \iota}^{\prime}_{*}$"
    eps_eff = r"$\epsilon_{\text{eff}}$"  # noqa
    eps_eff_32 = r"$\epsilon_{\text{eff}}^{3/2}$"  # noqa
    eps_eff_proxy = r"$\hat{\epsilon}_{\text{eff}}$"  # noqa
    sigma_bmax = r"$\sigma_{B_\text{max}}$"  # noqa
    sigma_bmin = r"$\sigma_{B_\text{min}}$"  # noqa
    sigma_pmax = r"$\sigma_{\varphi_\text{max}}$"  # noqa

    #  Figure parameters
    use_kde = False  # enable me for paper quality figures
    full_equilibrium = (
        "iota" in cfg.datamodule.labels
        and "xmn" in cfg.datamodule.labels
        and true_equi.ns == NS
    )
    save_tex = True  # save figure LaTex code

    #  Create figures folder
    os.mkdir(cfg.figures_output_dir)

    #  Plot histogram of equilibrium quantities
    for q, qt in (("vacuum_well", r"$W_d$"),):
        fig, ax = plt.subplots(1, 1, figsize=SMALL_SQUARED_FIGSIZE, tight_layout=True)
        full.histplot(q, ax=ax, labels=(true_l, pred_l), cut=0, bw_adjust=0.25)
        ax.set_xlabel(qt)
        savefig(
            fig,
            os.path.join(cfg.figures_output_dir, f"hist_{q}.pdf"),
            save_tex=save_tex,
        )

    #  Save metrics versus equilibrium properties
    fs_metric = FluxSurfaceError(ntheta=cfg.ntheta, nzeta=cfg.nzeta)
    iota_metric = IotaRelativeError()
    data = np.stack(
        (
            [getattr(t, "beta") for t, _ in full],
            [getattr(t, "curtor") for t, _ in full],
            [fs_metric(p.as_dict(), t.as_dict()).item() for t, p in full],
            [iota_metric(p.as_dict(), t.as_dict()).item() for t, p in full],
        )
    ).T
    np.savetxt(
        os.path.join(cfg.figures_output_dir, "metrics_versus_beta_itor.txt"),
        data,
        header="beta, curtor, FluxSurfaceError, IotaRelativeError",
    )

    #  Plot magnetic axis location versus beta
    fig, ax = plt.subplots(1, 1, figsize=SMALL_SQUARED_FIGSIZE, tight_layout=True)
    ax.scatter(
        [getattr(t, "beta") for t, _ in full],
        [getattr(t, "r_axis") for t, _ in full],
        color=true_c,
        label=true_l,
    )
    ax.scatter(
        [getattr(p, "beta") for _, p in full],
        [getattr(p, "r_axis") for _, p in full],
        color=pred_c,
        label=pred_l,
        alpha=0.4,
    )
    ax.legend()
    ax.set_xlabel(r"$\langle \beta \rangle$")
    ax.set_ylabel(r"$R_{\text{axis}}$")
    savefig(
        fig,
        os.path.join(cfg.figures_output_dir, "r_axis_versus_beta.pdf"),
        save_tex=save_tex,
    )

    #  Plot scatter plot for xmn Fourier coefficients
    quantities = []
    mpol = min(cfg.mpol, 1)
    ntor = min(cfg.ntor, 1)
    for label in ("rmnc", "lmns", "zmns"):
        for m in range(mpol + 1):
            for n in range(-ntor, ntor + 1):
                if m == 0 and n < 0:
                    continue
                quantities.append(f"{label}_" + "{" + f"{m},{n}" + "}")

    nplots = len(quantities)
    ncols = min(int(np.ceil(np.sqrt(nplots))), 3)
    nrows = min(int(np.ceil(nplots / ncols)), 3)
    nfigs = int(np.ceil(nplots / (ncols * nrows)))
    figs = [
        plt.figure(figsize=LARGE_THREETWO_FIGSIZE, tight_layout=True)
        for _ in range(nfigs)
    ]
    for i, q in enumerate(quantities):
        fig = figs[int(i / (nrows * ncols))]
        ax = fig.add_subplot(nrows, ncols, i % (nrows * ncols) + 1)
        full.scatterplot(q, ax=ax, axes_labels=get_figure_labels("$" + q + "$"))

    for i, fig in enumerate(figs):
        fig.subplots_adjust(hspace=0.5)
        savefig(
            fig,
            os.path.join(cfg.figures_output_dir, f"scatter_{i}.pdf"),
        )

    #  Plot scalar physics quantities of interest across equilibra
    for q, qt in (
        ("iota", ibar),
        ("shear", shear),
        ("shear_", shear),
        ("mean_shear", shear_star),
        ("well", r"$W_{\text{vacuum}}$"),
        ("well_", r"$W$"),
        ("vacuum_well", r"$\overline{W}_{\text{vacuum}}$"),
        ("finite_well", r"$\overline{W}$"),
        ("r_axis", r"$R_{\text{axis}}(\varphi=0)$"),
        ("gpp", r"$\langle |\nabla \Phi|^2 \rangle$"),
    ):
        fig, ax = plt.subplots(1, 1, figsize=SMALL_SQUARED_FIGSIZE, tight_layout=True)
        full.scatterplot(
            q,
            ax=ax,
            axes_labels=get_figure_labels(qt),
            use_kde=use_kde,
        )
        savefig(
            fig,
            os.path.join(cfg.figures_output_dir, f"scatter_{q}.pdf"),
            save_tex=save_tex,
        )

    #  Add scatter plots for Fourier b field components
    fig, axs = plt.subplots(2, 2, figsize=LARGE_SQUARED_FIGSIZE, tight_layout=True)
    for i, q in enumerate(["bmnc_{0,0}", "bmnc_{0,1}", "bmnc_{1,0}", "bmnc_{1,1}"]):
        full.scatterplot(
            q, ax=axs.reshape(-1)[i], axes_labels=get_figure_labels("$" + q + "$")
        )
    savefig(fig, os.path.join(cfg.figures_output_dir, "scatter_bmnc.pdf"))

    if full_equilibrium:
        import seaborn as sns

        #  Plot rmse for |B| across the radial profile
        fig, ax = plt.subplots(1, 1, figsize=SMALL_SQUARED_FIGSIZE, tight_layout=True)
        full.profileplot("b_full_mesh", metric_fn="l2", ax=ax, c=BLUE)
        ax.plot([0, 1], [1e-2, 1e-2], "--", c=RED, linewidth=1)
        ax.annotate("$10$ " + "mT", xy=(0.875, 0.012), c=RED)
        ax.legend(loc="upper left")
        ax.set_ylabel(r"$\text{rmse}_{|B|}$ [T]")

        #  Save rmse_b values to file
        save_axis_data_to_file(
            ax,
            fpath=os.path.join(
                cfg.figures_output_dir, f"b_rmse_{trainer.global_step}.txt"
            ),
            indices=(0,),
        )
        savefig(
            fig, os.path.join(cfg.figures_output_dir, "b_rmse.pdf"), save_tex=save_tex
        )

        #  Plot normalized <f> against FluxSurfaceError
        fs_metric = FluxSurfaceError(ntheta=cfg.ntheta, nzeta=cfg.nzeta)
        jg = sns.jointplot(
            [fs_metric(p.as_dict(), t.as_dict()).item() for t, p in full],
            [getattr(p, "rmse_f").item() for _, p in full],
            marginal_kws=dict(log_scale=True, kde=True),
        )
        jg.ax_joint.set_xscale("log")
        jg.ax_joint.set_yscale("log")
        jg.ax_marg_x.set_xticklabels([], minor=True)
        jg.ax_joint.set_xlabel("Flux Surface Error [m]")
        jg.ax_joint.set_ylabel(r"$\text{rmse}_{f_*}$")
        fig = jg.ax_joint.get_figure()
        savefig(
            fig,
            os.path.join(
                cfg.figures_output_dir, "fstar_against_flux_surface_error.pdf"
            ),
            save_tex=save_tex,
        )

    #  Plot iota profile
    if "iota" in cfg.datamodule.labels:
        fig, ax = plt.subplots(1, 1, figsize=SMALL_SQUARED_FIGSIZE)
        true_equi.profileplot("iota", "-", color=true_c, ax=ax, label=true_l)
        pred_equi.profileplot("iota", "--", color=pred_c, ax=ax, label=pred_l)
        ax.legend()
        ax.set_ylabel(ibar)
        savefig(
            fig, os.path.join(cfg.figures_output_dir, "iota.pdf"), save_tex=save_tex
        )

    #  Plot flux surfaces shape
    if "xmn" in cfg.datamodule.labels:
        fig, ax = plt.subplots(1, 1, figsize=LARGE_SQUARED_FIGSIZE, tight_layout=True)
        if true_equi.ns == NS:
            flux = torch.linspace(0, 1, steps=10) ** 2
            flux_idx = [int(i * (NS - 1)) for i in flux]
        else:
            flux_idx = None
            flux = true_equi.normalized_flux
        true_equi.fluxsurfacesplot(
            "-", s=flux_idx, ax=ax, label=true_l, fillstyle="none", alpha=0.2
        )
        pred_equi.fluxsurfacesplot(
            "--", s=flux_idx, ax=ax, label=pred_l, fillstyle="none"
        )

        #  Set neutral color (e.g., black) for legend
        ax.legend()
        leg = ax.get_legend()
        leg.legendHandles[0].set_color("black")
        leg.legendHandles[1].set_color("black")

        #  Add zoom-in region
        from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
        from mpl_toolkits.axes_grid1.inset_locator import mark_inset

        axins = zoomed_inset_axes(ax, 2.5, loc="upper left")
        true_equi.fluxsurfacesplot(
            "-", s=flux_idx, ax=axins, label=true_l, fillstyle="none", alpha=0.2
        )
        pred_equi.fluxsurfacesplot(
            "--", s=flux_idx, ax=axins, label=pred_l, fillstyle="none"
        )
        axins.set_xlim(5.45, 5.65)
        axins.set_ylim(0.85, 1.05)
        axins.set_xlabel(None)
        axins.set_ylabel(None)
        plt.xticks(visible=False)
        plt.yticks(visible=False)
        mark_inset(ax, axins, loc1=1, loc2=4, fc="none", ec="0.5")

        savefig(
            fig,
            os.path.join(cfg.figures_output_dir, "flux_surfaces.pdf"),
        )

    #  Plot xmn coefficients
    if "xmn" in cfg.datamodule.labels:

        fig, axs = plt.subplots(2, 2, figsize=LARGE_THREETWO_FIGSIZE, tight_layout=True)

        label = "rmnc_{0,0}"
        true_equi.profileplot(label, ax=axs[0, 0], label=true_l)
        pred_equi.profileplot(label, ax=axs[0, 0], label=pred_l)
        axs[0, 0].legend()
        axs[0, 0].set_ylabel("$" + label + r" \ [m]$")

        label = "zmns_{0,1}"
        true_equi.profileplot(label, ax=axs[0, 1], label=true_l)
        pred_equi.profileplot(label, ax=axs[0, 1], label=pred_l)
        axs[0, 1].legend()
        axs[0, 1].set_ylabel("$" + label + r" \ [m]$")

        label = "rmnc_{1,0}"
        true_equi.profileplot(label, ax=axs[1, 0], label=true_l)
        pred_equi.profileplot(label, ax=axs[1, 0], label=pred_l)
        axs[1, 0].legend()
        axs[1, 0].set_ylabel("$" + label + r" \ [m]$")

        label = "zmns_{1,0}"
        true_equi.profileplot(label, ax=axs[1, 1], label=true_l)
        pred_equi.profileplot(label, ax=axs[1, 1], label=pred_l)
        axs[1, 1].legend()
        axs[1, 1].set_ylabel("$" + label + r" \ [m]$")

        savefig(fig, os.path.join(cfg.figures_output_dir, "xmn.pdf"), save_tex=save_tex)

    if full_equilibrium:
        #  Plot <f>
        fig, ax = plt.subplots(1, 1, figsize=SMALL_SQUARED_FIGSIZE)
        flux = torch.linspace(0, 1, true_equi.ns)
        ax.plot(flux, torch.abs(true_equi.f), "-", color=true_c, label=true_l)
        ax.plot(flux, torch.abs(pred_equi.f).detach(), "--", color=pred_c, label=pred_l)
        ax.set_xlabel("$s$")
        ax.set_ylabel("$f_*$")
        ax.set_yscale("log")
        ax.legend()
        savefig(fig, os.path.join(cfg.figures_output_dir, "f.pdf"), save_tex=save_tex)

        #  Plot <normf>
        fig, ax = plt.subplots(1, 1, figsize=SMALL_SQUARED_FIGSIZE)
        true_equi.profileplot("normf", "-", color=true_c, label=true_l, ax=ax)
        pred_equi.profileplot("normf", "--", color=pred_c, label=pred_l, ax=ax)
        ax.set_ylabel(r"$\langle |f_*| \rangle / | \nabla p |$")
        ax.set_yscale("log")
        ax.legend()

        #  Save <normf> values to file
        save_axis_data_to_file(
            ax,
            fpath=os.path.join(
                cfg.figures_output_dir, f"normf_{trainer.global_step}.txt"
            ),
            indices=(0, 1),
        )
        savefig(
            fig, os.path.join(cfg.figures_output_dir, "normf.pdf"), save_tex=save_tex
        )

        #  Plot the full normalized force balance
        fig, ax = plt.subplots(1, 1, figsize=SMALL_SQUARED_FIGSIZE)
        true_equi.profileplot("normF", "-", color=true_c, label=true_l, ax=ax)
        pred_equi.profileplot("normF", "--", color=pred_c, label=pred_l, ax=ax)
        ax.set_yscale("log")
        ax.set_ylabel(r"$\langle |F| \rangle / | \nabla p |$")
        ax.legend()

        #  Save <normF> values to file
        save_axis_data_to_file(
            ax,
            fpath=os.path.join(
                cfg.figures_output_dir, f"normF_{trainer.global_step}.txt"
            ),
            indices=(0, 1),
        )
        savefig(
            fig, os.path.join(cfg.figures_output_dir, "normF.pdf"), save_tex=save_tex
        )

        #  Plot average B field on flux surface
        fig, ax = plt.subplots(1, 1, figsize=SMALL_SQUARED_FIGSIZE, tight_layout=True)
        true_equi.profileplot(
            "bmnc_{0,0}", "-", color=true_c, label=true_l, ax=ax, on_full_mesh=False
        )
        pred_equi.profileplot(
            "bmnc_{0,0}", "--", color=pred_c, label=pred_l, ax=ax, on_full_mesh=False
        )
        ax.set_ylabel(r"$B_{00}$ [T]")
        ax.legend()
        savefig(fig, os.path.join(cfg.figures_output_dir, "b00.pdf"), save_tex=save_tex)

        #  Plot the magnetic shear
        fig, ax = plt.subplots(1, 1, figsize=SMALL_SQUARED_FIGSIZE)
        true_equi.profileplot("shear", "-", color=true_c, label=true_l, ax=ax)
        pred_equi.profileplot("shear", "--", color=pred_c, label=pred_l, ax=ax)
        ax.legend()
        ax.set_ylabel("Magnetic shear")
        savefig(
            fig, os.path.join(cfg.figures_output_dir, "shear.pdf"), save_tex=save_tex
        )

        #  Plot the magnetic shear (derived from full mesh iota)
        fig, ax = plt.subplots(1, 1, figsize=SMALL_SQUARED_FIGSIZE)
        true_equi.profileplot("shear_", "-", color=true_c, label=true_l, ax=ax)
        pred_equi.profileplot("shear_", "--", color=pred_c, label=pred_l, ax=ax)
        ax.set_ylabel(shear)
        ax.legend()
        savefig(
            fig, os.path.join(cfg.figures_output_dir, "shear_.pdf"), save_tex=save_tex
        )

        #  Plot the magnetic well
        fig, ax = plt.subplots(1, 1, figsize=SMALL_SQUARED_FIGSIZE)
        true_equi.profileplot("well", "-", color=true_c, label=true_l, ax=ax)
        pred_equi.profileplot("well", "--", color=pred_c, label=pred_l, ax=ax)
        ax.set_ylabel(r"$W$")
        ax.legend()
        savefig(
            fig, os.path.join(cfg.figures_output_dir, "well.pdf"), save_tex=save_tex
        )

        #  Plot the magnetic well considering finite beta effects
        fig, ax = plt.subplots(1, 1, figsize=SMALL_SQUARED_FIGSIZE)
        true_equi.profileplot("well_", "-", color=true_c, label=true_l, ax=ax)
        pred_equi.profileplot("well_", "--", color=pred_c, label=pred_l, ax=ax)
        ax.set_ylabel(r"$W$")
        ax.legend()
        savefig(
            fig, os.path.join(cfg.figures_output_dir, "well_.pdf"), save_tex=save_tex
        )

        #  Plot the magnetic well in the Mercier criterion
        fig, ax = plt.subplots(1, 1, figsize=SMALL_SQUARED_FIGSIZE)
        true_equi.profileplot("dwell", "-", color=true_c, label=true_l, ax=ax)
        pred_equi.profileplot("dwell", "--", color=pred_c, label=pred_l, ax=ax)
        ax.set_ylabel(r"$D_W$")
        ax.legend()
        savefig(
            fig, os.path.join(cfg.figures_output_dir, "dwell.pdf"), save_tex=save_tex
        )

        #  Plot the grad-rho vector magnitude
        fig, ax = plt.subplots(1, 1, figsize=SMALL_SQUARED_FIGSIZE)
        true_equi.profileplot("gpp", "-", color=true_c, label=true_l, ax=ax)
        pred_equi.profileplot("gpp", "--", color=pred_c, label=pred_l, ax=ax)
        ax.set_ylabel(r"$\langle |\nabla \Phi|^2 \rangle$")
        ax.legend()
        savefig(fig, os.path.join(cfg.figures_output_dir, "gpp.pdf"), save_tex=save_tex)

        #  Plot |B| field at LCFS
        fig, axs = plt.subplots(
            1, 2, figsize=LARGE_SIXTEENNINE_FIGSIZE, tight_layout=True
        )
        true_equi.surfaceplot("b_full_mesh", ax=axs[0], levels=20)
        pred_equi.surfaceplot("b_full_mesh", ax=axs[1], levels=20)
        axs[0].set_title(r"$|B|_{\text{VMEC}}$ [T]")
        axs[1].set_title(r"$|B|_{\text{NN}}$ [T]")
        savefig(fig, os.path.join(cfg.figures_output_dir, "b_lcfs.pdf"))

        #  Plot B field at two radial locations
        fig, axs = plt.subplots(2, 2, figsize=LARGE_THREETWO_FIGSIZE, tight_layout=True)
        js = [7, -25]
        flux = true_equi.shalf
        for i, idx in enumerate(js):
            true_equi.surfaceplot("b", ax=axs[i, 0], js=idx, levels=25, fill=False)
            pred_equi.surfaceplot("b", ax=axs[i, 1], js=idx, levels=25, fill=False)
            axs[i, 0].set_title(
                r"$|B|_{\text{VMEC}}$ [T] at $s=" + f"{flux[idx]:.4f}" + r"$"
            )
            axs[i, 1].set_title(
                r"$|B|_{\text{NN}}$ [T] at $s=" + f"{flux[idx]:.4f}" + r"$"
            )
        savefig(fig, os.path.join(cfg.figures_output_dir, "b.pdf"))

        #  Plot |B| along a field line
        fig, ax = plt.subplots(1, 1, figsize=SMALL_SQUARED_FIGSIZE, tight_layout=True)
        js = int((true_equi.ns - 1) / 2)
        true_equi.fieldlineplot(
            "bmnc", "-", alpha=0, js=js, color=true_c, label=true_l, ax=ax
        )
        pred_equi.fieldlineplot(
            "bmnc", "--", alpha=0, js=js, color=pred_c, label=pred_l, ax=ax
        )
        ax.set_ylabel(r"$|B|$ [T]")
        savefig(
            fig,
            os.path.join(cfg.figures_output_dir, "b_fieldline.pdf"),
            save_tex=save_tex,
        )

    #  Plot me only with reduced test set size
    #  Plot model errors over equilibria
    # fig, axs = plt.subplots(2, 2, figsize=LARGE_THREETWO_FIGSIZE, tight_layout=True)
    # full.metricplot("curtor", "p0", "iota", metric_fn="l1_r", ax=axs[0, 0])
    # full.metricplot("iA", "iB", "iota", metric_fn="l1_r", ax=axs[0, 1])
    # full.metricplot("phiedge", "iA", "iota", metric_fn="l1_r", ax=axs[1, 0])
    # full.metricplot(
    #     "pressure_peaking_factor",
    #     "toroidal_current_peaking_factor",
    #     "iota",
    #     metric_fn="l1_r",
    #     ax=axs[1, 1],
    # )
    # savefig(fig, os.path.join(cfg.figures_output_dir, "metrics_iota.pdf"), save_tex=save_tex)

    # fig, axs = plt.subplots(2, 2, figsize=LARGE_THREETWO_FIGSIZE, tight_layout=True)
    # full.metricplot("curtor", "p0", "r", metric_fn="l2_r", ax=axs[0, 0])
    # full.metricplot("curtor", "p0", "l", metric_fn="l2_r", ax=axs[0, 1])
    # full.metricplot("curtor", "p0", "z", metric_fn="l2_r", ax=axs[1, 0])
    # full.metricplot(
    #     "pressure_peaking_factor",
    #     "toroidal_current_peaking_factor",
    #     "r",
    #     metric_fn="l2_r",
    #     ax=axs[1, 1],
    # )
    # savefig(fig, os.path.join(cfg.figures_output_dir, "metrics_xmn.pdf"), save_tex=save_tex)

    # #  These computations are slow, uncomment me if needed
    # #  Plot db00/ds
    # fig, ax = plt.subplots(1, 1, figsize=SMALL_SQUARED_FIGSIZE, tight_layout=True)
    # true_equi.profileplot("raderb00", "-", color=true_c, label=true_l, ax=ax)
    # pred_equi.profileplot("raderb00", "--", color=pred_c, label=pred_l, ax=ax)
    # ax.set_ylabel(r"$\frac{d B_{00}}{d s}$")
    # ax.legend()
    # savefig(
    #     fig, os.path.join(cfg.figures_output_dir, "raderb00.pdf"), save_tex=save_tex
    # )

    # #  Plot B field in Boozer coordinates at two radial locations
    # fig, axs = plt.subplots(2, 2, figsize=LARGE_THREETWO_FIGSIZE, tight_layout=True)
    # #  Index in Boozer compute_surfs based on range(1, 98)
    # js = [5, -25]
    # flux = true_equi.shalf_b
    # for i, idx in enumerate(js):
    #     true_equi.surfplot(ax=axs[i, 0], js=idx, fill=False)
    #     pred_equi.surfplot(ax=axs[i, 1], js=idx, fill=False)
    #     axs[i, 0].set_title(
    #         r"$|B|_{\text{VMEC}}$ [T] at $s=" + f"{flux[idx]:.4f}" + r"$"
    #     )
    #     axs[i, 1].set_title(r"$|B|_{\text{NN}}$ [T] at $s=" + f"{flux[idx]:.4f}" + r"$")
    # savefig(fig, os.path.join(cfg.figures_output_dir, "b_boozer.pdf"))

    # #  Plot |B| along a field line (in Boozer coordinates)
    # #  Use same radial position as figure above
    # #  Achtung, js here is the index for the bmnc_b coefficients,
    # #  thus the index for the compute_surfs array
    # fig, ax = plt.subplots(1, 1, figsize=SMALL_SQUARED_FIGSIZE, tight_layout=True)
    # true_equi.fieldlineplot(
    #     "bmnc_b", "-", alpha=0, js=js[0], boozer=True, color=true_c, label=true_l, ax=ax
    # )
    # pred_equi.fieldlineplot(
    #     "bmnc_b",
    #     "--",
    #     alpha=0,
    #     js=js[0],
    #     boozer=True,
    #     color=pred_c,
    #     label=pred_l,
    #     ax=ax,
    # )
    # ax.set_ylabel(r"$|B|$ [T]")
    # savefig(
    #     fig,
    #     os.path.join(cfg.figures_output_dir, "b_fieldline_boozer.pdf"),
    #     save_tex=save_tex,
    # )

    # #  Same plot as above, but multiple field lines
    # fig, ax = plt.subplots(1, 2, figsize=LARGE_THREETWO_FIGSIZE, tight_layout=True)
    # alpha = np.linspace(0, 2 * np.pi, 10)
    # true_equi.fieldlineplot(
    #     "bmnc_b", "-", alpha=alpha, js=js[0], boozer=True, color=true_c, ax=ax[0]
    # )
    # pred_equi.fieldlineplot(
    #     "bmnc_b", "-", alpha=alpha, js=js[0], boozer=True, color=pred_c, ax=ax[1]
    # )
    # ax[0].set_ylabel(r"$|B|$ [T]")
    # ax[0].set_title(r"$|B|_{\text{VMEC}}$ [T] at $s=" + f"{flux[js[0]]:.4f}" + r"$")
    # ax[1].set_ylabel(r"$|B|$ [T]")
    # ax[1].set_title(r"$|B|_{\text{NN}}$ [T] at $s=" + f"{flux[js[0]]:.4f}" + r"$")
    # savefig(
    #     fig,
    #     os.path.join(cfg.figures_output_dir, "b_fieldlines_boozer.pdf"),
    #     save_tex=save_tex,
    # )

    # #  Plot bounce points for |B| along a field line (in Boozer coordinates)
    # fig, axs = plt.subplots(2, 2, figsize=LARGE_THREETWO_FIGSIZE, tight_layout=True)
    # #  Plot surface close to the axis
    # js = [5, -25]
    # nzeta = 288
    # alpha = 0
    # for i, idx in enumerate(js):
    #     true_equi.fieldlineplot(
    #         "bmnc_b",
    #         "-",
    #         alpha=alpha,
    #         js=idx,
    #         boozer=True,
    #         color=true_c,
    #         nzeta=nzeta,
    #         ax=axs[i, 0],
    #         add_bounce_points=True,
    #         label=r"$|B|$",
    #     )
    #     pred_equi.fieldlineplot(
    #         "bmnc_b",
    #         "--",
    #         alpha=alpha,
    #         js=idx,
    #         boozer=True,
    #         color=pred_c,
    #         ax=axs[i, 1],
    #         nzeta=nzeta,
    #         add_bounce_points=True,
    #         label=r"$|B|$",
    #     )
    #     axs[i, 0].set_ylabel(r"$|B|_{\text{VMEC}}$ [T]")
    #     axs[i, 0].set_title(
    #         r"$|B|_{\text{VMEC}}$ [T] at $s="
    #         + f"{flux[idx]:.4f}"
    #         + r"$"
    #         + r", $\alpha="
    #         + f"{alpha:.2f}"
    #         + r"\ \text{rad}$"
    #     )
    #     axs[i, 1].set_ylabel(r"$|B|_{\text{NN}}$ [T]")
    #     axs[i, 1].set_title(
    #         r"$|B|_{\text{NN}}$ [T] at $s="
    #         + f"{flux[idx]:.4f}"
    #         + r"$"
    #         + r", $\alpha="
    #         + f"{alpha:.2f}"
    #         + r"\ \text{rad}$"
    #     )
    # savefig(
    #     fig,
    #     os.path.join(cfg.figures_output_dir, "b_fieldline_boozer_bounce_points.pdf"),
    #     save_tex=save_tex,
    # )

    # #  Plot the B field largest Fourier modes in Boozer coordinates
    # fig, ax = plt.subplots(1, 1, figsize=SMALL_SQUARED_FIGSIZE, tight_layout=True)
    # true_equi.modeplot(ax=ax, nmodes=5, linestyle="solid")
    # pred_equi.modeplot(ax=ax, nmodes=5, linestyle="dashed")
    # savefig(
    #     fig, os.path.join(cfg.figures_output_dir, "bmn_boozer.pdf"), save_tex=save_tex
    # )

    # #  Plot eps_eff**(3/2) and eps_eff across whole plasma volume (these are at half mesh)
    # #  This computation is slow, uncomment me if needed
    # fig, ax = plt.subplots(1, 1, figsize=SMALL_SQUARED_FIGSIZE, tight_layout=True)
    # compute_surfs = np.arange(1, true_equi.ns - 1, dtype=int)
    # true_equi._has_bx_run, pred_equi._has_bx_run = False, False
    # true_equi.compute_surfs = compute_surfs
    # pred_equi.compute_surfs = compute_surfs
    # flux = true_equi.shalf_b
    # ax.plot(flux, true_equi.eps_eff, "-", color=true_c, label=true_l)
    # ax.plot(flux, pred_equi.eps_eff, "--", color=pred_c, label=pred_l)
    # ax.set_xlabel(r"$s$")
    # ax.set_ylabel(eps_eff_32)
    # ax.set_yscale("log")
    # ax.legend()
    # savefig(fig, os.path.join(cfg.figures_output_dir, "eps_eff.pdf"), save_tex=save_tex)

    # fig, ax = plt.subplots(1, 1, figsize=SMALL_SQUARED_FIGSIZE, tight_layout=True)
    # ax.plot(flux, true_equi.eps_eff_, "-", color=true_c, label=true_l)
    # ax.plot(flux, pred_equi.eps_eff_, "--", color=pred_c, label=pred_l)
    # ax.set_yscale("log")
    # ax.set_yticklabels([], minor=True)
    # ax.set_xlabel(r"$s$")
    # ax.set_ylabel(eps_eff)
    # ax.legend()
    # savefig(
    #     fig, os.path.join(cfg.figures_output_dir, "eps_eff_.pdf"), save_tex=save_tex
    # )

    # #  For the following figures, evaluated Boozer only at selected radial locations
    # def set_compute_surfs(stop=None, num: int = 10):
    #     if stop is None:
    #         stop = true_equi.ns - 2
    #     compute_surfs = np.linspace(1, stop, num, dtype=int)
    #     for p, t in full:
    #         t.clear()
    #         p.clear()
    #         t.compute_surfs, p.compute_surfs = compute_surfs, compute_surfs

    # #  Plot scatterplot for eps_eff
    # #  Compute eps_eff at five radial location within s in [0, 0.33]
    # fig, ax = plt.subplots(1, 1, figsize=SMALL_SQUARED_FIGSIZE, tight_layout=True)
    # set_compute_surfs(stop=32, num=5)
    # full.scatterplot(
    #     "eps_eff_",
    #     ax=ax,
    #     whis=(0, 2),
    #     axes_labels=get_figure_labels(eps_eff),
    #     use_kde=use_kde,
    #     clear_equi=True,
    # )
    # savefig(
    #     fig,
    #     os.path.join(cfg.figures_output_dir, "scatter_eps_eff_.pdf"),
    #     save_tex=save_tex,
    # )

    # #  Plot scatterplot for eps_eff proxy
    # fig, ax = plt.subplots(1, 1, figsize=SMALL_SQUARED_FIGSIZE, tight_layout=True)
    # set_compute_surfs()
    # full.scatterplot(
    #     "eps_eff_proxy",
    #     ax=ax,
    #     axes_labels=get_figure_labels(eps_eff_proxy),
    #     use_kde=use_kde,
    #     clear_equi=True,
    # )
    # savefig(
    #     fig,
    #     os.path.join(cfg.figures_output_dir, "scatter_eps_eff_proxy.pdf"),
    #     save_tex=save_tex,
    # )

    # #  Plot scatterplot for vbmax
    # fig, ax = plt.subplots(1, 1, figsize=SMALL_SQUARED_FIGSIZE, tight_layout=True)
    # set_compute_surfs()
    # full.scatterplot(
    #     "vbmax",
    #     ax=ax,
    #     whis=(0, 2),
    #     axes_labels=get_figure_labels(sigma_bmax),
    #     use_kde=use_kde,
    #     clear_equi=True,
    # )
    # savefig(
    #     fig,
    #     os.path.join(cfg.figures_output_dir, "scatter_vbmax.pdf"),
    #     save_tex=save_tex,
    # )

    # #  Plot scatterplot for vbmin
    # fig, ax = plt.subplots(1, 1, figsize=SMALL_SQUARED_FIGSIZE, tight_layout=True)
    # set_compute_surfs()
    # full.scatterplot(
    #     "vbmin",
    #     ax=ax,
    #     whis=(0, 2),
    #     axes_labels=get_figure_labels(sigma_bmin),
    #     use_kde=use_kde,
    #     clear_equi=True,
    # )
    # savefig(
    #     fig,
    #     os.path.join(cfg.figures_output_dir, "scatter_vbmin.pdf"),
    #     save_tex=save_tex,
    # )

    # #  Plot scatterplot for vpmax
    # fig, ax = plt.subplots(1, 1, figsize=SMALL_SQUARED_FIGSIZE, tight_layout=True)
    # set_compute_surfs()
    # full.scatterplot(
    #     "vpmax",
    #     ax=ax,
    #     axes_labels=get_figure_labels(sigma_pmax),
    #     use_kde=use_kde,
    #     clear_equi=True,
    # )
    # savefig(
    #     fig,
    #     os.path.join(cfg.figures_output_dir, "scatter_vpmax.pdf"),
    #     save_tex=save_tex,
    # )

    # #  Plot sigma of delta across plasma volume
    # fig, ax = plt.subplots(1, 1, figsize=SMALL_SQUARED_FIGSIZE, tight_layout=True)
    # set_compute_surfs(num=10)
    # js = np.arange(10, dtype=int)
    # nalpha = 40
    # nzeta = 288
    # flux = true_equi.shalf_b
    # ax.plot(
    #     flux,
    #     np.nanstd(true_equi.delta(js=js, nalpha=nalpha, nzeta=nzeta), axis=-1),
    #     "-",
    #     color=true_c,
    #     label=true_l,
    # )
    # ax.plot(
    #     flux,
    #     np.nanstd(pred_equi.delta(js=js, nalpha=nalpha, nzeta=nzeta), axis=-1),
    #     "--",
    #     color=pred_c,
    #     label=pred_l,
    # )
    # ax.set_xlabel(r"$s$")
    # ax.set_ylabel(r"$\sigma_{\delta}$")
    # ax.legend()
    # savefig(fig, os.path.join(cfg.figures_output_dir, "delta.pdf"), save_tex=save_tex)

    # #  Add also additional physics metrics
    # mape = MeanAbsolutePercentageError()
    # rmse = lambda other, target: np.sqrt(np.mean((other - target) ** 2))
    # qs = ("eps_eff_proxy", "eps_eff_", "vbmax", "vbmin")
    # metrics = (mape, mape, rmse, rmse)
    # labels = ("mape", "mape", "rmse", "rmse")
    # physics_metrics = {
    #     f"predict/full/{label}_{q}": np.empty(len(full)) for label, q in zip(labels, qs)
    # }
    # set_compute_surfs()
    # for i, (t, p) in enumerate(full):
    #     for q, metric_fn, metric_label in zip(qs, metrics, labels):
    #         p_value = getattr(p, q)
    #         t_value = getattr(t, q)
    #         #  eps_eff_proxy may have nans
    #         if q == "eps_eff_proxy":
    #             indices = torch.logical_and(
    #                 ~torch.isnan(t_value), ~torch.isnan(p_value)
    #             )
    #             p_value, t_value = p_value[indices], t_value[indices]
    #         physics_metrics[f"predict/full/{metric_label}_{q}"][i] = metric_fn(
    #             p_value, t_value
    #         ).item()
    #     t.clear(), p.clear()

    # #  Compute delta Boozer distance for median equilibrium
    # #  Select same flux surface as boozer fieldline figure
    # true_equi.clear(), pred_equi.clear()
    # compute_surfs = true_equi.compute_surfs[5].reshape(-1)
    # true_equi.compute_surfs, pred_equi.compute_surfs = compute_surfs, compute_surfs
    # nalpha = 40
    # nzeta = 288
    # physics_metrics["predict/median/true_std_delta"] = np.nanstd(
    #     true_equi.delta(js=[-1], nalpha=nalpha, nzeta=nzeta), axis=-1
    # )
    # physics_metrics["predict/median/pred_std_delta"] = np.nanstd(
    #     pred_equi.delta(js=[-1], nalpha=nalpha, nzeta=nzeta), axis=-1
    # )

    # #  Log physics metrics to shell
    # for m, v in physics_metrics.items():
    #     log.info(f"{m:50s}: {v.mean():.2e}")

    if cfg.view:
        plt.show()

    finalize(loggers)
