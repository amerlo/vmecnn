"""VMECNN utility functions."""

import gc
import json
import os
import logging
import warnings
import subprocess
import psutil
from typing import List, Dict, Tuple, Optional

import hydra
import torch
import numpy as np
from pytorch_lightning import seed_everything as pl_seed_everthing
from pytorch_lightning.utilities import rank_zero_only
from omegaconf import DictConfig, ListConfig


#  Small value to be used in division by zero cases
#  eps ~ np.finfo(np.float64).eps
eps = 2.22e-16


def get_logger(name=__name__, level=logging.INFO) -> logging.Logger:
    """
    Initializes multi-GPU-friendly python logger.

    From: https://github.com/ashleve/lightning-hydra-template/blob/main/src/utils/utils.py
    """

    logger = logging.getLogger(name)
    logger.setLevel(level)

    # this ensures all logging levels get marked with the rank zero decorator
    # otherwise logs would get multiplied for each GPU process in multi-GPU setup
    for level in (
        "debug",
        "info",
        "warning",
        "error",
        "exception",
        "fatal",
        "critical",
    ):
        setattr(logger, level, rank_zero_only(getattr(logger, level)))

    return logger


def seed_everything(seed: int) -> None:
    """Set random seeds for reproducibility."""
    pl_seed_everthing(seed, workers=True)
    if torch.cuda.is_available():
        torch.backends.cudnn.deterministic = True
        torch.backends.cudnn.benchmark = False
    os.environ["PYTHONHASHSEED"] = str(seed)


def disable_warnings() -> None:
    """Disable python warnings."""
    warnings.filterwarnings("ignore")
    log = get_logger(__name__)
    log.info("Python warnings are disabled")


def dump(obj: any, path: str) -> None:
    """Dump object to file."""
    if path.endswith(".txt"):
        with open(path, "w") as f:
            f.write(obj)
    elif path.endswith(".json"):
        with open(path, "w") as f:
            json.dump(obj, f)
    else:
        raise TypeError("Unsupported file extnesion %s" % path.split(".")[1])


def load(path: str) -> any:
    """Load a file from disk."""
    if path.endswith(".json"):
        with open(path, "r") as f:
            data = f.read()
        return json.loads(data)
    else:
        raise TypeError("Unsupported file extnesion %s" % path.split(".")[1])


def get_git_revision_hash() -> str:
    """Get git revision hash."""
    return subprocess.check_output(["git", "rev-parse", "HEAD"]).decode("ascii").strip()


def get_git_revision_short_hash() -> str:
    """Get git revision short hash."""
    return (
        subprocess.check_output(["git", "rev-parse", "--short", "HEAD"])
        .decode("ascii")
        .strip()
    )


def instantiate_list(cfg: DictConfig, **kwargs) -> List[any]:
    """Instatiate a list through hydra instantiate."""
    objects: List[any] = []
    for _, cfg_ in cfg.items():
        if "_target_" in cfg_:
            #  Add kwargs if found under configuration
            kwargs_ = {k: v for k, v in kwargs.items() if k in cfg_}
            objects.append(hydra.utils.instantiate(cfg_, **kwargs_))
    return objects


def stack_keys(input: Dict[str, torch.Tensor], keys: List[str]) -> torch.Tensor:
    return torch.cat(
        [torch.atleast_2d(input[k]).flatten(start_dim=1) for k in keys],
        dim=1,
    )


def to_device(input, device):
    if isinstance(input, torch.Tensor):
        return input.to(device)
    if isinstance(input, dict):
        for k, v in input.items():
            input[k] = to_device(v, device)
        return input
    if isinstance(input, list):
        return [to_device(i, device) for i in input]
    return input


def gather(input, dim, value, **kwargs):
    num = input.size(dim)
    index = torch.round(value * (num - 1)).view(value.size(0), -1).to(torch.long)
    return torch.gather(input, dim=dim, index=index, **kwargs)


def get_ax(*args, ax=None, **kwargs):
    if ax is None:
        import matplotlib.pyplot as plt

        _, ax = plt.subplots(1, 1)
    return ax


def linspace(start, end, steps, *args, endpoint: bool = True, **kwargs):
    if not endpoint:
        steps += 1
    linspace = torch.linspace(start, end, steps, *args, **kwargs)
    if not endpoint:
        return linspace[:-1]
    return linspace


def swap(d: List[Tuple[any]]):
    """Swap nested collections."""
    lists = []
    dim = len(d[0])
    for i in range(dim):
        lists.append([t[i] for t in d])
    return tuple(lists)


def compute_conv_out_length(
    in_length: int,
    depth: int,
    conv_kernel_size: int,
    conv_stride: int,
    conv_padding: int,
    pool_kernel_size: int,
    pool_stride: int,
    pool_padding: int,
):

    out = in_length

    def validate_arg(arg):
        if isinstance(arg, (ListConfig, list)):
            assert len(arg) == depth
        else:
            arg = [arg] * depth
        return arg

    conv_kernel_size = validate_arg(conv_kernel_size)
    conv_stride = validate_arg(conv_stride)
    conv_padding = validate_arg(conv_padding)
    pool_kernel_size = validate_arg(pool_kernel_size)
    pool_stride = validate_arg(pool_stride)
    pool_padding = validate_arg(pool_padding)

    for i in range(depth):
        #  Conv1d
        out = int(
            (out + 2 * conv_padding[i] - conv_kernel_size[i]) / conv_stride[i] + 1
        )
        #  AvgPool1d
        if pool_kernel_size[i] > 0:
            out = int(
                (out + 2 * pool_padding[i] - pool_kernel_size[i]) / pool_stride[i] + 1
            )

    assert out > 0

    return out


def get_torch_memory(show=False, return_ids=False, in_ids=None):
    """Return total memory allocated for torch tensors."""
    memory = 0
    ids = []
    mems = []
    sizes = []
    for obj in gc.get_objects():
        try:
            if torch.is_tensor(obj) or (
                hasattr(obj, "data") and torch.is_tensor(obj.data)
            ):
                mem = obj.nelement() * obj.element_size()
                memory += mem
                ids.append(id(obj))
                mems.append(mem)
                sizes.append(obj.size())
        except Exception:
            pass

    if show:
        sort = np.argsort(mems)
        for i in sort:
            found = False
            if in_ids is not None:
                for obj_id in in_ids:
                    if obj_id == ids[i]:
                        found = True
                        break
            if found:
                continue
            print(sizes[i], "{:.2f} MiB".format(mems[i] / 1024**2))

    if return_ids:
        return memory, ids

    return memory


def print_process_memory():
    process_memory = psutil.Process(os.getpid()).memory_info().rss
    print("Process memory: {:.2f} MiB".format(process_memory / 1024**2))


def asarray(input):
    """Like np.asarray but with Tensor support."""
    if isinstance(input, torch.Tensor):
        return np.asarray(input.detach().numpy())
    if isinstance(input[0], torch.Tensor):
        return np.asarray([v.detach().numpy() for v in input])
    return np.asarray(input)


def flatten(d, parent_key: str = "", sep: str = "."):
    """
    See: https://stackoverflow.com/questions/6027558/flatten-nested-dictionaries-compressing-keys
    """
    items = []
    for k, v in d.items():
        new_key = parent_key + sep + k if parent_key else k
        if isinstance(v, (dict, DictConfig)):
            items.extend(flatten(v, new_key, sep=sep).items())
        else:
            items.append((new_key, v))
    return dict(items)


def compute_metric_on_iterable(metric, preds, target=None, attr: Optional[str] = None):
    """Compute Torchmetrics metric on iterable data."""
    data = preds
    if target is not None:
        data = zip(target, preds)
    for t, p in data:
        if attr is not None:
            p, t = getattr(p, attr).detach().view(-1), getattr(t, attr).detach().view(
                -1
            )
        metric.update(p, t)
    res = metric.compute()
    metric.reset()
    return res


def save_axis_data_to_file(ax, fpath: str, indices: Tuple[int]) -> None:
    """
    Dump axis line (x, y) values to txt file.

    This function assumes that all the lines share the same x data.
    """
    data = [ax.lines[0].get_xdata()]
    for idx in indices:
        data.append(ax.lines[idx].get_ydata())
    data = np.stack(data, axis=-1)
    np.savetxt(
        fpath,
        data,
        header=f"cwd={os.getcwd()}",
    )
