import hydra
from omegaconf import DictConfig

from vmecnn.utils import disable_warnings, seed_everything


@hydra.main(config_path="../configs/", config_name="config.yaml")
def main(cfg: DictConfig):

    if cfg.ignore_warnings:
        disable_warnings()

    if cfg.seed is not None:
        seed_everything(cfg.seed)

    #  Start action
    return hydra.utils.instantiate(cfg.action, cfg)


if __name__ == "__main__":
    main()
