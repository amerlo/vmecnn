"""
Compute noralization parameters for data set.

To execute this script, run:

$ python vmecnn action=prepare

TODO-0(@amerlo): merge with existing workflow or remove me
"""

import os

import numpy as np
from omegaconf import DictConfig

from w7x_datagen.datasets import VmecEquilibria

from vmecnn.utils import dump, get_logger


def prepare(cfg: DictConfig) -> None:
    """Compute mean and scale for input and output features."""

    log = get_logger(__name__)

    root = os.path.join(cfg.orig_cwd, cfg.datamodule.root)
    dataset = VmecEquilibria(root=root, train=True)
    log.info("Dataset: %s" % dataset)

    features = {}
    features["input"] = ["extcur", "phiedge", "p0", "curtor"]
    features["output"] = ["surfaces", "iota"]

    items = {k: [] for k in features["input"] + features["output"]}
    for item in dataset:
        items["extcur"].append(item["extcur"][1:7] / item["extcur"][0])
        items["phiedge"].append([item["phiedge"]])
        items["p0"].append(item["pressure"][:1])
        items["curtor"].append([item["curtor"]])
        surfaces = np.stack((item["rmnc"], item["lmns"], item["zmns"]), axis=-1)
        items["surfaces"].extend(surfaces.reshape(99, -1))
        items["iota"].extend(item["iota"].reshape(-1, 1))

    params = {k: {} for k in features.keys()}
    for k in params.keys():
        median = []
        mean = []
        scale = []
        std = []
        quantile = []
        for f in features[k]:
            values = np.stack(items[f])
            median.extend(np.median(values, axis=0).tolist())
            mean.extend(values.mean(axis=0).tolist())
            scale.extend(((values.max(axis=0) - values.min(axis=0)) / 2).tolist())
            std.extend(values.std(axis=0).tolist())
            quantile.extend(
                (
                    (
                        np.quantile(values, axis=0, q=0.95)
                        - np.quantile(values, axis=0, q=0.05)
                    )
                    / 2
                ).tolist()
            )
        params[k]["median"] = median
        params[k]["mean"] = mean
        params[k]["scale"] = scale
        params[k]["std"] = std
        params[k]["iqr"] = quantile

    #  Manually add scaling for:
    #  - pressure profile (99)
    #  - toroidal_current (99)
    #  - s (1)
    params["input"]["median"].extend([0.0] * (99 * 2 + 1))
    params["input"]["mean"].extend([0.0] * (99 * 2 + 1))
    params["input"]["scale"].extend([1.0] * (99 * 2 + 1))
    params["input"]["std"].extend([1.0] * (99 * 2 + 1))
    params["input"]["iqr"].extend([1.0] * (99 * 2 + 1))

    params_file_path = os.path.join(cfg.orig_cwd, cfg.datamodule.params_file_path)
    dump(params, params_file_path)
    log.info("Params saved to %s" % params_file_path)
