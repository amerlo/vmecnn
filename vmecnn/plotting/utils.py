"""Plotting utility functions."""

import os
import matplotlib.pyplot as plt

###############
# Definitions #
###############

#  Figure sizes
SMALL_SQUARED_FIGSIZE = (9 / 2.54, 9 / 2.54)
MEDIUM_SQUARED_FIGSIZE = (12 / 2.54, 12 / 2.54)
LARGE_SQUARED_FIGSIZE = (18 / 2.54, 18 / 2.54)

MEDIUM_THREETWO_FIGSIZE = (18 / 2.54, 12 / 2.54)
LARGE_THREETWO_FIGSIZE = (27 / 2.54, 18 / 2.54)

LARGE_SIXTEENNINE_FIGSIZE = (16 / 2.54, 9 / 2.54)

#  Figure colors
PINK = "#af8dc3"
GREEN = "#7fbf7b"
RED = "#e41a1c"
BLUE = "#377eb8"

#############
# Functions #
#############


def set_paper_style():
    """Set figure style for paper quality."""
    try:
        from rna.plotting import set_style
        import logging

        set_style(style="rna")

        #  TODO-1(@amerlo): fix LaTex fonts and remove this
        logging.getLogger("matplotlib").setLevel(logging.WARNING)
    except ImportError as e:
        print(e)
    plt.rcParams.update({"font.size": 10})


def savefig(fig, fname: str, *args, save_tex: bool = False, **kwargs):
    """Save figure and (optionally) LaTex code."""
    fig.savefig(fname, *args, **kwargs)
    if save_tex:
        try:
            import tikzplotlib

            try:
                tikzplotlib.clean_figure()
            except Exception as e:
                print(e)
            dname = os.path.dirname(fname)
            fname = os.path.basename(fname)
            fname = fname.split(".")[0] + ".pgf"
            #  Extra axis parameters
            extra_axis_parameters = ["axis line style={very thick}"]
            #  Work around for equal aspect ratio
            #  See: https://github.com/texworld/tikzplotlib/issues/314
            ax = fig.gca()
            if ax._aspect == "equal" or ax._aspect == 1.0:
                extra_axis_parameters.append("axis equal image")
            tikzplotlib.save(
                os.path.join(dname, fname), extra_axis_parameters=extra_axis_parameters
            )
        except Exception as e:
            print(e)
    plt.close(fig)
