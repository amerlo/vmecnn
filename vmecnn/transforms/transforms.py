"""
Dataset transforms.

See: https://github.com/pytorch/vision

TODO-1(@amerlo): can we fix the tuple unpacking in each __call__?
TODO-1(@amerlo): enrigh repr of normalize with mean and std values
"""

from typing import Tuple, List, Dict, Optional

import numpy as np
import torch
from torch import Tensor

from vmecnn.datamodules.utils import fluxspace, MPOL, NTOR


def normalize(
    tensor: Tensor,
    mean: List[float],
    std: List[float],
    inplace: Optional[bool] = False,
) -> Tensor:
    """Normalize a float tensor with mean and standard deviation.

    See: https://github.com/pytorch/vision/blob/main/torchvision/transforms/functional.py

    Args:
        tensor: Float tensor to be normalized.
        mean: Sequence of means for each feature.
        std: Sequence of standard deviations for each feature.
        inplace: Bool to make this operation inplace.
    Returns:
        The normalized tensor.
    """

    if not inplace:
        tensor = tensor.clone()

    dtype = tensor.dtype
    device = tensor.device
    mean = torch.as_tensor(mean, dtype=dtype, device=device)
    std = torch.as_tensor(std, dtype=dtype, device=device)
    tensor.sub_(mean).div_(std)

    return tensor


def back_normalize(
    tensor: Tensor,
    mean: List[float],
    std: List[float],
    inplace: Optional[bool] = False,
) -> Tensor:
    """Back normalize a float tensor with mean and standard deviation.

    Args:
        tensor: Float tensor to be back normalized.
        mean: Sequence of means for each feature.
        std: Sequence of standard deviations for each feature.
        inplace: Bool to make this operation inplace.
    Returns:
        The back normalized tensor.
    """

    if not inplace:
        tensor = tensor.clone()

    dtype = tensor.dtype
    device = tensor.device
    mean = torch.as_tensor(mean, dtype=dtype, device=device)
    std = torch.as_tensor(std, dtype=dtype, device=device)
    tensor.mul_(std).add_(mean)

    return tensor


class Compose:
    """Composes several transforms together. This transform does not support torchscript.

    See: https://github.com/pytorch/vision/blob/main/torchvision/transforms/transforms.py

    Args:
        transforms: list of transforms to compose.
    """

    def __init__(self, transforms):
        self.transforms = transforms

    def __call__(self, item):
        for t in self.transforms:
            item = t(item)
        return item

    def __repr__(self):
        format_string = self.__class__.__name__ + "("
        for t in self.transforms:
            format_string += "\n"
            format_string += "  {0},".format(t)
        format_string += "\n)"
        return format_string


class BuildItem:
    """
    Extract features from input dictionary.

    TODO-1(@amerlo): the if statement makes the call slow, how to improve it?
                     Maybe with a fn defined in the init?
    TODO-1(@amerlo): can we remove the astype statement?
    TODO-1(@amerlo): add documentation.
    """

    def __init__(
        self,
        features: List[str],
        labels: List[str],
        ns: int,
        to_double: bool,
        metadata: Optional[List[str]] = None,
    ):

        self.features = features
        self.labels = labels
        self.metadata = metadata
        self.ns = ns
        self.to_double = to_double

        self._flux_indices = fluxspace(ns)
        self._eps = np.finfo(np.float64 if to_double else np.float32).eps

    def __call__(self, item: Dict[str, np.ndarray]) -> Dict[str, Tensor]:

        #  TODO-1(@amerlo): find better name for this
        tensor = {}

        # i2, i3, i4, i5
        if "npc" in self.features:
            tensor["npc"] = torch.from_numpy(item["extcur"][1:5] / item["extcur"][0])

        # iA, iB
        if "pc" in self.features:
            tensor["pc"] = torch.from_numpy(item["extcur"][5:7] / item["extcur"][0])

        # phi_edge
        if "phiedge" in self.features:
            tensor["phiedge"] = torch.from_numpy(item["phiedge"]).view(-1)

        # flux surface index
        sidx = int(item["normalized_flux"] * (len(item["pressure"]) - 1))

        # p0
        if "p0" in self.features:
            tensor["p0"] = torch.from_numpy(item["pressure"][:1])

        #  gradp(s)
        if "gradp" in self.features:
            gradp = np.gradient(item["pressure"], 1 / (len(item["pressure"]) - 1))
            tensor["gradp"] = torch.from_numpy(gradp[sidx].reshape(-1))

        # I_tor
        if "curtor" in self.features:
            tensor["curtor"] = torch.from_numpy(item["curtor"]).view(-1)

        # I_tor(s)
        if "itor" in self.features:
            tensor["itor"] = torch.from_numpy(
                item["curtor"] * item["toroidal_current"][sidx].reshape(-1)
            )

        # pressure profile
        if "pressure" in self.features:
            tensor["pressure"] = torch.from_numpy(
                item["pressure"][self._flux_indices]
                / (item["pressure"][0] + self._eps),
            )

        # peaking factor
        if "peaking_factor" in self.features:
            tensor["peaking_factor"] = torch.as_tensor(
                [item["pressure"][0] / (item["pressure"].mean() + self._eps)],
            )

        # toroidal current
        if "toroidal_current" in self.features:
            tensor["toroidal_current"] = torch.from_numpy(
                item["toroidal_current"][self._flux_indices]
                / (item["toroidal_current"][-1] + self._eps)
            )

        # toroidal current density
        if "toroidal_current_density" in self.features:
            toroidal_current = torch.from_numpy(
                item["toroidal_current"][self._flux_indices]
                / (item["toroidal_current"][-1] + self._eps)
            )
            tensor["toroidal_current_density"] = torch.gradient(
                toroidal_current, spacing=1 / (toroidal_current.size(0) - 1)
            )[0]

        # radial like coordinate
        if "normalized_flux" in self.features:
            tensor["normalized_flux"] = torch.as_tensor([item["normalized_flux"]])

        #  Select flux surface coordinates
        if "xmn" in self.labels:
            xmn = []
            for key in ("rmnc", "lmns", "zmns"):
                xmn.append(torch.from_numpy(item[key]))
            tensor["xmn"] = torch.stack(xmn, dim=-1)

        #  Gradient of flux surface coordinates
        if "xsmn" in self.labels:
            xsmn = []
            for key in ("rsmnc", "lsmns", "zsmns"):
                xsmn.append(torch.from_numpy(item[key]))
            tensor["xsmn"] = torch.stack(xsmn, dim=-1)

        #  Add iota value at s
        if "iota" in self.labels:
            tensor["iota"] = torch.as_tensor([item["iota"]])

        if not self.to_double:
            for k, v in tensor.items():
                tensor[k] = v.to(torch.float32)

        for k in self.metadata:
            tensor[k] = item[k].item()

        return tensor

    def __repr__(self) -> str:
        return (
            self.__class__.__name__
            + "(features={0}, labels={1}, ns={2}, to_double={3})".format(
                self.features, self.labels, self.ns, self.to_double
            )
        )


class ToTuple(torch.nn.Module):
    def __init__(self, *keys: List[str]):
        super().__init__()
        self.keys = list(keys)

    def forward(self, item: Dict[str, Tensor]) -> Tuple[Dict[str, Tensor]]:
        """
        Pack dict keys into a tuple of dict tensors.
        """
        return tuple(
            [{k: v for k, v in item.items() if k in keys} for keys in self.keys]
        )

    def __repr__(self) -> str:
        return self.__class__.__name__ + "()"


class Crop(torch.nn.Module):
    def __init__(self, mpol: int, ntor: int):
        super().__init__()
        self.mpol = mpol
        self.ntor = ntor

    def forward(self, item: Dict[str, Tensor]) -> Dict[str, Tensor]:
        """
        Crop target tensor to desired Fourier resolution.
        """
        item = item.copy()
        for k in ("xmn", "xsmn"):
            if k not in item:
                continue
            if item[k].dim() == 3:
                item[k] = item[k][
                    0 : self.mpol + 1, NTOR - self.ntor : NTOR + self.ntor + 1, ...
                ]
            else:
                item[k] = item[k][
                    :, 0 : self.mpol + 1, NTOR - self.ntor : NTOR + self.ntor + 1, ...
                ]
        return item

    def __repr__(self) -> str:
        return self.__class__.__name__ + "(mpol={0}, ntor={1})".format(
            self.mpol, self.ntor
        )


class Pad(torch.nn.Module):
    def __init__(
        self,
        mpol: int,
        ntor: int,
    ):
        super().__init__()
        self.mpol = mpol
        self.ntor = ntor

        self._pad = torch.nn.ConstantPad2d(
            (NTOR - ntor, NTOR - ntor, 0, MPOL - mpol), value=0.0
        )

    def forward(self, item: Dict[str, Tensor]) -> Dict[str, Tensor]:
        item = item.copy()
        for k in ("xmn", "xsmn"):
            if k not in item:
                continue
            if item[k].dim() == 3:
                value = torch.permute(item[k], (2, 0, 1))
                value = self._pad(value)
                item[k] = torch.permute(value, (1, 2, 0))
            else:
                value = torch.permute(item[k], (0, 3, 1, 2))
                value = self._pad(value)
                item[k] = torch.permute(value, (0, 2, 3, 1))
        return item

    def __repr__(self):
        return (
            self.__class__.__name__
            + "("
            + "mpol={0}, ntor={1}".format(self.mpol, self.ntor)
            + ")"
        )


class ZeroKeys(torch.nn.Module):
    """
    Zero key values.
    """

    def __init__(self, keys: List[str]):
        super().__init__()
        self.keys = keys

    def forward(self, item: Dict[str, Tensor]) -> Dict[str, Tensor]:
        item = item.copy()
        for k in self.keys:
            item[k] = torch.zeros_like(item[k])
        return item

    def __repr__(self) -> str:
        return self.__class__.__name__ + f"(keys={self.keys})"


class Normalize(torch.nn.Module):
    def __init__(
        self,
        mean: Dict[str, List[float]],
        std: Dict[str, List[float]],
    ):
        super().__init__()
        self.mean = mean
        self.std = std

    def forward(self, item: Dict[str, Tensor]) -> Dict[str, Tensor]:
        for k in self.mean.keys():
            item[k] = normalize(item[k], self.mean[k], self.std[k])
        return item

    def __repr__(self):
        return self.__class__.__name__ + f"(mean={self.mean}, std={self.std})"


class BackNormalize(torch.nn.Module):
    def __init__(
        self,
        mean: Dict[str, List[float]],
        std: Dict[str, List[float]],
    ):
        super().__init__()
        self.mean = mean
        self.std = std

    def forward(self, item: Dict[str, Tensor]) -> Dict[str, Tensor]:
        for k in self.mean.keys():
            item[k] = back_normalize(item[k], self.mean[k], self.std[k])
        return item

    def __repr__(self):
        return self.__class__.__name__ + f"(mean={self.mean}, std={self.std})"


class ZeroAxis(torch.nn.Module):
    """Zero coefficients on axis."""

    def forward(self, item: Dict[str, Tensor]) -> Dict[str, Tensor]:
        item = item.copy()
        for k in ("xmn", "xsmn"):
            if k not in item:
                continue
            item[k] = item[k].clone()
            if item[k].dim() == 3:
                if item["normalized_flux"] == 0:
                    item[k][1:, :, [0, 2]] = 0  # r, z
                    item[k][:, :, 1] = 0  # l
            else:
                idx = torch.nonzero(item["normalized_flux"][:, 0] == 0)
                item[k][idx, 1:, :, [0, 2]] = 0  # r, z
                item[k][idx, :, :, 1] = 0  # l
        return item

    def __repr__(self):
        return self.__class__.__name__ + "()"
