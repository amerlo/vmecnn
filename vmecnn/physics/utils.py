"""Utilities for the physics module."""

import math
from functools import cache
from typing import Optional, Union, List, Tuple

import numpy as np
import torch
from torch import Tensor
from torch._vmap_internals import _vmap as vmap

from vmecnn.utils import linspace


#  Flux surface index from where to start to compute mean_f
MEAN_F_START = 2


def grad_1d(outputs, inputs, **kwargs):
    return torch.autograd.grad(
        outputs,
        inputs,
        grad_outputs=torch.ones(
            outputs.size(0), dtype=outputs.dtype, device=outputs.device
        ),
        **kwargs,
    )[0].view(outputs.shape)


def _grad_3d(outputs, inputs, **kwargs):
    """
    Non-vectorized version of grad_3d.
    """

    #  Force retain_graph to be true due to the for loop
    kwargs["retain_graph"] = True
    grad_outputs = torch.ones(
        outputs.size(0), dtype=outputs.dtype, device=outputs.device
    )
    shape = outputs.shape
    outputs = outputs.view(-1, outputs.size(1) * outputs.size(2))
    grads = [
        torch.autograd.grad(
            outputs[:, i],
            inputs,
            grad_outputs=grad_outputs,
            **kwargs,
        )[0]
        for i in range(outputs.shape[1])
    ]
    return torch.stack(grads, dim=1).view(shape)


def grad_3d(outputs, inputs, **kwargs):
    """
    Make use of vectorize `torch.autograd.grad` as done in `torch.autograd.jacobian`.

    See: https://pytorch.org/docs/stable/_modules/torch/autograd/functional.html#jacobian
    See: https://discuss.pytorch.org/t/jacobian-functional-api-batch-respecting-jacobian/84571/7
    """

    shape = outputs.shape
    ns = shape[0]
    length = shape[1] * shape[2]

    outputs = outputs.view(ns, length)

    outputs = outputs.sum(dim=0)
    grad_outputs = torch.eye(length, dtype=outputs.dtype, device=outputs.device)

    def get_vjp(v):
        return torch.autograd.grad(outputs, inputs, v, **kwargs)[0]

    vjp = vmap(get_vjp)
    grad = vjp(grad_outputs)

    return grad.T.view(shape)


def get_s_full_mesh(ns: int, dtype: torch.float64, device=None):
    """s on full mesh."""
    s_full_mesh = torch.linspace(0, 1, ns, dtype=dtype)
    if device is not None:
        s_full_mesh.to(device)
    return s_full_mesh


def get_s_half_mesh(
    ns: int,
    dtype=torch.float64,
    device=None,
    mode: str = "vmec",
):
    """
    s on half mesh.

    See VMEC2000/Release/profil1d.f#L449

    Args:
        ns: number of radial locations to include
        dtype: torch tensor dtype
        device: torch tensor device
        mode: how to generate the sqrt(s) values, `vmec` or `exact`
    Returns:
        A tensor of the square root of normalized toroidal flux computed at half-mesh.
    """

    hs = 1 / (ns - 1)

    if mode == "exact":
        s_half_mesh = torch.linspace(0, ns - 1, ns, dtype=dtype, device=device) * hs
        s_half_mesh[:-1] = 0.5 * (s_half_mesh[:-1] + s_half_mesh[1:])
    elif mode == "vmec":
        s_half_mesh = torch.empty(ns, dtype=dtype, device=device)
        for i in range(ns):
            s_half_mesh[i] = hs * abs(i - 0.5)  # i - 1.5 in Fortran
    else:
        raise ValueError("mode %s not supported" % mode)

    return s_half_mesh


def to_full_mesh(tensor, mode: str = "explicit", factor: float = 1.5):
    """
    Compute half-mesh tensor on VMEC full mesh.

    See: VMEC2000/Sources/General/add_fluxes.f90#L166
    """
    tensorf = torch.empty_like(tensor)
    tensorf[1:-1] = 0.5 * (tensor[1:-1] + tensor[2:])
    if mode == "explicit":
        tensorf[0] = factor * tensor[1] - (factor - 1) * tensor[2]
        tensorf[-1] = factor * tensor[-1] - (factor - 1) * tensor[-2]
    elif mode == "implicit":
        tensorf[0] = factor * tensorf[1] - (factor - 1) * tensorf[2]
        tensorf[-1] = factor * tensorf[-2] - (factor - 1) * tensorf[-3]
    else:
        raise ValueError("mode %s not supported" % mode)
    return tensorf


def to_half_mesh(tensorf, mode: str = "exact"):
    """
    Compute half-mesh representation of tensorf.

    See: VMEC2000/Sources/General/add_fluxes.f90#L166

    TODO-1(@amerlo): this is not exactly what VMEC does, understand why?
    """
    tensors = torch.empty_like(tensorf)
    tensors[0] = 0.0

    if mode == "simple":
        tensors[1:] = 0.5 * (tensorf[1:] + tensorf[:-1])
    elif mode == "exact":
        tensors[1] = 0.5 * (tensorf[0] + tensorf[1])
        for i in range(2, tensorf.size(0) - 1):
            tensors[i] = 2.0 * tensorf[i - 1] - tensors[i - 1]
        tensors[-1] = (2.0 * tensorf[-1] + tensors[-2]) / 3.0
    else:
        raise ValueError("Only 'simple' and 'exact' modes are supported")

    return tensors


def derivative(coef, func, var):
    """
    Compute the derivative of the Fourier series.
    """
    if var == "u":
        factor = torch.arange(coef.size(1), dtype=coef.dtype, device=coef.device)[
            None, ..., None
        ]
    elif var == "v":
        ntor = int((coef.size(2) - 1) / 2)
        factor = (
            -5
            * torch.arange(-ntor, ntor + 1, dtype=coef.dtype, device=coef.device)[
                None, None, ...
            ]
        )
    else:
        raise ValueError("var = %s not defined" % var)
    if func == "cos":
        factor *= -1
    return coef * factor


def gradient(
    tensor: Tensor,
    order: int = 1,
    factor: float = 1.5,
):
    hs = 1 / (tensor.size(0) - 1)
    #  Central finite differences scheme from torch
    if order == -2:
        return torch.gradient(tensor, spacing=hs, dim=0)[0]
    grad = torch.empty_like(tensor)
    #  Backward finite differences scheme
    if order == -1:
        grad[1:] = (tensor[1:] - tensor[:-1]) / hs
    elif order == 1:
        grad[1:-1] = (tensor[2:] - tensor[1:-1]) / hs
    #  Central finite differences scheme
    elif order == 2:
        grad[1:-1] = (tensor[2:] - tensor[:-2]) / (2 * hs)
    else:
        raise ValueError("order %s not supported" % order)
    #  Extend to s=0
    grad[0] = factor * grad[1] - (factor - 1) * grad[2]
    #  Extend to s=1
    if order > 0:
        grad[-1] = factor * grad[-2] - (factor - 1) * grad[-3]
    return grad


@cache
def get_fourier_basis(
    mpol: int,
    ntor: int,
    ntheta: int,
    nzeta: int,
    endpoint: bool,
    num_field_period: int,
    dtype: torch.dtype,
    device: torch.device,
):
    """Compute and cache the Fourier basis."""

    poloidal_modes = torch.arange(0, mpol + 1, dtype=dtype)[:, None]
    toroidal_modes = torch.arange(-ntor, ntor + 1, dtype=dtype)[None, :]

    thetas = linspace(0, 2 * math.pi, ntheta, endpoint=endpoint, dtype=dtype)
    phis = linspace(
        0, 2 * math.pi / num_field_period, nzeta, endpoint=endpoint, dtype=dtype
    )

    costzmn = torch.empty(ntheta, nzeta, mpol + 1, 2 * ntor + 1, dtype=dtype)
    sintzmn = torch.empty(ntheta, nzeta, mpol + 1, 2 * ntor + 1, dtype=dtype)

    for i, theta in enumerate(thetas):
        for j, phi in enumerate(phis):
            costzmn[i, j] = torch.cos(
                poloidal_modes * theta - num_field_period * toroidal_modes * phi
            )
            sintzmn[i, j] = torch.sin(
                poloidal_modes * theta - num_field_period * toroidal_modes * phi
            )

    costzmn = costzmn.to(device)
    sintzmn = sintzmn.to(device)

    return costzmn[None, ...], sintzmn[None, ...]


@cache
def get_even_odd_masks(mpol: int, double: bool):
    if double:
        dtype = torch.float64
    else:
        dtype = torch.float32
    even_mask = torch.tensor([(m + 1) % 2 for m in range(mpol + 1)], dtype=dtype)[
        None, ..., None
    ]
    odd_mask = torch.tensor([m % 2 for m in range(mpol + 1)], dtype=dtype)[
        None, ..., None
    ]
    return even_mask, odd_mask


@cache
def get_sqrts(ns: int, double: bool):
    """Compute and cache sqrt(s)."""
    if double:
        dtype = torch.float64
    else:
        dtype = torch.float32
    return torch.sqrt(get_s_full_mesh(ns, dtype=dtype))[..., None, None]


def to_even_odd(xmn: Tensor, basis: str, ntheta: int, nzeta: int):
    """
    Decompose the X quantity in the even and odd poloidal harmonics contribution.

    See VMEC2000/Sources/General/convert.f#L118
    See eq. (8b) and (8c) in Hirshman 1990, 10.1016/0021-9991(90)90259-4
    """

    device = xmn.device
    double = xmn.dtype == torch.float64

    even_mask, odd_mask = get_even_odd_masks(xmn.size(1) - 1, double=double)

    even_mask = even_mask.to(device)
    odd_mask = odd_mask.to(device)

    sqrts = get_sqrts(xmn.size(0), double=double)
    sqrts = sqrts.to(device)

    #  TODO-0(@amerlo): fix 1 / 0 in odd mode
    sqrts[0] = sqrts[1]

    xemn = xmn * even_mask
    xomn = 1 / sqrts * xmn * odd_mask

    if basis == "cos":
        xe = ift((xemn, None), ntheta, nzeta)
        xo = ift((xomn, None), ntheta, nzeta)
    else:
        xe = ift((None, xemn), ntheta, nzeta)
        xo = ift((None, xomn), ntheta, nzeta)

    return xe, xo


def ift(
    tensors: Union[List[Tensor], Tuple[Tensor]],
    ntheta: Optional[int] = None,
    nzeta: Optional[int] = None,
    endpoint: Optional[bool] = False,
    num_field_period: Optional[int] = 5,
):
    """
    Inverse Fourier transform.

    Examples:
        >>> from vmecnn.physics.equilibrium import ift
        >>> tensor = torch.rand(1, 2, 3)
        >>> ift((tensor, None)).size()
        torch.Size([1, 3, 3])

        >>> ift((tensor, None), ntheta=40, nzeta=36).size()
        torch.Size([1, 40, 36])

        >>> out = ift((tensor, None))
        >>> out[0, 0, 0] == tensor.sum()
        tensor(True)

    TODO-1(@amerlo): improv function signature
    TODO-1(@amerlo): time me and optimize me (this should be feasible with cartesian product)
    TODO-1(@amerlo): does it make sense to make it a class?
    TODO-1(@amerlo): add documentation.
    """

    assert len(tensors) == 2
    assert not (tensors[0] is None and tensors[1] is None)

    for tensor in tensors:
        if tensor is not None:
            assert (
                len(tensor.shape) == 3
            ), "Input tensor should be a tensor of (num_samples, poloidal_modes, toroidal_modes)."

    cosmn = tensors[0]
    sinmn = tensors[1]

    if cosmn is not None and sinmn is not None:
        assert cosmn.shape == sinmn.shape

    #  Select tensor as reference
    tensor = cosmn if cosmn is not None else sinmn

    mpol = tensor.size(1) - 1
    ntor = int((tensor.size(2) - 1) / 2)

    dtype = tensor.dtype
    device = tensor.device

    if ntheta is None:
        ntheta = 2 * mpol + 1
    if nzeta is None:
        nzeta = 2 * ntor + 1

    costzmn, sintzmn = get_fourier_basis(
        mpol,
        ntor,
        ntheta,
        nzeta,
        endpoint,
        num_field_period,
        dtype=dtype,
        device=device,
    )

    if cosmn is not None and sinmn is None:
        return torch.einsum("stzmn,smn->stz", costzmn, cosmn).contiguous()

    if sinmn is not None and cosmn is None:
        return torch.einsum("stzmn,smn->stz", sintzmn, sinmn).contiguous()

    return (
        torch.einsum("stzmn,smn->stz", costzmn, cosmn)
        + torch.einsum("stzmn,smn->stz", sintzmn, sinmn)
    ).contiguous()


def ft(
    tensor,
    basis,
    mpol,
    ntor,
    endpoint: Optional[bool] = False,
    num_field_period: Optional[int] = 5,
):
    """
    Compute the discrete Fourier transform of tensor.
    """

    assert (
        len(tensor.shape) == 3
    ), "Input tensor should be a tensor of (num_samples, thetas, zetas)."

    dtype = tensor.dtype
    device = tensor.device
    ntheta = tensor.size(1)
    nzeta = tensor.size(2)

    costzmn, sintzmn = get_fourier_basis(
        mpol,
        ntor,
        ntheta,
        nzeta,
        endpoint,
        num_field_period,
        dtype=dtype,
        device=device,
    )

    if basis == "cos":
        mn = torch.einsum("stzmn,stz->smn", costzmn, tensor).contiguous()
    else:
        mn = torch.einsum("stzmn,stz->smn", sintzmn, tensor).contiguous()

    #  Normalize by 1/n
    mn /= ntheta * nzeta

    #  Set to zero the m = 0, n < 0 as output in VMEC
    mn[:, 0, :ntor] = 0

    #  TODO-1(@amerlo): is this the correct way to handle them?
    #  This derives from the mscale and nscale factor in VMEC,
    #  where mscale[0] = nscale[0] = 1, and
    #  mscale[1:] = nscale[1:] = sqrt(2)
    #  See https://gitlab.minerva-central.net/minerva/jVMEC/-/blob/master/src/main/java/de/labathome/jvmec/FourierBasis.java#L148  # noqa
    mn *= 2
    mn[:, 0, ntor] /= 2

    return mn


def compute_jacobian(rmnc, zmns, rumns, zumnc, shalf, ntheta, nzeta):
    """
    See vmecnn.physics.Equilibrium.jacobian

    TODO-0(@amerlo): the value at si = 1 has to be still fixed.
    Using sqrt(s) (instead of sqrt(shalf)) in to_even_odd has greatly improved
    the computaiton, however, it has introduced a nan value on si = 1.
    """

    shalf = torch.sqrt(shalf)[..., None, None]

    rue, ruo = to_even_odd(rumns, "sin", ntheta, nzeta)
    ru12 = to_half_mesh(rue, mode="simple") + shalf * to_half_mesh(ruo, mode="simple")

    zue, zuo = to_even_odd(zumnc, "cos", ntheta, nzeta)
    zu12 = to_half_mesh(zue, mode="simple") + shalf * to_half_mesh(zuo, mode="simple")

    re, ro = to_even_odd(rmnc, "cos", ntheta, nzeta)
    r12 = to_half_mesh(re, mode="simple") + shalf * to_half_mesh(ro, mode="simple")
    rs = gradient(re, order=-1) + shalf * gradient(ro, order=-1)

    ze, zo = to_even_odd(zmns, "sin", ntheta, nzeta)
    zs = gradient(ze, order=-1) + shalf * gradient(zo, order=-1)

    #  Compose jacobian
    tau = ru12 * zs - rs * zu12
    tau[1:] += 0.25 * (
        ruo[1:] * zo[1:] + ruo[:-1] * zo[:-1] - zuo[1:] * ro[1:] - zuo[:-1] * ro[:-1]
    )
    tau[1:] += (
        0.25
        * (
            rue[1:] * zo[1:]
            + rue[:-1] * zo[:-1]
            - zue[1:] * ro[1:]
            - zue[:-1] * ro[:-1]
        )
        / shalf[1:]
    )

    #  See VMEC2000/Sources/General/bcovar.f#L198
    gsqrt = tau * r12

    #  See VMEC2000/Sources/General/bcovar.f#L200
    gsqrt[0] = gsqrt[1]

    return gsqrt


def compute_bounce_points(
    b: np.ndarray, bref: np.ndarray, thetas: np.ndarray, phis: np.ndarray
) -> np.ndarray:
    """
    Compute toroidal angular distance between bounce points.
    """

    assert bref.shape == (b.shape[0],)
    bref = bref[:, None, None]

    #  Get bounce points
    idx = np.argwhere(np.diff(np.sign(b - bref)))
    idx[:, -1] += 1

    #  Get bounce points closer to mid-period
    idx_half = int(len(phis) / 2)
    idx_clean = np.empty((2 * b.shape[0] * b.shape[1], 3), dtype=int)
    c = 0
    for i in range(b.shape[0]):
        for j in range(b.shape[1]):
            idx_ = idx[idx[:, 0] == i]
            idx_ = idx_[idx_[:, 1] == j]
            try:
                #  Take last from the left and first from the right
                idx_l = idx_[idx_[:, -1] <= idx_half][:, -1][-1]
                idx_r = idx_[idx_[:, -1] > idx_half][:, -1][0]
            except IndexError:
                #  Set index to -1 flag to signal that no bounce points have been found
                idx_l = -1
                idx_r = -1
            idx_clean[c] = np.array([i, j, idx_l], dtype=int)
            idx_clean[c + 1] = np.array([i, j, idx_r], dtype=int)
            c += 2
    idx = idx_clean

    delta = np.empty(b.shape[:2])
    delta.fill(np.nan)
    for i in range(int(idx.shape[0] / 2)):
        #  If no bounce points on the field line, skip it
        if idx[2 * i, 2] == -1 or idx[2 * i + 1, 2] == -1:
            continue
        delta_phi = phis[idx[2 * i + 1, 2]] - phis[idx[2 * i, 2]]
        delta_theta = (
            thetas[idx[2 * i, 0], idx[2 * i, 1], idx[2 * i + 1, 2]]
            - thetas[idx[2 * i, 0], idx[2 * i, 1], idx[2 * i, 2]]
        )
        delta[idx[2 * i, 0], idx[2 * i, 1]] = np.sqrt(delta_phi**2 + delta_theta**2)

    return delta
