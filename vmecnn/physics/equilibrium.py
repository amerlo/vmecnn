"""Equilibrium object."""

import math
import os
import re
import tempfile
import subprocess
from typing import Dict, Optional, Union, List, Tuple, Callable

try:
    import booz_xform
except ImportError as e:
    print(e)
    booz_xform = None
import matplotlib
import matplotlib.pyplot as plt
import netCDF4
import numpy as np
import torch
from matplotlib.colors import ListedColormap
from torch import Tensor
from torch.nn.functional import interpolate

from vmecnn.metrics.utils import mape, nrmse, r2_score
from vmecnn.utils import get_ax, asarray, eps, linspace
from vmecnn.physics.utils import (
    gradient,
    get_s_half_mesh,
    to_full_mesh,
    to_half_mesh,
    ift,
    ft,
    derivative,
    compute_jacobian,
    compute_bounce_points,
    MEAN_F_START,
)


def _to_vmec(tensor):
    array = tensor.detach().numpy()
    ns = array.shape[0]
    ntor = int((array.shape[2] - 1) / 2)
    return array.reshape(ns, -1)[:, ntor:]


def _to_booz(tensor):
    return _to_vmec(tensor).swapaxes(0, 1)


class Neo:
    """
    Booz_xform input is supported only.

    TODO-0(@amerlo): find good values for ntheta and nzeta
    TODO-1(@amerlo): provide function to discover neo path
    """

    def __init__(
        self,
        booz_xform,
        compute_surfs: List[int],
        ntheta: Optional[int] = 200,
        nzeta: Optional[int] = 200,
        executable: str = "~/bin/xneo",
    ):
        self.bx = booz_xform
        self.compute_surfs = compute_surfs
        self.ntheta = ntheta
        self.nzeta = nzeta
        self.executable = os.path.expanduser(executable)

        self.is_complete = False
        self.eps_eff = None

    @classmethod
    def from_booz(cls, booz_xform, *args, **kwargs):
        #  TODO-0(@amerlo): in order to get neo to work, we need to add +2 to
        #  the flux surfaces indices. Why?
        return cls(booz_xform=booz_xform, compute_surfs=booz_xform.compute_surfs + 2)

    def run(self) -> None:
        """
        TODO-0(@amerlo): put everything into try except or with statement
        """

        d = tempfile.TemporaryDirectory()

        #  Prepare booz_xform source file
        #  TODO-1(@amerlo): generate name from hash
        extension = "vmecnn"
        booz_file_name = os.path.join(d.name, "boozmn_" + extension + ".nc")
        self.bx.write_boozmn(booz_file_name)
        assert os.path.isfile(booz_file_name)

        #  Write neo input file
        neo_in_file_name = os.path.join(d.name, "neo_in." + extension)
        with open(neo_in_file_name, "w") as f:
            f.write(self._get_neo_in())
        assert os.path.isfile(neo_in_file_name)

        #  Run neo
        p = subprocess.Popen([self.executable, extension], cwd=d.name)
        p.wait()

        #  Read back result
        neo_out_file_name = os.path.join(d.name, "neo_out")
        assert os.path.isfile(neo_out_file_name)

        #  See output file structure at: https://princetonuniversity.github.io/STELLOPT/NEO
        #  with EOUT_SWI=1, the second column is eps_eff^(3/2)
        neo_out = np.loadtxt(neo_out_file_name)
        eps_eff = neo_out[:, 1]
        assert len(eps_eff) == len(self.compute_surfs)
        self.eps_eff = eps_eff

        self.is_complete = True
        d.cleanup()

    def _get_neo_in(self):
        #  TODO-0(@amerlo): which are the correct input values here for W7-X?
        #  See: https://princetonuniversity.github.io/STELLOPT/NEO%20tutorial
        neo_in = ""
        for _ in range(4):
            neo_in += "#" + "\n"
        neo_in += "neo_out" + "\n"
        neo_in += str(len(self.compute_surfs)) + "\n"
        neo_in += " ".join([str(s) for s in self.compute_surfs]) + "\n"
        neo_in += str(self.ntheta) + "\n"
        neo_in += str(self.nzeta) + "\n"
        neo_in += "0" + "\n"  # max_m_mode
        neo_in += "0" + "\n"  # max_n_mode
        neo_in += "75" + "\n"  # npart
        neo_in += "1" + "\n"  # multra
        neo_in += "0.01" + "\n"  # acc_req
        neo_in += "100" + "\n"  # no_bins
        neo_in += "75" + "\n"  # nstep_per
        neo_in += "500" + "\n"  # nstep_min
        neo_in += "2000" + "\n"  # nstep_max
        neo_in += "0" + "\n"  # calc_nstep_max
        neo_in += "1" + "\n"  # eout_swi
        neo_in += "0" + "\n"  # lab_swi
        neo_in += "0" + "\n"  # inp_swi
        neo_in += "2" + "\n"  # ref_swi
        neo_in += "0" + "\n"  # write_progress
        neo_in += "0" + "\n"  # write_output_files
        neo_in += "0" + "\n"  # spline_test
        neo_in += "0" + "\n"  # write_integrate
        neo_in += "0" + "\n"  # write_diagnostic
        for _ in range(3):
            neo_in += "#" + "\n"
        neo_in += "0" + "\n"  # calc_cur
        neo_in += "neo_cur" + "\n"
        neo_in += "0" + "\n"  # npart_cur
        neo_in += "0" + "\n"  # alpha_cur
        neo_in += "0" + "\n"  # write_cur_inte

        return neo_in


class Equilibrium:
    """
    An ideal-MHD equilibrium

    TODO-0(@amerlo): add typing and checks on input!
    TODO-0(@amerlo): consider to provide me via w7x
    TODO-1(@amerlo): add documentation.
    TODO-0(@amerlo): add computation of the following quantities:
    """

    mu0 = 4 * math.pi * 1e-7

    #  The sign of the Jacobian which is always negative in VMEC
    signgs = -1

    #  Stellarator symmetry
    lasym = False

    #  Number of field period
    num_field_period = 5

    #  TODO-1(@amerlo): fix mpol to 17
    #  TODO-1(@amerlo): where to place these?
    mpol_nyq = 16
    ntor_nyq = 18

    #  Training mgrid file
    mgrid = "mgrid_w7x_nv36_hires.nc"

    def __init__(
        self,
        rmnc=None,
        lmns=None,
        zmns=None,
        iota=None,
        normalized_flux=None,
        phiedge=None,
        pressure=None,
        toroidal_current=None,
        npc=None,
        pc=None,
        ntheta: int = 32,
        nzeta: int = 36,
        wout_path: str = None,
    ):
        self.rmnc = torch.as_tensor(rmnc) if rmnc is not None else None
        self.lmns = torch.as_tensor(lmns) if lmns is not None else None
        self.zmns = torch.as_tensor(zmns) if zmns is not None else None
        self.iota = torch.as_tensor(iota) if iota is not None else None

        self.normalized_flux = normalized_flux
        if normalized_flux is not None:
            self.normalized_flux = torch.as_tensor(normalized_flux)

        self.phiedge = torch.as_tensor(phiedge) if phiedge is not None else None
        self.pressure = torch.as_tensor(pressure) if pressure is not None else None

        self.toroidal_current = (
            torch.as_tensor(toroidal_current) if toroidal_current is not None else None
        )
        self.npc = torch.as_tensor(npc) if npc is not None else None
        self.pc = torch.as_tensor(pc) if pc is not None else None

        self.ntheta = ntheta
        self.nzeta = nzeta

        self.wout_path = wout_path

        #  booz_xform
        self.bx = None
        #  In Booz_xform, the flux surface with index 0 is the VMEC 1st flux surface on half-mesh
        #  TODO-1(@amerlo): put back the VMEC index 1, Booz_xform index 0
        self.compute_surfs = np.arange(1, self.ns - 1, dtype=int)
        self.mboz = (self.mpol + 1) * 6
        self.nboz = 2 * self.ntor + 1
        self._has_bx_run = False

        #  neo
        self.neo = None

    def __getattr__(self, name):
        res = re.search(r"(\w+)_{\s*(\d)\s*,\s*(-?\d)\s*}", name)
        if res is not None:
            _name = res.groups()[0]
            mpol = int(res.groups()[1])
            ntor = int(res.groups()[2])
            if _name in ("rmnc", "lmns", "zmns", "bmnc"):
                if _name in ("bmnc",):
                    ntor += self.ntor_nyq
                else:
                    ntor += self.ntor
                attr = getattr(self, _name)
                return attr[:, mpol, ntor]
        raise AttributeError("Argument %s does not exist" % name)

    @classmethod
    def from_dict(cls, data: Dict[str, Tensor]):
        """Get equilibrium from dict."""
        return cls(
            rmnc=data["rmnc"],
            lmns=data["lmns"],
            zmns=data["zmns"],
            iota=data["iota"],
            phiedge=data["phiedge"],
            pressure=data["pressure"],
            normalized_flux=data["normalized_flux"],
            toroidal_current=data["toroidal_current"],
            wout_path=data["wout_path"],
        )

    def as_dict(self):
        """Get equilibrium as model dict."""
        return {
            "xmn": torch.stack([self.rmnc, self.lmns, self.zmns], dim=-1),
            "iota": self.iota,
        }

    @classmethod
    def from_model(cls, data: Dict[str, Tensor]):
        """Get equilibrium from dict model output."""

        #  Extract the output
        rmnc, lmns, zmns, iota = None, None, None, None

        if "xmn" in data:
            assert data["xmn"].size(-1) == 3
            rmnc = data["xmn"][..., 0]
            lmns = data["xmn"][..., 1]
            zmns = data["xmn"][..., 2]

        if "iota" in data:
            iota = data["iota"].view(-1)

        #  Extract the inputs
        normalized_flux = None
        if "normalized_flux" in data:
            normalized_flux = data["normalized_flux"]
            #  Check s is ordered
            assert (normalized_flux[1:] - normalized_flux[:-1] > 0).all()

        def _get_and_validate_key(key: str):
            value = None
            if key in data:
                value = data[key][0]
                if isinstance(value, Tensor):
                    value = value.clone()
                    assert (data[key] == value).all()
                else:
                    for d in data[key]:
                        assert d == value
            return value

        pressure = _get_and_validate_key("pressure")
        p0 = _get_and_validate_key("p0")

        #  Interpolate pressure profile
        if pressure is not None:
            if pressure.size(0) < normalized_flux.size(0):
                pressure = interpolate(
                    pressure.view(1, 1, -1),
                    size=normalized_flux.size(0),
                    mode="linear",
                    align_corners=True,
                ).view(-1)
            if p0 is not None:
                pressure *= p0

        toroidal_current = _get_and_validate_key("toroidal_current")
        curtor = _get_and_validate_key("curtor")

        #  Interpolate toroidal current profile
        if toroidal_current is not None:
            if toroidal_current.size(0) < normalized_flux.size(0):
                toroidal_current = interpolate(
                    toroidal_current.view(1, 1, -1),
                    size=normalized_flux.size(0),
                    mode="linear",
                    align_corners=True,
                ).view(-1)
            if curtor is not None:
                toroidal_current *= curtor

        phiedge = _get_and_validate_key("phiedge")
        npc = _get_and_validate_key("npc")
        pc = _get_and_validate_key("pc")

        wout_path = _get_and_validate_key("wout_path")

        return cls(
            rmnc=rmnc,
            lmns=lmns,
            zmns=zmns,
            iota=iota,
            phiedge=phiedge,
            pressure=pressure,
            toroidal_current=toroidal_current,
            normalized_flux=normalized_flux,
            npc=npc,
            pc=pc,
            wout_path=wout_path,
        )

    @property
    def wout(self):
        if self.wout_path is None:
            return self._to_wout()
        return netCDF4.Dataset(self.wout_path)

    def _to_wout(self) -> netCDF4.Dataset:
        """Create a VMEC wout object from Equilibrium object."""

        wout = netCDF4.Dataset(
            "wout.nc",
            mode="w",
            diskless=True,
            persist=False,
            format="NETCDF4_CLASSIC",
        )

        out_dtype = np.float64 if self.rmnc.dtype == torch.float64 else np.float32
        mn_num = self.xm.shape[0]
        mn_num_nyq = self.xm_nyq.shape[0]

        #  TODO-1(@amerlo): add all relevant outputs
        dimensions = [
            ("dim_00200", 200),
            ("mn_mode", mn_num),
            ("mn_mode_nyq", mn_num_nyq),
            ("radius", self.ns),
        ]
        for dim in dimensions:
            wout.createDimension(*dim)

        variables = [
            ("nfp", np.int32, (), self.num_field_period),
            ("ns", np.int32, (), self.ns),
            ("xm", np.float64, ("mn_mode",), self.xm),
            ("xn", np.float64, ("mn_mode",), self.xn),
            ("phi", out_dtype, ("radius",), self.normalized_flux * self.phiedge),
            ("rmnc", out_dtype, ("radius", "mn_mode"), _to_vmec(self.rmnc)),
            ("lmns", out_dtype, ("radius", "mn_mode"), _to_vmec(self.lmns)),
            ("zmns", out_dtype, ("radius", "mn_mode"), _to_vmec(self.zmns)),
            ("iotaf", out_dtype, ("radius",), self.iota),
            ("presf", out_dtype, ("radius",), self.pressure),
            ("vp", out_dtype, ("radius",), self.vp),
            ("volume_p", out_dtype, (), self.volume),
            ("betatotal", out_dtype, (), self.beta),
            ("Rmajor_p", out_dtype, (), self.rmajor),
            ("Aminor_p", out_dtype, (), self.aminor),
            ("bmnc", out_dtype, ("radius", "mn_mode_nyq"), _to_vmec(self.bmnc)),
        ]

        for name, dtype, size, value in variables:
            var = wout.createVariable(name, dtype, size)
            var[:] = value

        #  Add mgrid_file
        mgrid = wout.createVariable("mgrid_file", np.dtype("S1"), ("dim_00200",))
        mgrid_file = self.mgrid + "".join([" "] * (200 - len(self.mgrid)))
        for i, v in enumerate(mgrid_file):
            mgrid[i] = v

        return wout

    @property
    def ns(self):
        return self.normalized_flux.size(0)

    @property
    def mpol(self):
        return self.rmnc.size(1) - 1

    @property
    def ntor(self):
        return int((self.rmnc.size(2) - 1) / 2)

    @property
    def xm(self):
        xm = torch.arange(0, self.mpol + 1)
        return torch.repeat_interleave(xm, 2 * self.ntor + 1)[self.ntor :]

    @property
    def xn(self):
        xn = torch.arange(-self.ntor, self.ntor + 1) * self.num_field_period
        return xn.repeat(self.mpol + 1)[self.ntor :]

    @property
    def xm_nyq(self):
        xm_nyq = torch.arange(0, self.mpol_nyq + 1)
        return torch.repeat_interleave(xm_nyq, 2 * self.ntor_nyq + 1)[self.ntor_nyq :]

    @property
    def xn_nyq(self):
        xn_nyq = torch.arange(-self.ntor_nyq, self.ntor_nyq + 1) * self.num_field_period
        return xn_nyq.repeat(self.mpol_nyq + 1)[self.ntor_nyq :]

    @property
    def p0(self):
        """
        Pressure value on axis in Pa.
        """
        return self.pressure[0]

    @property
    def curtor(self):
        """
        Total toroidal current enclosed in the plasma.
        """
        return self.toroidal_current[-1]

    @property
    def toroidal_current_peaking_factor(self):
        """
        The toroidal current peaking factor.
        """
        return self.toroidal_current[-1] / self.toroidal_current.mean()

    @property
    def pressure_peaking_factor(self):
        """
        The pressure profile peaking factor.
        """
        return self.pressure[0] / self.pressure.mean()

    @property
    def iA(self):
        """
        iA planar coil current ration.
        """
        return self.pc[0]

    @property
    def iB(self):
        """
        iB planar coil current ration.
        """
        return self.pc[1]

    @property
    def phip(self):
        """
        phip = d(Phi)/d(s)
        """
        return self.phiedge / math.pi / 2 / self.signgs

    @property
    def shalf(self):
        """
        s on half-mesh.
        """
        return get_s_half_mesh(
            self.ns,
            dtype=self.normalized_flux.dtype,
            device=self.normalized_flux.device,
        )

    @property
    def shalf_b(self):
        """
        s on half-mesh at Boozer indices.
        """
        return self.shalf[self.compute_surfs + 1]

    def to_vmec_indata(self, proto: str) -> str:
        """
        Create a vmec input text based on `proto` from equi.

        TODO-1(@amerlo): make proto optional and
                         use proto file from `data/input.standard`
        TODO-1(@amerlo): set:
            - boundary
            - pressure and current profiles
        """

        indata = None
        with open(proto, "r") as f:
            indata = f.read()

        #  Set input parameters
        #  13068 is the reference coil currents I1 used during training
        extcur = np.array([1] + self.npc.tolist() + self.pc.tolist() + [0] * 7) * 13068
        extcur = "EXTCUR = " + " ".join([str(i) for i in extcur.tolist()])
        group = re.search("EXTCUR = .*", indata).group()
        indata = indata.replace(group, extcur)

        group = re.search("PHIEDGE = .*", indata).group()
        indata = indata.replace(group, "PHIEDGE = " + str(self.phiedge.item()))

        group = re.search("PRES_SCALE = .*", indata).group()
        indata = indata.replace(group, "PRES_SCALE = " + str(self.p0.item()))

        group = re.search("CURTOR = .*", indata).group()
        indata = indata.replace(group, "CURTOR = " + str(self.curtor.item()))

        raxis = self.rmnc[0, 0, self.ntor :].tolist()
        raxis = "RAXIS_CC = " + " ".join([str(i) for i in raxis])
        group = re.search("RAXIS_CC = .*", indata).group()
        indata = indata.replace(group, raxis)

        zaxis = self.zmns[0, 0, self.ntor :].tolist()
        zaxis = "ZAXIS_CS = " + " ".join([str(i) for i in zaxis])
        group = re.search("ZAXIS_CS = .*", indata).group()
        indata = indata.replace(group, zaxis)

        return indata

    @property
    def dshear(self):
        """
        Compute magnetic shear as in Mercier criterion on full-mesh.

        The values on axis and at the LCFS are extrapolated from the closest flux surfaces.

        The magnetic shear is defined as shear = d(iota)/d(phi).
        As computed by VMEC, the magnetic shear requires the iota profile
        on the half-mesh (iotas).

        This compute the magnetic shear as show in eq. (4) of
        Carreras 1998, 10.1088/0029-5515/28/7/004

        See VMEC2000/Sources/Input_Output/mercier.f#L101
        """
        return (self.shear**2) / 4

    @property
    def iotas(self):
        """
        Compute iota on half-mesh (i.e., iotas).
        """
        return to_half_mesh(self.iota)

    @property
    def mean_iota(self):
        """
        Compute the mean iota value over the normalized toroidal flux.

        See: https://github.com/hiddenSymmetries/simsopt/blob/master/src/simsopt/mhd/vmec.py
        """
        return self.iotas[1:].mean()

    @property
    def volume(self):
        """Compute the plasma volume."""
        return self.vp[1:].mean() * 4 * math.pi**2

    @property
    def aminor(self):
        """
        Compute the plasma minor radius.

        See VMEC2000/Sources/General/aspectratio.f#L41
        """
        cross_area = 2 * math.pi * (self.r[-1] * self.zu[-1]).mean().abs()
        return torch.sqrt(cross_area / math.pi)

    @property
    def rmajor(self):
        """
        Compute the plasma major radius.

        See VMEC2000/Sources/General/aspectratio.f#L40
        """
        volume = 2 * math.pi**2 * (self.r[-1] ** 2 * self.zu[-1]).mean().abs()
        cross_area = 2 * math.pi * (self.r[-1] * self.zu[-1]).mean().abs()
        return volume / (2 * math.pi * cross_area)

    @property
    def mean_elongation(self):
        """
        Compute mean elongation.

        See VMEC2000/Sources/General/eqfor.f
        """
        guu = self.ru[-1] ** 2 + self.zu[-1] ** 2
        surf_area = torch.sqrt(
            self.r[-1] ** 2 * guu
            + (self.rv[-1] * self.zu[-1] - self.zv[-1] * self.ru[-1]) ** 2
        )
        surf_area_p = (2 * torch.pi) ** 2 * surf_area.mean()
        volume = 2 * math.pi**2 * (self.r[-1] ** 2 * self.zu[-1]).mean().abs()
        d_of_kappa = surf_area_p * self.aminor / (2 * volume)
        kappa_p = 1 + (torch.pi**2 / 8) * (
            d_of_kappa**2 + torch.sqrt((d_of_kappa**4 - 1).abs()) - 1
        )
        return kappa_p

    @property
    def mean_iota_(self):
        """Compute the volume averaged iota value over the normalized toroidal flux."""
        volume = self.vp[1:].mean()
        return (self.iotas * self.vp)[1:].mean() / volume

    @property
    def shear(self):
        """Compute magnetic shear on full mesh."""
        shear = gradient(self.iotas, order=1, factor=2.0)
        shear /= self.signgs * self.phiedge
        return shear

    @property
    def shear_(self):
        """Compute magnetic shear on full mesh directly from iota (on full mesh)."""
        shear = gradient(self.iota, order=2, factor=2.0)
        shear /= self.signgs * self.phiedge
        return shear

    @property
    def mean_shear(self):
        """
        Compute the mean magnetic shear over the normalized toroidal flux.

        See: https://github.com/hiddenSymmetries/simsopt/blob/master/src/simsopt/mhd/vmec.py
        """

        #  Fit a linear polynomial
        poly = np.polynomial.Polynomial.fit(
            get_s_half_mesh(self.ns)[1:].numpy(),
            asarray(self.iotas[1:]),
            deg=1,
        )

        #  Return the slope
        return torch.as_tensor(poly.deriv()(0))

    @property
    def volume_averaged_shear(self):
        """
        Compute the volume averaged magnetic shear.
        """
        vp = to_full_mesh(self.vp)
        return (self.shear * vp).mean() / vp.mean()

    @property
    def jacobian_(self):
        """
        Compute the Jacobian of the transformation on half-mesh.

        sqrt(g) = R * (Ru * Zs - Rs * Zu)

        The jacobian is computed as eq. (17b) of Hirshman 1983, 10.1063/1.864116

        This method uses the conservative differencing (scheme I), as in:
        eq. (3) of Hirshman1990, 10.1016/0021-9991(90)90259-4
        """

        ru = to_half_mesh(self.r * self.ru, mode="simple")
        zu = to_half_mesh(self.zu, mode="simple")

        zs = gradient(self.z, order=-1)
        rs = gradient(0.5 * self.r**2, order=-1)

        return ru * zs - rs * zu

    @property
    def jacobian(self):
        """
        Compute the Jacobian as in VMEC.

        See eq. (9a) and (9b) in Hirshman 1990, 10.1016/0021-9991(90)90259-4
        See VMEC2000/Sources/General/jacobian.f#L138
        """
        return compute_jacobian(
            self.rmnc,
            self.zmns,
            self.rumns,
            self.zumnc,
            self.shalf,
            self.ntheta,
            self.nzeta,
        )

    @property
    def gmnc(self):
        """
        Compute the Fourier transform of the Jacobian of the transformation.

        sqrt(g) = R * (Ru * Zs - Rs * Zu)
        """
        return ft(self.jacobian, "cos", self.mpol_nyq, self.ntor_nyq)

    @property
    def rumns(self):
        return derivative(self.rmnc, "cos", "u")

    @property
    def zsmns(self):
        return gradient(self.zmns, order=-1)

    @property
    def rsmnc(self):
        return gradient(self.rmnc, order=-1)

    @property
    def zumnc(self):
        return derivative(self.zmns, "sin", "u")

    @property
    def r(self):
        return ift((self.rmnc, None), self.ntheta, self.nzeta)

    @property
    def r_axis(self):
        """R on axis at phi=0."""
        return self.rmnc[0, 0].sum()

    @property
    def z(self):
        return ift((None, self.zmns), self.ntheta, self.nzeta)

    @property
    def ru(self):
        return ift((None, self.rumns), self.ntheta, self.nzeta)

    @property
    def zs(self):
        return ift((None, self.zsmns), self.ntheta, self.nzeta)

    @property
    def rs(self):
        return ift((self.rsmnc, None), self.ntheta, self.nzeta)

    @property
    def zu(self):
        return ift((self.zumnc, None), self.ntheta, self.nzeta)

    @property
    def bsupu(self):
        """
        Compute the contravariant theta component of the magnetic field.

        See eq. (3b) in Hirshman 1983, 10.1063/1.864116
        See VMEC2000/Sources/General/bcovar.f#L183
        """
        bsupu = (
            self.phip.view(-1, 1, 1)
            * (self.iotas.view(-1, 1, 1) - self.lv)
            / self.jacobian
        )
        bsupu[0] = 0
        return bsupu

    @property
    def l(self):  # noqa: E741, E743
        return ift((None, self.lmns), self.ntheta, self.nzeta)

    @property
    def lvmnc(self):
        return derivative(self.lmns, "sin", "v")

    @property
    def lv(self):
        return ift((self.lvmnc, None), self.ntheta, self.nzeta)

    @property
    def bsupv(self):
        """
        Compute the contravariant zeta component of the magnetic field.

        See eq. (3c) in Hirshman 1983, 10.1063/1.864116
        """
        bsupv = self.phip.view(-1, 1, 1) * (1 + self.lu) / self.jacobian
        bsupv[0] = 0
        return bsupv

    @property
    def lumnc(self):
        return derivative(self.lmns, "sin", "u")

    @property
    def lu(self):
        return ift((self.lumnc, None), self.ntheta, self.nzeta)

    @property
    def bsubs(self):
        """
        Compute the covariant radial component of the magnetic field.

        See eq. (3d) in Hirshman 1983, 10.1063/1.864116
        See VMEC2000/Sources/Input_Output/bss.f#L38
        """
        return self.bsupu * self.gus + self.bsupv * self.gvs

    @property
    def bsubu(self):
        """
        Compute the covariant theta component of the magnetic field.

        See eq. (3d) in Hirshman 1983, 10.1063/1.864116
        See VMEC2000/Sources/General/bcovar.f#L701
        """
        return self.bsupu * self.guu + self.bsupv * self.guv

    @property
    def bsubv(self):
        """
        Compute the covariant zeta component of the magnetic field.

        See eq. (3d) in Hirshman 1983, 10.1063/1.864116
        See VMEC2000/Sources/General/bcovar.f#L703
        """
        return self.bsupu * self.guv + self.bsupv * self.gvv

    @property
    def bsubumnc(self):
        return ft(self.bsubu, "cos", self.mpol_nyq, self.ntor_nyq)

    @property
    def bsubvmnc(self):
        return ft(self.bsubv, "cos", self.mpol_nyq, self.ntor_nyq)

    @property
    def b(self):
        """
        Compute mod-B on half mesh.

        See eq. (8b) in Hirshman 1983, 10.1063/1.864116
        """
        bsq = (
            self.bsupu**2 * self.guu
            + 2 * self.bsupu * self.bsupv * self.guv
            + self.bsupv**2 * self.gvv
        )
        return torch.sqrt(bsq)

    @property
    def b_full_mesh(self):
        """Compute mod-B on full mesh."""
        return to_full_mesh(self.b)

    @property
    def bmnc(self):
        """
        Compute the Fourier representation of mod-B.
        """
        return ft(self.b, "cos", self.mpol_nyq, self.ntor_nyq)

    @property
    def guu(self):
        """
        g_uu component of the metric tensor on half-mesh.

        See eq. (9a) in Hirshman 1983, 10.1063/1.864116
        See VMEC2000/Sources/General/bcovar.f#L587
        """
        #  TODO-0(@amerlo): fix even-odd scheme
        # rue, ruo = to_even_odd(self.rumns, "sin", self.ntheta, self.nzeta)
        # zue, zuo = to_even_odd(self.zumnc, "cos", self.ntheta, self.nzeta)
        # guu = (
        #     rue * rue
        #     + zue * zue
        #     + self.normalized_flux[..., None, None] * (ruo * ruo + zuo * zuo)
        # )
        # luu = 2 * (rue * ruo + zue * zuo)
        # shalf = torch.sqrt(self.shalf)[..., None, None]
        # return to_half_mesh(guu, mode="simple") + shalf * to_half_mesh(
        #     luu, mode="simple"
        # )
        guu = self.ru * self.ru + self.zu * self.zu
        return to_half_mesh(guu, mode="simple")

    @property
    def guv(self):
        """
        g_uv component of the metric tensor on half-mesh.

        See eq. (9a) in Hirshman 1983, 10.1063/1.864116
        See VMEC2000/Sources/General/bcovar.f#L597
        """
        #  TODO-0(@amerlo): fix even-odd scheme
        # rue, ruo = to_even_odd(self.rumns, "sin", self.ntheta, self.nzeta)
        # rve, rvo = to_even_odd(self.rvmns, "sin", self.ntheta, self.nzeta)
        # zue, zuo = to_even_odd(self.zumnc, "cos", self.ntheta, self.nzeta)
        # zve, zvo = to_even_odd(self.zvmnc, "cos", self.ntheta, self.nzeta)
        # guv = (
        #     rue * rve
        #     + zue * zve
        #     + self.normalized_flux[..., None, None] * (ruo * rvo + zuo * zvo)
        # )
        # luv = rue * rvo + ruo * rve + zue * zvo + zuo * zve
        # shalf = torch.sqrt(self.shalf)[..., None, None]
        # return to_half_mesh(guv, mode="simple") + shalf * to_half_mesh(
        #     luv, mode="simple"
        # )
        guv = self.ru * self.rv + self.zu * self.zv
        return to_half_mesh(guv, mode="simple")

    @property
    def rvmns(self):
        return derivative(self.rmnc, "cos", "v")

    @property
    def rv(self):
        return ift((None, self.rvmns), self.ntheta, self.nzeta)

    @property
    def zvmnc(self):
        return derivative(self.zmns, "sin", "v")

    @property
    def zv(self):
        return ift((self.zvmnc, None), self.ntheta, self.nzeta)

    @property
    def gvv(self):
        """
        g_vv component of the metric tensor on half-mesh.

        See eq. (9a) in Hirshman 1983, 10.1063/1.864116
        See VMEC2000/Sources/General/bcovar.f#L605
        """
        #  TODO-0(@amerlo): fix even-odd scheme
        # rve, rvo = to_even_odd(self.rvmns, "sin", self.ntheta, self.nzeta)
        # zve, zvo = to_even_odd(self.zvmnc, "cos", self.ntheta, self.nzeta)
        # re, ro = to_even_odd(self.rmnc, "cos", self.ntheta, self.nzeta)
        # gvv = (
        #     rve * rve
        #     + zve * zve
        #     + self.normalized_flux[..., None, None] * (rvo * rvo + zvo * zvo)
        # )
        # lvv = 2 * (rve * rvo + zve * zvo)
        # shalf = torch.sqrt(self.shalf)[..., None, None]
        # rsq = re * re + self.normalized_flux[..., None, None] * (ro * ro)
        # #  phipog == r1 in VMEC
        # r1 = 2 * re * ro
        # rsq = to_half_mesh(rsq, mode="simple") + shalf * to_half_mesh(r1, mode="simple")
        # gvv = to_half_mesh(gvv, mode="simple") + shalf * to_half_mesh(
        #     lvv, mode="simple"
        # )
        # return gvv + rsq
        gvv = self.rv * self.rv + self.r * self.r + self.zv * self.zv
        return to_half_mesh(gvv, mode="simple")

    @property
    def gus(self):
        """
        g_us component of the metric tensor on half-mesh.

        See eq. (9a) in Hirshman 1983, 10.1063/1.864116
        See VMEC2000/Sources/Input_Output/bss.f#L33
        """
        #  TODO-1(@amerlo): add even-odd scheme as for other components
        return (
            to_half_mesh(self.ru, mode="simple") * self.rs
            + to_half_mesh(self.zu, mode="simple") * self.zs
        )

    @property
    def gvs(self):
        """
        g_vs component of the metric tensor on half-mesh.

        See eq. (9a) in Hirshman 1983, 10.1063/1.864116
        See VMEC2000/Sources/Input_Output/bss.f#L34
        """
        #  TODO-1(@amerlo): add even-odd scheme as for other components
        return (
            to_half_mesh(self.rv, mode="simple") * self.rs
            + to_half_mesh(self.zv, mode="simple") * self.zs
        )

    @property
    def bsubsmns(self):
        return ft(self.bsubs, "sin", self.mpol_nyq, self.ntor_nyq)

    @property
    def bsubsumnc(self):
        return derivative(self.bsubsmns, "sin", "u")

    @property
    def bsubsu(self):
        """The poloidal derivative of the covariant radial component of the magnetic field."""
        return ift((self.bsubsumnc, None), self.ntheta, self.nzeta)

    @property
    def bsubsvmnc(self):
        return derivative(self.bsubsmns, "sin", "v")

    @property
    def bsubsv(self):
        """The toroidal derivative of the covariant radial component of the magnetic field."""
        return ift((self.bsubsvmnc, None), self.ntheta, self.nzeta)

    @property
    def bsubvumns(self):
        return derivative(self.bsubvmnc, "sin", "u")

    @property
    def bsubvu(self):
        """The poloidal derivative of the covariant toroidal component of the magnetic field."""
        return ift((None, self.bsubvumns), self.ntheta, self.nzeta)

    @property
    def bsubuvmns(self):
        return derivative(self.bsubumnc, "sin", "v")

    @property
    def bsubuv(self):
        """The toroidal derivative of the covariant poloidal component of the magnetic field."""
        return ift((None, self.bsubuvmns), self.ntheta, self.nzeta)

    @property
    def vp(self):
        r"""
        Compute the differential volume element on half-mesh.

        Vp = \int d\theta d\zeta |\sqrt(g)| = d(VOL)/d(s)

        The differential volume element is computed as in
        Hirshman 1983 (see below eq. (7), 10.1063/1.864116

        See VMEC2000/Sources/General/bcovar.f#L156
        """
        return torch.abs(torch.mean(self.jacobian, dim=(1, 2)))

    @property
    def vp_real(self):
        """Compute d(V)/d(Phi)."""
        return self.signgs * 4 * math.pi**2 * self.vp / self.phiedge

    @property
    def well(self):
        """
        Compute the magnetic well.

        Vpp = d(Vp)/d(Phi)

        See VMEC2000/Sources/General/mercier.f#L102
        """
        well = gradient(self.vp_real, order=1, factor=2.0) / self.phiedge
        return -well * self.signgs

    @property
    def well_(self):
        """
        Compute the full magnetic well, including beta effects.

        W = V / B**2 d(2 mu0 p + B**2) / dV

        See Eq (4) of Green1998.
        See https://github.com/PrincetonUniversity/STELLOPT/blob/develop/STELLOPTV2/Sources/Chisq/chisq_magwell.f90  # noqa
        """
        jacobian = self.jacobian.abs()

        #  Compute volume on full mesh with integral of
        vp = jacobian.mean(dim=(1, 2))
        hs = 1 / (vp.shape[0] - 1)
        volume = torch.zeros_like(vp)
        for js in range(1, vp.shape[0]):
            volume[js] = hs * vp[1 : js + 1].sum()

        #  Compute flux surface averaged B2 field
        b2 = (self.b**2 * jacobian).mean(dim=(1, 2)) / vp

        #  Compute flux surface averaged total pressure
        pres = to_half_mesh(2 * self.pressure * self.mu0)
        pres = pres + b2

        #  Compute well
        presgrad = gradient(pres, order=1, factor=2.0)
        return volume / to_full_mesh(b2) * presgrad / to_full_mesh(vp)

    @property
    def vacuum_well(self):
        """
        Compute a single number W that summarizes the vacuum magnetic well,
        given by the formula

        W = (dV/ds(s=0) - dV/ds(s=1)) / (dV/ds(s=0)

        See: https://github.com/hiddenSymmetries/simsopt/blob/master/src/simsopt/mhd/vmec.py
        """

        #  To get from the half grid to s=0 and s=1, we must
        #  extrapolate by 1/2 of a radial grid point:
        vp_s0 = 1.5 * self.vp[1] - 0.5 * self.vp[2]
        vp_s1 = 1.5 * self.vp[-1] - 0.5 * self.vp[-2]

        #  TODO-1(@amerlo): consider also to use 2 * (vp_s0 - vp_s1) / (vp_s0 + vp_s1)
        return (vp_s0 - vp_s1) / vp_s0

    @property
    def finite_well(self):
        """
        Summarize the finite-beta well into a single number.

        W = -(p*(s=0) - p*(s=1)) / p*(s=0)

        Where here p* = 2 mu0 p + B^2.
        """
        jacobian = self.jacobian.abs()
        vp = jacobian.mean(dim=(1, 2))

        #  Compute flux surface averaged B2 field
        b2 = (self.b**2 * jacobian).mean(dim=(1, 2)) / vp

        #  Compute flux surface averaged total pressure
        pres = to_half_mesh(2 * self.pressure * self.mu0)
        pres = pres + b2

        #  Extrapolate to magnetic axis and LCFS
        pres_s0 = 1.5 * pres[1] - 0.5 * pres[2]
        pres_s1 = 1.5 * pres[-1] - 0.5 * pres[-2]

        #  Compute finite well depth
        return -(pres_s0 - pres_s1) / pres_s0

    @property
    def mean_well_(self):
        """
        Compute the mean magnetic well over the normalized toroidal flux.

        This evaluation considers the sqrtg factor in the integral.
        """
        volume = self.vp[1:].mean()
        return (self.well * self.vp)[1:].mean() / volume

    @property
    def dwell(self):
        """
        Compute the magnetic well term in the Mercier criterion.

        See VMEC2000/Sources/General/mercier.f.
        """

        pres = to_half_mesh(self.pressure * self.mu0)
        presgrad = gradient(pres, order=1, factor=2.0) / self.phiedge

        gsqrt = to_full_mesh(self.jacobian) / self.phiedge

        b2 = to_full_mesh(self.b) ** 2

        #  tpp = <1 / B**2>
        tpp = 4 * math.pi**2 * (gsqrt / b2).mean(dim=(1, 2))

        gtt = self.ru**2 + self.zu**2

        #  gpp = 1 / gpp
        gpp = gsqrt**2 / (
            gtt * self.r**2 + (self.ru * self.zv - self.rv * self.zu) ** 2
        )

        #  tbb = <B**2/gpp>
        tbb = 4 * math.pi**2 * (b2 * gsqrt * gpp).mean(dim=(1, 2))

        #  mercier.f#L185
        dwell = presgrad * (self.well - presgrad * tpp) * tbb

        #  Extend dwell on axis and at the LCFS
        #  In VMEC, dwell[0]=dwell[-1]=0
        dwell[0] = 2.0 * dwell[1] - dwell[2]
        dwell[-1] = 2.0 * dwell[-2] - dwell[-3]

        return dwell

    @property
    def pres(self):
        """
        Compute pressure on half-mesh (i.e., iotas).
        """
        return to_half_mesh(self.pressure * self.mu0) / self.mu0

    @property
    def jcuru(self):
        return (
            -gradient(torch.mean(self.bsubv, dim=(1, 2)), order=1, factor=2.0)
            * self.signgs
        )

    @property
    def jcurv(self):
        return (
            gradient(torch.mean(self.bsubu, dim=(1, 2)), order=1, factor=2.0)
            * self.signgs
        )

    @property
    def itor(self):
        """
        Compute the integrated toroidal current.

        See mercier.f#L92 and mercier.f#L168
        """
        icurv = self.signgs * 2 * math.pi * self.bsubu.mean(dim=(1, 2))
        return to_full_mesh(icurv) / self.mu0

    @property
    def jtor(self):
        """
        Compute the flux surface averaged toroidal current density.
        This is <JSUPV> in the VMEC threed1 output file.

        See VMEC2000/Sources/Input_Output/eqfor.f#L225.
        """
        return self.jcurv / self.vp / self.mu0

    @property
    def peak_jtor(self):
        """
        Compute the peak of the toroidal current density.

        TODO-0(@amerlo): fix the values on the two innermost flux surfaces.
        """
        return self.jtor[MEAN_F_START:].abs().max()

    @property
    def equif(self):
        """
        Compute the normalized proxy for flux surface averaged radial force balance on full-mesh.
        This proxy assumes that fbeta = 0.

        The expression is also provided by VMEC in the threed1.txt file.

        See eq. (35) in Hirshman 1983, 10.1063/1.864116
        See VMEC2000/Sources/General/fbal.f#L47 and VMEC2000/Sources/Input_Output/eqfor.f#L196
        """

        #  Include mu0 term as done in VMEC
        pres = to_half_mesh(self.pressure * self.mu0)
        presgrad = gradient(pres, order=1, factor=2.0)

        vp = to_full_mesh(self.vp)

        equif = (
            self.phip * self.iota * self.jcurv - self.phip * self.jcuru + presgrad * vp
        )
        equif /= (
            torch.abs(self.phip * self.iota * self.jcurv)
            + torch.abs(self.phip * self.jcuru)
            + torch.abs(presgrad) * vp
        )

        equif[0] = 2.0 * equif[1] - equif[2]  # eqfor.f#L206
        equif[-1] = 2.0 * equif[-2] - equif[-3]  # eqfor.f#L213

        return equif

    @property
    def f(self):
        """
        Compute the proxy for the flux surface averaged radial force balance.

        There is a (2 * math.pi) ** 2 missing factor to be included. It is not included
        since this quantity is used only during training as a proxy for the full radial force
        balance, and a scaling factor does not affect the training.

        See Equilibrium.equif.
        """

        #  Include mu0 term as done in VMEC
        pres = to_half_mesh(self.pressure * self.mu0)
        presgrad = gradient(pres, order=1, factor=2.0)

        vp = to_full_mesh(self.vp)

        f = self.phip * self.iota * self.jcurv - self.phip * self.jcuru + presgrad * vp

        f[0] = 2.0 * f[1] - f[2]  # eqfor.f#L206
        f[-1] = 2.0 * f[-2] - f[-3]  # eqfor.f#L213

        return f

    @property
    def mean_f(self):
        """
        Compute the volume averaged radial force balance proxy.

        TODO-0(@amerlo): idx=[0,1] are not correct based on test_equilibrium.py::test_compute_f
                         We should first fix the computation of f, and then add back those indices.
        """
        return self.f[MEAN_F_START:].abs().mean()

    @property
    def normf(self):
        """
        Compute the pressure gradient normalized flux surface averaged radial force proxy.

        normf = |F| / |gradp|

        See Dudt 2020, 10.1063/5.0020743

        TODO-0(@amerlo): check this is the correct implementation.
        """

        #  Include mu0 term as done in VMEC
        pres = to_half_mesh(self.pressure * self.mu0)
        presgrad = gradient(pres, order=1, factor=2.0)

        vp = to_full_mesh(self.vp)

        normf = torch.abs(
            self.phip * self.iota * self.jcurv - self.phip * self.jcuru + presgrad * vp
        )
        normf /= torch.abs(presgrad * vp)

        normf[0] = 2.0 * normf[1] - normf[2]  # eqfor.f#L206
        normf[-1] = 2.0 * normf[-2] - normf[-3]  # eqfor.f#L213

        return normf

    @property
    def etaf(self):
        """Compute the volume averaged normalized radial force balance proxy."""

        #  Include mu0 term as done in VMEC
        pres = to_half_mesh(self.pressure * self.mu0)
        presgrad = gradient(pres, order=1, factor=2.0)

        vp = to_full_mesh(self.vp)

        f = torch.abs(
            self.phip * self.iota * self.jcurv - self.phip * self.jcuru + presgrad * vp
        )
        f[0] = 2.0 * f[1] - f[2]
        f[-1] = 2.0 * f[-2] - f[-3]

        p = torch.abs(presgrad * vp)

        return f[MEAN_F_START:].mean() / p[MEAN_F_START:].mean()

    @property
    def rmse_f(self):
        """
        Compute the normalized rmse for the surface averaged radial force balance proxy.

        See Hirshman1986, 10.1016/0010-4655(86)90127-X
        """

        #  Include mu0 term as done in VMEC
        pres = to_half_mesh(self.pressure * self.mu0)
        presgrad = gradient(pres, order=1, factor=2.0)

        vp = to_full_mesh(self.vp)

        f = (
            self.phip * self.iota * self.jcurv - self.phip * self.jcuru + presgrad * vp
        ) ** 2
        denom = (
            (self.phip * self.iota * self.jcurv) ** 2
            + (self.phip * self.jcuru) ** 2
            + (presgrad * vp) ** 2
        )

        return torch.sqrt(f[MEAN_F_START:].mean() / denom[MEAN_F_START:].mean())

    @property
    def avforce(self):
        """
        Compute the flux surface averaged radial force balance (i.e., fs = jxb - p').

        This quantity is called "avforce" in the VMEC jxbout file.
        """

        pres = to_half_mesh(self.pressure * self.mu0)
        presgrad = gradient(pres, order=1, factor=2.0)

        bsubsu = to_full_mesh(self.bsubsu)
        bsubsv = to_full_mesh(self.bsubsv)

        gsqrt = self.jacobian.abs()
        gsqrt_full_mesh = to_full_mesh(gsqrt)

        vp = to_full_mesh(gsqrt.mean(dim=(1, 2)))

        bsupu = to_full_mesh(self.bsupu * gsqrt) / gsqrt_full_mesh
        bsupv = to_full_mesh(self.bsupv * gsqrt) / gsqrt_full_mesh

        itheta = bsubsv - gradient(self.bsubv, order=1, factor=2.0)
        izeta = -bsubsu + gradient(self.bsubu, order=1, factor=2.0)

        avforce = itheta * bsupv - izeta * bsupu
        avforce = (avforce.mean(dim=(1, 2)) - presgrad) / ((2 * math.pi) ** 2 * vp)

        avforce[0] = 0
        avforce[-1] = 0

        return avforce / self.mu0

    @property
    def fs(self):
        """
        Compute the full flux surface averaged radial force balance (i.e., f_rho in VMEC).

        This is actually mu0 * f_rho / (2 * pi) ** 2, so to be in the same order of magnitude of f.
        """

        pres = to_half_mesh(self.pressure * self.mu0)
        presgrad = gradient(pres, order=1, factor=2.0)

        bsubsu = to_full_mesh(self.bsubsu)
        bsubsv = to_full_mesh(self.bsubsv)

        gsqrt = to_full_mesh(self.jacobian).abs()

        bsupu = to_full_mesh(self.bsupu)
        bsupv = to_full_mesh(self.bsupv)

        fs = (
            bsupv * (bsubsv - gradient(self.bsubv, order=1, factor=2.0))
            + bsupu * (bsubsu - gradient(self.bsubu, order=1, factor=2.0))
            - presgrad[:, None, None]
        ).abs()
        fs = (fs * gsqrt).mean(dim=(1, 2))

        fs[0] = 2 * fs[1] - fs[2]
        fs[-1] = 2 * fs[-2] - fs[3]

        return fs

    @property
    def F(self):
        """
        Compute the full flux surface averaged force balance.

        See eq.33 and eq.34b of Panici2022
        """

        pres = to_half_mesh(self.pressure * self.mu0)
        presgrad = gradient(pres, order=1, factor=2.0)

        bsubsu = to_full_mesh(self.bsubsu)
        bsubsv = to_full_mesh(self.bsubsv)

        gsqrt = to_full_mesh(self.jacobian).abs()

        bsupu = to_full_mesh(self.bsupu)
        bsupv = to_full_mesh(self.bsupv)

        #  Compute the covariant radial force component
        fs = (
            bsupv * (bsubsv - gradient(self.bsubv, order=1, factor=2.0))
            + bsupu * (bsubsu - gradient(self.bsubu, order=1, factor=2.0))
            - presgrad[:, None, None]
        )

        #  Compute the covariant helical force component
        bsubvu = to_full_mesh(self.bsubvu)
        bsubuv = to_full_mesh(self.bsubuv)
        fbeta = (bsubvu - bsubuv) / gsqrt

        #  Compute the grad tensors (squared) on full mesh
        grad_rho = (
            (self.ru**2 + self.zu**2) * self.r**2
            + (self.ru * self.zv - self.rv * self.zu) ** 2
        ) / gsqrt**2

        #  TODO-1(@amerlo): is this scheme here enough?
        rs = to_full_mesh(self.rs)
        zs = to_full_mesh(self.zs)

        #  The gsqrt**2 factor are cancelled out due to grad_beta
        grad_theta = (zs**2 + rs**2) * self.r**2 + (
            self.zv * rs - self.rv * zs
        ) ** 2
        grad_zeta = (zs * self.ru - self.zu * rs) ** 2
        grad_theta_zeta = (self.zv * rs - self.rv * zs) * (zs * self.ru - self.zu * rs)
        grad_beta = (
            grad_theta * bsupv**2
            + grad_zeta * bsupu**2
            - 2 * grad_theta_zeta * bsupv * bsupu
        )

        f = (torch.sqrt(fs**2 * grad_rho + fbeta**2 * grad_beta) * gsqrt).mean(
            dim=(1, 2)
        )

        return f

    @property
    def mean_F(self):
        """Compute the volume averaged force balance proxy."""
        return self.F[MEAN_F_START:].mean()

    @property
    def normF(self):
        """
        Compute the full normalized flux surface averaged force balance.

        See eq.33 and eq.34b of Panici2022
        """

        pres = to_half_mesh(self.pressure * self.mu0)
        presgrad = gradient(pres, order=1, factor=2.0)

        bsubsu = to_full_mesh(self.bsubsu)
        bsubsv = to_full_mesh(self.bsubsv)

        gsqrt = to_full_mesh(self.jacobian).abs()

        bsupu = to_full_mesh(self.bsupu)
        bsupv = to_full_mesh(self.bsupv)

        #  Compute the covariant radial force component
        fs = (
            bsupv * (bsubsv - gradient(self.bsubv, order=1, factor=2.0))
            + bsupu * (bsubsu - gradient(self.bsubu, order=1, factor=2.0))
            - presgrad[:, None, None]
        )

        #  Compute the covariant helical force component
        bsubvu = to_full_mesh(self.bsubvu)
        bsubuv = to_full_mesh(self.bsubuv)
        fbeta = (bsubvu - bsubuv) / gsqrt

        #  Compute the grad tensors (squared) on full mesh
        grad_rho = (
            (self.ru**2 + self.zu**2) * self.r**2
            + (self.ru * self.zv - self.rv * self.zu) ** 2
        ) / gsqrt**2

        #  TODO-1(@amerlo): is this scheme here enough?
        rs = to_full_mesh(self.rs)
        zs = to_full_mesh(self.zs)

        #  The gsqrt**2 factor are cancelled out due to grad_beta
        grad_theta = (zs**2 + rs**2) * self.r**2 + (
            self.zv * rs - self.rv * zs
        ) ** 2
        grad_zeta = (zs * self.ru - self.zu * rs) ** 2
        grad_theta_zeta = (self.zv * rs - self.rv * zs) * (zs * self.ru - self.zu * rs)
        grad_beta = (
            grad_theta * bsupv**2
            + grad_zeta * bsupu**2
            - 2 * grad_theta_zeta * bsupv * bsupu
        )

        f = (torch.sqrt(fs**2 * grad_rho + fbeta**2 * grad_beta) * gsqrt).mean(
            dim=(1, 2)
        )
        denom = ((presgrad[:, None, None].abs() * torch.sqrt(grad_rho)) * gsqrt).mean(
            dim=(1, 2)
        )

        return f / denom

    @property
    def etaF(self):
        """
        Compute the normalized volume averaged full force balance.

        See eq.33 and eq.34b of Panici2022
        """

        pres = to_half_mesh(self.pressure * self.mu0)
        presgrad = gradient(pres, order=1, factor=2.0)

        bsubsu = to_full_mesh(self.bsubsu)
        bsubsv = to_full_mesh(self.bsubsv)

        gsqrt = to_full_mesh(self.jacobian).abs()

        bsupu = to_full_mesh(self.bsupu)
        bsupv = to_full_mesh(self.bsupv)

        #  Compute the covariant radial force component
        fs = (
            bsupv * (bsubsv - gradient(self.bsubv, order=1, factor=2.0))
            + bsupu * (bsubsu - gradient(self.bsubu, order=1, factor=2.0))
            - presgrad[:, None, None]
        )

        #  Compute the covariant helical force component
        bsubvu = to_full_mesh(self.bsubvu)
        bsubuv = to_full_mesh(self.bsubuv)
        fbeta = (bsubvu - bsubuv) / gsqrt

        #  Compute the grad tensors (squared) on full mesh
        grad_rho = (
            (self.ru**2 + self.zu**2) * self.r**2
            + (self.ru * self.zv - self.rv * self.zu) ** 2
        ) / gsqrt**2

        #  TODO-1(@amerlo): is this scheme here enough?
        rs = to_full_mesh(self.rs)
        zs = to_full_mesh(self.zs)

        #  The gsqrt**2 factor are cancelled out due to grad_beta
        grad_theta = (zs**2 + rs**2) * self.r**2 + (
            self.zv * rs - self.rv * zs
        ) ** 2
        grad_zeta = (zs * self.ru - self.zu * rs) ** 2
        grad_theta_zeta = (self.zv * rs - self.rv * zs) * (zs * self.ru - self.zu * rs)
        grad_beta = (
            grad_theta * bsupv**2
            + grad_zeta * bsupu**2
            - 2 * grad_theta_zeta * bsupv * bsupu
        )

        f = (torch.sqrt(fs**2 * grad_rho + fbeta**2 * grad_beta) * gsqrt).mean(
            dim=(1, 2)
        )
        denom = ((presgrad[:, None, None].abs() * torch.sqrt(grad_rho)) * gsqrt).mean(
            dim=(1, 2)
        )

        return f[MEAN_F_START:].mean() / denom[MEAN_F_START:].mean()

    @property
    def wmhd(self):
        """
        Compute the total plasma energy.

        See eq. (8a) in Hirshman 1983, 10.1063/1.864116
        """

        pres = to_half_mesh(self.pressure * self.mu0)

        wb = self.jacobian * self.b**2 / 2
        wp = self.vp * pres

        wb = wb.abs()[1:].mean()
        wp = wp[1:].mean()

        #  gamma = 0
        return (wb - wp) * (2 * math.pi) ** 2

    @property
    def beta(self):
        """Compute the volume averaged plasma beta."""

        pres = to_half_mesh(self.pressure * self.mu0)

        #  eqfor.f#L319
        wp = (self.vp * pres)[1:].mean()

        #  eqfor.f#L386
        wb = (self.jacobian.abs() * self.b**2)[1:].mean() / 2

        return wp / wb

    @property
    def gss(self):
        """
        Compute the |grad-s| metric element on full-mesh.

        g^xx = |grad-s|^2 * (a^2/(4s))
        """

        gsqrt = to_full_mesh(self.jacobian.abs())

        gss = (
            (self.ru**2 + self.zu**2) * self.r**2
            + (self.ru * self.zv - self.rv * self.zu) ** 2
        ) / gsqrt**2

        return torch.sqrt(gss)

    @property
    def gpp(self):
        """
        Compute the flux surface averaged contravariant gss metric element.

        gpp = |grad-phi|**2 = phi_edge**2 |grad-s|**2

        See VMEC2000/Sources/Input_Output/mercier.f.
        """

        gsqrt = to_full_mesh(self.jacobian.abs())

        #  the gsqrt term for the flux surface averaged is already included in the denominator
        gpp = (
            (self.ru**2 + self.zu**2) * self.r**2
            + (self.ru * self.zv - self.rv * self.zu) ** 2
        ) / gsqrt

        return 4 * math.pi**2 * self.phiedge**2 * gpp.mean(dim=(1, 2))

    @property
    def mirror_ratio(self):
        """
        The mirror ratio.

        mr = (B(phi=0) - B(phi=36)) / (B(phi=0) + B(phi=36))

        See Geiger2008.
        """
        b = self.b
        b0 = b[:, :, 0]
        b36 = b[:, :, int(self.nzeta / 2)]
        mr = (b0 - b36) / (b0 + b36)
        return mr.mean(dim=1)

    @property
    def bmnc_b(self):
        """Magnetic field Fourier coefficients in Boozer coordinates."""
        if not self._has_bx_run:
            self._run_bx()
        bmnc = self.bx.bmnc_b.T
        mpol = self.bx.mboz
        ntor = self.bx.nboz
        bmnc = np.append(np.zeros((bmnc.shape[0], ntor)), bmnc, axis=1)
        return torch.from_numpy(bmnc.reshape(-1, mpol, 2 * ntor + 1))

    @property
    def b_b(self):
        """Magnetic field in Boozer coordinates."""
        return ift((self.bmnc_b, None), self.ntheta, self.nzeta)

    @property
    def raderb00(self):
        """
        Compute the radial derivative of B00 (in Boozer coordinates) on the full mesh.

        See https://github.com/PrincetonUniversity/STELLOPT/blob/CIEMAT/STELLOPTV2/Sources/Chisq/chisq_raderb00.f90  # noqa

        TODO-1(@amerlo): fix value on axis!
        """
        b00 = self.bmnc_b[:, 0, self.bx.nboz]
        b00s = torch.zeros(self.ns, dtype=b00.dtype, device=b00.device)
        hs = 1 / (self.ns - 1)
        b00s[1:-2] = (b00[1:] - b00[:-1]) / hs
        #  Extend derivative linarly on axis and at LCFS
        b00s[0] = 2 * b00s[1] - b00s[2]
        b00s[-2] = 2 * b00s[-3] - b00s[-4]
        b00s[-1] = 2 * b00s[-2] - b00s[-3]
        #  Normalize with b00 on axis (extrapolate it)
        return b00s / (1.5 * b00[0] - 0.5 * b00[1])

    @property
    def b0(self):
        """Reference value for the b field, usually B00 at the last closed flux surface."""
        assert self.compute_surfs[-1] == self.ns - 2
        bmnc = self.bmnc_b
        #  If last two flux surfaces are available, use interpolation,
        #  otherwise simply use last flux surfaces on half mesh
        if self.compute_surfs[-2] == self.ns - 3:
            return 1.5 * bmnc[-1, 0, self.bx.nboz] - 0.5 * bmnc[-2, 0, self.bx.nboz]
        return bmnc[-1, 0, self.bx.nboz]

    @property
    def toroidal_mirror_term(self):
        """
        The normalized magnetic field toroidal mirror term, i.e., b01.

        See Geiger2014, http://dx.doi.org/10.1088/0741-3335/57/1/014004
        """
        return self.bmnc_b[:, 0, self.bx.nboz + 1] / self.b0

    @property
    def toroidal_curvature_term(self):
        """
        The normalized magnetic field toroidal curvature term, i.e., b10.

        See Geiger2014, http://dx.doi.org/10.1088/0741-3335/57/1/014004
        """
        return self.bmnc_b[:, 1, self.bx.nboz] / self.b0

    @property
    def eps_eff_proxy(self):
        """
        A proxy for the effective helical ripple, as in Ch2.1, Geiger2014.

        eps_eff = - b11 * k ** (4/3)

        where, k = - b10 * R / r

        See Geiger2014, http://dx.doi.org/10.1088/0741-3335/57/1/014004
        """
        bmnc = self.bmnc_b
        b0 = self.b0
        b10 = bmnc[:, 1, self.bx.nboz] / b0
        b11 = bmnc[:, 1, self.bx.nboz + 1] / b0
        rho = torch.sqrt(self.shalf_b) * self.aminor
        kappa = -b10 * self.rmajor / rho
        return -b11 * kappa ** (4 / 3)

    def _find_variance_of_extrema_of_b_along_fieldline(
        self, maxima: bool, nalpha: int = 20, nzeta: int = 144, end: float = 8 * np.pi
    ):
        """
        Provide variance of the extrema (i.e., peaks) of |B|
        along multiple field lines in Boozer coordinates.

        See `vbmax` and `vbmin`.
        """
        alpha = np.linspace(0, 2 * np.pi, nalpha, endpoint=False)
        (_, phis), vb = self._compute_along_fieldline(
            scalar="bmnc_b", alpha=alpha, boozer=True, nzeta=nzeta, end=end
        )
        from scipy.signal import find_peaks

        variance = np.empty(vb.shape[0], dtype=np.float64)

        #  0.8 of the number of points in a toroidal period
        min_distance = 0.8 * nzeta / end * (2 * math.pi / self.num_field_period)
        for js in range(vb.shape[0]):
            extrema = []
            for a in range(vb.shape[1]):
                peaks, _ = find_peaks(
                    vb[js, a] if maxima else -vb[js, a],
                    distance=min_distance,
                )
                extrema.extend(vb[js, a, peaks].tolist())
            variance[js] = np.std(extrema)

        return variance

    @property
    def vbmax(self):
        """The variance of the maximum of |B| along field lines."""
        return self._find_variance_of_extrema_of_b_along_fieldline(maxima=True)

    @property
    def vbmin(self):
        """The variance of the minimum of |B| along field lines."""
        return self._find_variance_of_extrema_of_b_along_fieldline(maxima=False)

    @property
    def vpmax(self):
        """
        The variance of the toroidal Boozer angle for the contour of the
        maxima of B on a flux surface.
        """

        bmnc = self.bmnc_b
        ntheta = max(self.ntheta, 2 * self.bx.mboz + 1)
        nzeta = max(self.nzeta, 2 * self.bx.nboz + 1)
        modB = ift((bmnc, None), ntheta, nzeta).numpy()

        #  See vmecnn.physics.utils.get_fourier_basis
        phi = linspace(
            0,
            2 * math.pi / self.num_field_period,
            nzeta,
            endpoint=False,
        )
        theta = linspace(0, 2 * math.pi, ntheta, endpoint=False)

        from contourpy import contour_generator

        #  TODO-0(@amerlo): merge open contours on periodic boundaries
        ns = modB.shape[0]
        vpmax = np.empty(ns)
        for js in range(ns):
            cont_gen = contour_generator(x=phi, y=theta, z=modB[js])
            #  Select contour just below maxima
            maxB = np.max(modB[js]) * 0.995
            lines = cont_gen.lines(maxB)
            #  Extract phis
            phis = [line[:, 0] for line in lines]
            #  Average std of phi from all Bmax contours
            vpmax[js] = np.mean([np.std(p) for p in phis])

        return vpmax

    @property
    def qp_residual_norm_ratio(self):
        """The ratio between the norm of the non-QP symmetric and symmetric bmn modes."""
        return torch.sqrt(
            (self.bmnc_b[:, 1:] ** 2).sum(dim=(1, 2))
            / (self.bmnc_b[:, 0] ** 2).sum(dim=1)
        )

    def qh_residual_norm_ratio(self, helicity_m: int = 1, helicity_n: int = 1):
        """The ratio between the norm of the non-QH symmetric and symmetric bmn modes."""
        idx = torch.ones_like(self.bmnc_b, dtype=bool)
        idx[:, 0, self.bx.nboz] = False
        idx[:, helicity_m, self.bx.nboz + helicity_n] = False
        return torch.sqrt(
            (self.bmnc_b[idx] ** 2).sum()
            / (self.bmnc_b[torch.logical_not(idx)] ** 2).sum()
        )

    def _run_bx(self) -> None:

        #  See: https://github.com/hiddenSymmetries/simsopt/blob/master/src/simsopt/mhd/boozer.py
        self.bx = booz_xform.Booz_xform()

        self.bx.mboz = self.mboz
        self.bx.nboz = self.nboz

        self.bx.asym = self.lasym
        self.bx.nfp = self.num_field_period

        #  TODO-1(@amerlo): fix me when new mpol and ntor definition is adopted
        self.bx.mpol = self.mpol + 1
        self.bx.ntor = self.ntor

        #  Set mpol and ntor
        mnmax = (self.mpol + 1) * (2 * self.ntor + 1) - self.ntor
        self.bx.mnmax = mnmax
        self.bx.xm = self.xm.numpy()
        self.bx.xn = self.xn.numpy()

        assert len(self.xm) == mnmax
        assert len(self.xn) == mnmax
        assert len(self.bx.xm) == self.bx.mnmax
        assert len(self.bx.xn) == self.bx.mnmax

        #  Set mpol_nyq and ntor_nyq
        mnmax_nyq = (self.mpol_nyq + 1) * (2 * self.ntor_nyq + 1) - self.ntor_nyq
        self.bx.mpol_nyq = self.mpol_nyq
        self.bx.ntor_nyq = self.ntor_nyq
        self.bx.mnmax_nyq = mnmax_nyq
        self.bx.xm_nyq = self.xm_nyq.numpy()
        self.bx.xn_nyq = self.xn_nyq.numpy()

        assert len(self.xm_nyq) == mnmax_nyq
        assert len(self.xn_nyq) == mnmax_nyq
        assert len(self.bx.xm_nyq) == self.bx.mnmax_nyq
        assert len(self.bx.xn_nyq) == self.bx.mnmax_nyq

        # For stellarator-symmetric configs, the asymmetric
        # arrays have not been initialized.
        arr = np.array([[]])
        rmns = arr
        zmnc = arr
        lmnc = arr
        bmns = arr
        bsubumns = arr
        bsubvmns = arr

        self.bx.init_from_vmec(
            self.ns,
            asarray(self.iotas),
            _to_booz(self.rmnc),
            rmns,
            zmnc,
            _to_booz(self.zmns),
            lmnc,
            _to_booz(self.lmns),
            _to_booz(self.bmnc),
            bmns,
            _to_booz(self.bsubumnc),
            bsubumns,
            _to_booz(self.bsubvmnc),
            bsubvmns,
        )
        self.bx.compute_surfs = self.compute_surfs

        self.bx.verbose = False
        self.bx.run()
        self._has_bx_run = True

    @property
    def eps_eff(self):
        """The effective helical ripple, namely, eps_eff**(3/2)."""
        if self.neo is None or not self.neo.is_complete:
            self._run_neo()
        return torch.from_numpy(self.neo.eps_eff)

    @property
    def eps_eff_(self):
        """The effective helical ripple, namely, eps_eff."""
        return self.eps_eff ** (2 / 3)

    def _run_neo(self) -> None:
        if not self._has_bx_run:
            self._run_bx()
        self.neo = Neo.from_booz(booz_xform=self.bx)
        self.neo.run()

    def clear(self) -> None:
        """Reset cache for external codes (e.g., NEO)."""
        self.compute_surfs = np.arange(1, self.ns - 1, dtype=int)
        self.bx = None
        self._has_bx_run = False
        self.neo = None

    def profileplot(
        self,
        y: str,
        *args,
        ax=None,
        on_full_mesh: Optional[bool] = True,
        **kwargs,
    ):
        """Plot generic profile."""

        x = self.normalized_flux
        if not on_full_mesh:
            x = self.shalf

        #  Get attribute
        x, profile = map(asarray, (x, getattr(self, y)))

        #  Perform flux surface average if 3d profile is given
        if len(profile.shape) == 3:
            profile = profile.mean(axis=(1, 2))

        if not on_full_mesh:
            x = x[1:]
            profile = profile[1:]

        ax = get_ax(ax=ax, nrows=1, ncols=1)
        ax.plot(x, profile, *args, **kwargs)
        ax.set_xlabel(r"$s$")

        return ax

    def fluxsurfacesplot(
        self,
        *args,
        s: Union[int, List[int]] = None,
        ntheta: Optional[int] = 64,
        nzeta: Optional[int] = 5,
        ax=None,
        **kwargs,
    ):
        """
        Plot flux surfaces.

        TODO-1(@amerlo): add documentation.
        TODO-1(@amerlo): review input signature to support float values for s and phi
        """

        if s is None:
            indices = list(range(self.ns))
        elif isinstance(s, int):
            indices = [s]
        else:
            indices = s

        ax = get_ax(ax=ax, nrows=1, ncols=1)

        #  Extract plot label
        label = kwargs.pop("label", None)

        for i, idx in enumerate(indices):
            r = ift((self.rmnc[idx][None, ...], None), ntheta, nzeta, endpoint=True)
            z = ift((None, self.zmns[idx][None, ...]), ntheta, nzeta, endpoint=True)

            r = r.view(ntheta, nzeta)
            z = z.view(ntheta, nzeta)
            r, z = map(asarray, (r, z))

            for j in range(r.shape[1]):
                ax.plot(
                    r[:, j],
                    z[:, j],
                    *args,
                    label=label if i == 0 and j == 0 else None,
                    **kwargs,
                )

            #  Reset color cycles
            ax.set_prop_cycle(None)

        ax.axis("equal")
        ax.set_xlabel(r"$R \ [m]$")
        ax.set_ylabel(r"$Z \ [m]$")

        return ax

    def surfaceplot(
        self,
        scalar: str,
        *args,
        js: int = -1,
        ntheta: Optional[int] = 128,
        nzeta: Optional[int] = 144,
        fill: Optional[bool] = True,
        ax=None,
        **kwargs,
    ):
        """
        Plot scalar value over flux surface.

        TODO-1(@amerlo): add documentation.
        TODO-1(@amerlo): review input signature to support float values for s
        """

        #  Save and set requested resolution
        _ntheta = self.ntheta
        _nzeta = self.nzeta
        self.ntheta = ntheta
        self.nzeta = nzeta

        z = asarray(getattr(self, scalar))
        assert len(z.shape) == 3
        z = z[js]

        #  Set back Equilibrium original resolution
        self.ntheta = _ntheta
        self.nzeta = _nzeta

        #  Construct grid
        #  TODO-1(@amerlo): by construction,
        #  flux surface quantities are not computed with endpoint,
        #  is it ok to plot them in this way?
        x = np.linspace(0, 1, z.shape[1], endpoint=False)
        y = np.linspace(0, 1, z.shape[0], endpoint=False)

        ax = get_ax(ax=ax, nrows=1, ncols=1)
        if fill:
            cset = ax.contourf(x, y, z, *args, **kwargs)
        else:
            cset = ax.contour(x, y, z, *args, **kwargs)

        ax.set_xlabel(r"$N_{fp}\frac{\varphi}{2\pi}$")
        ax.set_ylabel(r"$\frac{\theta}{2\pi}$")

        #  Add continuous colorbar
        fig = ax.get_figure()
        norm = matplotlib.colors.Normalize(
            vmin=cset.cvalues.min(), vmax=cset.cvalues.max()
        )
        sm = plt.cm.ScalarMappable(norm=norm, cmap=cset.cmap)
        sm.set_array([])
        fig.colorbar(sm, ticks=cset.levels, ax=ax)

        return ax

    def surfplot(
        self,
        *args,
        ax=None,
        **kwargs,
    ):
        """
        Plot |B| on a surface vs the Boozer poloidal and toroidal angles.

        See https://github.com/hiddenSymmetries/booz_xform/blob/main/src/booz_xform/plots.py
        """
        if self._has_bx_run is False:
            self._run_bx()
        ax = get_ax(ax=ax, nrows=1, ncols=1)
        plt.sca(ax)
        booz_xform.surfplot(self.bx, *args, **kwargs)
        return ax

    def modeplot(
        self,
        *args,
        ax=None,
        **kwargs,
    ):
        """
        Plot the radial variation of the Fourier modes of |B| in Boozer coordinates.

        https://github.com/hiddenSymmetries/booz_xform/blob/main/src/booz_xform/plots.py
        """
        if self._has_bx_run is False:
            self._run_bx()
        ax = get_ax(ax=ax, nrows=1, ncols=1)
        plt.sca(ax)
        booz_xform.modeplot(self.bx, *args, **kwargs)
        #  Reset color cycles
        ax.set_prop_cycle(None)
        return ax

    def _find_theta_along_fieldline(self, alpha, phis, boozer, dtype):

        iotas = asarray(self.iotas)[:, None, None]
        alpha = np.asarray(alpha).reshape(-1)[None, :, None]
        phis = np.asarray(phis).reshape(-1)[None, None, :]

        if boozer:
            js = self.compute_surfs + 1
            iotas = iotas[js]
            return alpha + iotas * phis

        thetas_pest = alpha + iotas * phis
        thetas_vmec = np.empty_like(thetas_pest)

        poloidal_modes = np.arange(0, self.mpol + 1, dtype=dtype)[:, None]
        toroidal_modes = np.arange(-self.ntor, self.ntor + 1, dtype=dtype)[None, :]

        lmns = asarray(self.lmns)

        def residual(theta_v, phi, theta_pest_target, js):
            return theta_pest_target - (
                theta_v
                + np.sum(
                    lmns[js]
                    * np.sin(
                        poloidal_modes * theta_v
                        - self.num_field_period * toroidal_modes * phi
                    )
                )
            )

        from scipy.optimize import root_scalar

        for k in range(thetas_vmec.shape[0]):
            for j in range(thetas_vmec.shape[1]):
                for i in range(thetas_vmec.shape[2]):
                    thetas_vmec[k, j, i] = root_scalar(
                        residual,
                        args=(phis[0, 0, i], thetas_pest[k, j, i], k),
                        bracket=(
                            thetas_pest[k, j, i] - 1.0,
                            thetas_pest[k, j, i] + 1.0,
                        ),
                    ).root

        return thetas_vmec

    def _compute_along_fieldline(self, scalar, alpha, boozer, nzeta, end=None):
        """Compute given quantity along fieldlins."""

        #  We assume scalar quantity is represented by cosine basis
        #  TODO-1(@amerlo): generalize to any scalar quantity
        if boozer:
            assert scalar == "bmnc_b"
        else:
            assert scalar == "bmnc"

        #  Get scalar quantity
        xmn = asarray(getattr(self, scalar))
        assert len(xmn.shape) == 3

        dtype = xmn.dtype

        #  Define grid
        end = 2 * math.pi / self.num_field_period if end is None else end
        phis = np.linspace(0, end, nzeta, dtype=dtype)

        #  Get tetha angles along the field lines
        #  thetas.shape == (s, alphas, phis)
        thetas = self._find_theta_along_fieldline(
            alpha, phis, boozer=boozer, dtype=dtype
        )

        #  Compute quantity along the field line
        mpol = xmn.shape[1]
        ntor = int((xmn.shape[2] - 1) / 2)

        poloidal_modes = np.arange(0, mpol, dtype=dtype)
        toroidal_modes = np.arange(-ntor, ntor + 1, dtype=dtype)

        costzmn = np.cos(
            poloidal_modes[None, None, None, :, None] * thetas[..., None, None]
            - self.num_field_period
            * toroidal_modes[None, None, None, None, :]
            * phis[None, None, :, None, None]
        )

        x = np.einsum("stzmn,smn->stz", costzmn, xmn)

        return (thetas, phis), x

    def delta(
        self,
        js: Union[Tuple[int], List[int]],
        nB: Optional[int] = -1,
        nalpha: Optional[int] = 10,
        nzeta: Optional[int] = 144,
    ) -> np.ndarray:
        """
        Compute the Boozer angular separation on a set of flux surfaces.

        Args:
            js: set of flux surfaces where to compute delta
            nB: number of B field contours where to compute delta
            nalpha: number of field lines where to compute delta
            nzeta: number of toroidal locations where to evaluate well
        """

        #  TODO-1(@amerlo): add support for a set of B field contour
        #  between Bmin and Bmax
        assert nB == -1, f"nb={nB} is not currently supported"

        alpha = np.linspace(0, 2 * np.pi, nalpha, endpoint=False)
        (thetas, phis), x = self._compute_along_fieldline(
            scalar="bmnc_b", alpha=alpha, boozer=True, nzeta=nzeta
        )

        #  Select flux surfaces
        thetas = thetas[js]
        x = x[js]

        #  Get flux surface average B field
        b0 = self.bmnc[self.compute_surfs[js] + 1, 0, self.ntor_nyq].numpy()

        #  Get bounce points
        delta = compute_bounce_points(b=x, bref=b0, thetas=thetas, phis=phis)

        #  TODO-0(@amerlo): support multiple B contours, see TODO above.
        #  Add ghost dimension for nB.
        return delta[:, None, :]

    def fieldlineplot(
        self,
        scalar: str,
        *args,
        alpha: Optional[Union[float, List[float]]] = 0,
        boozer: Optional[bool] = False,
        js: Optional[int] = -1,
        nzeta: Optional[int] = 144,
        ax=None,
        add_bounce_points: Optional[bool] = False,
        **kwargs,
    ):
        """
        Plot any scalar function along the field line.

        See https://github.com/hiddenSymmetries/simsopt/blob/master/src/simsopt/mhd/vmec_diagnostics.py  # noqa

        TODO-0(@amerlo): make it general for full- or half-mesh.
        """

        (thetas, phis), x = self._compute_along_fieldline(scalar, alpha, boozer, nzeta)

        #  Select flux surface
        thetas = thetas[js]
        x = x[js]

        ax = get_ax(ax=ax, nrows=1, ncols=1)
        ax.plot(phis, x.T, *args, **kwargs)

        #  Compute bounce points in terms of average flux surface value
        if add_bounce_points:
            assert scalar in ("bmnc", "bmnc_b")
            for j in range(x.shape[0]):
                #  Use b00 as reference value
                if boozer:
                    x0 = self.bmnc[self.compute_surfs[js] + 1, 0, self.ntor_nyq].item()
                else:
                    x0 = self.bmnc[js, 0, self.ntor_nyq].item()

                #  Get first bounce points left and right to half period
                idx = np.argwhere(np.diff(np.sign(x[j] - x0))).reshape(-1) + 1
                idx_half = int(len(phis) / 2)
                idx_l = idx[idx < idx_half][-1]
                idx_r = idx[idx > idx_half][0]

                delta_phi = phis[idx_r] - phis[idx_l]
                delta_theta = thetas[j, idx_r] - thetas[j, idx_l]
                delta = np.sqrt(delta_phi**2 + delta_theta**2)

                ax.plot(phis, x0 * np.ones_like(phis), "k--", label=r"$B_{0}$")
                ax.plot(phis[idx_l], x[j, idx_l], "r*")
                ax.plot(phis[idx_r], x[j, idx_r], "r*")

                delta_text = r"$\delta=" + f"{delta:.2f}" + r" \ \text{rad}$"
                ax.annotate(delta_text, xy=(0.45, 0.9), xycoords="axes fraction")

        ax.set_xlabel(r"$\varphi$")
        ax.legend()

        #  Boozer radial indexing is shifted
        if boozer:
            shalf = self.shalf[self.compute_surfs[js] + 1]
        else:
            shalf = self.shalf[js]

        title = f"{scalar} along"
        if not hasattr(alpha, "__len__") or len(alpha) == 1:
            alpha = alpha[0] if hasattr(alpha, "__len__") else alpha
            title += f" the alpha={alpha:.4f} field line"
        else:
            title += " field lines"
        title += f" at s={shalf:.4f}"
        ax.set_title(title)

        return ax


def _get_metric_fn(metric_fn, shape=None):
    if callable(metric_fn):
        return metric_fn
    if metric_fn == "l1":
        return lambda pred, true: np.abs(pred - true)
    elif metric_fn == "l1_r":
        return lambda pred, true: np.abs(pred - true) / np.maximum(np.abs(true), eps)
    elif metric_fn == "l1_m":
        assert shape is not None
        assert len(shape) > 2
        axis = tuple(range(2, len(shape)))
        return lambda pred, true: np.abs(pred - true).max(axis=axis)
    elif metric_fn == "l2_r":
        assert shape is not None
        assert len(shape) > 2
        axis = tuple(range(2, len(shape)))
        return lambda pred, true: ((pred - true) ** 2).sum(axis=axis) / np.maximum(
            (np.abs(true) ** 2).sum(axis=axis), eps
        )
    elif metric_fn == "l2":
        assert shape is not None
        assert len(shape) > 2
        axis = tuple(range(2, len(shape)))
        return lambda pred, true: np.sqrt(((pred - true) ** 2).mean(axis=axis))
    elif metric_fn is None:
        return lambda pred, true: pred
    else:
        raise ValueError


class Equilibria:
    """
    TODO-1(@amerlo): find a better name.
    """

    def __init__(self, trues, preds):

        assert len(trues) == len(preds)

        self.trues = trues
        self.preds = preds

        #  Are the equilibria sorted?
        self._is_sorted = False

        #  Iterator index
        self._idx = 0

    def __len__(self) -> int:
        return len(self.trues)

    def __iter__(self):
        self._idx = 0
        return self

    def __next__(self) -> Tuple[Equilibrium]:
        if self._idx > len(self) - 1:
            raise StopIteration
        else:
            item = self[self._idx]
            self._idx += 1
            return item

    def __getitem__(self, idx: int) -> Tuple[Equilibrium]:
        return self.trues[idx], self.preds[idx]

    @property
    def best(self) -> Tuple[Equilibrium]:
        assert self._is_sorted
        return self.__getitem__(0)

    @property
    def median(self) -> Tuple[Equilibrium]:
        assert self._is_sorted
        return self.__getitem__(int((len(self) - 1) / 2))

    @property
    def worst(self) -> Tuple[Equilibrium]:
        assert self._is_sorted
        return self.__getitem__(-1)

    def save(self, filename: str) -> None:
        raise NotImplementedError

    @classmethod
    def from_model(
        cls,
        trues: List[Dict[str, torch.Tensor]],
        preds: List[Dict[str, torch.Tensor]],
        **kwargs,
    ):
        """Create equilibria from collections of dict model outputs."""
        return cls(
            [Equilibrium.from_model(t) for t in trues],
            [Equilibrium.from_model(p) for p in preds],
            **kwargs,
        )

    @classmethod
    def load(cls, filename: str):
        raise NotImplementedError

    def sort(self, sort_fn: Callable) -> None:
        """
        Sort equilibria in place.

        Args:
            sort_fn: callable with takes two equilibria and return a scalar float value

        Equilibria are sorted based on the averaged between the
        FluxSurfaceRelativeError and IotaRelativeError.
        """

        values = []
        for true, pred in self:
            true = true.as_dict()
            pred = pred.as_dict()
            values.append(sort_fn(pred, true))

        indices = np.argsort(values)

        #  Finally sort trues and preds
        self.trues = [self.trues[i] for i in indices]
        self.preds = [self.preds[i] for i in indices]

        self._is_sorted = True

    def scatterplot(
        self,
        scalar: str,
        *args,
        ax=None,
        use_kde: Optional[bool] = False,
        whis: Optional[Tuple[float]] = None,
        axes_labels: Optional[Tuple[str]] = ("Pred", "True"),
        include_all_metrics: Optional[bool] = False,
        clear_equi: Optional[bool] = False,
        cmap: Optional[str] = "plasma_r",
        **kwargs,
    ):
        """
        Plot predicted and true values in a scatterplot.

        TODO-1(@amerlo): add documentation
        """

        y = []
        y_hat = []

        for true, pred in self:
            y.append(getattr(true, scalar))
            y_hat.append(getattr(pred, scalar))
            if clear_equi:
                true.clear(), pred.clear()

        y, y_hat = map(asarray, (y, y_hat))
        y = y.reshape(-1)
        y_hat = y_hat.reshape(-1)

        #  Filter nan values
        indices = np.logical_and(~np.isnan(y), ~np.isnan(y_hat))
        y, y_hat = y[indices], y_hat[indices]

        #  Cut light colors from color map
        _cmap = matplotlib.colormaps[cmap]
        cut = 0.1
        if cmap.endswith("_r"):
            _cmap = ListedColormap(_cmap(np.linspace(cut, 1.0, 256)))
        else:
            _cmap = ListedColormap(_cmap(np.linspace(0, 1.0 - cut, 256)))

        #  Filter outliers
        if whis is not None:
            assert len(whis) == 2
            scale = y.max() - y.min()
            if whis[0] <= 0:
                ydown = y.min() + scale * whis[0]
            else:
                ydown = np.quantile(y, whis[0])
            if whis[1] >= 1:
                yup = y.max() + scale * (whis[1] - 1)
            else:
                yup = np.quantile(y, whis[1])
            indices = (y_hat <= yup) & (y_hat >= ydown)
            y, y_hat = y[indices], y_hat[indices]

        ax = get_ax(ax=ax, nrows=1, ncols=1)
        if use_kde:
            import seaborn as sns

            sns.kdeplot(
                x=y_hat,
                y=y,
                ax=ax,
                cbar=True,
                cbar_kws=dict(fraction=0.1),
                cmap=_cmap,
                cut=0,
            )

        else:
            _, _, _, im = ax.hist2d(
                y_hat,
                y,
                bins=50,
                cmap=_cmap,
                cmin=1,
                norm=matplotlib.colors.LogNorm(),
                **kwargs,
            )

            #  Add colorbar
            fig = ax.get_figure()
            cb = fig.colorbar(im, ax=ax, fraction=0.1)
            cb.ax.minorticks_off()
            cb.set_label("Counts", labelpad=2.0)

        ax.plot([y.min(), y.max()], [y.min(), y.max()], "r--", linewidth=1)
        ax.axis("equal")

        #  Add metrics
        metrics_text = r"$R^2$" + f"={r2_score(y_hat, y):.4f}"
        if include_all_metrics:
            metrics_text += (
                "\n" + f"mape={mape(y_hat, y):.2e}, nrmse={nrmse(y_hat, y):.2e}"
            )
        ax.annotate(metrics_text, xy=(0.05, 0.85), xycoords="axes fraction")

        assert len(axes_labels) == 2
        ax.set_xlabel(axes_labels[0])
        ax.set_ylabel(axes_labels[1])

        return ax

    def histplot(
        self,
        scalar: str,
        *args,
        ax=None,
        labels: Optional[Tuple[str]] = ("True", "Pred"),
        colors: Optional[Tuple[str]] = (None, None),
        **kwargs,
    ):
        """Plot predicted and true values in a histogram plot."""

        assert len(labels) == 2
        assert len(colors) == 2

        import seaborn as sns

        y = []
        y_hat = []

        for true, pred in self:
            y.append(getattr(true, scalar))
            y_hat.append(getattr(pred, scalar))

        y, y_hat = map(asarray, (y, y_hat))
        y = y.reshape(-1)
        y_hat = y_hat.reshape(-1)

        ax = get_ax(ax=ax, nrows=1, ncols=1)
        sns.kdeplot(x=y, ax=ax, fill=True, label=labels[0], color=colors[0], **kwargs)
        sns.kdeplot(
            x=y_hat, ax=ax, fill=True, label=labels[1], color=colors[1], **kwargs
        )

        label = scalar
        if "{" in scalar:
            label = f"${scalar}$"

        ax.set_xlabel(label)

        return ax

    def profileplot(
        self,
        profile: str,
        *args,
        metric_fn: Optional[Union[Callable, str]] = None,
        log: Optional[bool] = False,
        ax=None,
        **kwargs,
    ):
        """
        Plot relative error of `profile` along normalized toroidal flux.
        """

        y = []
        y_hat = []

        for true, pred in self:
            y.append(getattr(true, profile))
            y_hat.append(getattr(pred, profile))

        y, y_hat = map(asarray, (y, y_hat))

        assert y.shape == y_hat.shape

        fn = _get_metric_fn(metric_fn, shape=y.shape)
        values = fn(y_hat, y)

        del y, y_hat

        label = profile if not isinstance(metric_fn, str) else metric_fn

        if log:
            values = np.abs(values)

        mean = values.mean(axis=0)
        up = np.quantile(values, q=0.95, axis=0)
        down = np.quantile(values, q=0.05, axis=0)

        ax = get_ax(ax=ax, nrows=1, ncols=1)
        x = np.linspace(0, 1, mean.shape[0])
        ax.plot(x, mean, label="mean")
        ax.fill_between(x, down, up, alpha=0.2, label=r"5-95\% quantiles")

        if log:
            ax.set_yscale("log")

        ax.set_xlabel(r"$s$")
        ax.set_ylabel(label)

        return ax

    def metricplot(
        self,
        x: str,
        y: str,
        q: str,
        *args,
        metric_fn: Optional[Union[Callable, str]] = None,
        log: Optional[bool] = False,
        ax=None,
        **kwargs,
    ):
        """
        Make a scatter plot color coded with `metric_fn(q)`.
        """

        xdata = []
        ydata = []
        qdata = []
        qdata_hat = []

        for true, pred in self:
            xdata.append(getattr(true, x))
            ydata.append(getattr(true, y))
            qdata.append(getattr(true, q))
            qdata_hat.append(getattr(pred, q))

        xdata, ydata, qdata, qdata_hat = map(asarray, (xdata, ydata, qdata, qdata_hat))
        xdata, ydata = map(lambda x: x.reshape(-1), (xdata, ydata))

        assert qdata.shape == qdata_hat.shape

        fn = _get_metric_fn(metric_fn, shape=qdata.shape)
        colors = fn(qdata_hat, qdata)
        label = q if not isinstance(metric_fn, str) else q + " - " + metric_fn

        if log:
            colors = np.abs(colors)

        #  Average over equilibrium radial profile
        shape = colors.shape
        if len(shape) > 1:
            axis = tuple(range(1, len(colors.shape)))
            colors = colors.mean(axis=axis)

        ax = get_ax(ax=ax, nrows=1, ncols=1)
        im = ax.scatter(xdata, ydata, c=colors, cmap="viridis", **kwargs)
        #  Add colorbar
        fig = ax.get_figure()
        fig.colorbar(im, ax=ax, fraction=0.1)

        ax.set_title(label)
        ax.set_xlabel(x)
        ax.set_ylabel(y)

        return ax
