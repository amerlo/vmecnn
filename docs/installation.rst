.. highlight:: shell

============
Installation
============


Stable release
--------------

To install vmecnn, run this command in your terminal:

.. code-block:: console

    $ pip install vmecnn

This is the preferred method to install vmecnn, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/



From sources
------------

The sources for vmecnn can be downloaded from the `remote repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone git://gitlab.mpcdf.mpg.de/amerlo/vmecnn

Or download the `tarball`_:

.. code-block:: console

    $ curl -OJL https://gitlab.mpcdf.mpg.de/amerlo/vmecnn/tarball/master

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _remote repo: https://gitlab.mpcdf.mpg.de/amerlo/vmecnn
.. _tarball: https://gitlab.mpcdf.mpg.de/amerlo/vmecnn/tarball/master
