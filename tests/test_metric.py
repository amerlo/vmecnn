"""
Tests for custom torch metrics.

TODO-0(@amerlo): add tests for all metrics
TODO-1(@amerlo): check validity of the np.nan tests, is this behavior what I want?
TODO-2(@amerlo): integrate test from torchmetrics
"""

import numpy as np
import pytest
import torch

from vmecnn.datamodules.utils import get_xmn_mask, get_random_equi
from vmecnn.metrics.metric import (
    NormalizedReducedMeanSquaredError,
    compute_flux_surface_averaged_f,
    grad_1d,
    grad_3d,
)
from vmecnn.physics.equilibrium import Equilibrium, gradient
from vmecnn.physics.utils import to_half_mesh


@pytest.mark.parametrize("multioutput", ("uniform_average", "raw_values"))
@pytest.mark.parametrize("shape", ((3, 5), (3,), (3, 1)))
def test_nrmse_output_shape(multioutput, shape):
    num_outputs = shape[-1] if len(shape) > 1 else 1
    nrmse = NormalizedReducedMeanSquaredError(
        num_outputs=num_outputs, multioutput=multioutput
    )
    input_ = torch.randn((shape))
    target = torch.randn((shape))
    output = nrmse(input_, target)
    if multioutput == "uniform_average":
        assert output.size() == ()
    else:
        assert output.size() == torch.Size([num_outputs])


@pytest.mark.parametrize("ignore_nan", (True, False))
@pytest.mark.parametrize(
    "preds, target, outputs",
    (
        (torch.ones((32, 3)), torch.ones((32, 3)), (np.nan,) * 2),
        (torch.Tensor([[0, 1], [1, 1]]), torch.Tensor([[0, 1], [1, 1]]), (0, np.nan)),
        (torch.zeros((32, 3)), torch.ones((32, 3)), (np.inf,) * 2),
        (
            torch.arange(32, dtype=torch.float32).reshape(4, 8),
            torch.arange(32, dtype=torch.float32).reshape(4, 8),
            (0,) * 2,
        ),
        (
            torch.arange(6, dtype=torch.float32).reshape(2, 3) + 1,
            torch.arange(6, dtype=torch.float32).reshape(2, 3),
            (2 / 3,) * 2,
        ),
        (
            torch.arange(6, dtype=torch.float32).reshape(2, 3)
            + torch.tensor([0, 2, 4]),
            torch.arange(6, dtype=torch.float32).reshape(2, 3),
            ((4 / 3 + 8 / 3) / 3,) * 2,
        ),
    ),
)
def test_nrmse_call(ignore_nan, preds, target, outputs):
    nrmse = NormalizedReducedMeanSquaredError(
        num_outputs=target.size(1), ignore_nan=ignore_nan
    )
    out = nrmse(preds, target)
    if ignore_nan:
        output = outputs[0]
    else:
        output = outputs[1]
    np.testing.assert_almost_equal(out.item(), output)


@pytest.mark.parametrize(
    "case",
    (
        (lambda x: x, lambda x: torch.ones_like(x)),
        (lambda x: x**2, lambda x: 2 * x),
        (lambda x: torch.sqrt(x), lambda x: 1 / (2 * torch.sqrt(x))),
        (lambda x: torch.exp(x), lambda x: torch.exp(x)),
        (lambda x: torch.log(x), lambda x: 1 / x),
        (lambda x: 0.1 * x**2 + x**3, lambda x: 0.2 * x + 3 * x**2),
    ),
)
def test_grad_1d(case):
    func, grad_fn = case
    x = torch.rand(10, dtype=torch.float64).requires_grad_(True)
    y = func(x)
    grad = grad_1d(y, x)
    assert torch.allclose(grad, grad_fn(x), atol=0, rtol=0)


def get_xmn(normalized_flux, mpol: int, ntor: int):
    shape = (normalized_flux.size(0), mpol + 1, 2 * ntor + 1)
    xmn = torch.empty(*shape, dtype=torch.float64)
    grad = torch.empty(*shape, dtype=torch.float64)
    #  Use a s ** m * (xmn_0 + s) function to avoid zero gradient on axis
    for i in range(mpol + 1):
        for j in range(2 * ntor + 1):
            b = 0.1
            xmn[:, i, j] = b * normalized_flux**i + normalized_flux ** (i + 1)
            grad[:, i, j] = (
                b * i * normalized_flux ** (i - 1) + (i + 1) * normalized_flux**i
            )
    mask = get_xmn_mask(mpol, ntor, mpol, ntor, keep_zero_fcs=False)
    xmn[:, ~mask[..., 0]] = 0
    grad[:, ~mask[..., 0]] = 0
    xmn /= mpol + 1 + 2 * ntor + 1
    grad /= mpol + 1 + 2 * ntor + 1
    return xmn, grad


#  TODO-1(@amerlo): it sometimes fails for 1e-9 on the latest case
@pytest.mark.parametrize(
    "case",
    (
        (
            lambda x: x.repeat_interleave(6).view(-1, 2, 3),
            lambda x: torch.ones(x.size(0), 2, 3),
        ),
        (
            lambda x: x[..., None, None] * torch.arange(6).view(-1, 2, 3),
            lambda x: torch.arange(6, dtype=torch.float32)
            .view(-1, 2, 3)
            .repeat(10, 1, 1),
        ),
        (
            lambda x: x.repeat_interleave(6).view(-1, 2, 3) ** 2,
            lambda x: 2 * x.repeat_interleave(6).view(-1, 2, 3),
        ),
        (lambda x: get_xmn(x, 3, 2)[0], lambda x: get_xmn(x, 3, 2)[1]),
    ),
)
def test_grad_3d(case):
    func, grad = case
    x = torch.rand(10).requires_grad_(True)
    y = func(x)
    torch_grad = grad_3d(y, x)
    assert torch.allclose(torch_grad, grad(x), atol=0, rtol=0)


#  No linear case since when grad is zero, pytorch prunes the graph
#  See: https://github.com/pytorch/pytorch/issues/32304
@pytest.mark.parametrize(
    "case",
    (
        (lambda x: x**2, lambda x: 2 * torch.ones_like(x)),
        (lambda x: x**3 + x**2, lambda x: 6 * x + 2 * torch.ones_like(x)),
        (lambda x: torch.exp(x), lambda x: torch.exp(x)),
        (lambda x: x**4 / x**2, lambda x: 2 * torch.ones_like(x)),
        (lambda x: torch.sqrt(x), lambda x: -0.25 * (x ** (-3 / 2))),
    ),
)
def test_second_grad_1d(case):
    func, grad = case
    x = torch.rand(10, dtype=torch.float64).requires_grad_(True)
    y = func(x)
    first_grad = grad_1d(y, x, create_graph=True)
    second_grad = grad_1d(first_grad, x)
    assert torch.allclose(second_grad, grad(x), atol=1e-14, rtol=0)


@pytest.mark.parametrize(
    "case",
    (
        (
            lambda x: x.repeat_interleave(6).view(-1, 2, 3) ** 2,
            lambda x: 2 * torch.ones(x.size(0), 2, 3),
        ),
        (
            lambda x: x.repeat_interleave(6).view(-1, 2, 3) ** 3,
            lambda x: 6 * x.repeat_interleave(6).view(-1, 2, 3),
        ),
    ),
)
def test_second_grad_3d(case):
    func, grad = case
    x = torch.rand(10).requires_grad_(True)
    y = func(x)
    first_grad = grad_3d(y, x, create_graph=True)
    second_grad = grad_3d(first_grad, x)
    assert torch.allclose(second_grad, grad(x), atol=0, rtol=0)


@pytest.mark.parametrize(
    "func",
    (
        lambda x: x.repeat_interleave(6).view(-1, 2, 3),
        lambda x: x[..., None, None] * torch.arange(6).view(-1, 2, 3),
        lambda x: x.repeat_interleave(6).view(-1, 2, 3) ** 2,
        lambda x: get_xmn(x, 3, 2)[0],
        lambda x: get_xmn(x, 6, 4)[0] ** 2 / (x.view(-1, 1, 1) + 1),
    ),
)
def test_grad_finite_differences(func):
    dx = 1e-2
    x = torch.linspace(0, 1, int(1 / dx), dtype=torch.float64).requires_grad_(True)
    y = func(x)
    torch_grad = grad_3d(y, x)
    finite_differences_grad = gradient(y, order=-2)
    #  Central finite differences should be ~O(dx**2)
    atol = dx**2
    #  Do not consider borders
    assert torch.allclose(
        torch_grad[1:-1], finite_differences_grad[1:-1], atol=atol, rtol=0
    )


#  Add "+ 1" to sqrt(x) and 1/x to avoid finite differences errors due to ~1/0
@pytest.mark.parametrize(
    "func",
    (
        lambda x: x.repeat_interleave(6).view(-1, 2, 3) ** 2,
        lambda x: x[..., None, None] ** 3 * torch.arange(6).view(-1, 2, 3),
        lambda x: torch.sqrt(x.repeat_interleave(6).view(-1, 2, 3) + 1),
        lambda x: get_xmn(x, 3, 2)[0],
        lambda x: get_xmn(x, 6, 4)[0] ** 2 / (x.view(-1, 1, 1) + 1),
    ),
)
def test_second_grad_finite_differences(func):
    dx = 1e-2
    x = torch.linspace(0, 1, int(1 / dx), dtype=torch.float64).requires_grad_(True)
    y = func(x)
    torch_grad = grad_3d(y, x, create_graph=True)
    finite_differences_grad = gradient(y, order=-2)
    #  Compute second derivative of mean values as in equif
    torch_grad = grad_1d(torch_grad.mean(dim=(1, 2)), x)
    finite_differences_grad = gradient(
        finite_differences_grad.mean(dim=(1, 2)), order=-2
    )
    atol = dx
    #  Do not consider borders
    assert torch.allclose(
        torch_grad[2:-2], finite_differences_grad[2:-2], atol=atol, rtol=0
    )


@pytest.mark.parametrize("modes", ((1, 1), (3, 2), (6, 4)))
@pytest.mark.parametrize("k", range(99))
@pytest.mark.parametrize("key", ("rsmnc", "zsmns"))
def test_compute_output_analytical_gradients(modes, k, key):
    """Compute equif loss intermediate steps and check against analytical values."""

    dtype = torch.float64
    mpol, ntor = modes
    ns = 99
    ntheta = 16
    nzeta = 18

    equi = get_random_equi(ns=ns, mpol=mpol, ntor=ntor, dtype=dtype)

    rmnc = equi["rmnc"]
    lmns = equi["lmns"]
    zmns = equi["zmns"]
    iota = equi["iota"]
    normalized_flux = equi["normalized_flux"]
    phiedge = equi["phiedge"]
    pressure = equi["pressure"]

    mpols = equi["mpols"]
    x0 = equi["x0"]
    x1 = equi["x1"]
    x2 = equi["x2"]
    eps = equi["eps"]

    res = compute_flux_surface_averaged_f(
        rmnc=rmnc,
        lmns=lmns,
        zmns=zmns,
        iota=iota,
        normalized_flux=normalized_flux,
        phiedge=phiedge.expand(ns).clone(),
        pressure=pressure.expand(ns, ns).clone(),
        ntheta=ntheta,
        nzeta=nzeta,
        use_nn_gradients=True,
    )

    value_ = res[key]
    idx = 0 if key == "rsmnc" else 2

    #  xmn ~ sqrt(s) ** m * x0 * (1 + x1 * s + x2 * s ** 2)

    #  For m=0
    #  x0n ~ x0 * (1 + x1 * s + x2 * s ** 2)
    #  x0n' ~ x0 * (x1 + 2 * x2 * s)
    xs0n = x0 * (x1 + 2 * x2 * normalized_flux)

    value = xs0n[..., idx]
    assert torch.allclose(
        value[k, 0], value_[k, 0], atol=1e-15, rtol=0
    ), f"{torch.max(torch.abs(value_[k, 0] - value[k, 0])):.2e}"

    #  For m>0
    #  xmn' ~ x0 * [
    #                m * sqrt(s) ** (m - 1) * 1 / (2 * sqrt(s))
    #                * (1 + x1 * s + x2 * s ** 2)
    #                + sqrt(s) ** m * (x1 + 2 * x2 * s)
    #              ]
    normalized_flux = normalized_flux + eps
    xsmn = x0 * (
        mpols
        * torch.sqrt(normalized_flux) ** (mpols - 1)
        * 1
        / (2 * torch.sqrt(normalized_flux))
        * (1 + x1 * normalized_flux + x2 * normalized_flux**2)
        + torch.sqrt(normalized_flux) ** mpols * (x1 + 2 * x2 * normalized_flux)
    )

    value = xsmn[..., idx]
    assert torch.allclose(
        value[k, 1:], value_[k, 1:], atol=1e-15, rtol=0
    ), f"{torch.max(torch.abs(value[k, 1:] - value_[k, 1:])):.2e}"


#  The axis and LCFS are not considered given the different approaches to the gradient
#  computaiton at the boundary.
#  TODO-0(@amerlo): fix test and memory issues with nn gradients
#  TODO-0(@amerlo): add test for full force balance
@pytest.mark.parametrize("modes", ((3, 2), (6, 4)))
@pytest.mark.parametrize("k", range(1, 98))
@pytest.mark.parametrize("use_nn_gradients", (False, True))
@pytest.mark.parametrize("use_proxy", (False, True))
@pytest.mark.parametrize("key", ("rsmnc", "zsmns", "jacobian", "f", "wmhd"))
def test_compute_equif_loss(modes, k, use_nn_gradients, use_proxy, key):

    #  Do not perform naive checks in case of finite differences
    if not use_nn_gradients and key in ("rsmnc", "zsmns"):
        return

    #  wmhd is a scalar
    if key == "wmhd" and k > 1:
        return

    dtype = torch.float64
    mpol, ntor = modes
    ns = 99
    ntheta = 32
    nzeta = 36

    equi = get_random_equi(ns=ns, mpol=mpol, ntor=ntor, dtype=dtype)

    rmnc = equi["rmnc"]
    lmns = equi["lmns"]
    zmns = equi["zmns"]
    iota = equi["iota"]
    normalized_flux = equi["normalized_flux"]
    phiedge = equi["phiedge"]
    pressure = equi["pressure"]

    equi = Equilibrium(
        rmnc, lmns, zmns, iota, normalized_flux, phiedge, pressure, ntheta, nzeta
    )
    equi_ = compute_flux_surface_averaged_f(
        rmnc=rmnc,
        lmns=lmns,
        zmns=zmns,
        iota=iota,
        normalized_flux=normalized_flux,
        phiedge=phiedge.expand(ns).clone(),
        pressure=pressure.expand(ns, ns).clone(),
        ntheta=ntheta,
        nzeta=nzeta,
        use_nn_gradients=use_nn_gradients,
        use_proxy=use_proxy,
        full=True,
    )

    #  Select quantity to compare
    if key == "f" and not use_proxy:
        value = getattr(equi, "F")
    else:
        value = getattr(equi, key)
    value_ = equi_[key]

    #  Bring quantities which live on different meshes (e.g., jacobian)
    #  onto the same mesh (half-mesh)
    if use_nn_gradients and key in ("jacobian",):
        value_ = to_half_mesh(value_, mode="simple")

    atol = 0
    rtol = 0
    if use_nn_gradients:
        #  Finite difference scheme should be ~O(hs)
        #  TODO-0(@amerlo): fix tolerance here
        atol = 1 / (ns - 1)
        rtol = 0

    #  "wmhd" is a scalar quantity
    if key != "wmhd":
        value = value[k]
        value_ = value_[k]

    assert torch.allclose(
        value, value_, atol=atol, rtol=rtol
    ), f"{torch.abs(value - value_).max():.2e}"
