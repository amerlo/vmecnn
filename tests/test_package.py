"""Tests for the `vmecnn` package."""
import unittest

import vmecnn


class TestPackage(unittest.TestCase):
    """Tests for `vmecnn` package."""

    def setUp(self):
        """Set up test fixtures, if any."""

    def tearDown(self):
        """Tear down test fixtures, if any."""

    def test_version_type(self):
        """Assure that version type is str."""
        self.assertIsInstance(vmecnn.__version__, str)
