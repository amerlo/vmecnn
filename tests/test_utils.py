import numpy as np
import pytest
import torch

from vmecnn.utils import linspace
from vmecnn.datamodules.utils import get_num_input, get_xmn_mask


#################
# Package utils #
#################


@pytest.mark.parametrize("steps", (1, 10, 100, 101))
@pytest.mark.parametrize("endpoint", (True, False))
def test_linspace(steps, endpoint):
    start = torch.randn(1).item()
    end = start + torch.rand(1).item()
    np_linspace = np.linspace(start, end, steps, endpoint=endpoint)
    nn_linspace = linspace(
        start, end, steps, endpoint=endpoint, dtype=torch.float64
    ).numpy()
    assert np.allclose(np_linspace, nn_linspace, atol=1e-15, rtol=0)


####################
# Datamodule utils #
####################

ns = 99


@pytest.mark.parametrize(
    "case",
    (
        (["npc"], 4),
        (["pressure"], ns),
        (["pressure", "toroidal_current"], 2 * ns),
        (["toroidal_current", "normalized_flux"], ns + 1),
    ),
)
def test_get_num_input(case):
    features, num = case
    assert get_num_input(features, ns) == num


cases = (
    (
        (1, 1, 1, 1, int, False, True),
        [
            [[0, 0, 0], [1, 0, 0], [1, 1, 1]],
            [[1, 1, 1], [1, 1, 1], [1, 1, 1]],
        ],
    ),
    (
        (1, 1, 1, 1, bool, False, True),
        [
            [[False, False, False], [True, False, False], [True, True, True]],
            [[True, True, True], [True, True, True], [True, True, True]],
        ],
    ),
    (
        (1, 1, 1, 1, bool, False, False),
        [
            [[False, False, False], [True, False, False], [True, False, True]],
            [[False, False, False], [False, False, False], [False, False, False]],
        ],
    ),
    (
        (1, 0, 1, 1, bool, False, True),
        [
            [[False, False, False], [True, False, False], [False, False, False]],
            [[False, False, False], [True, True, True], [False, False, False]],
        ],
    ),
    (
        (0, 1, 1, 1, bool, False, True),
        [
            [[False, False, False], [True, False, False], [True, True, True]],
            [[False, False, False], [False, False, False], [False, False, False]],
        ],
    ),
    ((11, 12, 11, 12, bool, True, True), np.ones((12, 25, 3), dtype=bool)),
    (
        (2, 2, 2, 2, bool, False, True),
        [
            [
                [False] * 3,
                [False] * 3,
                [True, False, False],
                [True] * 3,
                [True] * 3,
            ],
            [[True] * 3, [True] * 3, [True] * 3, [True] * 3, [True] * 3],
            [[True] * 3, [True] * 3, [True] * 3, [True] * 3, [True] * 3],
        ],
    ),
    (
        (1, 1, 2, 2, bool, False, True),
        [
            [
                [False] * 3,
                [False] * 3,
                [True, False, False],
                [True] * 3,
                [False] * 3,
            ],
            [[False] * 3, [True] * 3, [True] * 3, [True] * 3, [False] * 3],
            [[False] * 3, [False] * 3, [False] * 3, [False] * 3, [False] * 3],
        ],
    ),
)


@pytest.mark.parametrize("case", cases)
def test_get_xmn_mask_shape(case):
    args, mask = case
    assert get_xmn_mask(*args).shape == np.array(mask).shape


@pytest.mark.parametrize("case", cases)
def test_get_xmn_mask(case):
    args, mask = case
    assert np.allclose(get_xmn_mask(*args), np.array(mask), rtol=0, atol=0)


#  TODO-1(@amerlo): parametrize test over Fourier resolution
@pytest.mark.parametrize("modes", ((2, 2), (11, 12)))
@pytest.mark.parametrize("keep_zero_fcs", (True, False))
def test_get_xmn_mask_11_slice(modes, keep_zero_fcs):
    mpol, ntor = modes
    comp = 3
    shape = (mpol + 1, 2 * ntor + 1, comp)
    indices = np.arange(np.prod(shape)).reshape(shape)
    #  mpol=1, ntor=1
    mask = get_xmn_mask(1, 1, mpol, ntor, keep_zero_fcs=keep_zero_fcs)
    masked_ = indices[mask]
    #  Build true mask
    if keep_zero_fcs:
        if mpol == 2 and ntor == 2:
            masked = np.array(
                [3, 4, 5, 6, 7, 8, 9, 10, 11, 18, 19, 20, 21, 22, 23, 24, 25, 26]
            )
        else:
            masked = np.array(
                [
                    33,
                    34,
                    35,
                    36,
                    37,
                    38,
                    39,
                    40,
                    41,
                    108,
                    109,
                    110,
                    111,
                    112,
                    113,
                    114,
                    115,
                    116,
                ]
            )
    else:
        if mpol == 2 and ntor == 2:
            masked = np.array([6, 9, 10, 11, 18, 19, 20, 21, 22, 23, 24, 25, 26])
        else:
            masked = np.array(
                [36, 39, 40, 41, 108, 109, 110, 111, 112, 113, 114, 115, 116]
            )
    assert np.allclose(masked, masked_, rtol=0, atol=0)
