"""
TODO-0(@amerlo): add tests!
TODO-1(@amerlo): port test to pytest framework
"""
import unittest

import numpy as np
import torch

from vmecnn.datamodules.vmec_equilibria import VmecEquilibriaDataModule
from vmecnn.transforms.transforms import Compose


class TestTransforms(unittest.TestCase):
    """Tests for the `transforms` function."""

    sample = {
        "rmnc": np.arange(12 * 25).reshape(12, 25),
        "lmns": np.arange(12 * 25).reshape(12, 25),
        "zmns": np.arange(12 * 25).reshape(12, 25),
        "extcur": np.ones(7),
        "phiedge": np.array(1.0),
        "pressure": np.ones(99),
        "curtor": np.array(1.0),
        "iota": 0.87,
        "normalized_flux": 0.0,
        "wout_path": np.array("test"),
    }

    root = "tests/resources/data"
    features = ["npc", "pc", "phiedge", "p0", "curtor", "normalized_flux"]
    labels = ["xmn", "iota"]
    metadata = ["wout_path"]

    def setUp(self):
        self.datamodule = VmecEquilibriaDataModule(
            root=self.root,
            features=self.features,
            labels=self.labels,
            metadata=self.metadata,
        )
        self.datamodule.setup()

    def get_transforms(self, *args, **kwargs):
        return Compose(self.datamodule.get_transforms(*args, **kwargs))

    def test_output_signature(self):
        transforms = self.get_transforms()
        sample = transforms(self.sample)
        self.assertEqual(len(sample), 3)
        x, y, metadata = sample
        for k in self.features:
            self.assertTrue(k in x)
        for k in self.labels:
            self.assertTrue(k in y)
        for k in self.metadata:
            self.assertTrue(k in metadata)

    def test_zeros_input(self):
        self.datamodule.zeros_input = True
        transforms = self.get_transforms()
        x, _, _ = transforms(self.sample)
        for k in self.features:
            self.assertTrue(torch.allclose(x[k], torch.zeros_like(x[k])))

    def test_features(self):
        transforms = self.get_transforms()
        x, _, _ = transforms(self.sample)
        np.testing.assert_almost_equal(self.sample["extcur"][1:5], x["npc"].numpy())
        np.testing.assert_almost_equal(self.sample["extcur"][5:7], x["pc"].numpy())
        np.testing.assert_almost_equal(self.sample["phiedge"], x["phiedge"].numpy())
        np.testing.assert_almost_equal(self.sample["pressure"][0], x["p0"].numpy())
        np.testing.assert_almost_equal(self.sample["curtor"], x["curtor"].numpy())
        np.testing.assert_almost_equal(
            self.sample["normalized_flux"], x["normalized_flux"].numpy()
        )

    def test_labels(self):
        transforms = self.get_transforms()
        _, y, _ = transforms(self.sample)
        np.testing.assert_almost_equal(self.sample["iota"], y["iota"].numpy())
        np.testing.assert_almost_equal(self.sample["rmnc"], y["xmn"][..., 0].numpy())
        np.testing.assert_almost_equal(self.sample["lmns"], y["xmn"][..., 1].numpy())
        np.testing.assert_almost_equal(self.sample["zmns"], y["xmn"][..., 2].numpy())


if __name__ == "__main__":
    unittest.main()
