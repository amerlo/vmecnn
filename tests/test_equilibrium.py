import os
import pytest
import torch
import numpy as np
import netCDF4
import booz_xform

from w7x_datagen.datasets import VmecEquilibria

from vmecnn.physics.equilibrium import Equilibrium, Neo
from vmecnn.physics.utils import (
    derivative,
    gradient,
    ft,
    ift,
    to_full_mesh,
    to_half_mesh,
    get_fourier_basis,
    linspace,
)
from vmecnn.utils import get_torch_memory

DTYPE = torch.float64

#########
# Utils #
#########


def _gradient(array, factor: float = 1.5):
    grad = np.empty_like(array)
    hs = 1 / (array.shape[0] - 1)
    grad[1:-1] = (array[2:] - array[1:-1]) / hs
    #  Extend to s=0 and s=1
    grad[0] = factor * grad[1] - (factor - 1) * grad[2]
    grad[-1] = factor * grad[-2] - (factor - 1) * grad[-3]
    return grad


def _to_full_mesh(array, mode: str = "explicit", factor: float = 1.5):
    """Numpy version of vmecnn.physics.utils.to_full_mesh."""
    assert mode in ("explicit", "implicit"), "mode %s not supported" % mode
    full = np.empty_like(array)
    full[1:-1] = 0.5 * (array[1:-1] + array[2:])
    if mode == "explicit":
        full[0] = factor * array[1] - (factor - 1) * array[2]
        full[-1] = factor * array[-1] - (factor - 1) * array[-2]
    else:
        full[0] = factor * full[1] - (factor - 1) * full[2]
        full[-1] = factor * full[-2] - (factor - 1) * full[-3]
    return full


def np_mae(target, other):
    return np.mean(np.abs(other - target))


def np_mape(target, other):
    mape = np.abs(other - target) / np.abs(target)
    return np.mean(mape[~np.isnan(mape)])


def mape(target, other):
    mape = torch.abs(other - target) / torch.abs(target)
    return torch.mean(mape[~mape.isnan()])


def mae(target, other):
    return torch.mean(torch.abs(other - target))


def assert_all_close(input, other, atol, f_atol=None):
    """Cast input before asserting equality."""
    if input.dtype != other.dtype:
        atol = f_atol
        input = input.type(other.dtype)
    assert torch.allclose(input, other, atol=atol, rtol=0), "{:.2e}".format(
        mae(other, input)
    )


def _plot_profiles(input, other, *args, **kwargs):
    import matplotlib.pyplot as plt

    x = torch.linspace(0, 1, input.size(0), dtype=input.dtype)
    plt.plot(x, other, "-o", *args, label="other", fillstyle="none", **kwargs)
    plt.plot(x, input, "-x", *args, label="input", fillstyle="none", **kwargs)
    plt.legend()
    plt.show()


###################################
# Inverse Fourier transform tests #
###################################

ns = (1, 2, 10)
mpol = (0, 1, 2, 6, 11)
ntor = (0, 1, 2, 4, 12)
ntheta = (None, 8, 16, 32)
nzeta = (None, 9, 18, 36)


@pytest.fixture(scope="session")
def r():
    rmn = torch.as_tensor(
        [[[0.0, 1.0, -0.1], [-0.5, 0.2, 0.5]], [[0.0, 1.0, -0.1], [-0.5, 0.1, 0.5]]],
        dtype=torch.float64,
    )
    return ift((rmn, None), ntheta=ntheta[-1], nzeta=nzeta[-1], endpoint=False)


@pytest.fixture(scope="session")
def z():
    zmn = torch.as_tensor(
        [[[0.0, 1.0, -0.1], [-0.5, 0.2, 0.5]], [[0.0, 1.0, -0.2], [-0.5, 0.2, 0.5]]],
        dtype=torch.float64,
    )
    return ift((None, zmn), ntheta=ntheta[-1], nzeta=nzeta[-1], endpoint=False)


@pytest.mark.parametrize("mpol", mpol)
@pytest.mark.parametrize("ntor", ntor)
@pytest.mark.parametrize("ntheta", (8, 16, 32))
@pytest.mark.parametrize("nzeta", (9, 18, 36))
@pytest.mark.parametrize("endpoint", (True, False))
def test_fourier_basis_shape(mpol, ntor, ntheta, nzeta, endpoint):
    costzmn, sintzmn = get_fourier_basis(
        mpol,
        ntor,
        ntheta,
        nzeta,
        endpoint,
        num_field_period=1,
        dtype=DTYPE,
        device="cpu",
    )
    shape = (1, ntheta, nzeta, mpol + 1, 2 * ntor + 1)
    assert costzmn.shape == shape
    assert sintzmn.shape == shape


#  TODO-1(@amerlo): torch.einsum(, a, b) is not equal to torch.sum(a*b)
@pytest.mark.parametrize("ns", ns)
@pytest.mark.parametrize("mpol", mpol)
@pytest.mark.parametrize("ntor", ntor)
@pytest.mark.parametrize("ntheta", ntheta)
@pytest.mark.parametrize("nzeta", nzeta)
def test_ift_analytical_values(ns, mpol, ntor, ntheta, nzeta):
    tensor = torch.rand(ns, mpol + 1, 2 * ntor + 1, dtype=DTYPE)
    ntheta = max(ntheta, 2 * mpol + 1) if ntheta is not None else None
    nzeta = max(nzeta, 2 * ntor + 1) if nzeta is not None else None
    grid = ift((tensor, None), ntheta, nzeta, endpoint=False)
    for s in range(ns):
        assert torch.isclose(
            grid[s, 0, 0], tensor[s].sum(), atol=1e-11, rtol=0
        ), "{} - {:.2e}".format(s, mae(grid[s, 0, 0], tensor[s].sum()))
        assert torch.isclose(
            grid[s].sum(), tensor[s, 0, ntor] * grid[s].numel(), atol=1e-11, rtol=0
        ), "{} - {:.2e}".format(
            s, mae(grid[s].sum(), tensor[s, 0, ntor] * grid[s].numel())
        )


@pytest.mark.parametrize("mpol", mpol)
@pytest.mark.parametrize("ntor", ntor)
@pytest.mark.parametrize("ntheta", ntheta)
@pytest.mark.parametrize("nzeta", nzeta)
def test_ift_call(mpol, ntor, ntheta, nzeta):
    #  Use only one flux surface
    ns = 1
    endpoint = False
    num_field_period = 5
    tensor = torch.rand(ns, mpol + 1, 2 * ntor + 1, dtype=DTYPE)
    ntheta = max(ntheta, 2 * mpol + 1) if ntheta is not None else 2 * mpol + 1
    nzeta = max(nzeta, 2 * ntor + 1) if nzeta is not None else 2 * ntor + 1
    thetas = linspace(0, 2 * torch.pi, ntheta, endpoint=endpoint, dtype=DTYPE)
    zetas = linspace(
        0, 2 * torch.pi / num_field_period, nzeta, endpoint=endpoint, dtype=DTYPE
    )
    grid = ift((tensor, None), ntheta, nzeta, endpoint=endpoint)
    #  Compute ift
    for i, theta in enumerate(thetas):
        for j, zeta in enumerate(zetas):
            res = 0
            for im, m in enumerate(range(mpol + 1)):
                for jn, n in enumerate(range(-ntor, ntor + 1)):
                    angle = m * theta - n * num_field_period * zeta
                    res += tensor[0, im, jn] * torch.cos(angle)
            assert torch.isclose(grid[0, i, j], res, atol=1e-12, rtol=0)


@pytest.mark.parametrize("mpol", mpol)
@pytest.mark.parametrize("ntor", ntor)
@pytest.mark.parametrize("ntheta", ntheta)
@pytest.mark.parametrize("nzeta", nzeta)
def test_ift_grad(mpol, ntor, ntheta, nzeta):
    #  Use only one flux surface
    ns = 1
    endpoint = False
    num_field_period = 5
    tensor = torch.rand(ns, mpol + 1, 2 * ntor + 1, dtype=DTYPE)
    tensor.requires_grad_(True)
    ntheta = max(ntheta, 2 * mpol + 1) if ntheta is not None else 2 * mpol + 1
    nzeta = max(nzeta, 2 * ntor + 1) if nzeta is not None else 2 * ntor + 1
    thetas = linspace(0, 2 * torch.pi, ntheta, endpoint=endpoint, dtype=DTYPE)
    zetas = linspace(
        0, 2 * torch.pi / num_field_period, nzeta, endpoint=endpoint, dtype=DTYPE
    )
    grid = ift((tensor, None), ntheta, nzeta, endpoint=endpoint)
    #  Compute the gradient of ift
    for i, theta in enumerate(thetas):
        for j, zeta in enumerate(zetas):
            torch_grad = torch.autograd.grad(grid[0, i, j], tensor, retain_graph=True)[
                0
            ]
            assert torch_grad.shape == tensor.shape
            grad = torch.empty(ns, mpol + 1, 2 * ntor + 1, dtype=DTYPE)
            for im, m in enumerate(range(mpol + 1)):
                for jn, n in enumerate(range(-ntor, ntor + 1)):
                    angle = m * theta - n * num_field_period * zeta
                    grad[0, im, jn] = torch.cos(angle)
            assert torch.allclose(torch_grad, grad, atol=1e-12, rtol=0)


@pytest.mark.parametrize("mpol", mpol)
@pytest.mark.parametrize("ntor", ntor)
@pytest.mark.parametrize("ntheta", ntheta)
@pytest.mark.parametrize("nzeta", nzeta)
def test_ift_shape(mpol, ntor, ntheta, nzeta):
    tensor = torch.rand(1, mpol + 1, 2 * ntor + 1)
    grid = ift((tensor, None), ntheta=ntheta, nzeta=nzeta)
    if ntheta is None:
        ntheta = 2 * mpol + 1
    if nzeta is None:
        nzeta = 2 * ntor + 1
    assert grid.shape == (1, ntheta, nzeta)


@pytest.mark.parametrize(
    "k, i, j, target",
    [
        (0, 0, 0, 1.1),  # s=0, theta=0, zeta=0
        (1, 0, 0, 1.0),  # s=1, theta=0, zeta=0
        (0, 0, 9, 1.2),  # s=0, theta=0, zeta=pi / 10
        (0, 8, 0, 0.9),  # s=0, theta=pi / 10, zeta=0
        (0, 8, 9, 2.0),  # s=0, theta=pi / 10, zeta = pi / 10
    ],
)
def test_r(r, k, i, j, target):
    assert torch.isclose(
        torch.tensor(target, dtype=r.dtype), r[k, i, j], atol=1e-15, rtol=0
    )


@pytest.mark.parametrize(
    "k, i, j, target",
    [
        (0, 0, 0, 0.0),
        (0, 0, 9, -0.9),
        (1, 0, 9, -0.8),
        (0, 8, 0, 0.2),
        (0, 8, 9, 0.3),
    ],
)
def test_z(z, k, i, j, target):
    assert torch.isclose(
        torch.tensor(target, dtype=z.dtype), z[k, i, j], atol=1e-15, rtol=0
    )


@pytest.mark.parametrize("ns", (99,))
def test_ift_memory(ns):
    mem0, ids0 = get_torch_memory(return_ids=True)
    xmn = torch.randn(ns, 12, 25, dtype=torch.float64)
    x = ift((xmn, None), 32, 36)  # noqa: F841
    mem1 = get_torch_memory()
    nelement = ns * 12 * 25  # xmn
    nelement += ns * 32 * 36  # x
    nelement += (32 * 36 * 12 * 25) * 2  # costzmn, sintzmn
    mem = 8 * nelement  # torch.float64
    assert (mem1 - mem0) == mem, "{:.2f} MiB".format((mem1 - mem) / 1024**2)


###########################
# Fourier transform tests #
###########################


mpol = (4, 11, 16)
ntor = (4, 12, 18)


@pytest.mark.parametrize("mpol", mpol)
@pytest.mark.parametrize("ntor", ntor)
def test_ft_analytical_values(mpol, ntor):
    #  Create grid with enough sampled points
    tensor = torch.randn(1, 2 * mpol + 1, 2 * ntor + 1, dtype=DTYPE)
    mn = ft(tensor, "cos", mpol, ntor, endpoint=False)
    assert torch.isclose(mn.sum(), tensor[0, 0, 0])
    assert torch.isclose(
        tensor.sum(), mn[0, 0, ntor] * tensor.numel(), rtol=0, atol=1e-12
    )


@pytest.mark.parametrize("mpol", mpol)
@pytest.mark.parametrize("ntor", ntor)
@pytest.mark.parametrize("basis", ("cos", "sin"))
@pytest.mark.parametrize("endpoint", (False,))
def test_ft_of_ift(mpol, ntor, basis, endpoint):
    mn = torch.randn(1, mpol + 1, 2 * ntor + 1, dtype=DTYPE)
    #  Mimic VMEC like coeffs with m=0, n<0 null
    if basis == "cos":
        mn[:, 0, :ntor] = 0
    else:
        mn[:, 0, : ntor + 1] = 0
    #  Sample at the Nyquist limit
    if basis == "cos":
        tensor = ift(
            (mn, None), ntheta=2 * mpol + 1, nzeta=2 * ntor + 1, endpoint=endpoint
        )
    else:
        tensor = ift(
            (None, mn), ntheta=2 * mpol + 1, nzeta=2 * ntor + 1, endpoint=endpoint
        )
    mn_ = ft(tensor, basis, mpol, ntor, endpoint=endpoint)
    assert torch.allclose(mn_, mn, rtol=0, atol=1e-13)


#####################
# Equilibrium tests #
#####################


@pytest.mark.parametrize(
    "coef",
    (
        torch.arange(10 * 3 * 2).view(10, 2, 3),
        torch.rand(10 * 3 * 2).view(10, 2, 3),
        torch.rand(99 * 12 * 25).view(99, 12, 25),
    ),
)
@pytest.mark.parametrize("func", ("cos", "sin"))
def test_derivative_along_s(coef, func):
    numpy_grad = torch.from_numpy(np.gradient(coef, 1 / (coef.size(0) - 1), axis=0))
    grad = gradient(coef, order=-2)
    assert torch.allclose(grad, numpy_grad, rtol=0, atol=0)


@pytest.mark.parametrize(
    "coef",
    (
        torch.rand(10 * 3 * 2, dtype=DTYPE).view(10, 2, 3),
        torch.rand(99 * 12 * 25, dtype=DTYPE).view(99, 12, 25),
    ),
)
@pytest.mark.parametrize("func", ("cos", "sin"))
@pytest.mark.parametrize("var", ("u", "v"))
def test_derivative_along_angle(coef, func, var):
    grad = derivative(coef, func=func, var=var)
    if var == "u":
        for m in range(coef.size(1)):
            if func == "cos":
                assert torch.allclose(grad[:, m, :], -coef[:, m, :] * m, atol=0, rtol=0)
            else:
                assert torch.allclose(grad[:, m, :], coef[:, m, :] * m, atol=0, rtol=0)
    else:
        #  TODO-1(@amerlo): why tolerances are different?
        ntor = int((coef.size(2) - 1) / 2)
        for i, n in enumerate(range(-ntor, ntor + 1)):
            if func == "cos":
                assert torch.allclose(
                    grad[:, :, i], coef[:, :, i] * n * 5, atol=1e-14, rtol=0
                )
            else:
                assert torch.allclose(
                    grad[:, :, i], -coef[:, :, i] * n * 5, atol=1e-14, rtol=0
                )


#  Number of runs in data
root = "tests/resources/data"
num_samples = len(os.listdir(root))


@pytest.fixture(scope="session")
def data():
    return VmecEquilibria(root=root, train_test_split=1.0)


@pytest.fixture(scope="session")
def runs(data):
    return [netCDF4.Dataset(item["wout_path"]) for item in data]


@pytest.fixture(scope="session")
def equis(data):
    return [
        Equilibrium(
            rmnc=item["rmnc"],
            lmns=item["lmns"],
            zmns=item["zmns"],
            iota=item["iota"],
            phiedge=item["phiedge"],
            pressure=item["pressure"],
            toroidal_current=item["toroidal_current"] * item["curtor"],
            normalized_flux=np.linspace(0, 1, len(item["iota"]), dtype=np.float64),
            wout_path=item["wout_path"],
        )
        for item in data
    ]


#  This test assesses how much we loose from the type casting
@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(99))
def test_compute_iotaf(runs, equis, s, k):
    equi = equis[k]
    run = runs[k]
    iota = torch.from_numpy(run["iotaf"][:].data)
    assert_all_close(equi.iota[s], iota[s], atol=0.0, f_atol=1e-6)


@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(99))
def test_compute_iotaf_from_iotas(runs, s, k):
    run = runs[k]
    vmec_iotaf = torch.from_numpy(run["iotaf"][:].data)
    iotas = torch.from_numpy(run["iotas"][:].data)
    iotaf = to_full_mesh(iotas)
    assert torch.allclose(iotaf[s], vmec_iotaf[s], atol=0, rtol=0)


@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(99))
def test_compute_iotas(runs, equis, s, k):
    equi = equis[k]
    run = runs[k]
    iotas = torch.from_numpy(run["iotas"][:].data)
    assert_all_close(equi.iotas[s], iotas[s], atol=1e-15, f_atol=1e-6)


@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(1, 98))
def test_compute_dshear(runs, equis, s, k):
    equi = equis[k]
    run = runs[k]
    dshear = torch.from_numpy(run["DShear"][:].data)
    assert_all_close(equi.dshear[s], dshear[s], atol=1e-14, f_atol=1e-6)


@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(1, 98))
def test_compute_shear(runs, equis, k, s):
    equi = equis[k]
    run = runs[k]
    dshear = torch.from_numpy(run["DShear"][:].data)
    shear = torch.sqrt(dshear * 4)
    shear_ = torch.abs(equi.shear)
    assert_all_close(shear_[s], shear[s], atol=1e-13, f_atol=1e-6)


#  TODO-0(@amerlo): fix value on s=1
@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(1, 99))
@pytest.mark.parametrize("ntheta", (32,))
@pytest.mark.parametrize("nzeta", (36,))
def test_compute_jacobian(runs, equis, k, s, ntheta, nzeta):
    equi = equis[k]
    run = runs[k]
    gmnc = run["gmnc"][:].data
    gmnc = np.append(np.zeros((gmnc.shape[0], 18)), gmnc, axis=1)
    gmnc = torch.from_numpy(gmnc.reshape(-1, 17, 37))
    jacobian = ift((gmnc, None), ntheta, nzeta)
    equi.ntheta = ntheta
    equi.nzeta = nzeta
    assert_all_close(equi.jacobian[s], jacobian[s], atol=1e-11, f_atol=1e-6)


#  We know this scheme is not accurate enough, especially on axis
@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(1, 99))
@pytest.mark.parametrize("ntheta", (32,))
@pytest.mark.parametrize("nzeta", (36,))
def test_compute_jacobian_(runs, equis, k, s, ntheta, nzeta):
    equi = equis[k]
    run = runs[k]
    gmnc = run["gmnc"][:].data
    gmnc = np.append(np.zeros((gmnc.shape[0], 18)), gmnc, axis=1)
    gmnc = torch.from_numpy(gmnc.reshape(-1, 17, 37))
    jacobian = ift((gmnc, None), ntheta, nzeta)
    equi.ntheta = ntheta
    equi.nzeta = nzeta
    assert_all_close(equi.jacobian_[s], jacobian[s], atol=1e-4, f_atol=1e-4)


#  TODO-0(@amerlo): fix last poloidal and toroidal mode numbers
#  (Pdb) (equi.gmnc[2:, 0, :-1] - gmnc[2:, 0, :-1]).abs().max()
#  tensor(6.2395e-14, dtype=torch.float64)
@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(1, 99))
def test_compute_gmnc(equis, runs, k, s):
    equi = equis[k]
    run = runs[k]
    gmnc = run["gmnc"][:].data
    gmnc = np.append(np.zeros((gmnc.shape[0], 18)), gmnc, axis=1)
    gmnc = torch.from_numpy(gmnc.reshape(-1, 17, 37))
    assert_all_close(equi.gmnc[s], gmnc[s], atol=1e-11, f_atol=1e-6)


@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(99))
def test_compute_vp(equis, runs, k, s):
    equi = equis[k]
    run = runs[k]
    vp = torch.from_numpy(run["vp"][:].data)
    assert_all_close(equi.vp[s], vp[s], atol=1e-13, f_atol=1e-6)


@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(99))
def test_compute_vp_from_vmec(equis, runs, k, s):
    run = runs[k]
    vmec_vp = run["vp"][:]
    vp = np.abs(run["gmnc"][:, 0].data)
    assert np.isclose(vp[s], vmec_vp[s], atol=0, rtol=0), "mae: {:.2e}".format(
        abs(vp[s] - vmec_vp[s])
    )


@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(99))
@pytest.mark.parametrize("ntheta", (32,))
@pytest.mark.parametrize("nzeta", (36,))
def test_compute_bsupu_from_vmec(runs, k, s, ntheta, nzeta):
    run = runs[k]
    bsupumnc = run["bsupumnc"][:].data
    bsupumnc = np.append(np.zeros((bsupumnc.shape[0], 18)), bsupumnc, axis=1)
    bsupumnc = torch.from_numpy(bsupumnc.reshape(-1, 17, 37))
    vmec_bsupu = ift((bsupumnc, None), ntheta, nzeta)

    lmns = run["lmns"][:].data
    lmns = np.append(np.zeros((lmns.shape[0], 12)), lmns, axis=1)
    lmns = torch.from_numpy(lmns.reshape(-1, 12, 25))

    lvmnc = derivative(lmns, "sin", "v")
    lv = ift((lvmnc, None), ntheta, nzeta)

    gmnc = run["gmnc"][:].data
    gmnc = np.append(np.zeros((gmnc.shape[0], 18)), gmnc, axis=1)
    gmnc = torch.from_numpy(gmnc.reshape(-1, 17, 37))
    jacobian = ift((gmnc, None), ntheta, nzeta)

    iotas = torch.from_numpy(run["iotas"][:])
    phips = torch.from_numpy(run["phips"][:])

    bsupu = (phips.view(-1, 1, 1) / jacobian) * (iotas.view(-1, 1, 1) - lv)

    #  See VMEC2000/Sources/General/bcovar.f#L676
    bsupu[0] = 0

    assert_all_close(bsupu[s], vmec_bsupu[s], atol=1e-13, f_atol=1e-13)


@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(0, 98))
@pytest.mark.parametrize("ntheta", (32,))
@pytest.mark.parametrize("nzeta", (36,))
def test_compute_bsupv_from_vmec(runs, k, s, ntheta, nzeta):
    run = runs[k]
    bsupvmnc = run["bsupvmnc"][:].data
    bsupvmnc = np.append(np.zeros((bsupvmnc.shape[0], 18)), bsupvmnc, axis=1)
    bsupvmnc = torch.from_numpy(bsupvmnc.reshape(-1, 17, 37))
    vmec_bsupv = ift((bsupvmnc, None), ntheta, nzeta)

    lmns = run["lmns"][:].data
    lmns = np.append(np.zeros((lmns.shape[0], 12)), lmns, axis=1)
    lmns = torch.from_numpy(lmns.reshape(-1, 12, 25))

    lumnc = derivative(lmns, "sin", "u")
    lu = ift((lumnc, None), ntheta, nzeta)

    gmnc = run["gmnc"][:].data
    gmnc = np.append(np.zeros((gmnc.shape[0], 18)), gmnc, axis=1)
    gmnc = torch.from_numpy(gmnc.reshape(-1, 17, 37))
    jacobian = ift((gmnc, None), ntheta, nzeta)

    phips = torch.from_numpy(run["phips"][:])

    bsupv = phips.view(-1, 1, 1) * (1 + lu) / jacobian

    #  See VMEC2000/Sources/General/bcovar.f#L677
    bsupv[0] = 0

    assert_all_close(bsupv[s], vmec_bsupv[s], atol=1e-13, f_atol=1e-13)


@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(99))
@pytest.mark.parametrize("ntheta", (32,))
@pytest.mark.parametrize("nzeta", (36,))
def test_compute_bsupu(equis, runs, k, s, ntheta, nzeta):
    equi = equis[k]
    run = runs[k]
    bsupumnc = run["bsupumnc"][:].data
    bsupumnc = np.append(np.zeros((bsupumnc.shape[0], 18)), bsupumnc, axis=1)
    bsupumnc = torch.from_numpy(bsupumnc.reshape(-1, 17, 37))
    bsupu = ift((bsupumnc, None), ntheta, nzeta)
    equi.ntheta = ntheta
    equi.nzeta = nzeta
    assert_all_close(equi.bsupu[s], bsupu[s], atol=1e-11, f_atol=1e-6)


@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(99))
@pytest.mark.parametrize("ntheta", (32,))
@pytest.mark.parametrize("nzeta", (36,))
def test_compute_bsupv(equis, runs, k, s, ntheta, nzeta):
    equi = equis[k]
    run = runs[k]
    bsupvmnc = run["bsupvmnc"][:].data
    bsupvmnc = np.append(np.zeros((bsupvmnc.shape[0], 18)), bsupvmnc, axis=1)
    bsupvmnc = torch.from_numpy(bsupvmnc.reshape(-1, 17, 37))
    bsupv = ift((bsupvmnc, None), ntheta, nzeta)
    equi.ntheta = ntheta
    equi.nzeta = nzeta
    assert_all_close(equi.bsupv[s], bsupv[s], atol=1e-11, f_atol=1e-6)


#  TODO-1(@amerlo): how to atomically test these components?
@pytest.mark.parametrize("component", ("uu", "uv", "vv"))
def test_compute_metric_tensor(equis, component):
    equi = equis[-1]
    g = getattr(equi, f"g{component}")
    assert g.shape == (equi.rmnc.size(0), equi.ntheta, equi.nzeta)


#  TODO-0(@amerlo): fix me!
@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(99))
@pytest.mark.parametrize("ntheta", (32,))
@pytest.mark.parametrize("nzeta", (36,))
def test_compute_bsubu(equis, runs, k, s, ntheta, nzeta):
    equi = equis[k]
    run = runs[k]
    bsubumnc = run["bsubumnc"][:].data
    bsubumnc = np.append(np.zeros((bsubumnc.shape[0], 18)), bsubumnc, axis=1)
    bsubumnc = torch.from_numpy(bsubumnc.reshape(-1, 17, 37))
    bsubu = ift((bsubumnc, None), ntheta, nzeta)
    equi.ntheta = ntheta
    equi.nzeta = nzeta
    assert_all_close(equi.bsubu[s], bsubu[s], atol=1e-6, f_atol=1e-6)


#  TODO-0(@amerlo): fix me!
@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(99))
@pytest.mark.parametrize("ntheta", (32,))
@pytest.mark.parametrize("nzeta", (36,))
def test_compute_bsubv(equis, runs, k, s, ntheta, nzeta):
    equi = equis[k]
    run = runs[k]
    bsubvmnc = run["bsubvmnc"][:].data
    bsubvmnc = np.append(np.zeros((bsubvmnc.shape[0], 18)), bsubvmnc, axis=1)
    bsubvmnc = torch.from_numpy(bsubvmnc.reshape(-1, 17, 37))
    bsubv = ift((bsubvmnc, None), ntheta, nzeta)
    equi.ntheta = ntheta
    equi.nzeta = nzeta
    assert_all_close(equi.bsubv[s], bsubv[s], atol=1e-6, f_atol=1e-6)


#  TODO-0(@amerlo): fix me!
@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(99))
@pytest.mark.parametrize("ntheta", (32,))
@pytest.mark.parametrize("nzeta", (36,))
def test_compute_bsubs(equis, runs, k, s, ntheta, nzeta):
    equi = equis[k]
    run = runs[k]
    bsubsmns = run["bsubsmns"][:].data
    bsubsmns = np.append(np.zeros((bsubsmns.shape[0], 18)), bsubsmns, axis=1)
    bsubsmns = torch.from_numpy(bsubsmns.reshape(-1, 17, 37))
    bsubs = ift((None, bsubsmns), ntheta, nzeta)
    equi.ntheta = ntheta
    equi.nzeta = nzeta
    assert_all_close(equi.bsubs[s], bsubs[s], atol=1e-6, f_atol=1e-6)


#  TODO-0(@amerlo): fix me!
@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(99))
@pytest.mark.parametrize("ntheta", (32,))
@pytest.mark.parametrize("nzeta", (36,))
def test_compute_jcuru(equis, runs, k, s, ntheta, nzeta):
    equi = equis[k]
    run = runs[k]
    bsubvmnc = torch.from_numpy(run["bsubvmnc"][:, 0].data)
    signgs = torch.from_numpy(run["signgs"][:].data)
    jcuru = -gradient(bsubvmnc, order=1, factor=2.0) * signgs
    equi.ntheta = ntheta
    equi.nzeta = nzeta
    assert_all_close(equi.jcuru[s], jcuru[s], atol=1e-5, f_atol=1e-5)


#  TODO-0(@amerlo): fix me!
@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(99))
@pytest.mark.parametrize("ntheta", (32,))
@pytest.mark.parametrize("nzeta", (36,))
def test_compute_jcurv(equis, runs, k, s, ntheta, nzeta):
    equi = equis[k]
    run = runs[k]
    bsubumnc = torch.from_numpy(run["bsubumnc"][:, 0].data)
    signgs = torch.from_numpy(run["signgs"][:].data)
    jcurv = gradient(bsubumnc, order=1, factor=2.0) * signgs
    equi.ntheta = ntheta
    equi.nzeta = nzeta
    assert_all_close(equi.jcurv[s], jcurv[s], atol=1e-5, f_atol=1e-5)


#  TODO-0(@amerlo): fix me!
@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(99))
def test_compute_b(equis, runs, k, s):
    equi = equis[k]
    run = runs[k]
    bmnc = run["bmnc"][:].data
    bmnc = np.append(np.zeros((bmnc.shape[0], 18)), bmnc, axis=1)
    bmnc = torch.from_numpy(bmnc.reshape(-1, 17, 37))
    b = ift((bmnc, None), 32, 36)
    assert_all_close(equi.b[s], b[s], atol=1e-5, f_atol=1e-5)


#  TODO-0(@amerlo): fix me!
@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(99))
def test_compute_bmnc(equis, runs, k, s):
    equi = equis[k]
    run = runs[k]
    bmnc = run["bmnc"][:].data
    bmnc = np.append(np.zeros((bmnc.shape[0], 18)), bmnc, axis=1)
    bmnc = torch.from_numpy(bmnc.reshape(-1, 17, 37))
    bmnc = to_full_mesh(bmnc)
    assert_all_close(equi.bmnc[s], bmnc[s], atol=1e-3, f_atol=1e-3)


#  TODO-0(@amerlo): decrease tollerance once jacobian on js=1 has been fixed
@pytest.mark.parametrize("k", range(num_samples))
def test_compute_vacuum_well(equis, runs, k):
    equi = equis[k]
    run = runs[k]
    #  vp on half-mesh
    vp = torch.from_numpy(run["vp"][:].data)
    # See: https://github.com/hiddenSymmetries/simsopt/blob/master/src/simsopt/mhd/vmec.py
    vp_s0 = 1.5 * vp[1] - 0.5 * vp[2]
    vp_s1 = 1.5 * vp[-1] - 0.5 * vp[-2]
    vacuum_well = (vp_s0 - vp_s1) / vp_s0
    assert_all_close(equi.vacuum_well, vacuum_well, atol=1e-4, f_atol=1e-4)


@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(99))
def test_compute_well(equis, runs, k, s):
    equi = equis[k]
    run = runs[k]
    #  vp on half-mesh
    vp = torch.from_numpy(run["vp"][:].data)
    #  mercier.f#L86
    vp = vp * 4 * np.pi**2 / run["phi"][:].data[-1] * run["signgs"][:].data
    #  mercier.f#L102
    #  WELL in mercier.txt
    #  well is not defined on axis and at LCFS, here we extrapolate
    well = gradient(vp, order=1) / run["phi"][:].data[-1]
    #  mercier.f#L167
    well = -well * run["signgs"][:].data
    assert_all_close(equi.well[s], well[s], atol=1e-10, f_atol=1e-6)


#  TODO-1(@amerlo): understand how to compare them
@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(99))
def test_compare_well_and_well_(equis, runs, k, s):
    equi = equis[k]
    assert_all_close(equi.well[s], equi.well_[s], atol=1e-10, f_atol=1e-6)


@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(99))
def test_compute_pres_from_presf(runs, equis, s, k):
    run = runs[k]
    #  Use VMEC internal representation
    mu0 = 4 * np.pi * 1e-7
    vmec_pres = torch.from_numpy(run["pres"][:].data)
    presf = torch.from_numpy(run["presf"][:].data) * mu0
    pres = to_half_mesh(presf) / mu0
    assert_all_close(pres[s], vmec_pres[s], atol=1e-9, f_atol=1e-9)


@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(99))
def test_compute_presf_from_pres(runs, s, k):
    run = runs[k]
    #  Use VMEC internal representation
    mu0 = 4 * np.pi * 1e-7
    vmec_presf = torch.from_numpy(run["presf"][:].data)
    pres = torch.from_numpy(run["pres"][:].data) * mu0
    presf = to_full_mesh(pres) / mu0
    assert torch.allclose(presf[s], vmec_presf[s], atol=0, rtol=0)


#  When casting from double to float, we loose precision, especially
#  on a quantity such as the pressure.
@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(99))
def test_compute_pres(runs, equis, s, k):
    equi = equis[k]
    run = runs[k]
    pres = torch.from_numpy(run["pres"][:].data)
    assert_all_close(equi.pres[s], pres[s], atol=1e-9, f_atol=1e-0)


@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(99))
def test_compute_equif_from_vmec(runs, k, s):

    run = runs[k]

    mu0 = 4 * np.pi * 1e-7
    twopi = 2 * np.pi

    #  <RADIAL FORCE> in threed1.txt
    vmec_equif = run["equif"][:]

    #  Pressure on half-mesh
    #  Include the mu0 factor here as done in VMEC
    pres = run["pres"][:] * mu0

    signgs = run["signgs"][:]
    phiedge = run["phi"][-1]

    #  d(PRES)/d(PHI) = d(PRES)/ds / phiedge
    #  d(PRES)/d(PHI) in threed1.txt
    presgrad = _gradient(pres, factor=2.0)

    bsubu = run["bsubumnc"][:, 0]
    bsubv = run["bsubvmnc"][:, 0]

    #  d(Vol)/ds
    vp = _to_full_mesh(run["vp"][:], mode="explicit", factor=2.0)

    #  <JSUPU> and <JSUPV> in threed1.txt
    #  <JSUPx> = jcurx / vp / mu0
    jcuru = -_gradient(bsubv, factor=2.0) * signgs
    jcurv = _gradient(bsubu, factor=2.0) * signgs

    #  IOTA in threed1.txt
    iotaf = run["iotaf"][:]

    #  d(Phi)/ds
    phipf = phiedge / twopi / signgs

    #  Note that vp == d(PHI)/d(s)
    equif = phipf * iotaf * jcurv - phipf * jcuru + presgrad * vp
    equif /= abs(phipf * iotaf * jcurv) + abs(phipf * jcuru) + abs(presgrad) * vp

    equif[0] = 2.0 * equif[1] - equif[2]  # eqfor.f#L206
    equif[-1] = 2.0 * equif[-2] - equif[-3]  # eqfor.f#L213

    assert np.isclose(
        equif[s], vmec_equif[s], atol=1e-14, rtol=0
    ), "mae: {:.2e}".format(abs(equif[s] - vmec_equif[s]))


#  TODO-0(@amerlo): fix me!
@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(99))
def test_compute_equif(equis, runs, k, s):
    equi = equis[k]
    run = runs[k]
    equif = torch.from_numpy(run["equif"][:].data)
    assert_all_close(equi.equif[s], equif[s], atol=1e-6, f_atol=1e-6)


#  TODO-0(@amerlo): fix me!
@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(99))
def test_compute_f(equis, runs, k, s):
    equi = equis[k]
    run = runs[k]

    vmec_equif = torch.from_numpy(run["equif"][:].data)

    mu0 = 4 * np.pi * 1e-7
    twopi = 2 * np.pi

    signgs = torch.from_numpy(run["signgs"][:].data)
    phiedge = torch.from_numpy(run["phi"][-1].data)
    iotaf = torch.from_numpy(run["iotaf"][:].data)
    pres = torch.from_numpy(run["pres"][:].data) * mu0

    phipf = phiedge / twopi / signgs

    presgrad = gradient(pres, order=1, factor=2.0)

    vp = to_full_mesh(torch.from_numpy(run["vp"][:].data))

    bsubu = torch.from_numpy(run["bsubumnc"][:, 0].data)
    bsubv = torch.from_numpy(run["bsubvmnc"][:, 0].data)

    jcuru = -gradient(bsubv, order=1, factor=2.0) * signgs
    jcurv = gradient(bsubu, order=1, factor=2.0) * signgs

    #  First make sure that equif in torch is equal to equif from vmec
    equif = phipf * iotaf * jcurv - phipf * jcuru + presgrad * vp
    equif /= abs(phipf * iotaf * jcurv) + abs(phipf * jcuru) + abs(presgrad) * vp
    equif[0] = 2.0 * equif[1] - equif[2]
    equif[-1] = 2.0 * equif[-2] - equif[-3]

    assert torch.allclose(equif[s], vmec_equif[s], atol=1e-14, rtol=0)
    del equif

    #  Now compute f
    f = phipf * iotaf * jcurv - phipf * jcuru + presgrad * vp
    f[0] = 2.0 * f[1] - f[2]
    f[-1] = 2.0 * f[-2] - f[-3]

    assert_all_close(equi.f[s], f[s], atol=1e-6, f_atol=1e-6)


#  TODO-0(@amerlo): fix me!
@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(1, 98))
@pytest.mark.parametrize("ntheta", (32,))
@pytest.mark.parametrize("nzeta", (36,))
def test_compute_dwell_from_vmec(runs, k, s, ntheta, nzeta):

    run = runs[k]

    mu0 = 4 * np.pi * 1e-7
    twopi = 2 * np.pi

    vmec_dwell = torch.from_numpy(run["DWell"][:].data)

    signgs = run["signgs"][:].data
    phiedge = run["phi"][:].data[-1]

    pres = torch.from_numpy(run["pres"][:].data) * mu0
    presgrad = gradient(pres, order=1, factor=2.0) / phiedge

    #  mercier.f#L86, mercier.f#L102
    vp = torch.from_numpy(run["vp"][:].data)
    vp = vp * signgs * twopi**2 / phiedge
    vpp = gradient(vp, order=1) / phiedge

    gmnc = run["gmnc"][:].data
    gmnc = np.append(np.zeros((gmnc.shape[0], 18)), gmnc, axis=1)
    gmnc = torch.from_numpy(gmnc.reshape(-1, 17, 37))
    jacobian = ift((gmnc, None), ntheta, nzeta)

    gsqrt_full = to_full_mesh(jacobian) / phiedge

    bmnc = run["bmnc"][:].data
    bmnc = np.append(np.zeros((bmnc.shape[0], 18)), bmnc, axis=1)
    bmnc = torch.from_numpy(bmnc.reshape(-1, 17, 37))
    b2 = to_full_mesh(ift((bmnc, None), ntheta, nzeta)) ** 2

    #  tpp = <1 / B ** 2>
    tpp = twopi**2 * (gsqrt_full / b2).mean(dim=(1, 2))

    rmnc = run["rmnc"][:].data
    rmnc = np.append(np.zeros((rmnc.shape[0], 12)), rmnc, axis=1)
    rmnc = torch.from_numpy(rmnc.reshape(-1, 12, 25))

    r = ift((rmnc, None), ntheta, nzeta)

    #  ru = rt
    rumns = derivative(rmnc, "cos", "u")
    rt = ift((None, rumns), ntheta, nzeta)

    #  rv = rz
    rvmns = derivative(rmnc, "cos", "v")
    rz = ift((None, rvmns), ntheta, nzeta)

    zmns = run["zmns"][:].data
    zmns = np.append(np.zeros((zmns.shape[0], 12)), zmns, axis=1)
    zmns = torch.from_numpy(zmns.reshape(-1, 12, 25))

    #  zu = zt
    zumnc = derivative(zmns, "sin", "u")
    zt = ift((zumnc, None), ntheta, nzeta)

    #  zv = zz
    zvmnc = derivative(zmns, "sin", "v")
    zz = ift((zvmnc, None), ntheta, nzeta)

    #  gtt is equivalent to guu
    gtt = rt**2 + zt**2

    #  gpp = 1 / gpp as in VMEC
    #  gss is equivalent to gpp
    #  mercier.f#L124
    gpp = gsqrt_full**2 / (gtt * r**2 + (rt * zz - rz * zt) ** 2)

    #  tbb = <B ** 2 / gpp>
    tbb = twopi**2 * (b2 * gsqrt_full * gpp).mean(dim=(1, 2))

    dwell = presgrad * (vpp - presgrad * tpp) * tbb

    assert_all_close(dwell[s], vmec_dwell[s], atol=1e-6, f_atol=1e-6)


#  TODO-0(@amerlo): fix me!
@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(1, 98))
def test_compute_dwell(equis, runs, k, s):
    equi = equis[k]
    run = runs[k]
    dwell = torch.from_numpy(run["DWell"][:].data)
    assert_all_close(equi.dwell[s], dwell[s], atol=1e-6, f_atol=1e-6)


#  TODO-0(@amerlo): fix me!
#  Force balance is computed in MKS unit (i.e., * mu0).
#  Error in computation of ~1e-1 are therefore small compared to
#  the pressure gradient order of magnitude.
#  Namely, the errors in avforce (~1e-1) are small compared to
#  the error in ovp * presgrad (~1e3).
@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(1, 98))
@pytest.mark.parametrize("ntheta", (32,))
@pytest.mark.parametrize("nzeta", (36,))
def test_compute_avforce_from_vmec(runs, equis, k, s, ntheta, nzeta):

    run = runs[k]
    equi = equis[k]

    mu0 = 4 * np.pi * 1e-7

    #  Get jxbout file
    wout_path = equi.wout_path
    wout_filename = os.path.basename(wout_path.item())
    vmec_id = wout_filename.split("_")[1].split(".")[0]
    jxbout_filename = "jxbout_" + vmec_id + ".nc"
    jxbout_path = os.path.join(os.path.dirname(wout_path.item()), jxbout_filename)
    jxbout = netCDF4.Dataset(jxbout_path)

    vmec_avforce = torch.from_numpy(jxbout["avforce"][:].data)
    vmec_pprime = torch.from_numpy(jxbout["pprime"][:].data)

    pres = torch.from_numpy(run["pres"][:].data)
    presgrad = gradient(pres, order=1, factor=2.0)

    vp = torch.from_numpy(run["vp"][:].data)
    ovp = 1 / ((2 * np.pi) ** 2 * to_full_mesh(vp))

    #  See jxbforce.f#L642
    pprime = ovp * presgrad

    assert_all_close(pprime[s], vmec_pprime[s], atol=1e-9, f_atol=1e-6)

    bsubsmns = run["bsubsmns"][:].data
    bsubsmns = np.append(np.zeros((bsubsmns.shape[0], 18)), bsubsmns, axis=1)
    bsubsmns = torch.from_numpy(bsubsmns.reshape(-1, 17, 37))

    bsubsumnc = derivative(bsubsmns, "sin", "u")
    bsubsu = ift((bsubsumnc, None), ntheta, nzeta)

    bsubsvmnc = derivative(bsubsmns, "sin", "v")
    bsubsv = ift((bsubsvmnc, None), ntheta, nzeta)

    bsupvmnc = run["bsupvmnc"][:].data
    bsupvmnc = np.append(np.zeros((bsupvmnc.shape[0], 18)), bsupvmnc, axis=1)
    bsupvmnc = torch.from_numpy(bsupvmnc.reshape(-1, 17, 37))
    bsupv = ift((bsupvmnc, None), ntheta, nzeta)

    bsupumnc = run["bsupumnc"][:].data
    bsupumnc = np.append(np.zeros((bsupumnc.shape[0], 18)), bsupumnc, axis=1)
    bsupumnc = torch.from_numpy(bsupumnc.reshape(-1, 17, 37))
    bsupu = ift((bsupumnc, None), ntheta, nzeta)

    bsubvmnc = run["bsubvmnc"][:].data
    bsubvmnc = np.append(np.zeros((bsubvmnc.shape[0], 18)), bsubvmnc, axis=1)
    bsubvmnc = torch.from_numpy(bsubvmnc.reshape(-1, 17, 37))
    bsubv = ift((bsubvmnc, None), ntheta, nzeta)

    bsubumnc = run["bsubumnc"][:].data
    bsubumnc = np.append(np.zeros((bsubumnc.shape[0], 18)), bsubumnc, axis=1)
    bsubumnc = torch.from_numpy(bsubumnc.reshape(-1, 17, 37))
    bsubu = ift((bsubumnc, None), ntheta, nzeta)

    gmnc = run["gmnc"][:].data
    gmnc = np.append(np.zeros((gmnc.shape[0], 18)), gmnc, axis=1)
    gmnc = torch.from_numpy(gmnc.reshape(-1, 17, 37))
    gsqrt = ift((gmnc, None), ntheta, nzeta)

    gsqrt_full_mesh = to_full_mesh(gsqrt)

    bsubsv = to_full_mesh(bsubsv)
    bsubsu = to_full_mesh(bsubsu)

    #  See jxbforce.f#L632
    bsupu = to_full_mesh(bsupu * gsqrt) / gsqrt_full_mesh
    bsupv = to_full_mesh(bsupv * gsqrt) / gsqrt_full_mesh

    #  See jxbforce.f#L625
    itheta = (bsubsv - gradient(bsubv, order=1, factor=2.0)) / mu0
    izeta = (-bsubsu + gradient(bsubu, order=1, factor=2.0)) / mu0

    #  See jxbforce.f#L638
    avforce = itheta * bsupv - izeta * bsupu
    avforce = ovp * (avforce.mean(dim=(1, 2)) - presgrad)

    avforce[0] = 0
    avforce[-1] = 0

    assert_all_close(avforce[s], vmec_avforce[s], atol=1e-6, f_atol=1e-6)


#  TODO-0(@amerlo): fix me!
#  See test_compute_avforce_from_vmec
#  In addition, we know that points close to the axes are wrong (see test_compute_f)
@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(1, 98))
def test_compute_avforce(equis, runs, k, s):

    equi = equis[k]

    #  Get jxbout file
    wout_path = equi.wout_path
    wout_filename = os.path.basename(wout_path.item())
    vmec_id = wout_filename.split("_")[1].split(".")[0]
    jxbout_filename = "jxbout_" + vmec_id + ".nc"
    jxbout_path = os.path.join(os.path.dirname(wout_path.item()), jxbout_filename)

    jxbout = netCDF4.Dataset(jxbout_path)
    avforce = torch.from_numpy(jxbout["avforce"][:].data)

    assert_all_close(equi.avforce[s], avforce[s], atol=1e-6, f_atol=1e-6)


#  TODO-0(@amerlo): fix me!
#  For low beta equilibria,
#  the denominator is too low
@pytest.mark.parametrize("k", range(num_samples))
def test_compute_normF(equis, runs, k):
    equi = equis[k]
    #  We do not have a VMEC benchmark,
    #  but from what observed from Panici2022,
    #  at NS=99, M=N=12,
    #  this should be 1e-1 < ... < 1
    assert equi.normF[2:].mean() < 5e-1


@pytest.mark.parametrize("k", range(num_samples))
def test_compute_curtor_from_vmec(runs, k):

    run = runs[k]

    mu0 = 4 * np.pi * 1e-7
    signgs = run["signgs"][:].data
    bsubu = run["bsubumnc"][:, 0].data
    curtor = run["ctor"][:] * mu0

    #  Compute bsubu on full mesh and scale it
    #  See bcovar.f#L382
    curtor_ = 2 * np.pi * signgs * (1.5 * bsubu[-1] - 0.5 * bsubu[-2])

    assert np.isclose(curtor, curtor_, rtol=0, atol=1e-15)


@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(99))
def test_compute_itor_from_vmec(equis, runs, k, s):

    run = runs[k]
    equi = equis[k]

    mu0 = 4 * np.pi * 1e-7
    signgs = run["signgs"][:].data
    bsubu = run["bsubumnc"][:, 0].data
    itor = equi.toroidal_current.numpy() * mu0

    #  Compute bsubu on full mesh and scale it
    #  See mercier.f#L92 and mercier.f#L168
    itor_ = 2 * np.pi * signgs * bsubu
    itor_ = _to_full_mesh(itor_)

    assert np.isclose(itor[s], itor_[s], atol=1e-4), "{:.2e}".format(
        np.abs(itor[s] - itor_[s])
    )


#  TODO-0(@amerlo): improve tolerance
@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(99))
def test_compute_itor(equis, runs, k, s):
    equi = equis[k]
    itor = equi.toroidal_current
    assert_all_close(equi.itor[s], itor[s], atol=1e-2, f_atol=1e-2)


#  TODO-0(@amerlo): fix me!
@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(1, 99))
def test_iota_from_itor(equis, runs, k, s):
    equi = equis[k]
    #  2 * pi * signgs factor should be in the flux surface averages
    icurv = to_half_mesh(equi.toroidal_current * equi.mu0) / (
        2 * torch.pi * equi.signgs
    )
    #  See add_fluxes.f#193
    overg = 1 / equi.jacobian
    top = icurv + (overg * (equi.guu * equi.lv - (1 + equi.lu) * equi.guv)).mean(
        dim=(1, 2)
    )
    bot = (overg * equi.guu).mean(dim=(1, 2))
    iotas = top / bot
    assert_all_close(iotas[s], equi.iotas[s], atol=1e-6, f_atol=1e-6)


@pytest.mark.parametrize("k", range(num_samples))
def test_compute_phiedge(equis, k):

    equi = equis[k]

    #  See: https://terpconnect.umd.edu/~mattland/assets/notes/vmec_signs.pdf
    phiedge = torch.mean((equi.bsupv * torch.abs(equi.jacobian))[1:]) * 2 * np.pi

    assert torch.isclose(phiedge, equi.phiedge, rtol=0, atol=1e-15)


@pytest.mark.parametrize("k", range(num_samples))
def test_compute_volume_from_vmec(runs, k):

    run = runs[k]

    vp = run["vp"][:].data
    volume = run["volume_p"][:]

    vp_real = 4 * np.pi**2 * vp
    volume_ = vp_real[1:].mean()

    assert np.isclose(volume, volume_, rtol=0, atol=1e-6)


@pytest.mark.parametrize("k", range(num_samples))
def test_compute_aminor_from_vmec(runs, k):

    ntheta = 32
    nzeta = 36

    run = runs[k]

    aminor = run["Aminor_p"][:].data.item()

    rmnc = run["rmnc"][:].data
    rmnc = np.append(np.zeros((rmnc.shape[0], 12)), rmnc, axis=1)
    rmnc = torch.from_numpy(rmnc.reshape(-1, 12, 25))

    r = ift((rmnc, None), ntheta, nzeta)

    zmns = run["zmns"][:].data
    zmns = np.append(np.zeros((zmns.shape[0], 12)), zmns, axis=1)
    zmns = torch.from_numpy(zmns.reshape(-1, 12, 25))

    zumnc = derivative(zmns, "sin", "u")
    zu = ift((zumnc, None), ntheta, nzeta)

    #  See General/aspectratio.f
    mean_cross_sectional_area = 2 * torch.pi * (r[-1] * zu[-1]).mean().abs()
    aminor_ = torch.sqrt(mean_cross_sectional_area / torch.pi).item()

    assert np.isclose(aminor, aminor_, rtol=0, atol=1e-14)


@pytest.mark.parametrize("k", range(num_samples))
def test_compute_aminor(equis, runs, k):
    equi = equis[k]
    run = runs[k]
    aminor = run["Aminor_p"][:].data.item()
    assert abs(aminor - equi.aminor) < 1e-14, "mae: {:.2e}".format(
        abs(aminor - equi.aminor)
    )


@pytest.mark.parametrize("k", range(num_samples))
def test_compute_rmajor(equis, runs, k):
    equi = equis[k]
    run = runs[k]
    rmajor = run["Rmajor_p"][:].data.item()
    assert abs(rmajor - equi.rmajor) < 1e-12, "mae: {:.2e}".format(
        abs(rmajor - equi.rmajor)
    )


@pytest.mark.parametrize("k", range(num_samples))
def test_compare_mean_iota(equis, k):
    equi = equis[k]
    assert torch.isclose(equi.mean_iota, equi.mean_iota_, rtol=0, atol=1e-3)


@pytest.mark.parametrize("k", range(num_samples))
def test_compute_wmhd_from_vmec(runs, k):

    run = runs[k]

    mu0 = 4 * np.pi * 1e-7
    twopi = 2 * np.pi
    gamma = 0

    ntheta = 32
    nzeta = 36

    #  See eqsolve.f#L126
    vmec_wmhd = run["wb"][:].data + run["wp"][:].data / (gamma - 1)
    vmec_wmhd *= twopi**2

    bmnc = run["bmnc"][:].data
    bmnc = np.append(np.zeros((bmnc.shape[0], 18)), bmnc, axis=1)
    bmnc = torch.from_numpy(bmnc.reshape(-1, 17, 37))

    gmnc = run["gmnc"][:].data
    gmnc = np.append(np.zeros((gmnc.shape[0], 18)), gmnc, axis=1)
    gmnc = torch.from_numpy(gmnc.reshape(-1, 17, 37))

    b = ift((bmnc, None), ntheta, nzeta)
    gsqrt = ift((gmnc, None), ntheta, nzeta)
    vp = torch.from_numpy(run["vp"][:].data)
    pres = torch.from_numpy(run["pres"][:].data)

    wb = gsqrt * b**2 / 2
    wp = vp * pres * mu0

    #  See bcovar.f#L242
    wb = wb.abs()[1:].mean()
    wp = wp[1:].mean()

    wmhd = wb + wp / (gamma - 1)
    wmhd *= twopi**2

    assert abs(wmhd - vmec_wmhd) < 1e-12, "mae: {:.2e}".format(abs(wmhd - vmec_wmhd))


#  TODO-0(@amerlo): fix me!
@pytest.mark.parametrize("k", range(num_samples))
def test_compute_wmhd(equis, runs, k):
    equi = equis[k]
    run = runs[k]

    gamma = 0
    twopi = 2 * np.pi

    #  See eqsolve.f#L126
    wmhd = run["wb"][:].data + run["wp"][:].data / (gamma - 1)
    wmhd *= twopi**2

    assert abs(wmhd - equi.wmhd) < 1e-3, "mae: {:.2e}".format(abs(wmhd - equi.wmhd))


@pytest.mark.parametrize("k", range(num_samples))
def test_compute_betatot(equis, runs, k):
    equi = equis[k]
    run = runs[k]
    beta = run["betatotal"][:].data.item()
    assert abs(beta - equi.beta) < 1e-6, "mae: {:.2e}".format(abs(beta - equi.beta))


################
# Boozer tests #
################


@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(5))
def test_compute_bmnc_b_from_vmec(equis, runs, data, k, s):

    equi = equis[k]

    bx = booz_xform.Booz_xform()
    bx.read_wout(data[k]["wout_path"].item())

    compute_surfs = [np.linspace(0, equi.ns - 2, 5, dtype=int)[s]]

    bx.verbose = False
    bx.compute_surfs = compute_surfs
    bx.run()

    #  See: https://github.com/hiddenSymmetries/simsopt/blob/master/src/simsopt/mhd/boozer.py
    wout = runs[k]  # shorthand
    bx_ = booz_xform.Booz_xform()
    bx_.asym = bool(wout["lasym__logical__"][:])
    bx_.nfp = wout["nfp"][:]
    bx_.mpol = wout["mpol"][:]
    bx_.ntor = wout["ntor"][:]
    bx_.mnmax = wout["mnmax"][:]
    bx_.xm = wout["xm"][:]
    bx_.xn = wout["xn"][:]
    bx_.mpol_nyq = int(wout["xm_nyq"][:][-1])
    bx_.ntor_nyq = int(wout["xn_nyq"][:][-1] / wout["nfp"][:])
    bx_.mnmax_nyq = wout["mnmax_nyq"][:]
    bx_.xm_nyq = wout["xm_nyq"][:]
    bx_.xn_nyq = wout["xn_nyq"][:]

    arr = np.array([[]])
    rmns = arr
    zmnc = arr
    lmnc = arr
    bmns = arr
    bsubumns = arr
    bsubvmns = arr

    bx_.init_from_vmec(
        wout["ns"][:],
        wout["iotas"][:],
        wout["rmnc"][:].swapaxes(0, 1),
        rmns,
        zmnc,
        wout["zmns"][:].swapaxes(0, 1),
        lmnc,
        wout["lmns"][:].swapaxes(0, 1),
        wout["bmnc"][:].swapaxes(0, 1),
        bmns,
        wout["bsubumnc"][:].swapaxes(0, 1),
        bsubumns,
        wout["bsubvmnc"][:].swapaxes(0, 1),
        bsubvmns,
    )
    bx_.compute_surfs = compute_surfs
    bx_.mboz = bx.mboz
    bx_.nboz = bx.nboz
    bx_.run()
    bmnc_b_ = bx_.bmnc_b

    assert np.allclose(bx.bmnc_b, bmnc_b_, rtol=0, atol=0)


@pytest.mark.parametrize("k", range(num_samples))
def test_compute_bmnc_b_shape(equis, data, k):

    equi = equis[k]
    compute_surfs = np.linspace(0, equi.ns - 2, 5, dtype=int)
    equi.compute_surfs = compute_surfs

    bmnc_b = equi.bmnc_b
    shape = (len(compute_surfs), equi.bx.mboz, 2 * equi.bx.nboz + 1)
    assert bmnc_b.shape == shape


#  TODO-0(@amerlo): fix me!
@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(5))
def test_compute_bmnc_b(equis, data, k, s):

    equi = equis[k]

    bx = booz_xform.Booz_xform()
    bx.read_wout(data[k]["wout_path"].item())

    compute_surfs = [np.linspace(1, equi.ns - 2, 5, dtype=int)[s]]

    bx.verbose = False
    bx.compute_surfs = compute_surfs
    bx.run()

    equi.compute_surfs = compute_surfs
    equi.mboz = bx.mboz
    equi.nboz = bx.nboz

    #  Trigger Boozer computation, only needed since we are using bx.bmnc_b directly
    equi._run_bx()

    #  Use equi.bx.bmnc_b to match Booz_xform shape
    assert np.allclose(bx.bmnc_b, equi.bx.bmnc_b, rtol=0, atol=1e-3), "{:.2e}".format(
        np_mae(bx.bmnc_b, equi.bx.bmnc_b)
    )


@pytest.mark.parametrize("k", range(num_samples))
def test_compute_raderb00_shape(equis, data, k):
    equi = equis[k]
    compute_surfs = list(range(1, equi.ns - 1))
    equi.compute_surfs = compute_surfs
    assert equi.raderb00.shape == (equi.ns,)


@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(5))
def test_compute_toroidal_mirror_term(equis, data, k, s):

    equi = equis[k]

    bx = booz_xform.Booz_xform()
    bx.read_wout(data[k]["wout_path"].item())

    compute_surfs = np.linspace(1, equi.ns - 2, 5, dtype=int)

    bx.verbose = False
    bx.compute_surfs = compute_surfs
    bx.run()

    equi.compute_surfs = compute_surfs
    equi.mboz = bx.mboz
    equi.nboz = bx.nboz

    toroidal_mirror_term = bx.bmnc_b[1, s] / bx.bmnc_b[0, -1]
    assert (
        abs(toroidal_mirror_term - equi.toroidal_mirror_term[s].item()) < 1e-5
    ), "mae: {:.2e} at s={:.2f}".format(
        abs(toroidal_mirror_term - equi.toroidal_mirror_term[s].item()),
        equi.shalf[compute_surfs[s] + 1],
    )


@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(5))
def test_compute_toroidal_curvature_term(equis, data, k, s):

    equi = equis[k]

    bx = booz_xform.Booz_xform()
    bx.read_wout(data[k]["wout_path"].item())

    compute_surfs = np.linspace(1, equi.ns - 2, 5, dtype=int)

    bx.verbose = False
    bx.compute_surfs = compute_surfs
    bx.run()

    equi.compute_surfs = compute_surfs
    equi.mboz = bx.mboz
    equi.nboz = bx.nboz

    toroidal_curvature_term = bx.bmnc_b[2 * bx.nboz + 1, s] / bx.bmnc_b[0, -1]
    assert (
        abs(toroidal_curvature_term - equi.toroidal_curvature_term[s].item()) < 1e-4
    ), "mae: {:.2e} at s={:.2f}".format(
        abs(toroidal_curvature_term - equi.toroidal_curvature_term[s].item()),
        equi.shalf[compute_surfs[s] + 1],
    )


@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("s", range(5))
def test_compute_eps_eff_proxy(equis, runs, data, k, s):

    equi = equis[k]
    run = runs[k]

    bx = booz_xform.Booz_xform()
    bx.read_wout(data[k]["wout_path"].item())

    compute_surfs = np.linspace(1, equi.ns - 2, 5, dtype=int)

    equi.compute_surfs = compute_surfs
    equi.mboz = bx.mboz
    equi.nboz = bx.nboz

    bx.verbose = False
    bx.compute_surfs = compute_surfs
    bx.run()

    aminor = run["Aminor_p"][:].data.item()
    rmajor = run["Rmajor_p"][:].data.item()

    ns = run["ns"][:].data.item()
    shalf = np.linspace(0, 1, ns) - 0.5 * 1 / (ns - 1)
    bmnc = bx.bmnc_b
    b0 = bmnc[0, -1]
    b10 = bmnc[2 * bx.nboz + 1] / b0
    b11 = bmnc[2 * bx.nboz + 2] / b0
    rho = np.sqrt(shalf[compute_surfs + 1]) * aminor
    kappa = -b10 * rmajor / rho
    eps_eff_proxy = -b11 * kappa ** (4 / 3)

    #  eps_eff is a % quantity on the order of 1e-2,
    #  an accuracy of 1e-4 should be sufficient
    assert np.allclose(
        eps_eff_proxy[s], equi.eps_eff_proxy[s].numpy(), rtol=0, atol=1e-4
    )


@pytest.mark.parametrize("k", range(num_samples))
def test_compute_vbmax(equis, data, k):

    equi = equis[k]

    bx = booz_xform.Booz_xform()
    bx.read_wout(data[k]["wout_path"].item())

    compute_surfs = np.linspace(1, equi.ns - 2, 5, dtype=int)

    bx.verbose = False
    bx.compute_surfs = compute_surfs
    bx.run()

    equi.compute_surfs = compute_surfs
    equi.mboz = bx.mboz
    equi.nboz = bx.nboz

    vbmax = equi.vbmax

    #  Set the true bmnc_b to equilibrium and compute again vbmax
    equi.bx = bx
    vmec_vbmax = equi.vbmax

    assert np.allclose(vmec_vbmax, vbmax, rtol=0, atol=1e-4)


#  Test only with 1 equilibrium since they have the same radial resolution
@pytest.mark.parametrize("k", (0,))
def test_compute_s_b(equis, data, k):

    equi = equis[k]

    bx = booz_xform.Booz_xform()
    bx.read_wout(data[k]["wout_path"].item())

    compute_surfs = np.arange(1, equi.ns - 1, dtype=int)
    bx.verbose = False
    bx.compute_surfs = compute_surfs
    bx.run()

    np.allclose(bx.s_b, equi.shalf_b.numpy(), atol=0, rtol=0)


#  Test only on "2nd" equilibium under resources since it has a clear
#  single magnetic well, therefore, the delta computation does not suffer
#  from the uniqueness of the well
@pytest.mark.parametrize("k", (2,))
@pytest.mark.parametrize("js", ([1], [1, 2]))
@pytest.mark.parametrize("nB", (-1,))
@pytest.mark.parametrize("nalpha", (1, 2, 10))
def test_delta_shape(equis, k, js, nB, nalpha):

    equi = equis[k]

    #  Evaluate it only on 5 flux surfaces
    equi.compute_surfs = np.linspace(1, equi.ns - 2, 5, dtype=int)
    delta = equi.delta(js=js, nB=nB, nalpha=nalpha)

    shape = (len(js), 1 if nB == -1 else nB, nalpha)
    assert delta.shape == shape


@pytest.mark.parametrize("k", (2,))
@pytest.mark.parametrize(
    "case",
    (
        (
            [1, -1],
            -1,
            2,
            [
                [[0.9273154683302354, 0.8629185608073028]],
                [[1.084589008507578, 0.7853920406434188]],
            ],
        ),
    ),
)
def test_compute_delta(equis, k, case):

    equi = equis[k]
    js, nB, nalpha, res = case

    #  Evaluate it only on 5 flux surfaces
    equi.compute_surfs = np.linspace(1, equi.ns - 2, 5, dtype=int)
    delta = equi.delta(js=js, nB=nB, nalpha=nalpha)

    res = np.asarray(res)
    assert np.allclose(res, delta, rtol=0, atol=1e-6)


@pytest.mark.parametrize("k", (2,))
def test_vpmax_shape(equis, k):

    equi = equis[k]

    compute_surfs = np.linspace(1, equi.ns - 2, 5, dtype=int)
    equi.compute_surfs = compute_surfs

    shape = (5,)
    vpmax = equi.vpmax

    assert vpmax.shape == shape


#############
# Neo tests #
#############


#  TODO-0(@amerlo): fix me!
@pytest.mark.parametrize("k", range(num_samples))
def test_compute_eps_eff(equis, data, k):

    equi = equis[k]

    bx = booz_xform.Booz_xform()
    bx.read_wout(data[k]["wout_path"].item())

    num_surfs = 10
    #  Avoid to compute on magnetic axis where we know we are wrong
    compute_surfs = np.linspace(1, equi.ns - 2, num_surfs, dtype=int)

    bx.verbose = False
    bx.compute_surfs = compute_surfs
    bx.run()

    neo = Neo.from_booz(booz_xform=bx)
    neo.run()

    equi.compute_surfs = compute_surfs

    #  Do for here since eps_eff computation is expensive
    for i in range(num_surfs):
        assert np.allclose(
            neo.eps_eff[i], equi.eps_eff[i], rtol=0, atol=1e-6
        ), "{:.2e} at si={}".format(
            np.abs(neo.eps_eff[i] - equi.eps_eff[i]), compute_surfs[i]
        )


#  Compare if eps_eff proxy is close to eps_eff
#  The proxy is valid if b01~0
@pytest.mark.parametrize("k", range(num_samples))
def test_compare_eps_eff_proxy(runs, data, k):

    run = runs[k]

    bx = booz_xform.Booz_xform()
    bx.read_wout(data[k]["wout_path"].item())

    ns = run["ns"][:].data.item()
    num_surfs = 10
    #  Avoid to compute on magnetic axis where we know we are wrong
    compute_surfs = np.linspace(1, ns - 2, num_surfs, dtype=int)

    bx.verbose = False
    bx.compute_surfs = compute_surfs
    bx.run()

    neo = Neo.from_booz(booz_xform=bx)
    neo.run()

    #  The eps_eff given by NEO is eps_eff ** (3/2)
    eps_eff = neo.eps_eff ** (2 / 3)

    aminor = run["Aminor_p"][:].data.item()
    rmajor = run["Rmajor_p"][:].data.item()

    bmnc = bx.bmnc_b
    b0 = bmnc[0, -1]
    b10 = bmnc[2 * bx.nboz + 1] / b0
    b11 = bmnc[2 * bx.nboz + 2] / b0
    rho = np.sqrt(bx.s_b) * aminor
    kappa = -b10 * rmajor / rho
    eps_eff_proxy = -b11 * kappa ** (4 / 3)

    for i in range(num_surfs):
        assert np.allclose(
            eps_eff[i], eps_eff_proxy[i], rtol=0, atol=1e-2
        ), "{:.2e} at si={}".format(
            np.abs(eps_eff[i] - eps_eff_proxy[i]), compute_surfs[i]
        )


#################
# Simsopt tests #
#################


@pytest.mark.parametrize("k", range(num_samples))
@pytest.mark.parametrize("nzeta", (36, 72))
@pytest.mark.parametrize("alpha", (0, 0.5, 1.0))
@pytest.mark.parametrize("s", np.linspace(1, 98, 5, dtype=int))
def test_find_theta_along_fieldline(equis, data, k, nzeta, alpha, s):

    from simsopt.mhd.vmec import Vmec
    from simsopt.mhd.vmec_diagnostics import vmec_fieldlines

    equi = equis[k]
    shalf = equi.shalf[s]

    v = Vmec(data[k]["wout_path"].item())
    phis = np.linspace(0, 2 * np.pi / equi.num_field_period, nzeta)
    fl = vmec_fieldlines(v, s=shalf, alpha=alpha, phi1d=phis)

    theta_vmec = equi._find_theta_along_fieldline(
        alpha=alpha, phis=phis, boozer=False, dtype=np.float64
    )

    assert np.allclose(fl.iota, equi.iotas[s])
    assert np.allclose(fl.theta_vmec, theta_vmec[s, 0], rtol=0, atol=1e-14)
