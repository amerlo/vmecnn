import pytest
import numpy as np
import torch

from vmecnn.transforms.transforms import Normalize, BackNormalize


@pytest.mark.parametrize("size", (1, 10))
def test_normalize_and_backnormalize(size):
    keys = ("a", "b")
    tensor = {k: torch.randn(size) for k in keys}
    mean = {k: np.random.random(size).tolist() for k in keys}
    std = {k: np.random.random(size).tolist() for k in keys}
    normalize = Normalize(mean=mean, std=std)
    backnormalize = BackNormalize(mean=mean, std=std)
    tensor_ = normalize(tensor)
    tensor_ = backnormalize(tensor_)
    for k in keys:
        assert torch.allclose(tensor[k], tensor_[k], rtol=0, atol=0)
