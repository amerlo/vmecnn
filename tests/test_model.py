import pytest
from time import time

import torch
from omegaconf import OmegaConf

from vmecnn.utils import seed_everything
from vmecnn.models import FullyConnected, MHDNet
from vmecnn.datamodules.utils import get_xmn_mask, get_num_input
from vmecnn.metrics.metric import compute_flux_surface_averaged_f

#  Set random seed for reproducibility
seed_everything(42)

batch_size = 99
ns = batch_size


def get_X_y(mpol: int, ntor: int):
    X = {
        "npc": torch.randn(batch_size, 4),
        "pc": torch.randn(batch_size, 2),
        "phiedge": torch.randn(batch_size, 1),
        "normalized_flux": torch.rand(batch_size, 1).requires_grad_(True),
        "p0": torch.rand(batch_size, 1),
        "curtor": torch.rand(batch_size, 1),
        "pressure": torch.rand(batch_size, ns),
        "toroidal_current": torch.rand(batch_size, ns),
    }
    y = {
        "xmn": torch.randn(batch_size, mpol + 1, 2 * ntor + 1, 3),
        "iota": torch.rand(batch_size, 1),
    }
    y["xmn"][:, 0, :ntor, 0] = 0
    y["xmn"][:, 0, : ntor + 1, [1, 2]] = 0
    return X, y


def get_params(model):
    params = {}
    for layer, param in model.named_parameters():
        params[layer] = param.clone()
    return params


def train(model, X, y):
    # See https://github.com/pytorch/examples/blob/master/mnist/main.py
    optimizer = model.configure_optimizers()
    model.train()
    optimizer.zero_grad()
    y_hat = model(X)
    y_ = torch.cat([v.view(batch_size, -1) for v in y.values()], dim=1)
    y_hat_ = torch.cat([v.view(batch_size, -1) for v in y_hat.values()], dim=1)
    loss = torch.nn.MSELoss()(y_, y_hat_)
    loss.backward()
    optimizer.step()
    return model


def get_model(model: str, mpol: int, ntor: int):
    optimizer = OmegaConf.create({"lr": 3e-3, "_target_": "torch.optim.Adam"})
    X, _ = get_X_y(mpol, ntor)
    kwargs = {
        "input_features": get_num_input(X.keys(), ns=ns),
        "mpol": mpol,
        "ntor": ntor,
        "ns": ns,
        "labels": ["xmn", "iota"],
        "criterion": {"_target_": torch.nn.MSELoss},
        "optimizer": optimizer,
    }
    if model == "fully_connected":
        kwargs["activation"] = {"_target_": "torch.nn.SiLU"}
        model = FullyConnected(**kwargs)
    elif model == "mhdnet":
        kwargs["branch_activation"] = {"_target_": "torch.nn.SiLU"}
        kwargs["trunk_activation"] = {"_target_": "torch.nn.SiLU"}
        model = MHDNet(**kwargs)
    return model


@pytest.mark.parametrize("model", ("fully_connected", "mhdnet"))
@pytest.mark.parametrize("modes", ((1, 1), (3, 2), (6, 4), (11, 12)))
def test_zero_fcs_on_axis(model, modes):
    atol = 0
    if model == "mhdnet":
        #  MHDNet has a small eps on axis to avoid nan in gradients
        atol = 1e-6
    mpol, ntor = modes
    model = get_model(model, mpol, ntor)
    X, _ = get_X_y(mpol, ntor)
    X["normalized_flux"] = torch.zeros(batch_size, 1)
    y_hat = model(X)
    mask = get_xmn_mask(
        mpol,
        ntor,
        mpol,
        ntor,
        keep_zero_axis_fcs=False,
    )
    mask = torch.from_numpy(mask)
    assert sum(y_hat["xmn"][0][~mask]) <= atol


@pytest.mark.parametrize("model", ("fully_connected", "mhdnet"))
@pytest.mark.parametrize("modes", ((1, 1), (3, 2), (6, 4)))
def test_weight_changes(model, modes):
    mpol, ntor = modes
    model = get_model(model, mpol=mpol, ntor=ntor)
    init_params = get_params(model)
    X, y = get_X_y(mpol, ntor)
    model = train(model, X, y)
    after_params = get_params(model)
    for k in init_params.keys():
        assert torch.all(init_params[k] != after_params[k]), k


@pytest.mark.parametrize("model", ("fully_connected", "mhdnet"))
@pytest.mark.parametrize("modes", ((1, 1), (3, 2), (6, 4)))
def test_non_zero_grads(model, modes):
    mpol, ntor = modes
    model = get_model(model, mpol=mpol, ntor=ntor)
    X, y = get_X_y(mpol, ntor)
    model = train(model, X, y)
    for layer, param in model.named_parameters():
        assert torch.all(param.grad != 0), layer


#  TODO(@amerlo): speed up MHDNet forward backward pass for large number of modes
@pytest.mark.parametrize("model", ("fully_connected", "mhdnet"))
@pytest.mark.parametrize("modes", ((1, 1), (3, 2), (6, 4), (8, 6), (8, 8)))
def test_time_compute_f(model, modes):
    mpol, ntor = modes
    model = get_model(model, mpol, ntor)
    X, _ = get_X_y(mpol, ntor)
    equi = model(X)

    rmnc = equi["xmn"][..., 0]
    lmns = equi["xmn"][..., 1]
    zmns = equi["xmn"][..., 2]
    iota = equi["iota"]
    normalized_flux = X["normalized_flux"]
    phiedge = X["phiedge"]
    pressure = X["pressure"]

    iterations = 10
    t0 = time()
    for _ in range(iterations):
        compute_flux_surface_averaged_f(
            rmnc=rmnc,
            lmns=lmns,
            zmns=zmns,
            iota=iota,
            normalized_flux=normalized_flux,
            phiedge=phiedge,
            pressure=pressure,
            ntheta=16,
            nzeta=18,
            use_nn_gradients=True,
            use_proxy=True,
        )
    tottime = time() - t0
    time_per_batch = tottime / iterations

    maxtime_per_batch = 1
    assert (
        time_per_batch <= maxtime_per_batch
    ), f"time_per_batch: {time_per_batch * 1e3:.2f} ms"
