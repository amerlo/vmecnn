========================
Introduction to `vmecnn`
========================


.. pypi
.. image:: https://img.shields.io/pypi/v/vmecnn.svg
    :target: https://pypi.python.org/pypi/vmecnn

.. ci
    .. image:: https://img.shields.io/travis/amerlo/vmecnn.svg
        :target: https://travis-ci.com/amerlo/vmecnn
.. image:: https://gitlab.mpcdf.mpg.de/amerlo/vmecnn/badges/master/pipeline.svg
    :target: https://gitlab.mpcdf.mpg.de/amerlo/vmecnn/commits/master

.. coverage
.. image:: https://gitlab.mpcdf.mpg.de/amerlo/vmecnn/badges/master/coverage.svg
    :target: https://gitlab.mpcdf.mpg.de/amerlo/vmecnn/commits/master

.. readthedocs
.. image:: https://readthedocs.org/projects/vmecnn/badge/?version=latest
    :target: https://vmecnn.readthedocs.io/en/latest/?badge=latest
    :alt: Documentation Status

.. pyup crosschecks your dependencies. Github is default, gitlab more complicated: https://pyup.readthedocs.io/en/latest/readme.html#run-your-first-update 
    .. image:: https://pyup.io/repos/github/amerlo/vmecnn/shield.svg
        :target: https://pyup.io/repos/github/amerlo/vmecnn/
        :alt: Updates

.. image:: https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white
   :target: https://github.com/pre-commit/pre-commit
   :alt: pre-commit


NN surrogate model of the ideal-MHD code VMEC.


Licensed under the ``MIT License``

Resources
---------

* Source code: https://gitlab.mpcdf.mpg.de/amerlo/vmecnn
* Documentation: https://vmecnn.readthedocs.io
* Pypi: https://pypi.python.org/pypi/vmecnn
  
Data
----

If you want to generate data on a MPCDF cluster,
first install STELLOPT on the cluster following these `instructions <https://princetonuniversity.github.io/STELLOPT/STELLOPT%20Compilation%20on%20Hydra%20(RZG-MPG)>`_.
Consult also `MPCDF Getting Started Instructions <https://docs.google.com/document/d/13XQJSv8NG7Xbko2YhU0ZV8r01qRr1d9hpljEB7G_jHs/edit>`_.

As February 2022, this is the list of working modules to compile VMEC:

.. code-block:: bash

                $ module load git/2.31
                $ module load intel/19.1.3
                $ module load mkl/2020.1
                $ module load impi/2019.9
                $ module load hdf5-mpi/1.8.21
                $ module load netcdf-mpi/4.4.1
                $ module load fftw-mpi/3.3.9
                $ module load anaconda/3/2021.11

To generate a data set of VMEC runs, please follow these steps:

.. code-block:: bash

                $ cd ``w7x_datagen root folder``
                $ python w7x_datagen vmec_equilibria ~/tmp/vmec --samples 256 --domain FiniteBetaDomain -s -n 8

Then create a tar archive of the generated runs (optionally transfer the generated folder locally):

.. code-block:: bash

                $ make ``path to the generated runs folder``.tar.gz
                $ rsync -rP username@ip_address:``path to the generated runs folder``.tar.gz .

Installation
------------

Check how to install torch for your system here (https://pytorch.org/get-started/locally/).

It is suggested to use a virtual environment where to install all the project dependencies.

.. code-block:: bash

                $ conda create -n myenv python=3.9
                $ conda activate myenv
                $ pip install -e .

Visualization
-------------

Evaluate training runs with ``TensorBoard`` or ``MLFlow``.

.. code-block:: bash

                $ python vmecnn logger=tensorboard
                $ python vmecnn logger=mlflow

Than, spawn the dashboards.

.. code-block:: bash

                $ tensorboard --logdir outputs
                $ mlflow ui

To visualize the data, select the figures under ``configs/figures`` and run the following command:

.. code-block:: bash

                $ python vmecnn action=visualize figures=all

                
Tests
-----

Run the tests suite with ``pytest``:

.. code-block:: bash

                $ make test

Perform the following tests to check your pipeline:

* ``figures/raw_data.pdf``: visualize the distributions of raw data provided by the dataset;
* ``figures/nn_data.pdf``: visualize the distributions of the data as seen by the model (e.g., ``model(x)``);
* check loss prior to training: initialize all the model weights apart from the last layer bias to zero, and the last layer bias to the mean of the output feature. Prior to training, the model mse should be close to the standard deviation of the output feature. In other words, the nrmse loss should close to 1;
* disable validation and train on a single batch;
* check loss with zeros inputs: check that the model is learning something with respect to the case where the inputs feautres are zeros;

.. code-block:: bash

                $ python vmecnn/figures.py hydra.run.dir=/tmp seed=42
                $ python vmecnn experiment=loss_init
                $ python vmecnn experiment=overfit
                $ python vmecnn experiment=zeros_input

Hyperparameters Search
----------------------

.. code-block:: bash

                $ python vmecnn -m hydra/sweeper=ax experiment=m6_n4_hp


Notes
-----

To disable hydra output, fake it by setting the current directory as wd (https://github.com/facebookresearch/hydra/issues/910).

.. code-block:: bash

                $ python hydra.run.dir=.

Visualize gradient with https://gist.github.com/apaszke/f93a377244be9bfcb96d3547b9bc424d.
